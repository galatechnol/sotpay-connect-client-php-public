<?php

namespace Src\main\client\models;

use JsonSerializable;
use Src\main\client\models\bases\Base;
use Src\main\client\models\components\merchantKey;
use Src\main\client\models\enums\Type;

class MerchantTokenWrite extends Base implements JsonSerializable {
    private merchantKey $merchantKey;
    private Type $type;
    private string $description;
    private int $expiresAfter;

    public function __construct($merchantKey, $type) {
        $this->merchantKey = $merchantKey;
        $this->type = $type;
    }

    public function setMerchantKey(merchantKey $merchantKey): void {
        $this->merchantKey = $merchantKey;
    }

    // Setter for $type
    public function setType(Type $type): void {
        $this->type = $type;
    }

    // Setter for $description
    public function setDescription(string $description): void {
        $this->description = $description;
    }

    // Setter for $expiresAfter
    public function setExpiresAfter(int $expiresAfter): void {
        $this->expiresAfter = $expiresAfter;
    }

    public function jsonSerialize(): array
    {
        return array_merge(
            array('merchantKey' => $this->merchantKey),
            array('type' => $this->type->value),
            isset($this->description) ? array('description' => $this->description): array() ,
            isset($this->expiresAfter) ? array('expiresAfter' => $this->expiresAfter): array() ,
        );
    }
}