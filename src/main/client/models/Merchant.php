<?php

namespace Src\main\client\models;

use JsonSerializable;
use Src\main\client\models\bases\Base;
use Src\main\client\models\enums\Country;
use Src\main\client\models\enums\Currency;
use Src\main\client\models\MerchantObjects\MerchantDefaultPaymentGateways;
use Src\main\client\models\MerchantObjects\MerchantSignatureKeys;
use Src\main\client\models\MerchantObjects\MerchantTransactionDefaults;

class Merchant extends Base implements JsonSerializable
{
    private string $object;
    private string $merchant;
    private string $name;
    private string $nameAbbreviated;
    private Country $country;
    private string $vatNumber;
    private bool $enabled;
    private bool $financialInstitution;
    private string $transactionIdentifier;
    private array $currencies;
    private Currency $defaultCurrency;
    private MerchantTransactionDefaults $transactionDefaults;
    private MerchantDefaultPaymentGateways $defaultPaymentGateways;
    private MerchantSignatureKeys $signatureKeys;
    private string $tokenAdminKey;
    private string $created;
    private string $updated;

    public function setObject(string $object): void
    {
        $this->object = $object;
    }

    public function getObject(): string
    {
        return $this->object;
    }
    public function setMerchant(string $merchant): void
    {
        $this->merchant = $merchant;
    }
    public function getMerchant(): string
    {
        return $this->merchant;
    }
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }
    public function setNameAbbreviated(string $nameAbbreviated): void
    {
        $this->nameAbbreviated = $nameAbbreviated;
    }
    public function getNameAbbreviated(): string
    {
        return $this->nameAbbreviated;
    }
    public function setCountry(Country $country): void
    {
        $this->country = $country;
    }
    public function getCountry(): Country
    {
        return $this->country;
    }
    public function setVatNumber(string $vatNumber): void
    {
        $this->vatNumber = $vatNumber;
    }
    public function getVatNumber(): string
    {
        return $this->vatNumber;
    }
    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }
    public function isEnabled(): bool
    {
        return $this->enabled;
    }
    public function setFinancialInstitution(bool $financialInstitution): void
    {
        $this->financialInstitution = $financialInstitution;
    }
    public function isFinancialInstitution(): bool
    {
        return $this->financialInstitution;
    }
    public function setTransactionIdentifier(string $transactionIdentifier): void
    {
        $this->transactionIdentifier = $transactionIdentifier;
    }
    public function getTransactionIdentifier(): string
    {
        return $this->transactionIdentifier;
    }
    public function setCurrencies(array $currencies): void
    {
        $this->currencies = $currencies;
    }
    public function getCurrencies(): array
    {
        return $this->currencies;
    }
    public function getDefaultCurrency(): Currency
    {
        return $this->defaultCurrency;
    }
    public function setDefaultCurrency(Currency $defaultCurrency): void
    {
        $this->defaultCurrency = $defaultCurrency;
    }
    public function setTransactionDefaults(MerchantTransactionDefaults $transactionDefaults): void
    {
        $this->transactionDefaults = $transactionDefaults;
    }
    public function getTransactionDefaults(): MerchantTransactionDefaults
    {
        return $this->transactionDefaults;
    }
    public function setDefaultPaymentGateways(MerchantDefaultPaymentGateways $defaultPaymentGateways): void
    {
        $this->defaultPaymentGateways = $defaultPaymentGateways;
    }
    public function getDefaultPaymentGateways(): MerchantDefaultPaymentGateways
    {
        return $this->defaultPaymentGateways;
    }
    public function setSignatureKeys(MerchantSignatureKeys $signatureKeys): void
    {
        $this->signatureKeys = $signatureKeys;
    }
    public function getSignatureKeys(): MerchantSignatureKeys
    {
        return $this->signatureKeys;
    }
    public function setTokenAdminKey(string $tokenAdminKey): void
    {
        $this->tokenAdminKey = $tokenAdminKey;
    }
    public function getTokenAdminKey(): string
    {
        return $this->tokenAdminKey;
    }
    public function setCreated(string $created): void
    {
        $this->created = $created;
    }
    public function getCreated(): string
    {
        return $this->created;
    }
    public function setUpdated(string $updated): void
    {
        $this->updated = $updated;
    }
    public function getUpdated(): string
    {
        return $this->updated;
    }

    public static function withBody(string $JSONBody): Merchant //string -> Json Body
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString(): string {
        return "[Object: $this->object]\n\n"
            . "Merchant $this->merchant\n"
            . "Name: $this->name\n"
            . "Name Abbreviated: " . ($this->nameAbbreviated ?? 'NULL') . "\n"
            . "Country: " . ($this->country->value ?? 'NULL') . "\n"
            . "Vat Number: " . ($this->vatNumber ?? 'NULL') . "\n"
            . "Enabled: " . ($this->enabled ? 'true' : 'false') . "\n"
            . "Financial Institution: " . ($this->financialInstitution ? 'true': 'false') . "\n"
            . "Transaction Identifier: " . ($this->transactionIdentifier ?? 'NULL') . "\n"
            . "Currencies: " . (json_encode($this->currencies) ?? 'NULL') . "\n"
            . "Default Currency: " . ($this->defaultCurrency->value ?? 'NULL') . "\n"
            . "Transaction Defaults: " .  ($this->transactionDefaults ?? 'NULL') . "\n"
            . "Default Payment Gateways: " . ($this->defaultPaymentGateways ?? 'NULL') . "\n"
            . "Signature Keys: " .  ($this->signatureKeys ?? 'NULL') . "\n"
            . "Token Admin Key: " .  ($this->tokenAdminKey ?? 'NULL') . "\n"
            . "Created: " . ($this->created ?? 'NULL') . "\n"
            . "Updated: " . ($this->updated ?? 'NULL') . "\n";
    }

    public function jsonSerialize(): array
    {
        return array_merge(
            array('merchant' => $this->merchant),
            array('name' => $this->name),
            isset($this->nameAbbreviated) ? array('nameAbbreviated' => $this->nameAbbreviated): array(),
            isset($this->country) ? array('country' => $this->country): array(),
            isset($this->vatNumber) ? array('vatNumber' => $this->vatNumber): array(),
            array('enabled' => (bool)$this->enabled),
            array('financialInstitution' => (bool)$this->financialInstitution),
            array('transactionIdentifier' => $this->transactionIdentifier),
            array('currencies' => $this->currencies),
            array('defaultCurrency' => $this->defaultCurrency),
            isset($this->transactionDefaults) ? array('transactionDefaults' => $this->transactionDefaults): array(),
            isset($this->defaultPaymentGateways) ? array('defaultPaymentGateways'=> $this->defaultPaymentGateways): array(),
            isset($this->signatureKeys) ? array('signatureKeys'=> $this->signatureKeys): array(),
            array('tokenAdminKey' => $this->tokenAdminKey),
            isset($this->created) ? array('created' => $this->created): array(),
            isset($this->updated) ? array('updated' => $this->updated): array()
        );
    }
}