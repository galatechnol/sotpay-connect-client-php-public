<?php
namespace Src\main\client\models;
use DateTime;
use Src\main\client\models\bases\Base;
use Src\main\client\models\components\merchantKey;

class TransactionInfo extends Base {
    protected string $object;
    protected string $urn;
    protected merchantKey $merchantKey;
    protected DateTime $created;
    protected DateTime $updated;

    public function exists($property, $subproperty = null) {
        if (isset($this->$property) && $subproperty !== null) {
            return $this->$property->exists($subproperty);
        }
        else {
            return isset($this->$property);
        }
    }


    public function getReference() {
        return $this->reference;
    }

    public function getStatusCode() {
        return $this->statusCode;
    }

    public function getCardType() {
        return $this->cardType;
    }

    public function getMaskedCardNumber() {
        return $this->maskedCardNumber;
    }

    public function getUpdated(): DateTime
    {
        return $this->updated;
    }

    public function getObject(): string
    {
        return $this->object;
    }

    public function getMerchantKey(): merchantKey
    {
        return $this->merchantKey;
    }

    public function getCreated(): DateTime
    {
        return $this->created;
    }

    public function getUrn(): string
    {
        return $this->urn;
    }

    public function setObject(string $object): void {
        $this->object = $object;
    }
    public function setUrn(string $urn): void {
        $this->urn = $urn;
    }
    public function setMerchantKey(merchantKey $merchantKey): void {
        $this->merchantKey = $merchantKey;
    }

    public function setCreated(DateTime $created): void {
        $this->created = $created;
    }

    public function setUpdated(DateTime $updated): void {
        $this->updated = $updated;
    }
    public function __toString()
    {
        return
            "[Object: $this->object]\n\n" .
            "URN: " . ($this->urn ?? "NULL") . "\n" .
            "Merchant Key: " . ($this->merchantKey ?? "NULL") . "\n" .
            "Created: " . ($this->created->format('Y-m-d H:i:s') ?? "NULL") . "\n" .
            "Updated: " . ($this->updated->format('Y-m-d H:i:s') ?? "NULL") . "\n";
    }
}