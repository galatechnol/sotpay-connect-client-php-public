<?php
namespace Src\main\client\models;
use Src\main\client\models\bases\Base;
use Src\main\client\models\components\merchantKey;
use Src\main\client\models\enums\Currency;
use Src\main\client\models\enums\PaymentGateway;
use Src\main\client\models\enums\Status;
use Src\main\client\models\TransactionObjects\transactionAddress;
use Src\main\client\models\TransactionObjects\transactionDetails;
use Src\main\client\models\TransactionObjects\transactionLocales;
use Src\main\client\models\TransactionObjects\TransactionContact;
use Src\main\client\models\TransactionObjects\TransactionOverrideUrls;
use Src\main\client\models\TransactionObjects\TransactionMedium;
use Src\main\client\models\TransactionObjects\transactionMessage;
use Src\main\client\models\TransactionObjects\TransactionOperationValidity;
use Src\main\client\models\TransactionObjects\transactionPostbackUrls;
use Src\main\client\models\TransactionObjects\transactionPrimaryRecipient;
use Src\main\client\models\TransactionObjects\transactionProgress;

class Transaction extends Base
{
    private string $object;
    private string $urn;
    public merchantKey $merchantKey;
    private float $amount;
    private Currency $currency;
    private PaymentGateway $paymentGateway;
    private string $termsUrl; //max length 255
    private bool $termsAccepted;
    private int $expiresAfter;
    private Status $status;
    private TransactionDetails $details;
    private TransactionContact $contact;
    private TransactionContact $notificationContact;
    private TransactionContact $notificationContactSecondary;
    private TransactionAddress $billingAddress;
    private TransactionAddress $deliveryAddress;
    private TransactionPrimaryRecipient $primaryRecipient;
    private TransactionLocales $locales;
    private TransactionMessage $payMessage;
    private TransactionMessage $receiptMessage;
    private TransactionMessage $notificationMessage;
    private TransactionMessage $notificationMessageSecondary;
    private TransactionOverrideUrls $overrideUrls;
    private TransactionPostbackUrls $postbackUrls;
    private TransactionProgress $progress;

    private TransactionMedium $medium;

    private TransactionOperationValidity $operationValidity;

    private bool $createCardOnFile;
    private string $createdCardOnFileId;
    private string $chargeCardOnFileId;
    private string $signature;
    private bool $debug;
    private string $created;
    private string $updated;
    private string $expiry;

    public function exists($property, $subproperty = null) {
        if (isset($this->$property) && $subproperty !== null) {
            return $this->$property->exists($subproperty);
        }
        else {
            return isset($this->$property);
        }
    }

    public function setObject(string $object): void
    {
        $this->object = $object;
    }

    public function setUrn(string $urn): void
    {
        $this->urn = $urn;
    }

    public function setMerchantKey(merchantKey $merchantKey): void
    {

        $this->merchantKey = $merchantKey;
    }

    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    public function setCurrency(Currency $currency): void
    {
        $this->currency = $currency;
    }

    public function setPaymentGateway(PaymentGateway $paymentGateway): void
    {
        $this->paymentGateway = $paymentGateway;
    }

    public function setTermsUrl(string $termsUrl): void
    {
        $this->termsUrl = $termsUrl;
    }

    public function setTermsAccepted(bool $termsAccepted): void
    {
        $this->termsAccepted = $termsAccepted;
    }

    public function setExpiresAfter(int $expiresAfter): void
    {
        $this->expiresAfter = $expiresAfter;
    }

    public function setStatus(Status $status): void
    {
        $this->status = $status;
    }

    public function setDetails(transactionDetails $details): void
    {
        $this->details = $details;
    }

    public function setContact(TransactionContact $contact): void
    {
        $this->contact = $contact;
    }

    public function setNotificationContact(TransactionContact $notificationContact): void
    {
        $this->notificationContact = $notificationContact;
    }
    public function setNotificationContactSecondary(TransactionContact $notificationContactSecondary): void
    {
        $this->notificationContactSecondary = $notificationContactSecondary;
    }

    public function setBillingAddress(transactionAddress $billingAddress): void
    {
        $this->billingAddress = $billingAddress;
    }

    public function setDeliveryAddress(transactionAddress $deliveryAddress): void
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    public function setLocales(transactionLocales $locales): void
    {
        $this->locales = $locales;
    }

    public function setPayMessage(TransactionMessage $payMessage): void
    {
        $this->payMessage = $payMessage;
    }

    public function setReceiptMessage(transactionMessage $receiptMessage): void
    {
        $this->receiptMessage = $receiptMessage;
    }

    public function setNotificationMessage(TransactionMessage $notificationMessage): void
    {
        $this->notificationMessage = $notificationMessage;
    }
    public function setNotificationMessageSecondary(TransactionMessage $notificationMessageSecondary): void
    {
        $this->notificationMessageSecondary = $notificationMessageSecondary;
    }
    public function setPostbackUrls(TransactionPostbackUrls $postbackUrls): void
    {
        $this->postbackUrls = $postbackUrls;
    }
    public function setOverrideUrls(TransactionOverrideUrls $overrideUrls): void
    {
        $this->overrideUrls = $overrideUrls;
    }
    public function setProgress(transactionProgress $progress): void
    {
        $this->progress = $progress;
    }
    public function setMedium(TransactionMedium $medium): void
    {
        $this->medium = $medium;
    }
    public function setOperationValidity(TransactionOperationValidity $operationValidity): void
    {
        $this->operationValidity = $operationValidity;
    }

    public function setCreateCardOnFile(bool $createCardOnFile): void
    {
        $this->createCardOnFile = $createCardOnFile;
    }

    public function setDebug(bool $debug): void
    {
        $this->debug = $debug;
    }

    public function setCreated(string $created): void
    {
        $this->created = $created;
    }

    public function setUpdated(string $updated): void
    {
        $this->updated = $updated;
    }

    public function setExpiry(string $expiry): void
    {
        $this->expiry = $expiry;
    }

    public function getObject(): string
    {
        return $this->object;
    }

    public function getUrn(): string
    {
        return $this->urn;
    }

    public function getMerchantKey(): merchantKey
    {
        return $this->merchantKey;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public function getPaymentGateway(): PaymentGateway
    {
        return $this->paymentGateway;
    }

    public function getTermsUrl(): string
    {
        return $this->termsUrl;
    }

    public function getTermsAccepted(): bool
    {
        return $this->termsAccepted;
    }

    public function getExpiresAfter(): int
    {
        return $this->expiresAfter;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function getDetails(): transactionDetails
    {
        return $this->details;
    }

    public function getContact(): TransactionContact
    {
        return $this->contact;
    }

    public function getNotificationContact(): TransactionContact
    {
        return $this->notificationContact;
    }

    public function getBillingAddress(): transactionAddress
    {
        return $this->billingAddress;
    }

    public function getDeliveryAddress(): transactionAddress
    {
        return $this->deliveryAddress;
    }

    public function getNotificationMessageSecondary(): TransactionMessage
    {
        return $this->notificationMessageSecondary;
    }

    public function getChargeCardOnFileId(): string
    {
        return $this->chargeCardOnFileId;
    }

    public function getCreatedCardOnFileId(): string
    {
        return $this->createdCardOnFileId;
    }

    public function getOverrideUrls(): TransactionOverrideUrls
    {
        return $this->overrideUrls;
    }

    public function getNotificationContactSecondary(): TransactionContact
    {
        return $this->notificationContactSecondary;
    }

    public function getPostbackUrls(): transactionPostbackUrls
    {
        return $this->postbackUrls;
    }

    public function getPrimaryRecipient(): transactionPrimaryRecipient
    {
        return $this->primaryRecipient;
    }

    public function getSignature(): string
    {
        return $this->signature;
    }


    public function getLocales(): transactionLocales
    {
        return $this->locales;
    }

    public function getPayMessage(): TransactionMessage
    {
        return $this->payMessage;
    }

    public function getReceiptMessage(): TransactionMessage
    {
        return $this->receiptMessage;
    }

    public function getNotificationMessage(): TransactionMessage
    {
        return $this->notificationMessage;
    }

    public function getProgress(): transactionProgress
    {
        return $this->progress;
    }
    public function getMedium(): TransactionMedium
    {
        return $this->medium;
    }
    public function getOperationValidity(): TransactionOperationValidity
    {
        return $this->operationValidity;
    }

    public function getCreateCardOnFile(): bool
    {
        return $this->createCardOnFile;
    }

    public function getDebug(): bool
    {
        return $this->debug;
    }

    public function getCreated(): string
    {
        return $this->created;
    }

    public function getUpdated(): string
    {
        return $this->updated;
    }

    public function getExpiry(): string
    {
        return $this->expiry;
    }

    public static function withBody(string $JSONBody): Transaction //string -> Json Body
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString(): string {
        return "[Object: $this->object]\n\n"
            . "URN: $this->urn\n"
            . "Merchant Key:\n{$this->merchantKey}\n"
            . "Amount: $this->amount\n"
            . "Currency: {$this->currency->value}\n"
            . "Payment Gateway: " . ($this->paymentGateway->value ?? 'NULL') . "\n"
            . "Terms URL:" .  ($this->termsUrl ?? 'NULL') .  "\n"
            . "Terms Accepted: " . ($this->termsAccepted ?? 'NULL') . "\n"
            . "Expires After: " . ($this->expiresAfter ?? 'NULL') . "\n"
            . "Status: {$this->status->value}\n"
            . "\nDetails:\n" . ($this->details ?? 'NULL' . "\n") . "\n"
            . "Contact:\n" . ($this->contact ?? 'NULL' . "\n") . "\n"
            . "Notification Contact:\n" . ($this->notificationContact ?? 'NULL' . "\n") . "\n"
            . "Notification Contact Secondary:\n" . ($this->notificationContactSecondary ?? 'NULL' . "\n") . "\n"
            . "Billing Address:\n" . ($this->billingAddress ?? 'NULL' . "\n") . "\n"
            . "Delivery Address:\n" . ($this->deliveryAddress ?? 'NULL' . "\n") . "\n"
            . "Primary Recipient:\n" . ($this->primaryRecipient ?? 'NULL' . "\n") . "\n"
            . "Locales:\n" . ($this->locales ?? 'NULL' . "\n") . "\n"
            . "Pay Message:\n" . ($this->payMessage ?? 'NULL' . "\n") . "\n"
            . "Receipt Message:\n" . ($this->receiptMessage ?? 'NULL' . "\n") . "\n"
            . "Notification Message:\n" . ($this->notificationMessage ?? 'NULL' . "\n") . "\n"
            . "Notification Message Secondary:\n" . ($this->notificationMessageSecondary ?? 'NULL' . "\n") . "\n"
            . "Override URLs:\n" . ($this->overrideUrls ?? 'NULL' . "\n") . "\n"
            . "Postback URLs:\n" . ($this->postbackUrls ?? 'NULL' . "\n") . "\n"
            . "Progress:\n" . ($this->progress ?? 'NULL' . "\n") . "\n"
            . "Medium:\n" . ($this->medium ?? 'NULL' . "\n") . "\n"
            . "Operation Validity:\n" . ($this->operationValidity ?? 'NULL' . "\n") . "\n"
            . "Create Card on File: " . ($this->createCardOnFile ?? 'NULL') . "\n"
            . "Created Card on File ID: " . ($this->createdCardOnFileId ?? 'NULL') . "\n"
            . "Charge Card on File ID: " . ($this->chargeCardOnFileId ?? 'NULL') . "\n"
            . "Signature: " . ($this->signature ?? 'NULL') . "\n"
            . "Debug: " . ($this->debug ? "true" : "false") . "\n"
            . "Created: " . ($this->created ?? 'NULL') . "\n"
            . "Updated: " . ($this->updated ?? 'NULL') . "\n"
            . "Expiry: " . ($this->expiry ?? 'NULL') . "\n\n";
    }
}