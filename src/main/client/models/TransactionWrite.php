<?php

namespace Src\main\client\models;
use JsonSerializable;
use Src\main\client\models\components\merchantKey;
use Src\main\client\models\enums\Currency;
use Src\main\client\models\enums\PaymentGateway;
use Src\main\client\models\TransactionObjects\transactionAddress;
use Src\main\client\models\TransactionObjects\transactionDetails;
use Src\main\client\models\TransactionObjects\transactionLocales;
use Src\main\client\models\TransactionObjects\TransactionContact;
use Src\main\client\models\TransactionObjects\TransactionOverrideUrls;
use Src\main\client\models\TransactionObjects\transactionPostbackUrls;
use Src\main\client\models\TransactionObjects\transactionPrimaryRecipient;
use Src\main\client\models\TransactionObjects\TransactionWriteMessage;

class TransactionWrite implements JsonSerializable
{
    private merchantKey $merchantKey;
    private float $amount;
    private Currency $currency;
    private ?PaymentGateway $paymentGateway;
    private ?string $termsUrl;
    private ?int $expiresAfter;
    private ?transactionDetails $details;
    private ?TransactionContact $contact;
    private ?TransactionContact $notificationContact;
    private ?TransactionContact $notificationContactSecondary;
    private ?transactionAddress $billingAddress;
    private ?transactionAddress $deliveryAddress;
    private ?transactionPrimaryRecipient $primaryRecipient;
    private ?transactionLocales $locales;
    private ?TransactionWriteMessage $payMessage;
    private ?TransactionWriteMessage $receiptMessage;
    private ?TransactionWriteMessage $notificationMessage;
    private ?TransactionWriteMessage $notificationMessageSecondary;
    private ?TransactionOverrideUrls $overrideUrls;
    private ?TransactionPostbackUrls $postbackUrls;
    private ?bool $createCardOnFile;
    private ?string $chargeCardOnFileId;
    private ?bool $debug;

    public function __construct(merchantKey $merchantKey,
                                float $amount,
                                Currency $currency,
                                ?PaymentGateway $paymentGateway = null,
                                ?string $termsUrl = null,
                                ?int $expiresAfter = null,
                                ?transactionDetails $details = null,
                                ?TransactionContact $contact = null,
                                ?TransactionContact          $notificationContact = null,
                                ?TransactionContact          $notificationContactSecondary = null,
                                ?transactionAddress          $billingAddress = null,
                                ?transactionAddress          $deliveryAddress = null,
                                ?transactionPrimaryRecipient $primaryRecipient = null,
                                ?transactionLocales          $locales = null,
                                ?TransactionWriteMessage     $payMessage = null,
                                ?TransactionWriteMessage     $receiptMessage = null,
                                ?TransactionWriteMessage     $notificationMessage = null,
                                ?TransactionWriteMessage     $notificationMessageSecondary = null,
                                ?TransactionOverrideUrls     $overrideUrls = null,
                                ?TransactionPostbackUrls     $postbackUrls = null,
                                ?bool                        $createCardOnFile = null,
                                ?string                      $chargeCardOnFileId = null,
                                ?bool                        $debug = null) {
        $this->merchantKey = $merchantKey;
        $this->amount = $amount;
        $this->currency = $currency;
        $paymentGateway ? $this->paymentGateway = $paymentGateway : '';
        $termsUrl ? $this->termsUrl = $termsUrl : '';
        $expiresAfter ? $this->expiresAfter = $expiresAfter : '';
        $details ? $this->details = $details : '';
        $contact ? $this->contact = $contact : '';
        $notificationContact ? $this->notificationContact = $notificationContact : '';
        $notificationContactSecondary ? $this->notificationContactSecondary = $notificationContactSecondary : '';
        $billingAddress ? $this->billingAddress = $billingAddress : '';
        $deliveryAddress ? $this->deliveryAddress = $deliveryAddress : '';
        $primaryRecipient ? $this->primaryRecipient = $primaryRecipient : '';
        $locales ? $this->locales = $locales : '';
        $payMessage ? $this->payMessage = $payMessage : '';
        $receiptMessage ? $this->receiptMessage = $receiptMessage : '';
        $notificationMessage ? $this->notificationMessage = $notificationMessage : '';
        $notificationMessageSecondary ? $this->notificationMessageSecondary = $notificationMessageSecondary : '';
        $overrideUrls ? $this->overrideUrls = $overrideUrls : '';
        $postbackUrls ? $this->postbackUrls = $postbackUrls : '';
        $createCardOnFile ? $this->createCardOnFile = $createCardOnFile : '';
        $chargeCardOnFileId ? $this->chargeCardOnFileId = $chargeCardOnFileId : '';
        $debug ? $this->debug = $debug : '';
    }

    public function setMerchantKey(merchantKey $merchantKey): void
    {
        $this->merchantKey = $merchantKey;
    }
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    public function setCurrency(Currency $currency): void
    {
        $this->currency = $currency;
    }

    public function setPaymentGateway(PaymentGateway $paymentGateway): void
    {
        $this->paymentGateway = $paymentGateway;
    }

    public function setTermsUrl(string $termsUrl): void
    {
        $this->termsUrl = $termsUrl;
    }

    public function setExpiresAfter(int $expiresAfter): void
    {
        $this->expiresAfter = $expiresAfter;
    }

    public function setDetails(transactionDetails $details): void
    {
        $this->details = $details;
    }

    public function setContact(TransactionContact $contact): void
    {
        $this->contact = $contact;
    }

    public function setNotificationContact(TransactionContact $notificationContact): void
    {
        $this->notificationContact = $notificationContact;
    }

    public function setNotificationContactSecondary(TransactionContact $notificationContactSecondary): void
    {
        $this->notificationContactSecondary = $notificationContactSecondary;
    }

    public function setBillingAddress(transactionAddress $billingAddress): void
    {
        $this->billingAddress = $billingAddress;
    }

    public function setDeliveryAddress(transactionAddress $deliveryAddress): void
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    public function setPrimaryRecipient(transactionPrimaryRecipient $primaryRecipient): void
    {
        $this->primaryRecipient = $primaryRecipient;
    }

    public function setLocales(transactionLocales $locales): void
    {
        $this->locales = $locales;
    }

    public function setPayMessage(TransactionWriteMessage $payMessage): void
    {
        $this->payMessage = $payMessage;
    }

    public function setReceiptMessage(TransactionWriteMessage $receiptMessage): void
    {
        $this->receiptMessage = $receiptMessage;
    }

    public function setNotificationMessage(TransactionWriteMessage $notificationMessage): void
    {
        $this->notificationMessage = $notificationMessage;
    }

    public function setNotificationMessageSecondary(TransactionWriteMessage $notificationMessageSecondary): void
    {
        $this->notificationMessageSecondary = $notificationMessageSecondary;
    }

    public function setOverrideUrls(TransactionOverrideUrls $customUrls): void
    {
        $this->overrideUrls = $customUrls;
    }

    public function setPostbackUrls(transactionPostbackUrls $postbackUrls): void
    {
        $this->postbackUrls = $postbackUrls;
    }

    public function setCreateCardOnFile(bool $createCardOnFile): void
    {
        $this->createCardOnFile = $createCardOnFile;
    }

    public function setChargeCardOnFileId(string $chargeCardOnFileId): void
    {
        $this->chargeCardOnFileId = $chargeCardOnFileId;
    }

    public function setDebug(bool $debug): void
    {
        $this->debug = $debug;
    }

    public function getAmount(): float {
        return $this->amount;
    }

    public function getCurrency(): Currency {
        return $this->currency;
    }

    public function getPaymentGateway(): PaymentGateway {
        return $this->paymentGateway;
    }

    public function getTermsUrl(): string {
        return $this->termsUrl;
    }

    public function getExpiresAfter(): int {
        return $this->expiresAfter;
    }

    public function getDetails(): transactionDetails {
        return $this->details;
    }

    public function getContact(): TransactionContact {
        return $this->contact;
    }

    public function getNotificationContact(): TransactionContact {
        return $this->notificationContact;
    }

    public function getNotificationContactSecondary(): TransactionContact {
        return $this->notificationContactSecondary;
    }

    public function getBillingAddress(): transactionAddress {
        return $this->billingAddress;
    }

    public function getDeliveryAddress(): transactionAddress {
        return $this->deliveryAddress;
    }

    public function getPrimaryRecipient(): transactionPrimaryRecipient {
        return $this->primaryRecipient;
    }

    public function getLocales(): transactionLocales {
        return $this->locales;
    }

    public function getPayMessage(): TransactionWriteMessage {
        return $this->payMessage;
    }

    public function getReceiptMessage(): TransactionWriteMessage {
        return $this->receiptMessage;
    }

    public function getNotificationMessage(): TransactionWriteMessage {
        return $this->notificationMessage;
    }

    public function getNotificationMessageSecondary(): TransactionWriteMessage {
        return $this->notificationMessageSecondary;
    }

    public function getCustomUrls(): TransactionOverrideUrls {
        return $this->customUrls;
    }

    public function getPostbackUrls(): transactionPostbackUrls {
        return $this->postbackUrls;
    }

    public function getCreateCardOnFile(): bool {
        return $this->createCardOnFile;
    }

    public function getChargeCardOnFileId(): string {
        return $this->chargeCardOnFileId;
    }

    public function getDebug(): bool {
        return $this->debug;
    }
    public function jsonSerialize(): object
    {
        return (object) array_merge(
            array('merchantKey' => $this->merchantKey),
            array('amount' => $this->amount),
            array('currency' => $this->currency),
            isset($this->paymentGateway) ? array('paymentGateway' => $this->paymentGateway): array(),
            isset($this->termsUrl) ? array('termsUrl' => $this->termsUrl): array(),
            isset($this->expiresAfter) ? array('expiresAfter' => $this->expiresAfter): array(),
            isset($this->details) ? array('details' => $this->details): array(),
            isset($this->contact) ? array('contact' => $this->contact): array(),
            isset($this->notificationContact) ? array('notificationContact' => $this->notificationContact): array(),
            isset($this->notificationContactSecondary) ?
                array('notificationContactSecondary' => $this->notificationContactSecondary): array() ,
            isset($this->billingAddress) ? array('billingAddress' => $this->billingAddress): array(),
            isset($this->deliveryAddress) ? array('deliveryAddress'=> $this->deliveryAddress): array(),
            isset($this->primaryRecipient) ? array('primaryRecipient'=> $this->primaryRecipient): array(),
            isset($this->locales) ? array('locales' => $this->locales): array(),
            isset($this->payMessage) ? array('payMessage' => $this->payMessage): array(),
            isset($this->receiptMessage) ? array('receiptMessage' => $this->receiptMessage): array(),
            isset($this->notificationMessage) ? array('notificationMessage' => $this->notificationMessage): array(),
            isset($this->notificationMessageSecondary) ?
                array('notificationMessageSecondary' => $this->notificationMessageSecondary): array(),
            isset($this->overrideUrls) ? array('overrideUrls' => $this->overrideUrls): array(),
            isset($this->postbackUrls) ? array('postbackUrls' => $this->postbackUrls): array(),
            isset($this->createCardOnFile) ? array('createCardOnFile' => $this->createCardOnFile): array(),
            isset($this->chargeCardOnFileId) ? array('chargeCardOnFileId' => $this->chargeCardOnFileId): array(),
            isset($this->debug) ? array('debug' => $this->debug): array());
    }
}