<?php

namespace Src\main\client\models\utils;
class JWSSignature {

    public static function base64url_encode($data) {

        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');

    }
    public static function createSignature($signatureKeyInbound, $payload): string {

    //Create JWS Header
    $JWSHeader = '{"b64":false,"crit":["b64","iat"],"iat":' . time() . ',"alg":"HS256"}';
    $JWSHeader = JWSSignature::base64url_encode($JWSHeader);


    //Generate our Hash
    $Hash = $JWSHeader . '.' . $payload;
    $Hash = JWSSignature::base64url_encode(hash_hmac('sha256', $Hash, $signatureKeyInbound, true));
    //Create JWS Signature -> JWSHeader..Hash
    return $JWSHeader . '..' . $Hash;
    }
}