<?php

namespace Src\main\client\models;

use DateTime;
use Src\main\client\models\bases\Base;
use Src\main\client\models\components\merchantKey;
use Src\main\client\models\enums\Type;

class MerchantToken extends Base {
    protected string $object;
    protected string $id;
    protected string $value; //required
    protected merchantKey $merchantKey;
    protected Type $type;
    protected string $description;
    protected DateTime $expiry;
    protected DateTime $created;
    protected DateTime $updated;

    public function setObject(string $object): void {
        $this->object = $object;
    }

    // Setter for $id
    public function setId(string $id): void {
        $this->id = $id;
    }

    // Setter for $value (assuming it's required)
    public function setValue(string $value): void {
        $this->value = $value;
    }

    // Setter for $merchantKey
    public function setMerchantKey(merchantKey $merchantKey): void {
        $this->merchantKey = $merchantKey;
    }

    // Setter for $type
    public function setType(Type $type): void {
        $this->type = $type;
    }

    // Setter for $description
    public function setDescription(string $description): void {
        $this->description = $description;
    }

    // Setter for $expiry
    public function setExpiry(DateTime $expiry): void {
        $this->expiry = $expiry;
    }

    // Setter for $created
    public function setCreated(DateTime $created): void {
        $this->created = $created;
    }

    // Setter for $updated
    public function setUpdated(DateTime $updated): void {
        $this->updated = $updated;
    }

    public static function withBody(string $JSONBody): MerchantToken //string -> Json Body
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString(): string {
        return "[Object: $this->object]\n\n"
            . "ID: $this->id\n"
            . "Value: $this->value\n"
            . "Merchant Key $this->merchantKey\n"
            . "Type: " . ($this->type->value) . "\n"
            . "Description: " .  (isset($this->description) ? $this->description: 'NULL') . "\n"
            . "Expiry: " . ($this->expiry->format('Y-m-d H:i:s')) . "\n"
            . "Created: " . ($this->expiry->format('Y-m-d H:i:s')) . "\n"
            . "Updated: " . ($this->expiry->format('Y-m-d H:i:s')) . "\n";
    }
}