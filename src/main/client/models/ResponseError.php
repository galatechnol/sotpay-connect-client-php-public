<?php
namespace Src\main\client\models;
use Exception;
use Src\main\client\models\bases\Base;

class ResponseError extends Base
{
    private $object;
    private $id;
    private $message;
    private $messageSupplement;

    public function setObject($object)
    {
        $this->object = $object;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function setSupplement($supplement)
    {
        $this->messageSupplement = $supplement;
    }

    public function __construct()
    {
    }

    public static function withBody(string $JSONBody): ResponseError {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString(): string {
        return "[Object: " . ($this->object ?? 'NULL') . "]\n"
            . "ID: " . ($this->id ?? 'NULL') . "\n"
            . "Message: " . ($this->message ?? 'NULL') . "\n"
            . "Supplement: " . ($this->messageSupplement ?? 'NULL');
    }
}