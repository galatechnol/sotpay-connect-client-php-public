<?php
namespace Src\main\client\models;
use DateTime;
use PHPUnit\Logging\Exception;
use JsonMapper;
use Src\main\client\models\bases\Base;

class HeartbeatResult extends Base {
    private string $object;
    private string $dbTimezone;
    private DateTime $dbTimestampNow;
    private DateTime $dbTimestampNowLondon;

    public function setObject(string $object): void
    {
        $this->object = $object;
    }

    public function setTimeZone(string $dbTimezone): void
    {
        $this->dbTimezone = $dbTimezone;
    }

    public function setTimeStamp(DateTime $dbTimestampNow): void
    {
        $this->dbTimestampNow = $dbTimestampNow;
    }

    public function setTimeStampLondon(DateTime $dbTimestampNowLondon): void
    {
        $this->dbTimestampNowLondon = $dbTimestampNowLondon;
    }

    public function __construct()
    {
    }

    //Static helper method as a constructor
    public static function withBody(string $JSONBody): HeartbeatResult //string -> our JSON
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString(): string {
        return "[Object: $this->object]\n\n"
            . "dbTimezone: $this->dbTimezone\n"
            . "dbTimeStampNow: " .  ($this->dbTimestampNow->format('Y-m-d H:i:s')) . "\n"
            . "dbTimeStampNowLondon: " .  ($this->dbTimestampNowLondon->format('Y-m-d H:i:s')) . "\n";
    }
}