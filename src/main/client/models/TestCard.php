<?php
namespace Src\main\client\models;
use Exception;
use JsonMapper;
use Src\main\client\models\enums\PaymentGateway;
class TestCard
{
    private string $object;
    private PaymentGateway $paymentGateway;
    private string $cardType;
    private string $cardNumber;
    private string $cardholderName;
    private string $expiry;
    private string $securityCode;
    private string $d3s;
    private string $accountUsername;
    private string $accountPassword;
    private string $detailsLink;


    //Setters for testing
    public function setObject($object): void
    {
        $this->object = $object;
    }

    public function setPaymentGateway(PaymentGateway $paymentGateway): void
    {
        $this->paymentGateway = $paymentGateway;
    }

    public function setCardType(string $cardType): void
    {
        $this->cardType = $cardType;
    }

    public function setCardNumber(string $cardNumber): void
    {
        $this->cardNumber = $cardNumber;
    }

    public function setCardholderName(string $cardholderName): void
    {
        $this->cardholderName = $cardholderName;
    }

    public function setExpiry(string $expiryDate): void
    {
        $this->expiry = $expiryDate;
    }

    public function setSecurityCode(string $securityCode): void
    {
        $this->securityCode = $securityCode;
    }

    public function setD3s(string $d3s): void
    {
        $this->d3s = $d3s;
    }

    public function setAccountUsername(string $username): void
    {
        $this->accountUsername = $username;
    }

    public function setAccountPassword(string $password): void
    {
        $this->accountPassword = $password;
    }

    public function setDetailsLink(string $link): void
    {
        $this->detailsLink = $link;
    }

    public function __construct()
    {
    }

    public static function withBody(string $JSONBody): TestCard //string -> our JSON
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    private function set($body) {
        $data = json_decode($body);
        //Check for valid JSON, after string validation
        if ($data) {
            $mapper = new JsonMapper();
            $mapper->bIgnoreVisibility = true; //allow private access
            $mapper->bExceptionOnUndefinedProperty = true; //if we get unexpected properties
            $obj = $mapper->map($data, $this);
            return $obj;

        } else { // sending invalid data
            throw new Exception('Wrong data type sent to instance of TestCard, expected JSON body');
        }
    }

    public function __toString() {
        return
            "[Object: $this->object]\n" .
            "Payment Gateway: " . ($this->paymentGateway->value ?? "NULL") . "\n" .
            "Card Type: " . ($this->cardType ?? "NULL") . "\n" .
            "Card Number: " . ($this->cardNumber ?? "NULL") . "\n" .
            "Cardholder Name: " . ($this->cardholderName ?? "NULL") . "\n" .
            "Expiry: " . ($this->expiry ?? "NULL") . "\n" .
            "Security Code: " . ($this->securityCode ?? "NULL") . "\n" .
            "D3S: " . ($this->d3s ?? "NULL") . "\n" .
            "Account Username: " . ($this->accountUsername ?? "NULL") . "\n" .
            "Account Password: " . ($this->accountPassword ?? "NULL") . "\n" .
            "Details Link: " . ($this->detailsLink ?? "NULL") . "\n";
    }
}