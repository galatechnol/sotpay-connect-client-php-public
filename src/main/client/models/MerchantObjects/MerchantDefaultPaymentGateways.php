<?php

namespace Src\main\client\models\MerchantObjects;

use JsonSerializable;
use Src\main\client\models\enums\PaymentGateway;

class MerchantDefaultPaymentGateways implements JsonSerializable
{
    private PaymentGateway $paymentGatewayForCard;
    private PaymentGateway $paymentGatewayForOpenBanking;

    public function getPaymentGatewayForCard(): PaymentGateway
    {
        return $this->paymentGatewayForCard;
    }
    public function getPaymentGatewayForOpenBanking(): PaymentGateway
    {
        return $this->paymentGatewayForOpenBanking;
    }
    public function setPaymentGatewayForCard(PaymentGateway $paymentGatewayForCard): void
    {
        $this->paymentGatewayForCard = $paymentGatewayForCard;
    }
    public function setPaymentGatewayForOpenBanking(PaymentGateway $paymentGatewayForOpenBanking): void
    {
        $this->paymentGatewayForOpenBanking = $paymentGatewayForOpenBanking;
    }

    public function jsonSerialize(): array
    {
        return array_merge(
            isset($this->paymentGatewayForCard) ? array('paymentGatewayForCard' => $this->paymentGatewayForCard): array(),
            isset($this->paymentGatewayForOpenBanking) ? array('paymentGatewayForOpenBanking' => $this->paymentGatewayForOpenBanking): array()
        );
    }


    public function __toString() {
        return "Payment Gateway For Card: " . ($this->paymentGatewayForCard->value ?? 'NULL') . "\n"
            . "Payment Gateway For Open Banking: " . ($this->paymentGatewayForOpenBanking->value ?? 'NULL') . "\n";
    }
}