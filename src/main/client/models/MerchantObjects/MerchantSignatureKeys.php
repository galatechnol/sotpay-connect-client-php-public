<?php

namespace Src\main\client\models\MerchantObjects;

class MerchantSignatureKeys
{
    private string $signatureKeyInbound;
    private string $signatureKeyOutbound;


    public function getSignatureKeyInbound(): string
    {
        return $this->signatureKeyInbound;
    }
    public function getSignatureKeyOutbound(): string
    {
        return $this->signatureKeyOutbound;
    }
    public function setSignatureKeyInbound(string $signatureKeyInbound): void
    {
        $this->signatureKeyInbound = $signatureKeyInbound;
    }
    public function setSignatureKeyOutbound(string $signatureKeyOutbound): void
    {
        $this->signatureKeyOutbound = $signatureKeyOutbound;
    }

    public function jsonSerialize(): array
    {
        return array_merge(
            isset($this->signatureKeyInbound) ? array('signatureKeyInbound' => $this->signatureKeyInbound): array(),
            isset($this->signatureKeyOutbound) ? array('signatureKeyOutbound' => $this->signatureKeyOutbound): array()
        );
    }

    public function __toString() {
        return "Signature Key Inbound: " . ($this->signatureKeyInbound ?? 'NULL') . "\n"
            . "Signature Key Outbound: " . ($this->signatureKeyOutbound ?? 'NULL') . "\n";
    }
}