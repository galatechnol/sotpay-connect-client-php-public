<?php

namespace Src\main\client\models\MerchantObjects;

use JsonSerializable;
use Src\main\client\models\components\TransactionWriteMessage;
use Src\main\client\models\TransactionObjects\TransactionContact;
use Src\main\client\models\TransactionObjects\TransactionOverrideUrls;
use Src\main\client\models\TransactionObjects\TransactionPostbackUrls;

class MerchantTransactionDefaults implements JsonSerializable {
    private string $termsUrl;
    private int $expiresAfter;
    private TransactionContact $notificationContact;
    private TransactionContact $notificationContactSecondary;
    private TransactionWriteMessage $payMessage;
    private TransactionWriteMessage $receiptMessage;
    private TransactionWriteMessage $notificationMessage;
    private TransactionWriteMessage $notificationMessageSecondary;
    private TransactionOverrideUrls $overrideUrls;
    private TransactionPostbackUrls $postbackUrls;

    public function getTermsUrl(): string
    {
        return $this->termsUrl;
    }
    public function getExpiresAfter(): int
    {
        return $this->expiresAfter;
    }
    public function getNotificationContact(): TransactionContact
    {
        return $this->notificationContact;
    }
    public function getNotificationContactSecondary(): TransactionContact
    {
        return $this->notificationContactSecondary;
    }
    public function getPayMessage(): TransactionWriteMessage
    {
        return $this->payMessage;
    }
    public function getReceiptMessage(): TransactionWriteMessage
    {
        return $this->receiptMessage;
    }
    public function getNotificationMessage(): TransactionWriteMessage
    {
        return $this->notificationMessage;
    }
    public function getNotificationMessageSecondary(): TransactionWriteMessage
    {
        return $this->notificationMessageSecondary;
    }
    public function getOverrideUrls(): TransactionOverrideUrls
    {
        return $this->overrideUrls;
    }
    public function getPostbackUrls(): TransactionPostbackUrls
    {
        return $this->postbackUrls;
    }
    public function setTermsUrl(string $termsUrl): void
    {
        $this->termsUrl = $termsUrl;
    }
    public function setExpiresAfter(int $expiresAfter): void
    {
        $this->expiresAfter = $expiresAfter;
    }
    public function setNotificationContact(TransactionContact $notificationContact): void
    {
        $this->notificationContact = $notificationContact;
    }
    public function setNotificationContactSecondary(TransactionContact $notificationContactSecondary): void
    {
        $this->notificationContactSecondary = $notificationContactSecondary;
    }
    public function setPayMessage(TransactionWriteMessage $payMessage): void
    {
        $this->payMessage = $payMessage;
    }
    public function setReceiptMessage(TransactionWriteMessage $receiptMessage): void
    {
        $this->receiptMessage = $receiptMessage;
    }
    public function setNotificationMessage(TransactionWriteMessage $notificationMessage): void
    {
        $this->notificationMessage = $notificationMessage;
    }
    public function setNotificationMessageSecondary(TransactionWriteMessage $notificationMessageSecondary): void
    {
        $this->notificationMessageSecondary = $notificationMessageSecondary;
    }
    public function setOverrideUrls(TransactionOverrideUrls $overrideUrls): void
    {
        $this->overrideUrls = $overrideUrls;
    }
    public function setPostbackUrls(TransactionPostbackUrls $postbackUrls): void
    {
        $this->postbackUrls = $postbackUrls;
    }

    public function __toString(): string {
        return "Terms Url:" . ($this->termsUrl ?? 'NULL') . "\n"
            . "Expires After: " . ($this->expiresAfter ?? 'NULL') . "\n"
            . "Notification Contact: " . ($this->notificationContact ?? 'NULL') . "\n"
            . "Notification Contact Secondary: " . ($this->notificationContactSecondary ?? 'NULL') . "\n"
            . "Pay Message: " . ($this->payMessage ?? 'NULL') . "\n"
            . "Receipt Message: " . ($this->receiptMessage ?? 'NULL') . "\n"
            . "Notification Message: " . ($this->notificationMessage ?? 'NULL') . "\n"
            . "Notification Message Secondary: " . ($this->notificationMessageSecondary ?? 'NULL') . "\n"
            . "Override Urls: " . ($this->overrideUrls ?? 'NULL') . "\n"
            . "Postback Urls: " . ($this->postbackUrls ?? 'NULL') . "\n";
    }

    public function jsonSerialize(): array
    {
        return array_merge(
            isset($this->termsUrl) ? array('termsUrl' => $this->termsUrl): array(),
            isset($this->expiresAfter) ? array('expiresAfter' => $this->expiresAfter): array(),
            isset($this->notificationContact) ? array('notificationContact' => $this->notificationContact): array(),
            isset($this->notificationContactSecondary) ? array('notificationContactSecondary' => $this->notificationContactSecondary): array(),
            isset($this->payMessage) ? array('payMessage' => $this->payMessage): array(),
            isset($this->receiptMessage) ? array('receiptMessage'=> $this->receiptMessage): array(),
            isset($this->notificationMessage) ? array('notificationMessage'=> $this->notificationMessage): array(),
            isset($this->notificationMessageSecondary) ? array('notificationMessageSecondary' => $this->notificationMessageSecondary): array(),
            isset($this->overrideUrls) ? array('overrideUrls' => $this->overrideUrls): array(),
            isset($this->postbackUrls) ? array('postbackUrls' => $this->postbackUrls): array()
        );
    }
}