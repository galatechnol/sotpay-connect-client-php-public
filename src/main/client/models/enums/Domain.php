<?php

namespace Src\main\client\models\enums;
enum Domain: string
{
    case SANDBOX = 'SANDBOX';
    case LIVE = 'LIVE';

    public static function toVal(string $val) {
        return constant("self::$val");
    }

    public static function toString($enumValue) {
        switch ($enumValue) {
            case self::SANDBOX:
                return 'SANDBOX';
            case self::LIVE:
                return 'LIVE';
        }
    }
}