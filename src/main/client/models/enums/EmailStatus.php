<?php

namespace Src\main\client\models\enums;

enum EmailStatus: string
{
    case REQUESTED = "REQUESTED";
    case SEND_SUCCESS = "SEND_SUCCESS";
    case SEND_FAILURE = "SEND_FAILURE";
    case DELIVERY_SUCCESS = "DELIVERY_SUCCESS";
    case DELIVERY_FAILURE = "DELIVERY_FAILURE";
    case OPENED = "OPENED";
    case NULL = 'NULL';
}