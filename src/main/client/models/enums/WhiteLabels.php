<?php

namespace Src\main\client\models\enums;

enum WhiteLabels : string
{
    case ACQUIRED = "ACQUIRED";
    case BANKOFIRELAND = "BANKOFIRELAND";
    case BARCLAYCARD = "BARCLAYCARD";
    case CARDNETPLUS = "CARDNETPLUS";
    case CARDSTREAM = "CARDSTREAM";
    case CHECKOUTCOM = "CHECKOUTCOM";
    case DOODLEPAY = "DOODLEPAY";
    case EGATEWAY = "EGATEWAY";
    case ELAVON = "ELAVON";
    case ELAVONREALEX = "ELAVONREALEX";
    case ENCODED = "ENCODED";
    case EVOPAYMENTS = "EVOPAYMENTS";
    case FREEDOMPAY = "FREEDOMPAY";
    case GLOBALPAYMENTS = "GLOBALPAYMENTS";
    case GLOBALPAYMENTSREALEX = "GLOBALPAYMENTSREALEX";
    case HANDEPAY = "HANDEPAY";
    case INSPIREPAYMENTS = "INSPIREPAYMENTS";
    case LANGTRANSACT = "LANGTRANSACT";
    case NUAPAY = "NUAPAY";
    case OPAYO = "OPAYO";
    case OPAYOSERVER = "OPAYOSERVER";
    case PAY360 = "PAY360";
    case PAYSAFE = "PAYSAFE";
    case RMS = "RMS";
    case SCP = "SCP";
    case SOTPAYPLUS = "SOTPAYPLUS";
    case STRIPE = "STRIPE";
    case SYBC = "SYBC";
    case TAKEPAYMENTS = "TAKEPAYMENTS";
    case UTP = "UTP";
    case YORKSHIREPAYMENTS  = "YORKSHIREPAYMENTS ";

    public static function toVal(string $val) {
        return constant("self::$val");
    }
}
