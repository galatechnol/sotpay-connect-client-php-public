<?php

namespace Src\main\client\models\enums;

enum Progress: string
{
    case REGISTERED = "REGISTERED";
    case STARTED = "STARTED";
    case PAYING = "PAYING";
    case AUTHENTICATING = "AUTHENTICATING";
    case COMPLETED = "COMPLETED";
}
