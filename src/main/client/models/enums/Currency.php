<?php
namespace Src\main\client\models\enums;
enum Currency: string {
    case GBP = 'GBP';
    case EUR = 'EUR';
    case USD = 'USD';
    case AUD = 'AUD';
    case BGN = 'BGN';
    case BRL = 'BRL';
    case CAD = 'CAD';
    case CHF = 'CHF';
    case CNY = 'CNY';
    case CZK = 'CZK';
    case DKK = 'DKK';
    case HKD = 'HKD';
    case IDR = 'IDR';
    case INR = 'INR';
    case JPY = 'JPY';
    case KRW = 'KRW';
    case MYR = 'MYR';
    case NZD = 'NZD';
    case PHP = 'PHP';
    case PLN = 'PLN';
    case RON = 'RON';
    case RUB = 'RUB';
    case SEK = 'SEK';
    case SGD = 'SGD';
    case THB = 'THB';
    case ZAR = 'ZAR';

    public static function toVal(string $val) {
        return constant("self::$val");
    }
}