<?php
namespace Src\main\client\models\enums;

enum sortOrder:string {
    case ASCENDING = 'ASCENDING';
    case DESCENDING = 'DESCENDING';

    public static function toVal(string $val) {
        return constant("self::$val");
    }
}