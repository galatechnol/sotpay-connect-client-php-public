<?php

namespace Src\main\client\models\enums;
enum  PaymentGateway: string
{
    case acquired = "acquired";
    case barclaycard = "barclaycard";
    case cardstream = "cardstream";
    case checkoutcom = "checkoutcom";
    case elavon = "elavon";
    case encoded = "encoded";
    case evopayments = "evopayments";
    case freedompay = "freedompay";
    case globalpayments = "globalpayments";
    case globalpaymentsrealex = "globalpaymentsrealex";
    case nuapay = "nuapay";
    case opayo = "opayo";
    case opayoserver = "opayoserver";
    case pay360 = "pay360";
    case paysafe = "paysafe";
    case scp = "scp";
    case stripe = "stripe";
    case sybc = "sybc";
    case trustpayments = "trustpayments";

    public function toCardOnFile(): string {
        return match ($this) {
            self::acquired => 'AcquiredCardOnFile',
            self::cardstream => 'CardstreamCardOnFile',
            self::checkoutcom => 'CheckoutComCardOnFile',
            self::elavon => 'ElavonCardOnFile',
            self::encoded => 'EncodedCardOnFile',
            self::evopayments => 'EVOPaymentsCardOnFile',
            self::freedompay => 'FreedomPayCardOnFile',
            self::globalpayments => 'GlobalPaymentsCardOnFile',
            self::globalpaymentsrealex => 'GlobalPaymentsRealexCardOnFile',
            self::opayo => 'OpayoCardOnFile',
            self::opayoserver => 'OpayoServerCardOnFile',
            self::pay360 => 'Pay360CardOnFile',
            self::paysafe => 'PaysafeCardOnFile',
            self::sybc => 'SYBCCardOnFile'
        };
    }

    public static function toString($enumValue) {
        switch ($enumValue) {
            case self::acquired:
                return "Acquired";
            case self::barclaycard:
                return "Barclaycard";
            case self::cardstream:
                return "Cardstream";
            case self::checkoutcom:
                return "Checkout.com";
            case self::elavon:
                return "Elavon";
            case self::encoded:
                return "Encoded";
            case self::evopayments:
                return "Evo Payments";
            case self::freedompay:
                return "FreedomPay";
            case self::globalpayments:
                return "Global Payments";
            case self::globalpaymentsrealex:
                return "Global Payments Realex";
            case self::nuapay:
                return "Nuapay";
            case self::opayo:
                return "Opayo";
            case self::opayoserver:
                return "Opayo Server";
            case self::pay360:
                return "Pay360";
            case self::paysafe:
                return "Paysafe";
            case self::scp:
                return "SCP";
            case self::stripe:
                return "Stripe";
            case self::sybc:
                return "SYBC";
            case self::trustpayments :
                return "Trust Payments";
        }
    }

    public static function toVal(string $val) {
        return constant("self::$val");
    }
}