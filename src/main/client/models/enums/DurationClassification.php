<?php

namespace Src\main\client\models\enums;

enum DurationClassification: string
{
    case FASTEST = "FASTEST";
    case FAST = "FAST";
    case SLOW = "SLOW";
    case SLOWEST = "SLOWEST";
}