<?php
namespace Src\main\client\models\enums;

enum sortPropertyCardOnFile: string {
    case ID = 'ID';
    case REFERENCE = 'REFERENCE';
    case FIRSTURN = 'FIRSTURN';
    case FIRSTREFERENCE = 'FIRSTREFERENCE';
    case CHARGECOUNT = 'CHARGECOUNT';
    case CARDTYPE = 'CARDTYPE';
    case MASKEDCARDNUMBER = 'MASKEDCARDNUMBER';
    case CARDHOLDER = 'CARDHOLDER';
    case CONTACTEMAIL = 'CONTACTEMAIL';
    case CONTACTPHONE = 'CONTACTPHONE';
    case EXPIRY = 'EXPIRY';
    case CREATED = 'CREATED';
    case UPDATED = 'UPDATED';

    public static function toVal(string $val) {
        return constant("self::$val");
    }

}