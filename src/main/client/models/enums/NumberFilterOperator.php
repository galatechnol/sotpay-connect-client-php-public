<?php

namespace Src\main\client\models\enums;
enum NumberFilterOperator: string {
    case EQUALS = 'EQUALS';
    case GREATERTHAN = 'GREATERTHAN';
    case GREATERTHANOREQUALS = 'GREATERTHANOREQUALS';
    case LESSTHAN = 'LESSTHAN';
    case LESSTHANOREQUALS = 'LESSTHANOREQUALS';
}