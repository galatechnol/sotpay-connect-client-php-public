<?php

namespace Src\main\client\models\enums;

enum StringFilterOperator : string {
    case EQUALS = 'EQUALS';
    case CONTAINS = 'CONTAINS';
    case STARTSWITH = 'STARTSWITH';
    case ENDSWITH = 'ENDSWITH';
}
