<?php

namespace Src\main\client\models\enums;

enum Status: string
{
    case NEW = 'NEW';
    case PAID = 'PAID';
    case FAILED = 'FAILED';
    case ABORTED = 'ABORTED';
    case EXPIRED = 'EXPIRED';
}