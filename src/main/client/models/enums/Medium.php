<?php

namespace Src\main\client\models\enums;

enum Medium: string
{
    case SMS = "SMS";
    case EMAIL = "EMAIL";
    case CARDONFILE = "CARDONFILE";
    case UNKOWN = "UNKNOWN";
}
