<?php

namespace Src\main\client\models\enums;

enum Type: string
{
    case READ = 'READ';
    case READWRITE = 'READWRITE';

    public static function toVal(string $val) {
        return constant("self::$val");
    }
}
