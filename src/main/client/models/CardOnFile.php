<?php
namespace Src\main\client\models;
use DateTime;
use Src\main\client\models\bases\Base;
use Src\main\client\models\components\merchantKey;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionObjects\transactionAddress;
use Src\main\client\models\TransactionObjects\TransactionContact;

class CardOnFile extends Base {

    private string $object;
    private string $id;
    private merchantKey $merchantKey;
    private string $reference;
    private string $firstUrn;
    private string $firstReference;
    private int $chargeCount;
    private CardType $cardType;
    private string $maskedCardNumber;
    private string $cardHolder;
    private string $expiry;
    private DateTime $created;
    private DateTime $updated;

    public function setObject(string $object): void {
        $this->object = $object;
    }
    public function setId(string $id): void {
        $this->id = $id;
    }

    public function setMerchantKey(merchantKey $merchantKey): void {
        $this->merchantKey = $merchantKey;
    }

    public function setReference(string $reference): void {
        $this->reference = $reference;
    }

    public function setFirstUrn(string $firstUrn): void {
        $this->firstUrn = $firstUrn;
    }

    public function setFirstReference(string $firstReference): void {
        $this->firstReference = $firstReference;
    }

    public function setChargeCount(int $chargeCount): void {
        $this->chargeCount = $chargeCount;
    }

    public function setCardType(CardType $cardType): void {
        $this->cardType = $cardType;
    }
    public function setMaskedCardNumber(string $maskedCardNumber): void {
        $this->maskedCardNumber = $maskedCardNumber;
    }

    public function setCardHolder(string $cardHolder): void {
        $this->cardHolder = $cardHolder;
    }

    public function setExpiry(string $expiry): void {
        $this->expiry = $expiry;
    }

    public function setCreated(DateTime $created): void {
        $this->created = $created;
    }

    public function setUpdated(DateTime $updated): void {
        $this->updated = $updated;
    }
    public function getObject(): string {
        return $this->object;
    }

    public function getId(): string {
        return $this->id;
    }

    public function getMerchantKey(): merchantKey {
        return $this->merchantKey;
    }

    public function getReference(): string {
        return $this->reference;
    }

    public function getFirstUrn(): string {
        return $this->firstUrn;
    }

    public function getFirstReference(): string {
        return $this->firstReference;
    }

    public function getSequenceCount(): int {
        return $this->sequenceCount;
    }

    public function getCardType(): CardType {
        return $this->cardType;
    }

    public function getMaskedCardNumber(): string {
        return $this->maskedCardNumber;
    }

    public function getCardHolder(): string {
        return $this->cardHolder;
    }

    public function getExpiry(): string {
        return $this->expiry;
    }

    public function getCreated(): DateTime {
        return $this->created;
    }

    public function getUpdated(): DateTime {
        return $this->updated;
    }

    public function setContact(TransactionContact $contact): void {
        $this->contact = $contact;
    }

    public function setBillingAddress(transactionAddress $billingAddress): void {
        $this->billingAddress = $billingAddress;
    }

    public function getBillingAddress(): transactionAddress
    {
        return $this->billingAddress;
    }

    public function getContact(): TransactionContact
    {
        return $this->contact;
    }
    public function getCardReference(): string
    {
        return $this->cardReference;
    }

    public function getPayerReference(): string
    {
        return $this->payerReference;
    }


    public function __toString(): string {
        return "[Object $this->object]\n\n"
            . "ID: $this->id \n"
            . "Merchant Key $this->merchantKey\n"
            . "Reference: " . ($this->reference ?? 'NULL') . "\n"
            . "First URN: " . ($this->firstUrn ?? 'NULL') . "\n"
            . "First Reference: " . ($this->firstReference ?? 'NULL') . "\n"
            . "Sequence Count: " . ($this->sequenceCount ?? 'NULL') . "\n"
            . "Card Type: " . ($this->cardType->value ?? 'NULL') . "\n"
            . "Masked Card Number: " . ($this->maskedCardNumber ?? 'NULL') . "\n"
            . "CardHolder: " . ($this->cardHolder ?? 'NULL') . "\n"
            . "Expiry: " . ($this->expiry ?? 'NULL') . "\n"
            . "Created: " . ($this->created->format('Y-m-d H:i:s') ?? 'NULL') . "\n"
            . "Updated: " . ($this->updated->format('Y-m-d H:i:s') ?? 'NULL') . "\n";
    }
}