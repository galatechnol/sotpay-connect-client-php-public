<?php

namespace Src\main\client\models\TransactionObjects;
use Exception;
use JsonSerializable;

class TransactionPrimaryRecipient implements JsonSerializable
{
    private string $accountNumber;
    private string $surname;
    private string $postcode;
    private string $dateofBirth;


    public function __construct(
        string $accountNumber,
        string $surname,
        string $postcode,
        string $dateofBirth
    ) {
        $this->accountNumber = $accountNumber;
        $this->surname = $surname;
        $this->postcode = $postcode;
        $this->dateofBirth = $dateofBirth;
    }
    public function getSurname(): string
    {
        return $this->surname;
    }
    public function getPostcode(): string
    {
        return $this->postcode;
    }
    public function getAccountNumber(): string
    {
        return $this->accountNumber;
    }

    public function getDateofBirth(): string
    {
        return $this->dateofBirth;
    }
    public function exists($property) {
        return isset($this->$property);
    }

    public function __toString() {
        return "Account Number: {$this->accountNumber}\n" .
            "Surname: {$this->surname}\n" .
            "Postcode: {$this->postcode}\n" .
            "Date of Birth: " . ($this->dateofBirth ?? 'NULL') . "\n";
    }

    public function jsonSerialize(): object {
        return (object) [
            'accountNumber' => $this->accountNumber,
            'surname' => $this->surname,
            'postcode'=> $this->postcode,
            'dateofBirth' => $this->dateofBirth
        ];
    }

}