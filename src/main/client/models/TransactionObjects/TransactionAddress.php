<?php

namespace Src\main\client\models\TransactionObjects;

use JsonSerializable;
use Src\main\client\models\enums\Country;

class TransactionAddress implements JsonSerializable
{
    private string $firstName;
    private string $surname;
    private string $displayName;
    private string $line1;
    private string $line2;
    private string $city;
    private string $postcode;
    private string $state;
    private Country $country;

    public function __construct(
        string $firstName,
        string $surname,
        string $line1,
        string $city,
        Country $country,
        string $line2 = null,
        string $postcode = null,
        string $state = null,
        string $displayName = null,

    ) {
        $this->firstName = $firstName;
        $this->surname = $surname;
        $this->line1 = $line1;
        $this->city = $city;
        $this->country = $country;
        $line2 ? $this->line2 = $line2 : '';
        $postcode ? $this->postcode = $postcode : '';
        $state ? $this->state = $state : '';
        $displayName ? $this->displayName = $displayName : '';
    }
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getCountry(): Country
    {
        return $this->country;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLine1(): string
    {
        return $this->line1;
    }

    public function getLine2(): string
    {
        return $this->line2;
    }

    public function getPostcode(): string
    {
        return $this->postcode;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function exists($property) {
        return isset($this->$property);
    }

    public function jsonSerialize(): object
    {
        return (object) array_merge(
            array('firstName' => $this->firstName),
            array('surname' => $this->surname),
            isset($this->displayName) ? array('displayName' => $this->displayName): array(),
            array('line1' => $this->line1),
            isset($this->line2) ? array('line2' => $this->line2): array(),
            array('city' => $this->city),
            isset($this->postcode) ? array('postcode' => $this->postcode): array(),
            isset($this->state) ? array('state' => $this->state): array(),
            array('country'=> $this->country->value)
        );
    }

    public function __toString() {
        return "First Name: " . ($this->firstName ?? 'NULL') . "\n" .
            "Surname: " . ($this->surname ?? 'NULL') . "\n" .
            "Display Name: " . ($this->displayName ?? 'NULL') . "\n" .
            "Line 1: " . ($this->line1 ?? 'NULL') . "\n" .
            "Line 2: " . ($this->line2 ?? 'NULL') . "\n" .
            "City: " . ($this->city ?? 'NULL') . "\n" .
            "Postcode: " . ($this->postcode ?? 'NULL') . "\n" .
            "State: " . ($this->state ?? 'NULL') . "\n" .
            "Country: " . ($this->country->value ?? 'NULL') . "\n";
    }



}