<?php

namespace Src\main\client\models\TransactionObjects;
use Exception;
use JsonSerializable;
use stdClass;

class TransactionPostbackUrls implements JsonSerializable
{
    private ?string $postbackUrl;
    private ?string $postbackUrlSecondary;
    private bool $progressPostbacks;

    public function getPostbackUrl(): string
    {
        return $this->postbackUrl;
    }

    public function getPostbackUrlSecondary(): string
    {
        return $this->postbackUrlSecondary;
    }

    public function getProgressPostbacks(): bool
    {
        return $this->progressPostbacks;
    }
    public function setPostbackUrl(string $postbackUrl): void
    {
        $this->postbackUrl = $postbackUrl;
    }
    public function setPostbackUrlSecondary(string $postbackUrlSecondary): void
    {
        $this->postbackUrlSecondary = $postbackUrlSecondary;
    }
    public function setProgressPostbacks(bool $progressPostbacks): void
    {
        $this->progressPostbacks = $progressPostbacks;
    }
    public function exists($property) {
        return isset($this->$property);
    }

    public function __construct(string $postbackUrl, ?string $postbackUrlSecondary = null, bool $progressPostbacks = false)
    {
        $this->postbackUrl = $postbackUrl;
        $postbackUrlSecondary ? $this->postbackUrlSecondary = $postbackUrlSecondary : '';
        $this->progressPostbacks = $progressPostbacks;
    }

    public function verifyFieldsNotNull() {

        if ($this->postbackUrl == null && $this->postbackUrlSecondary == null) {
            throw new Exception('Instantiation of Postback Urls Error: No fields have been set');
        }
    }

    public function jsonSerialize(): object
    {
        return (object) array_merge(
            isset($this->postbackUrl) ? array('postbackUrl' => $this->postbackUrl): array(),
            isset($this->postbackUrlSecondary) ? array('postbackUrlSecondary' => $this->postbackUrlSecondary): array(),
            isset($this->progressPostbacks) ? array('progressPostbacks' => $this->progressPostbacks): array(),
        );
    }

    public function __toString() {
        return "PostbackURL: " . ($this->postbackUrl ?? 'NULL') . "\n"
            . "PostbackURLSecondary: " . ($this->postbackUrlSecondary ?? 'NULL') . "\n"
            . "ProgressPostbacks: " . ($this->progressPostbacks ? "true" : "false") . "\n";
    }
}