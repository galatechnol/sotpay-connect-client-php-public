<?php

namespace Src\main\client\models\TransactionObjects;
use Exception;
use Src\main\client\models\enums\EmailStatus;
use Src\main\client\models\enums\SMSStatus;

class TransactionMessage
{
    private bool $email;
    private bool $sms;
    private EmailStatus $emailStatus;
    private SMSStatus $smsStatus;

    public function __construct(
        bool $email,
        bool $sms,
    ) {
        $this->email = $email;
        $this->sms = $sms;
    }


    public function getSmsStatus(): SMSStatus
    {
        return $this->smsStatus;
    }

    public function getSms(): bool
    {
        return $this->sms;
    }

    public function getEmail(): bool
    {
        return $this->email;
    }

    public function getEmailStatus(): EmailStatus
    {
        return $this->emailStatus;
    }

    public function setEmailStatus(EmailStatus $emailStatus) {
        $this->emailStatus = $emailStatus;
    }
    public function setSMSStatus(SMSStatus $SMSStatus) {
        $this->smsStatus = $SMSStatus;
    }
    public function exists($property) {
        return isset($this->$property);
    }

    public function __toString() {
        return "Email: " . ($this->email ? "true" : "false") . "\n"
            . "SMS: " . ($this->sms ? "true" : "false") . "\n"
            . "Email Status: " . ($this->emailStatus->value ?? 'NULL') . "\n"
            . "SMS Status: " . ($this->smsStatus->value ?? 'NULL') . "\n";
    }

}