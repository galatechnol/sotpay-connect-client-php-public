<?php

namespace Src\main\client\models\TransactionObjects;
use Exception;
use JsonSerializable;


class TransactionLocales implements JsonSerializable
{
    private string $agent;
    private string $customer;

    public function __construct(
        string $agent,
        string $customer
    ) {
        $this->agent = $agent;
        $this->customer = $customer;
    }
    public function getAgent(): string
    {
        return $this->agent;
    }
    public function getCustomer(): string
    {
        return $this->customer;
    }
    public function exists($property) {
        return isset($this->$property);
    }

    public function jsonSerialize(): object
    {
        return (object) [
            'agent' => $this->agent,
            'customer' => $this->customer,
        ];
    }

    public function __toString() {
        return "Agent: " . ($this->agent ?? 'NULL') . "\n" .
            "Customer: " . ($this->customer ?? 'NULL') . "\n";
    }
}