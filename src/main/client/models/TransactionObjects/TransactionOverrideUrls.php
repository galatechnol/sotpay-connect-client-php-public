<?php

namespace Src\main\client\models\TransactionObjects;
use Exception;
use JsonSerializable;
use stdClass;

class TransactionOverrideUrls implements JsonSerializable
{
    private ?string $progressUrl;
    private ?string $statusUrl;
    private ?string $errorUrl;
    public function exists($property) {
        return isset($this->$property);
    }


    public function getProgressUrl(): ?string
    {
        return $this->progressUrl;
    }
    public function getErrorUrl(): string
    {
        return $this->errorUrl;
    }

    public function getStatusUrl(): string
    {
        return $this->statusUrl;
    }
    public function setProgressUrl(?string $progressUrl): void
    {
        $this->progressUrl = $progressUrl;
    }
    public function setErrorUrl(string $errorUrl): void
    {
        $this->errorUrl = $errorUrl;
    }
    public function setStatusUrl(string $statusUrl): void
    {
        $this->statusUrl = $statusUrl;
    }
    public function __construct(string $progressUrl = null, string $statusUrl = null, string $errorUrl = null)
    {
        $progressUrl ? $this->progressUrl = $progressUrl : '';
        $statusUrl ? $this->statusUrl = $statusUrl : '';
        $errorUrl ? $this->errorUrl = $errorUrl : '';
    }

    public function __toString() {
        return "Progress URL: " . ($this->progressUrl ?? 'NULL') . "\n"
            . "Status URL: " . ($this->statusUrl ?? 'NULL') . "\n"
            . "Error URL: " . ($this->errorUrl ?? 'NULL') . "\n";
    }

    public function jsonSerialize(): object
    {
        return (object) array_merge(
            isset($this->progressUrl) ? array('progressUrl' => $this->progressUrl): array(),
            isset($this->statusUrl) ? array('statusUrl' => $this->statusUrl): array(),
            isset($this->errorUrl) ? array('errorUrl' => $this->errorUrl): array(),
        );
    }
}