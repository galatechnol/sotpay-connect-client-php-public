<?php

namespace Src\main\client\models\TransactionObjects;
use Exception;
use JsonSerializable;

class TransactionContact implements JsonSerializable
{
    private string $email;
    private ?string $phone;

    public function getEmail(): string
    {
        return $this->email;
    }
    public function getPhone(): string
    {
        return $this->phone;
    }
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }
    public function __construct(string $email, ?string $phone = null)
    {
        $this->email = $email;
        $phone ? $this->phone = $phone : '';
    }
    public function exists($property) {
        return isset($this->$property);
    }

    public function jsonSerialize(): object
    {
        return (object) array_merge(
            array('email' => $this->email),
            isset($this->phone) ? array('phone' => $this->phone): array(),
        );
    }

    public function __toString() {
        return "Email: " . ($this->email ?? 'NULL') . "\n"
            . "Phone: " . ($this->phone ?? 'NULL') . "\n";
    }

}