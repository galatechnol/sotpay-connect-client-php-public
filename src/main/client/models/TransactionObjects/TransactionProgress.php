<?php

namespace Src\main\client\models\TransactionObjects;
use DateTime;
use Exception;
use Src\main\client\models\enums\DurationClassification;
use Src\main\client\models\enums\Medium;
use Src\main\client\models\enums\Progress;


class TransactionProgress
{
    private Progress $progress;
    private ?Medium $completionMedium;
    private ?DateTime $startedTimestamp;
    private ?DateTime $payingTimestamp;
    private ?DateTime $authenticatingTimestamp;
    private ?DateTime $completedTimestamp;
    private ?string $payingAgent;
    private bool $locked;
    private ?int $completionDuration;
    private ?string $completionDurationDescription;
    private ?DurationClassification $completionDurationClassification;

    public function getProgress(): Progress
    {
        return $this->progress;
    }

    public function getCompletionMedium(): ?Medium
    {
        return $this->completionMedium;
    }

    public function getStartedTimestamp(): ?DateTime
    {
        return $this->startedTimestamp;
    }

    public function getPayingTimestamp(): ?DateTime
    {
        return $this->payingTimestamp;
    }

    public function getAuthenticatingTimestamp(): ?DateTime
    {
        return $this->authenticatingTimestamp;
    }

    public function getCompletedTimestamp(): ?DateTime
    {
        return $this->completedTimestamp;
    }

    public function getPayingAgent(): ?string
    {
        return $this->payingAgent;
    }

    public function getLocked(): bool
    {
        return $this->locked;
    }

    public function getCompletionDuration(): ?int
    {
        return $this->completionDuration;
    }

    public function getCompletionDurationDescription(): ?string
    {
        return $this->completionDurationDescription;
    }

    public function getCompletionDurationClassification(): ?DurationClassification
    {
        return $this->completionDurationClassification;
    }


    public function __construct(
        Progress $progress,
        ?Medium $completionMedium,
        ?DateTime $startedTimestamp,
        ?DateTime $payingTimestamp,
        ?DateTime $authenticatingTimestamp,
        ?DateTime $completedTimestamp,
        ?string $payingAgent,
        bool $locked,
        ?int $completionDuration,
        ?string $completionDurationDescription,
        ?DurationClassification $completionDurationClassification
    ) {
        $this->progress = $progress;
        $this->completionMedium = $completionMedium;
        $this->startedTimestamp = $startedTimestamp;
        $this->payingTimestamp = $payingTimestamp;
        $this->authenticatingTimestamp = $authenticatingTimestamp;
        $this->completedTimestamp = $completedTimestamp;
        $this->payingAgent = $payingAgent;
        $this->locked = $locked;
        $this->completionDuration = $completionDuration;
        $this->completionDurationDescription = $completionDurationDescription;
        $this->completionDurationClassification = $completionDurationClassification;
    }
    public function exists($property) {
        return isset($this->$property);
    }

    public function __toString() {
        return "Progress:" . $this->progress->value . "\n"
            . "CompletionMedium: " . ($this->completionMedium->value ?? 'NULL') . "\n"
            . "StartedTimestamp: " . (isset($this->startedTimestamp) ? $this->startedTimestamp->format('Y-m-d H:i:s') : 'NULL') . "\n"
            . "PayingTimestamp: " . (isset($this->payingTimestamp) ? $this->payingTimestamp->format('Y-m-d H:i:s') : 'NULL') . "\n"
            . "AuthenticatingTimestamp: " . (isset($this->authenticatingTimestamp) ? $this->authenticatingTimestamp->format('Y-m-d H:i:s') : 'NULL') . "\n"
            . "CompletedTimestamp: " . (isset($this->completedTimestamp) ? $this->completedTimestamp->format('Y-m-d H:i:s') : 'NULL') . "\n"
            . "Paying Agent: " . ($this->payingAgent ?? 'NULL') . "\n"
            . "Locked: " . ($this->locked ? 'True' : 'False') . "\n"
            . "CompletionDuration: " . ($this->completionDuration ?? 'NULL') . "\n"
            . "CompletionDurationDescription: " . ($this->completionDurationDescription ?? 'NULL') . "\n"
            . "CompletionDurationClassification: " . ($this->completionDurationClassification->value ?? 'NULL') . "\n";
    }
}