<?php

namespace Src\main\client\models\TransactionObjects;
use JsonSerializable;
use Src\main\client\models\enums\Medium;


class TransactionMedium implements JsonSerializable
{
    private Medium $medium;
    private string $device;

    public function __construct(
        Medium $medium,
        string $device
    ) {
        $this->medium = $medium;
        $this->device = $device;
    }


    public function getMedium(): Medium
    {
        return $this->medium;
    }
    public function getDevice(): string
    {
        return $this->device;
    }
    public function setMedium(Medium $medium): void
    {
        $this->medium = $medium;
    }
    public function setDevice(string $device): void
    {
        $this->device = $device;
    }


    public function jsonSerialize(): array
    {
        return [
          'medium' => $this->medium,
          'device' => $this->device
        ];
    }

    public function __toString() {
        return "medium: " . ($this->medium->value ?? 'NULL') . "\n" .
               "device: " . ($this->device ?? 'NULL') . "\n";
    }
}