<?php

namespace Src\main\client\models\TransactionObjects;

use JsonSerializable;

class TransactionWriteMessage implements JsonSerializable
{
    private bool $email;
    private bool $sms;

    public function __construct($email, $sms) {
        $this->email = $email;
        $this->sms = $sms;
    }

    public function jsonSerialize(): object {
        return (object) [
            'email' => $this->email,
            'sms' => $this->sms
        ];
    }
}