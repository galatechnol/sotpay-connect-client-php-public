<?php

namespace Src\main\client\models\TransactionObjects;
use Exception;
use JsonSerializable;

class TransactionDetails implements JsonSerializable
{
    private ?string $reference;
    private ?string $referenceSecondary;
    private ?string $description;
    private ?string $agent;
    private ?bool $attended;

    public function exists($property) {
        return isset($this->$property);
    }
    public function getReference(): string
    {
        return $this->reference;
    }
    public function getReferenceSecondary(): string
    {
        return $this->referenceSecondary;
    }
    public function getDescription(): string
    {
        return $this->description;
    }
    public function getAgent(): string
    {
        return $this->agent;
    }
    public function getAttended(): bool
    {
        return $this->attended;
    }
    public function setReference(string $reference): void
    {
        $this->reference = $reference;
    }
    public function setReferenceSecondary(string $referenceSecondary): void
    {
        $this->referenceSecondary = $referenceSecondary;
    }
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
    public function setAgent(string $agent): void
    {
        $this->agent = $agent;
    }
    public function setAttended(bool $attended): void
    {
        $this->attended = $attended;
    }

    public function __construct(?string $reference = null, ?string $referenceSecondary = null, ?string $description = null, ?string $agent = null, ?bool $attended = null)
    {
        $reference ? $this->reference = $reference : '';
        $referenceSecondary ? $this->referenceSecondary = $referenceSecondary : '';
        $description ? $this->description = $description : '';
        $agent ? $this->agent = $agent : '';
        $attended ? $this->attended = $attended : '';
    }

    public function jsonSerialize(): object
    {
        return (object) array_merge(
          isset($this->reference) ? array('reference' => $this->reference): array(),
            isset($this->referenceSecondary) ? array('referenceSecondary' => $this->referenceSecondary): array(),
            isset($this->description) ? array('description' => $this->description): array(),
            isset($this->agent) ? array('agent' => $this->agent): array(),
            isset($this->attended) ? array('attended' => $this->attended): array(),
        );
    }

    public function __toString() {
        return "Reference: " . ($this->reference ?? 'NULL') . "\n"
            . "Secondary Reference: " . ($this->referenceSecondary ?? 'NULL') . "\n"
            . "Description: " . ($this->description ?? 'NULL') . "\n"
            . "Agent: " . ($this->agent ?? 'NULL') . "\n"
            . "Attended: " . ($this->attended ? 'true' : 'false') . "\n";
    }
}