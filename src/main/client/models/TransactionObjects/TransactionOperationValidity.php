<?php

namespace Src\main\client\models\TransactionObjects;
use JsonSerializable;


class TransactionOperationValidity implements JsonSerializable
{
    private bool $isValidForCreateDuplicate;
    private bool $isValidForExpiry;
    private bool $isValidForResendPayMessage;

    public function __construct(
        bool $isValidForCreateDuplicate,
        bool $isValidForExpiry,
        bool $isValidForResendPayMessage,
    ) {
        $this->isValidForCreateDuplicate = $isValidForCreateDuplicate;
        $this->isValidForExpiry = $isValidForExpiry;
        $this->isValidForResendPayMessage = $isValidForResendPayMessage;
    }

    public function isValidForCreateDuplicate(): bool
    {
        return $this->isValidForCreateDuplicate;
    }
    public function isValidForExpiry(): bool
    {
        return $this->isValidForExpiry;
    }
    public function isValidForResendPayMessage(): bool
    {
        return $this->isValidForResendPayMessage;
    }
    public function setIsValidForCreateDuplicate(bool $isValidForCreateDuplicate): void
    {
        $this->isValidForCreateDuplicate = $isValidForCreateDuplicate;
    }
    public function setIsValidForExpiry(bool $isValidForExpiry): void
    {
        $this->isValidForExpiry = $isValidForExpiry;
    }
    public function setIsValidForResendPayMessage(bool $isValidForResendPayMessage): void
    {
        $this->isValidForResendPayMessage = $isValidForResendPayMessage;
    }


    public function jsonSerialize(): array
    {
        return [
          'isValidForCreateDuplicate' => $this->isValidForCreateDuplicate,
          'isValidForExpiry' => $this->isValidForExpiry,
          'isValidForResendPayMessage' => $this->isValidForResendPayMessage
        ];
    }

    public function __toString() {
        return "isValidForCreateDuplicate: " . ($this->isValidForCreateDuplicate ? "true" : "false") . "\n" .
               "isValidForExpiry: " . ($this->isValidForExpiry ? "true" : "false") . "\n" .
               "isValidForResendPayMessage: " . ($this->isValidForResendPayMessage ? "true" : "false") . "\n";
    }
}