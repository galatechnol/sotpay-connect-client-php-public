<?php

namespace Src\main\client\models;

use Src\main\client\models\bases\Base;

class BooleanResult extends Base
{
    private string $object;
    private boolean $result;

    /**
     * @param string $object
     */
    public function setObject(string $object): void
    {
        $this->object = $object;
    }

    /**
     * @param bool $result
     */
    public function setResult(bool $result): void
    {
        $this->result = $result;
    }

    /**
     * @return string
     */
    public function getObject(): string
    {
        return $this->object;
    }

    /**
     * @return bool
     */
    public function isResult(): bool
    {
        return $this->result;
    }

    public function __construct()
    {
    }

    public static function withBody(string $JSONBody): BooleanResult {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString(): string {
        return "[Object: " . ($this->object ?? 'NULL') . "]\n"
            . "Result: " . ($this->result ?? 'NULL');
    }
}