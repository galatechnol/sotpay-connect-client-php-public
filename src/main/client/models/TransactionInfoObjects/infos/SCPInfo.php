<?php
namespace Src\main\client\models\TransactionInfoObjects\infos;
use Src\main\client\models\TransactionInfo;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionInfoObjects\infos\enums\SCPState;
use Src\main\client\models\TransactionInfoObjects\infos\enums\SCPStatusCode;

class SCPInfo extends TransactionInfo {
    protected string $reference;
    protected string $referenceSecondary;
    protected SCPStatusCode $statusCode;
    protected SCPState $state;
    protected string $errorCode;
    protected string $errorText;
    protected CardType $cardType;
    protected string $maskedCardNumber;


    public function getReferenceSecondary(): string
    {
        return $this->referenceSecondary;
    }
    public function getState(): SCPState
    {
        return $this->state;
    }
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }
    public function getErrorText(): string
    {
        return $this->errorText;
    }

    public function setReference(string $reference): void {
        $this->reference = $reference;
    }

    public function setReferenceSecondary(string $referenceSecondary): void {
        $this->referenceSecondary = $referenceSecondary;
    }

    public function setStatusCode(SCPStatusCode $statusCode): void {
        $this->statusCode = $statusCode;
    }

    public function setState(SCPState $state): void {
        $this->state = $state;
    }

    public function setErrorCode(string $errorCode): void {
        $this->errorCode = $errorCode;
    }

    public function setErrorText(string $errorText): void {
        $this->errorText = $errorText;
    }

    public function setCardType(CardType $cardType): void {
        $this->cardType = $cardType;
    }

    public function setMaskedCardNumber(string $maskedCardNumber): void {
        $this->maskedCardNumber = $maskedCardNumber;
    }
    public static function withBody($JSONBody): SCPInfo
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString() {
        return
            parent::__toString() .
            "Reference: " . ($this->reference ?? "NULL") . "\n" .
            "Secondary Reference: " . ($this->referenceSecondary ?? "NULL") . "\n" .
            "Status Code: " . ($this->statusCode->value ?? "NULL") . "\n" .
            "State: " . ($this->state->value ?? "NULL") . "\n" .
            "Error Code: " . ($this->errorCode ?? "NULL") . "\n" .
            "Error Text: " . ($this->errorText ?? "NULL") . "\n" .
            "Card Type: " . ($this->cardType->value ?? "NULL") . "\n" .
            "Masked Card Number: " . ($this->maskedCardNumber ?? "NULL") . "\n";
    }

}