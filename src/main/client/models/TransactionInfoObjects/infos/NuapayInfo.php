<?php
namespace Src\main\client\models\TransactionInfoObjects\infos;
use Src\main\client\models\TransactionInfo;
use Src\main\client\models\TransactionInfoObjects\infos\enums\Bank;
use Src\main\client\models\TransactionInfoObjects\infos\enums\NuapayStatusCode;

class NuapayInfo extends TransactionInfo {
    protected string $reference;
    protected string $referenceSecondary;
    protected NuapayStatusCode $statusCode;
    protected string $errorCode;
    protected string $errorText;
    protected Bank $bank;
    protected string $bankAccount;


    public function getReferenceSecondary(): string
    {
        return $this->referenceSecondary;
    }
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }
    public function getErrorText(): string
    {
        return $this->errorText;
    }
    public function getBank(): Bank
    {
        return $this->bank;
    }
    public function getBankAccount(): string
    {
        return $this->bankAccount;
    }
    protected function setReference(string $reference): void {
        $this->reference = $reference;
    }

    public function setReferenceSecondary(string $referenceSecondary): void {
        $this->referenceSecondary = $referenceSecondary;
    }

    public function setStatusCode(NuapayStatusCode $statusCode): void {
        $this->statusCode = $statusCode;
    }

    public function setErrorCode(string $errorCode): void {
        $this->errorCode = $errorCode;
    }

    public function setErrorText(string $errorText): void {
        $this->errorText = $errorText;
    }

    public function setBank(Bank $bank): void {
        $this->bank = $bank;
    }

    public function setBankAccount(string $bankAccount): void {
        $this->bankAccount = $bankAccount;
    }


    public static function withBody($JSONBody): NuapayInfo
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString() {
        return
            parent::__toString() .
            "Reference: " . ($this->reference ?? "NULL") . "\n" .
            "Secondary Reference: " . ($this->referenceSecondary ?? "NULL") . "\n" .
            "Status Code: " . ($this->statusCode->value ?? "NULL") . "\n" .
            "Error Code: " . ($this->errorCode ?? "NULL") . "\n" .
            "Error Text: " . ($this->errorText ?? "NULL") . "\n" .
            "Bank: " . ($this->bank->value ?? "NULL") . "\n" .
            "Bank Account: " . ($this->bankAccount ?? "NULL") . "\n";
    }
}