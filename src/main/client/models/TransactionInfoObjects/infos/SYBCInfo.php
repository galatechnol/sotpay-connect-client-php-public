<?php
namespace Src\main\client\models\TransactionInfoObjects\infos;
use Src\main\client\models\TransactionInfo;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionInfoObjects\infos\enums\ResultClassification;
use Src\main\client\models\TransactionInfoObjects\infos\enums\RiskClassification;
use Src\main\client\models\TransactionInfoObjects\infos\enums\SYBCAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\SYBCD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\SYBCSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\SYBCStatusCode;

class SYBCInfo extends TransactionInfo {
    protected string $reference;
    protected SYBCStatusCode $statusCode;
    protected string $statusText;
    protected SYBCSecurityCodeResult $securityCodeResult;
    protected SYBCD3sResult $d3sResult;
    protected SYBCAddressResult $addressResult;
    protected float $riskScore;
    protected CardType $cardType;
    protected string $maskedCardNumber;
    protected RiskClassification $riskScoreClassification;
    protected ResultClassification $securityCodeResultClassification;
    protected ResultClassification $d3sResultClassification;
    protected ResultClassification $addressResultClassification;
    public function getStatusText(): string
    {
        return $this->statusText;
    }
    public function getSecurityCodeResult(): SYBCSecurityCodeResult
    {
        return $this->securityCodeResult;
    }
    public function getD3sResult(): SYBCD3sResult
    {
        return $this->d3sResult;
    }
    public function getAddressResult(): SYBCAddressResult
    {
        return $this->addressResult;
    }
    public function getRiskScore(): float
    {
        return $this->riskScore;
    }
    public function getRiskScoreClassification(): RiskClassification
    {
        return $this->riskScoreClassification;
    }
    public function getSecurityCodeResultClassification(): ResultClassification
    {
        return $this->securityCodeResultClassification;
    }
    public function getD3sResultClassification(): ResultClassification
    {
        return $this->d3sResultClassification;
    }

    public function getAddressResultClassification(): ResultClassification
    {
        return $this->addressResultClassification;
    }


    //SETTERS for testing purposes
    public function setReference(string $reference) {
        $this->reference = $reference;
    }

    public function setStatusCode(SYBCStatusCode $statusCode) {
        $this->statusCode = $statusCode;
    }

    public function setStatusText(string $statusText) {
        $this->statusText = $statusText;
    }

    public function setSecurityCodeResult(SYBCSecurityCodeResult $securityCodeResult) {
        $this->securityCodeResult = $securityCodeResult;
    }

    public function setD3sResult(SYBCD3sResult $d3sResult) {
        $this->d3sResult = $d3sResult;
    }

    public function setAddressResult(SYBCAddressResult $addressResult) {
        $this->addressResult = $addressResult;
    }

    public function setRiskScore(float $riskScore) {
        $this->riskScore = $riskScore;
    }

    public function setCardType(CardType $cardType) {
        $this->cardType = $cardType;
    }

    public function setMaskedCardNumber(string $maskedCardNumber) {
        $this->maskedCardNumber = $maskedCardNumber;
    }

    public function setRiskScoreClassification(RiskClassification $riskScoreClassification) {
        $this->riskScoreClassification = $riskScoreClassification;
    }

    public function setSecurityCodeResultClassification(ResultClassification $securityCodeResultClassification) {
        $this->securityCodeResultClassification = $securityCodeResultClassification;
    }

    public function setD3sResultClassification(ResultClassification $d3sResultClassification) {
        $this->d3sResultClassification = $d3sResultClassification;
    }

    public function setAddressResultClassification(ResultClassification $addressResultClassification) {
        $this->addressResultClassification = $addressResultClassification;
    }
    public static function withBody(string $JSONBody): SYBCInfo
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString() {
        return
            parent::__toString() .
            "Reference: " . ($this->reference ?? "NULL") . "\n" .
            "Status Code: " . ($this->statusCode->value ?? "NULL") . "\n" .
            "Status Text: " . ($this->statusText ?? "NULL") . "\n" .
            "Security Code Result: " . ($this->securityCodeResult->value ?? "NULL") . "\n" .
            "D3S Result: " . ($this->d3sResult->value ?? "NULL") . "\n" .
            "Address Result: " . ($this->addressResult->value ?? "NULL") . "\n" .
            "Risk Score: " . ($this->riskScore ?? "NULL") . "\n" .
            "Card Type: " . ($this->cardType->value ?? "NULL") . "\n" .
            "Masked Card Number: " . ($this->maskedCardNumber ?? "NULL") . "\n" .
            "Risk Score Classification: " . ($this->riskScoreClassification->value ?? "NULL") . "\n" .
            "Security Code Result Classification: " . ($this->securityCodeResultClassification->value ?? "NULL") . "\n" .
            "D3S Result Classification: " . ($this->d3sResultClassification->value ?? "NULL") . "\n" .
            "Address Result Classification: " . ($this->addressResultClassification->value ?? "NULL") . "\n";
    }
}