<?php
namespace Src\main\client\models\TransactionInfoObjects\infos;
use JsonMapper;
use PHPUnit\Logging\Exception;
use Src\main\client\models\TransactionInfo;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardstreamAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardstreamD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardstreamPostcodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardstreamSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardstreamState;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardstreamStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionInfoObjects\infos\enums\ResultClassification;

class CardstreamInfo extends TransactionInfo {
    protected string $reference;
    protected string $referenceSecondary;
    protected CardstreamStatusCode $statusCode;
    protected string $statusText;
    protected CardstreamState $state;
    protected CardstreamSecurityCodeResult $securityCodeResult;
    protected CardstreamD3sResult $d3sResult;
    protected string $d3sReference;
    protected CardstreamAddressResult $addressResult;
    protected CardstreamPostcodeResult $postcodeResult;
    protected CardType $cardType;
    protected string $maskedCardNumber;
    protected bool $applePay;
    protected bool $googlePay;
    protected ResultClassification $securityCodeResultClassification;
    protected ResultClassification $d3sResultClassification;
    protected ResultClassification $addressResultClassification;
    protected ResultClassification $postcodeResultClassification;

    public function getReferenceSecondary(): string
    {
        return $this->referenceSecondary;
    }
    public function getStatusText(): string
    {
        return $this->statusText;
    }
    public function getState(): CardstreamState
    {
        return $this->state;
    }
    public function getSecurityCodeResult(): CardstreamSecurityCodeResult
    {
        return $this->securityCodeResult;
    }
    public function getD3sResult(): CardstreamD3sResult
    {
        return $this->d3sResult;
    }
    public function getAddressResult(): CardstreamAddressResult
    {
        return $this->addressResult;
    }
    public function getPostcodeResult(): CardstreamPostcodeResult
    {
        return $this->postcodeResult;
    }
    public function getD3sReference(): string
    {
        return $this->d3sReference;
    }
    public function isApplePay(): bool
    {
        return $this->applePay;
    }
    public function isGooglePay(): bool
    {
        return $this->googlePay;
    }
    public function getSecurityCodeResultClassification(): ResultClassification
    {
        return $this->securityCodeResultClassification;
    }
    public function getD3sResultClassification(): ResultClassification
    {
        return $this->d3sResultClassification;
    }
    public function getAddressResultClassification(): ResultClassification
    {
        return $this->addressResultClassification;
    }
    public function getPostcodeResultClassification(): ResultClassification
    {
        return $this->postcodeResultClassification;
    }

    public function setReference(string $reference): void {
        $this->reference = $reference;
    }

    public function setReferenceSecondary(string $referenceSecondary): void {
        $this->referenceSecondary = $referenceSecondary;
    }

    public function setStatusCode(CardstreamStatusCode $statusCode): void {
        $this->statusCode = $statusCode;
    }

    public function setStatusText(string $statusText): void {
        $this->statusText = $statusText;
    }

    public function setState(CardstreamState $state): void {
        $this->state = $state;
    }

    public function setSecurityCodeResult(CardstreamSecurityCodeResult $securityCodeResult): void {
        $this->securityCodeResult = $securityCodeResult;
    }

    public function setD3sResult(CardstreamD3sResult $d3sResult): void {
        $this->d3sResult = $d3sResult;
    }

    public function setD3sReference(string $d3sReference): void {
        $this->d3sReference = $d3sReference;
    }

    public function setAddressResult(CardstreamAddressResult $addressResult): void {
        $this->addressResult = $addressResult;
    }

    public function setPostcodeResult(CardstreamPostcodeResult $postcodeResult): void {
        $this->postcodeResult = $postcodeResult;
    }

    public function setCardType(CardType $cardType): void {
        $this->cardType = $cardType;
    }

    public function setMaskedCardNumber(string $maskedCardNumber): void {
        $this->maskedCardNumber = $maskedCardNumber;
    }

    public function setApplePay(bool $applePay): void {
        $this->applePay = $applePay;
    }

    public function setGooglePay(bool $googlePay): void {
        $this->googlePay = $googlePay;
    }

    public function setSecurityCodeResultClassification(ResultClassification $securityCodeResultClassification): void {
        $this->securityCodeResultClassification = $securityCodeResultClassification;
    }

    public function setD3sResultClassification(ResultClassification $d3sResultClassification): void {
        $this->d3sResultClassification = $d3sResultClassification;
    }

    public function setAddressResultClassification(ResultClassification $addressResultClassification): void {
        $this->addressResultClassification = $addressResultClassification;
    }

    public function setPostcodeResultClassification(ResultClassification $postcodeResultClassification): void {
        $this->postcodeResultClassification = $postcodeResultClassification;
    }
    public static function withBody($JSONBody): CardstreamInfo
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    protected function set(string $body)
    {
        $data = json_decode($body);
        //Check for valid JSON
        if ($data) {
            if ($data->addressResult) {
                $data->addressResult = 'R' . $data->addressResult;
            }
            if ($data->postcodeResult) {
                $data->postcodeResult = 'R' . $data->postcodeResult;
            }
            if ($data->securityCodeResult) {
                $data->securityCodeResult = 'R' . $data->securityCodeResult;
            }
            if ($data->statusCode) {
                $data->statusCode = 'R' . $data->statusCode;
            }

            $mapper = new JsonMapper();
            $mapper->bIgnoreVisibility = true; //allow protected property access
            $mapper->bExceptionOnUndefinedProperty = true; //if we get unexpected properties
            $obj = $mapper->map($data, $this);
            return $obj;

        } else { // sending non-JSON data
            throw new Exception('Wrong data type sent to instance of Cardstreaminfo, expected JSON body');
        }
    }

    public function __toString() {
        return
            parent::__toString() .
            "Reference: " . ($this->reference ?? 'NULL') . "\n" .
            "Secondary Reference: " . ($this->referenceSecondary ?? 'NULL') . "\n" .
            "Status Code: " . (str_replace("R", '', $this->statusCode->value)  ?? 'NULL') . "\n" .
            "Status Text: " . ($this->statusText ?? 'NULL') . "\n" .
            "State: " . ($this->state->value ?? 'NULL') . "\n" .
            "Security Code Result: " . (str_replace("R", '', $this->securityCodeResult->value)  ?? 'NULL') . "\n" .
            "D3S Result: " . ($this->d3sResult->value ?? 'NULL') . "\n" .
            "D3S Reference: " . ($this->d3sReference ?? 'NULL') . "\n" .
            "Address Result: " . (str_replace("R", '', $this->addressResult->value)  ?? 'NULL') . "\n" .
            "Postcode Result: " . (str_replace("R", '', $this->postcodeResult->value)  ?? 'NULL') . "\n" .
            "Card Type: " . ($this->cardType->value ?? 'NULL') . "\n" .
            "Masked Card Number: " . ($this->maskedCardNumber ?? 'NULL') . "\n" .
            "Apple Pay: " . ($this->applePay ? 'Yes' : 'No') . "\n" .
            "Google Pay: " . ($this->googlePay ? 'Yes' : 'No') . "\n" .
            "Security Code Result Classification: " . ($this->securityCodeResultClassification->value ?? 'NULL') . "\n" .
            "D3S Result Classification: " . ($this->d3sResultClassification->value ?? 'NULL') . "\n" .
            "Address Result Classification: " . ($this->addressResultClassification->value ?? 'NULL') . "\n" .
            "Postcode Result Classification: " . ($this->postcodeResultClassification->value ?? 'NULL') . "\n";
    }
}