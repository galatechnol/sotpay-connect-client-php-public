<?php
namespace Src\main\client\models\TransactionInfoObjects\infos;
use Src\main\client\models\TransactionInfo;
use Src\main\client\models\TransactionInfoObjects\infos\enums\AcquiredAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\AcquiredD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\AcquiredPostcodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\AcquiredSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\AcquiredStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionInfoObjects\infos\enums\ResultClassification;

class AcquiredInfo extends TransactionInfo {
    protected string $reference;
    protected AcquiredStatusCode $statusCode;
    protected string $statusText;
    protected AcquiredSecurityCodeResult $securityCodeResult;
    protected AcquiredD3sResult $d3sResult;
    protected AcquiredAddressResult $addressResult;
    protected AcquiredPostcodeResult $postcodeResult;
    protected CardType $cardType;
    protected string $maskedCardNumber;
    protected bool $applePay;
    protected bool $googlePay;
    protected ResultClassification $securityCodeResultClassification;
    protected ResultClassification $d3sResultClassification;
    protected ResultClassification $addressResultClassification;
    protected ResultClassification $postcodeResultClassification;



    public function getStatusText(): string
    {
        return $this->statusText;
    }

    public function getSecurityCodeResult(): AcquiredSecurityCodeResult
    {
        return $this->securityCodeResult;
    }

    public function getD3sResult(): AcquiredD3sResult
    {
        return $this->d3sResult;
    }

    public function getAddressResult(): AcquiredAddressResult
    {
        return $this->addressResult;
    }

    public function getPostcodeResult(): AcquiredPostcodeResult
    {
        return $this->postcodeResult;
    }

    public function isApplePay(): bool
    {
        return $this->applePay;
    }

    public function isGooglePay(): bool
    {
        return $this->googlePay;
    }

    public function getSecurityCodeResultClassification(): ResultClassification
    {
        return $this->securityCodeResultClassification;
    }

    public function getD3sResultClassification(): ResultClassification
    {
        return $this->d3sResultClassification;
    }

    public function getAddressResultClassification(): ResultClassification
    {
        return $this->addressResultClassification;
    }

    public function getPostcodeResultClassification(): ResultClassification
    {
        return $this->postcodeResultClassification;
    }


    public function setReference(string $reference): void {
        $this->reference = $reference;
    }

    public function setStatusCode(AcquiredStatusCode $statusCode): void {
        $this->statusCode = $statusCode;
    }

    public function setStatusText(string $statusText): void {
        $this->statusText = $statusText;
    }

    public function setSecurityCodeResult(AcquiredSecurityCodeResult $securityCodeResult): void {
        $this->securityCodeResult = $securityCodeResult;
    }

    public function setD3sResult(AcquiredD3sResult $d3sResult): void {
        $this->d3sResult = $d3sResult;
    }

    public function setAddressResult(AcquiredAddressResult $addressResult): void {
        $this->addressResult = $addressResult;
    }

    public function setPostcodeResult(AcquiredPostcodeResult $postcodeResult): void {
        $this->postcodeResult = $postcodeResult;
    }

    public function setCardType(CardType $cardType): void {
        $this->cardType = $cardType;
    }

    public function setMaskedCardNumber(string $maskedCardNumber): void {
        $this->maskedCardNumber = $maskedCardNumber;
    }

    public function setApplePay(bool $applePay): void {
        $this->applePay = $applePay;
    }

    public function setGooglePay(bool $googlePay): void {
        $this->googlePay = $googlePay;
    }

    public function setSecurityCodeResultClassification(ResultClassification $securityCodeResultClassification): void {
        $this->securityCodeResultClassification = $securityCodeResultClassification;
    }

    public function setD3sResultClassification(ResultClassification $d3sResultClassification): void {
        $this->d3sResultClassification = $d3sResultClassification;
    }

    public function setAddressResultClassification(ResultClassification $addressResultClassification): void {
        $this->addressResultClassification = $addressResultClassification;
    }

    public function setPostcodeResultClassification(ResultClassification $postcodeResultClassification): void {
        $this->postcodeResultClassification = $postcodeResultClassification;
    }
    public static function withBody($JSONBody): AcquiredInfo
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }
    public function __toString() {
        return
            parent::__toString() .
            "Reference: " . ($this->reference ?? "NULL") . "\n" .
            "Status Code: " . ($this->statusCode->value ?? "NULL") . "\n" .
            "Status Text: " . ($this->statusText ?? "NULL") . "\n" .
            "Security Code Result: " . ($this->securityCodeResult->value ?? "NULL") . "\n" .
            "D3S Result: " . ($this->d3sResult->value ?? "NULL") . "\n" .
            "Address Result: " . ($this->addressResult->value ?? "NULL") . "\n" .
            "Postcode Result: " . ($this->postcodeResult->value ?? "NULL") . "\n" .
            "Card Type: " . ($this->cardType->value ?? "NULL") . "\n" .
            "Masked Card Number: " . ($this->maskedCardNumber ?? "NULL") . "\n" .
            "Apple Pay: " . ($this->applePay ? "true" : "false") . "\n" .
            "Google Pay: " . ($this->googlePay ? "true" : "false") . "\n" .
            "Security Code Result Classification: " . ($this->securityCodeResultClassification->value ?? "NULL") . "\n" .
            "D3S Result Classification: " . ($this->d3sResultClassification->value ?? "NULL") . "\n" .
            "Address Result Classification: " . ($this->addressResultClassification->value ?? "NULL") . "\n" .
            "Postcode Result Classification: " . ($this->postcodeResultClassification->value ?? "NULL") . "\n";
    }
}