<?php
namespace Src\main\client\models\TransactionInfoObjects\infos;
use JsonMapper;
use PHPUnit\Logging\Exception;
use Src\main\client\models\TransactionInfo;
use Src\main\client\models\TransactionInfoObjects\infos\enums\BarclaycardAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\BarclaycardD3SResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\BarclaycardSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\BarclaycardStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionInfoObjects\infos\enums\ResultClassification;

class BarclaycardInfo extends TransactionInfo {
    protected string $reference;
    protected BarclaycardStatusCode $statusCode;
    protected string $statusText;
    protected string $errorCode;
    protected string $errorText;
    protected BarclaycardSecurityCodeResult $securityCodeResult;
    protected BarclaycardD3SResult $d3sResult;
    protected BarclaycardAddressResult $addressResult;
    protected CardType $cardType;
    protected string $maskedCardNumber;
    protected ResultClassification $securityCodeResultClassification;
    protected ResultClassification $d3sResultClassification;
    protected ResultClassification $addressResultClassification;


    public function getStatusText(): string
    {
        return $this->statusText;
    }

    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    public function getErrorText(): string
    {
        return $this->errorText;
    }

    public function getSecurityCodeResult(): BarclaycardSecurityCodeResult
    {
        return $this->securityCodeResult;
    }

    public function getD3sResult(): BarclaycardD3SResult
    {
        return $this->d3sResult;
    }

    public function getAddressResult(): BarclaycardAddressResult
    {
        return $this->addressResult;
    }

    public function getSecurityCodeResultClassification(): ResultClassification
    {
        return $this->securityCodeResultClassification;
    }

    public function getD3sResultClassification(): ResultClassification
    {
        return $this->d3sResultClassification;
    }

    public function getAddressResultClassification(): ResultClassification
    {
        return $this->addressResultClassification;
    }

    public function setReference(string $reference): void {
        $this->reference = $reference;
    }

    public function setStatusCode(BarclaycardStatusCode $statusCode): void {
        $this->statusCode = $statusCode;
    }

    public function setStatusText(string $statusText): void {
        $this->statusText = $statusText;
    }

    public function setErrorCode(string $errorCode): void {
        $this->errorCode = $errorCode;
    }

    public function setErrorText(string $errorText): void {
        $this->errorText = $errorText;
    }

    public function setSecurityCodeResult(BarclaycardSecurityCodeResult $securityCodeResult): void {
        $this->securityCodeResult = $securityCodeResult;
    }

    public function setD3sResult(BarclaycardD3SResult $d3sResult): void {
        $this->d3sResult = $d3sResult;
    }

    public function setAddressResult(BarclaycardAddressResult $addressResult): void {
        $this->addressResult = $addressResult;
    }

    public function setCardType(CardType $cardType): void {
        $this->cardType = $cardType;
    }

    public function setMaskedCardNumber(string $maskedCardNumber): void {
        $this->maskedCardNumber = $maskedCardNumber;
    }

    public function setSecurityCodeResultClassification(ResultClassification $securityCodeResultClassification): void {
        $this->securityCodeResultClassification = $securityCodeResultClassification;
    }

    public function setD3sResultClassification(ResultClassification $d3sResultClassification): void {
        $this->d3sResultClassification = $d3sResultClassification;
    }

    public function setAddressResultClassification(ResultClassification $addressResultClassification): void {
        $this->addressResultClassification = $addressResultClassification;
    }

    public static function withBody($JSONBody): BarclaycardInfo
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    protected function set(string $body)
    {

        $data = json_decode($body);
        //Check for valid JSON
        if ($data) {
            //For enum mapping
            if ($data->d3sResult) {
                $data->d3sResult = 'R' . $data->d3sResult;
            }
            if ($data->statusCode) {
                $data->statusCode = 'R' . $data->statusCode;
            }
            $mapper = new JsonMapper();
            $mapper->bIgnoreVisibility = true; //allow protected property access
            $mapper->bExceptionOnUndefinedProperty = true; //if we get unexpected properties
            $obj = $mapper->map($data, $this);
            return $obj;

        } else { // sending non-JSON data
            throw new Exception('Wrong data type sent to instance of BarclaycardInfo, expected JSON body');
        }
    }
    public function __toString() {
        return
            parent::__toString() .
            "Reference: " . ($this->reference ?? "NULL") . "\n" .
            "Status Code: " . (str_replace("R", '', $this->statusCode->value)  ?? 'NULL') . "\n" .
            "Status Text: " . ($this->statusText ?? "NULL") . "\n" .
            "Error Code: " . ($this->errorCode ?? "NULL") . "\n" .
            "Error Text: " . ($this->errorText ?? "NULL") . "\n" .
            "Security Code Result: " . ($this->securityCodeResult->value ?? "NULL") . "\n" .
            "D3S Result: " . (str_replace("R", '', $this->d3sResult->value)  ?? 'NULL') . "\n" .
            "Address Result: " . ($this->addressResult->value ?? "NULL") . "\n" .
            "Card Type: " . ($this->cardType->value ?? "NULL") . "\n" .
            "Masked Card Number: " . ($this->maskedCardNumber ?? "NULL") . "\n" .
            "Security Code Result Classification: " . ($this->securityCodeResultClassification->value ?? "NULL") . "\n" .
            "D3S Result Classification: " . ($this->d3sResultClassification->value ?? "NULL") . "\n" .
            "Address Result Classification: " . ($this->addressResultClassification->value ?? "NULL") . "\n";
    }

}