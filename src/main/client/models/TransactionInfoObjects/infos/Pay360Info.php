<?php
namespace Src\main\client\models\TransactionInfoObjects\infos;
use Src\main\client\models\TransactionInfo;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionInfoObjects\infos\enums\Pay360AddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\Pay360D3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\Pay360PostcodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\Pay360SecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\Pay360StatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\ResultClassification;
use Src\main\client\models\TransactionInfoObjects\infos\enums\RiskClassification;

class Pay360Info extends TransactionInfo {
    protected string $reference;
    protected Pay360StatusCode $statusCode;
    protected string $statusText;
    protected Pay360SecurityCodeResult $securityCodeResult;
    protected Pay360D3sResult $d3sResult;
    protected Pay360AddressResult $addressResult;
    protected Pay360PostcodeResult $postcodeResult;
    protected float $riskScore;
    protected CardType $cardType;
    protected string $maskedCardNumber;
    protected RiskClassification $riskScoreClassification;
    protected ResultClassification $securityCodeResultClassification;
    protected ResultClassification $d3sResultClassification;
    protected ResultClassification $addressResultClassification;
    protected ResultClassification $postcodeResultClassification;


    public function getStatusText(): string
    {
        return $this->statusText;
    }
    public function getSecurityCodeResult(): Pay360SecurityCodeResult
    {
        return $this->securityCodeResult;
    }
    public function getD3sResult(): Pay360D3sResult
    {
        return $this->d3sResult;
    }
    public function getAddressResult(): Pay360AddressResult
    {
        return $this->addressResult;
    }
    public function getPostcodeResult(): Pay360PostcodeResult
    {
        return $this->postcodeResult;
    }
    public function getRiskScore(): float
    {
        return $this->riskScore;
    }
    public function getRiskScoreClassification(): RiskClassification
    {
        return $this->riskScoreClassification;
    }
    public function getSecurityCodeResultClassification(): ResultClassification
    {
        return $this->securityCodeResultClassification;
    }
    public function getD3sResultClassification(): ResultClassification
    {
        return $this->d3sResultClassification;
    }
    public function getAddressResultClassification(): ResultClassification
    {
        return $this->addressResultClassification;
    }
    public function getPostcodeResultClassification(): ResultClassification
    {
        return $this->postcodeResultClassification;
    }

    public function setReference(string $reference): void {
        $this->reference = $reference;
    }

    public function setStatusCode(Pay360StatusCode $statusCode): void {
        $this->statusCode = $statusCode;
    }

    public function setStatusText(string $statusText): void {
        $this->statusText = $statusText;
    }

    public function setSecurityCodeResult(Pay360SecurityCodeResult $securityCodeResult): void {
        $this->securityCodeResult = $securityCodeResult;
    }

    public function setD3sResult(Pay360D3sResult $d3sResult): void {
        $this->d3sResult = $d3sResult;
    }

    public function setAddressResult(Pay360AddressResult $addressResult): void {
        $this->addressResult = $addressResult;
    }

    public function setPostcodeResult(Pay360PostcodeResult $postcodeResult): void {
        $this->postcodeResult = $postcodeResult;
    }

    public function setRiskScore(float $riskScore): void {
        $this->riskScore = $riskScore;
    }

    public function setCardType(CardType $cardType): void {
        $this->cardType = $cardType;
    }

    public function setMaskedCardNumber(string $maskedCardNumber): void {
        $this->maskedCardNumber = $maskedCardNumber;
    }

    public function setRiskScoreClassification(RiskClassification $riskScoreClassification): void {
        $this->riskScoreClassification = $riskScoreClassification;
    }

    public function setSecurityCodeResultClassification(ResultClassification $securityCodeResultClassification): void {
        $this->securityCodeResultClassification = $securityCodeResultClassification;
    }

    public function setD3sResultClassification(ResultClassification $d3sResultClassification): void {
        $this->d3sResultClassification = $d3sResultClassification;
    }

    public function setAddressResultClassification(ResultClassification $addressResultClassification): void {
        $this->addressResultClassification = $addressResultClassification;
    }

    public function setPostcodeResultClassification(ResultClassification $postcodeResultClassification): void {
        $this->postcodeResultClassification = $postcodeResultClassification;
    }
    public static function withBody($JSONBody): Pay360Info
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString() {
        return
            parent::__toString() .
            "Reference: " . ($this->reference ?? "NULL") . "\n" .
            "Status Code: " . ($this->statusCode->value ?? "NULL") . "\n" .
            "Status Text: " . ($this->statusText ?? "NULL") . "\n" .
            "Security Code Result: " . ($this->securityCodeResult->value ?? "NULL") . "\n" .
            "D3S Result: " . ($this->d3sResult->value ?? "NULL") . "\n" .
            "Address Result: " . ($this->addressResult->value ?? "NULL") . "\n" .
            "Postcode Result: " . ($this->postcodeResult->value ?? "NULL") . "\n" .
            "Risk Score: " . ($this->riskScore ?? "NULL") . "\n" .
            "Card Type: " . ($this->cardType->value ?? "NULL") . "\n" .
            "Masked Card Number: " . ($this->maskedCardNumber ?? "NULL") . "\n" .
            "Risk Score Classification: " . ($this->riskScoreClassification->value ?? "NULL") . "\n" .
            "Security Code Result Classification: " . ($this->securityCodeResultClassification->value ?? "NULL") . "\n" .
            "D3S Result Classification: " . ($this->d3sResultClassification->value ?? "NULL") . "\n" .
            "Address Result Classification: " . ($this->addressResultClassification->value ?? "NULL") . "\n" .
            "Postcode Result Classification: " . ($this->postcodeResultClassification->value ?? "NULL") . "\n";
    }
}