<?php
namespace Src\main\client\models\TransactionInfoObjects\infos;
use JsonMapper;
use PHPUnit\Logging\Exception;
use Src\main\client\models\TransactionInfo;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionInfoObjects\infos\enums\FreedomPayAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\FreedomPayD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\FreedomPaySecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\FreedomPayState;
use Src\main\client\models\TransactionInfoObjects\infos\enums\FreedomPayStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\ResultClassification;

class FreedomPayInfo extends TransactionInfo {
    protected string $reference;
    protected FreedomPayStatusCode $statusCode;
    protected string $statusText;
    protected FreedomPayState $state;
    protected string $errorText;
    protected FreedomPaySecurityCodeResult $securityCodeResult;
    protected FreedomPayD3sResult $d3sResult;
    protected FreedomPayAddressResult $addressResult;
    protected CardType $cardType;
    protected string $maskedCardNumber;
    protected ResultClassification $securityCodeResultClassification;
    protected ResultClassification $d3sResultClassification;
    protected ResultClassification $addressResultClassification;


    public function getStatusText(): string
    {
        return $this->statusText;
    }

    public function getState(): FreedomPayState
    {
        return $this->state;
    }

    public function getErrorText(): string
    {
        return $this->errorText;
    }

    public function getSecurityCodeResult(): FreedomPaySecurityCodeResult
    {
        return $this->securityCodeResult;
    }

    public function getD3sResult(): FreedomPayD3sResult
    {
        return $this->d3sResult;
    }

    public function getAddressResult(): FreedomPayAddressResult
    {
        return $this->addressResult;
    }

    public function getSecurityCodeResultClassification(): ResultClassification
    {
        return $this->securityCodeResultClassification;
    }

    public function getD3sResultClassification(): ResultClassification
    {
        return $this->d3sResultClassification;
    }

    public function getAddressResultClassification(): ResultClassification
    {
        return $this->addressResultClassification;
    }

    public function setReference(string $reference): void {
        $this->reference = $reference;
    }

    public function setStatusCode(FreedomPayStatusCode $statusCode): void {
        $this->statusCode = $statusCode;
    }

    public function setStatusText(string $statusText): void {
        $this->statusText = $statusText;
    }

    public function setState(FreedomPayState $state): void {
        $this->state = $state;
    }

    public function setErrorText(string $errorText): void {
        $this->errorText = $errorText;
    }

    public function setSecurityCodeResult(FreedomPaySecurityCodeResult $securityCodeResult): void {
        $this->securityCodeResult = $securityCodeResult;
    }

    public function setD3sResult(FreedomPayD3sResult $d3sResult): void {
        $this->d3sResult = $d3sResult;
    }

    public function setAddressResult(FreedomPayAddressResult $addressResult): void {
        $this->addressResult = $addressResult;
    }

    public function setCardType(CardType $cardType): void {
        $this->cardType = $cardType;
    }

    public function setMaskedCardNumber(string $maskedCardNumber): void {
        $this->maskedCardNumber = $maskedCardNumber;
    }

    public function setSecurityCodeResultClassification(ResultClassification $securityCodeResultClassification): void {
        $this->securityCodeResultClassification = $securityCodeResultClassification;
    }

    public function setD3sResultClassification(ResultClassification $d3sResultClassification): void {
        $this->d3sResultClassification = $d3sResultClassification;
    }

    public function setAddressResultClassification(ResultClassification $addressResultClassification): void {
        $this->addressResultClassification = $addressResultClassification;
    }
    public static function withBody($JSONBody): FreedomPayInfo
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    protected function set(string $body)
    {

        $data = json_decode($body);
        //Check for valid JSON
        if ($data) {
            if ($data->securityCodeResult) {
                $data->securityCodeResult = 'R' . $data->securityCodeResult;
            }
            if ($data->statusCode) {
                $data->statusCode = 'R' . $data->statusCode;
            }

            $mapper = new JsonMapper();
            $mapper->bIgnoreVisibility = true; //allow private property access
            $mapper->bExceptionOnUndefinedProperty = true; //if we get unexpected properties
            $obj = $mapper->map($data, $this);
            return $obj;

        } else { // sending non-JSON data
            throw new Exception('Wrong data type sent to instance of FreedomPayInfo, expected JSON body');
        }
    }

    public function __toString() {
        return
            parent::__toString() .
            "Reference: " . ($this->reference ?? "NULL") . "\n" .
            "Status Code: " . (str_replace("R", '', $this->statusCode->value)  ?? 'NULL') . "\n" .
            "Status Text: " . ($this->statusText ?? "NULL") . "\n" .
            "State: " . ($this->state->value ?? "NULL") . "\n" .
            "Error Text: " . ($this->errorText ?? "NULL") . "\n" .
            "Security Code Result: " . (str_replace("R", '', $this->securityCodeResult->value)  ?? 'NULL') . "\n" .
            "D3S Result: " . ($this->d3sResult->value ?? "NULL") . "\n" .
            "Address Result: " . ($this->addressResult->value ?? "NULL") . "\n" .
            "Card Type: " . ($this->cardType->value ?? "NULL") . "\n" .
            "Masked Card Number: " . ($this->maskedCardNumber ?? "NULL") . "\n" .
            "Security Code Result Classification: " . ($this->securityCodeResultClassification->value ?? "NULL") . "\n" .
            "D3S Result Classification: " . ($this->d3sResultClassification->value ?? "NULL") . "\n" .
            "Address Result Classification: " . ($this->addressResultClassification->value ?? "NULL") . "\n";
    }

}