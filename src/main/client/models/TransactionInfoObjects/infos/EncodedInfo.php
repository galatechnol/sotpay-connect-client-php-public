<?php
namespace Src\main\client\models\TransactionInfoObjects\infos;
use Exception;
use JsonMapper;
use Src\main\client\models\TransactionInfo;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionInfoObjects\infos\enums\EncodedD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\EncodedStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\ResultClassification;

class EncodedInfo extends TransactionInfo {
    protected string $reference;
    protected EncodedStatusCode $statusCode;
    protected string $statusText;
    protected string $errorCode;
    protected string $errorText;
    protected EncodedD3sResult $d3sResult;
    protected CardType $cardType;
    protected string $maskedCardNumber;
    protected ResultClassification $d3sResultClassification;

    public function getReference(): string
    {
        return $this->reference;
    }
    public function getStatusCode(): EncodedStatusCode
    {
        return $this->statusCode;
    }
    public function getStatusText(): string
    {
        return $this->statusText;
    }
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }
    public function getErrorText(): string
    {
        return $this->errorText;
    }
    public function getD3sResult(): EncodedD3sResult
    {
        return $this->d3sResult;
    }
    public function getCardType(): CardType
    {
        return $this->cardType;
    }
    public function getMaskedCardNumber(): string
    {
        return $this->maskedCardNumber;
    }
    public function getD3sResultClassification(): ResultClassification
    {
        return $this->d3sResultClassification;
    }

    public function setReference(string $reference): void {
        $this->reference = $reference;
    }
    public function setStatusCode(EncodedStatusCode $statusCode): void {
        $this->statusCode = $statusCode;
    }
    public function setStatusText(string $statusText): void {
        $this->statusText = $statusText;
    }
    public function setErrorCode(string $errorCode): void
    {
        $this->errorCode = $errorCode;
    }
    public function setErrorText(string $errorText): void
    {
        $this->errorText = $errorText;
    }
    public function setD3sResult(EncodedD3sResult $d3sResult): void
    {
        $this->d3sResult = $d3sResult;
    }
    public function setCardType(CardType $cardType): void {
        $this->cardType = $cardType;
    }
    public function setMaskedCardNumber(string $maskedCardNumber): void {
        $this->maskedCardNumber = $maskedCardNumber;
    }

    public function setD3sResultClassification(ResultClassification $d3sResultClassification): void {
        $this->d3sResultClassification = $d3sResultClassification;
    }


    public static function withBody($JSONBody): EncodedInfo
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    protected function set(string $body)
    {
        $data = json_decode($body);
        //Check for valid JSON
        if ($data) {
            $mapper = new JsonMapper();
            $mapper->bIgnoreVisibility = true; //allow protected property access
            $mapper->bExceptionOnUndefinedProperty = true; //if we get unexpected properties
            $obj = $mapper->map($data, $this);
            return $obj;

        } else { // sending non-JSON data
            throw new Exception('Wrong data type sent to instance of EncodedInfo, expected JSON body');
        }
    }

    public function __toString() {
        return
            parent::__toString() .
            "Reference: " . ($this->reference ?? 'NULL') . "\n" .
            "Status Code: " . (str_replace("R", '', $this->statusCode->value)  ?? 'NULL') . "\n" .
            "Status Text: " . ($this->statusText ?? 'NULL') . "\n" .
            "Error Code: " . ($this->errorCode ?? 'NULL') . "\n" .
            "Error Text: " . ($this->errorText ?? 'NULL') . "\n" .
            "D3S Result: " . ($this->d3sResult->value ?? 'NULL') . "\n" .
            "Card Type: " . ($this->cardType->value ?? 'NULL') . "\n" .
            "Masked Card Number: " . ($this->maskedCardNumber ?? 'NULL') . "\n" .
            "D3S Result Classification: " . ($this->d3sResultClassification->value ?? 'NULL') . "\n";
    }
}