<?php
namespace Src\main\client\models\TransactionInfoObjects\infos;
use JsonMapper;
use PHPUnit\Logging\Exception;
use Src\main\client\models\TransactionInfo;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionInfoObjects\infos\enums\GlobalPaymentsAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\GlobalPaymentsD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\GlobalPaymentsPostcodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\GlobalPaymentsSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\GlobalPaymentsStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\ResultClassification;

class GlobalPaymentsInfo extends TransactionInfo {
    protected string $reference;
    protected GlobalPaymentsStatusCode $statusCode;
    protected string $statusText;
    protected string $errorCode;
    protected string $errorText;
    protected GlobalPaymentsSecurityCodeResult $securityCodeResult;
    protected GlobalPaymentsD3sResult $d3sResult;
    protected GlobalPaymentsAddressResult $addressResult;
    protected GlobalPaymentsPostcodeResult $postcodeResult;
    protected CardType $cardType;
    protected string $maskedCardNumber;
    protected bool $applePay;
    protected bool $googlePay;
    protected ResultClassification $securityCodeResultClassification;
    protected ResultClassification $d3sResultClassification;
    protected ResultClassification $addressResultClassification;
    protected ResultClassification $postcodeResultClassification;

    public function getStatusText(): string
    {
        return $this->statusText;
    }

    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    public function getErrorText(): string
    {
        return $this->errorText;
    }

    public function getSecurityCodeResult(): GlobalPaymentsSecurityCodeResult
    {
        return $this->securityCodeResult;
    }

    public function getD3sResult(): GlobalPaymentsD3sResult
    {
        return $this->d3sResult;
    }

    public function getAddressResult(): GlobalPaymentsAddressResult
    {
        return $this->addressResult;
    }

    public function getPostcodeResult(): GlobalPaymentsPostcodeResult
    {
        return $this->postcodeResult;
    }

    public function isApplePay(): bool
    {
        return $this->applePay;
    }

    public function isGooglePay(): bool
    {
        return $this->googlePay;
    }

    public function getSecurityCodeResultClassification(): ResultClassification
    {
        return $this->securityCodeResultClassification;
    }

    public function getD3sResultClassification(): ResultClassification
    {
        return $this->d3sResultClassification;
    }
    public function getPostcodeResultClassification(): ResultClassification
    {
        return $this->postcodeResultClassification;
    }

    public function getAddressResultClassification(): ResultClassification
    {
        return $this->addressResultClassification;
    }
    public function setReference(string $reference): void {
        $this->reference = $reference;
    }

    public function setStatusCode(GlobalPaymentsStatusCode $statusCode): void {
        $this->statusCode = $statusCode;
    }
    public function setErrorCode(string $errorCode): void
    {
        $this->errorCode = $errorCode;
    }
    public function setErrorText(string $errorText): void
    {
        $this->errorText = $errorText;
    }

    public function setStatusText(string $statusText): void {
        $this->statusText = $statusText;
    }

    public function setSecurityCodeResult(GlobalPaymentsSecurityCodeResult $securityCodeResult): void {
        $this->securityCodeResult = $securityCodeResult;
    }

    public function setD3sResult(GlobalPaymentsD3SResult $d3sResult): void {
        $this->d3sResult = $d3sResult;
    }

    public function setAddressResult(GlobalPaymentsAddressResult $addressResult): void {
        $this->addressResult = $addressResult;
    }

    public function setPostcodeResult(GlobalPaymentsPostcodeResult $postcodeResult): void {
        $this->postcodeResult = $postcodeResult;
    }

    public function setCardType(CardType $cardType): void {
        $this->cardType = $cardType;
    }

    public function setMaskedCardNumber(string $maskedCardNumber): void {
        $this->maskedCardNumber = $maskedCardNumber;
    }

    public function setApplePay(bool $applePay): void {
        $this->applePay = $applePay;
    }

    public function setGooglePay(bool $googlePay): void {
        $this->googlePay = $googlePay;
    }

    public function setSecurityCodeResultClassification(ResultClassification $securityCodeResultClassification): void {
        $this->securityCodeResultClassification = $securityCodeResultClassification;
    }

    public function setD3sResultClassification(ResultClassification $d3sResultClassification): void {
        $this->d3sResultClassification = $d3sResultClassification;
    }

    public function setAddressResultClassification(ResultClassification $addressResultClassification): void {
        $this->addressResultClassification = $addressResultClassification;
    }

    public function setPostcodeResultClassification(ResultClassification $postcodeResultClassification): void {
        $this->postcodeResultClassification = $postcodeResultClassification;
    }
    public static function withBody($JSONBody): GlobalPaymentsInfo
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    protected function set(string $body)
    {

        $data = json_decode($body);
        //Check for valid JSON
        if ($data) {
            if ($data->d3sResult) {
                $data->d3sResult = 'R' . $data->d3sResult;
            }

            $mapper = new JsonMapper();
            $mapper->bIgnoreVisibility = true; //allow private property access
            $mapper->bExceptionOnUndefinedProperty = true; //if we get unexpected properties
            $obj = $mapper->map($data, $this);
            return $obj;

        } else { // sending non-JSON data
            throw new Exception('Wrong data type sent to instance of GlobalPaymentsInfo, expected JSON body');
        }
    }
    public function __toString() {
        return
            parent::__toString() .
            "Reference: " . ($this->reference ?? "NULL") . "\n" .
            "Status Code: " . ($this->statusCode ?? "NULL") . "\n" .
            "Status Text: " . ($this->statusText ?? "NULL") . "\n" .
            "Error Code: " . ($this->errorCode ?? "NULL") . "\n" .
            "Error Text: " . ($this->errorText ?? "NULL") . "\n" .
            "Security Code Result: " . ($this->securityCodeResult->value ?? "NULL") . "\n" .
            "D3S Result: " . (str_replace("R", '', $this->d3sResult->value)  ?? 'NULL') . "\n" .
            "Address Result: " . ($this->addressResult->value ?? "NULL") . "\n" .
            "Postcode Result: " . ($this->postcodeResult->value ?? "NULL") . "\n" .
            "Card Type: " . ($this->cardType->value ?? "NULL") . "\n" .
            "Masked Card Number: " . ($this->maskedCardNumber ?? "NULL") . "\n" .
            "Apple Pay: " . ($this->applePay ? 'Yes' : 'No') . "\n" .
            "Google Pay: " . ($this->googlePay ? 'Yes' : 'No') . "\n" .
            "Security Code Result Classification: " . ($this->securityCodeResultClassification->value ?? "NULL") . "\n" .
            "D3S Result Classification: " . ($this->d3sResultClassification->value ?? "NULL") . "\n" .
            "Address Result Classification: " . ($this->addressResultClassification->value ?? "NULL") . "\n" .
            "Postcode Result Classification: " . ($this->postcodeResultClassification->value ?? "NULL") . "\n";
    }
}