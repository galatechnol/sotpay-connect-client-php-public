<?php
namespace Src\main\client\models\TransactionInfoObjects\infos;
use JsonMapper;
use PHPUnit\Logging\Exception;
use Src\main\client\models\TransactionInfo;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CheckoutComAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CheckoutComD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CheckoutComSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CheckoutComStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\ResultClassification;

class CheckoutComInfo extends TransactionInfo {
    protected string $reference;
    protected CheckoutComStatusCode $statusCode;
    protected string $statusText;
    protected CheckoutComSecurityCodeResult $securityCodeResult;
    protected CheckoutComD3sResult $d3sResult;
    protected CheckoutComAddressResult $addressResult;
    protected CardType $cardType;
    protected string $maskedCardNumber;
    protected ResultClassification $securityCodeResultClassification;
    protected ResultClassification $d3sResultClassification;
    protected ResultClassification $addressResultClassification;


    public function getReference(): string
    {
        return $this->reference;
    }
    public function getStatusCode(): CheckoutComStatusCode
    {
        return $this->statusCode;
    }
    public function getStatusText(): string
    {
        return $this->statusText;
    }
    public function getSecurityCodeResult(): CheckoutComSecurityCodeResult
    {
        return $this->securityCodeResult;
    }
    public function getD3sResult(): CheckoutComD3sResult
    {
        return $this->d3sResult;
    }
    public function getAddressResult(): CheckoutComAddressResult
    {
        return $this->addressResult;
    }
    public function getCardType(): CardType
    {
        return $this->cardType;
    }
    public function getMaskedCardNumber(): string
    {
        return $this->maskedCardNumber;
    }
    public function getSecurityCodeResultClassification(): ResultClassification
    {
        return $this->securityCodeResultClassification;
    }
    public function getD3sResultClassification(): ResultClassification
    {
        return $this->d3sResultClassification;
    }
    public function getAddressResultClassification(): ResultClassification
    {
        return $this->addressResultClassification;
    }

    public function setReference(string $reference): void {
        $this->reference = $reference;
    }

    public function setStatusCode(CheckoutComStatusCode $statusCode): void {
        $this->statusCode = $statusCode;
    }

    public function setStatusText(string $statusText): void {
        $this->statusText = $statusText;
    }
    public function setSecurityCodeResult(CheckoutComSecurityCodeResult $securityCodeResult): void
    {
        $this->securityCodeResult = $securityCodeResult;
    }
    public function setD3sResult(CheckoutComD3sResult $d3sResult): void
    {
        $this->d3sResult = $d3sResult;
    }
    public function setAddressResult(CheckoutComAddressResult $addressResult): void
    {
        $this->addressResult = $addressResult;
    }
    public function setCardType(CardType $cardType): void {
        $this->cardType = $cardType;
    }

    public function setMaskedCardNumber(string $maskedCardNumber): void {
        $this->maskedCardNumber = $maskedCardNumber;
    }

    public function setSecurityCodeResultClassification(ResultClassification $securityCodeResultClassification): void {
        $this->securityCodeResultClassification = $securityCodeResultClassification;
    }

    public function setD3sResultClassification(ResultClassification $d3sResultClassification): void {
        $this->d3sResultClassification = $d3sResultClassification;
    }

    public function setAddressResultClassification(ResultClassification $addressResultClassification): void {
        $this->addressResultClassification = $addressResultClassification;
    }


    public static function withBody($JSONBody): CheckoutComInfo
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    protected function set(string $body)
    {
        $data = json_decode($body);
        //Check for valid JSON
        if ($data) {
            $mapper = new JsonMapper();
            $mapper->bIgnoreVisibility = true; //allow protected property access
            $mapper->bExceptionOnUndefinedProperty = true; //if we get unexpected properties
            $obj = $mapper->map($data, $this);
            return $obj;

        } else { // sending non-JSON data
            throw new Exception('Wrong data type sent to instance of Cardstreaminfo, expected JSON body');
        }
    }

    public function __toString() {
        return
            parent::__toString() .
            "Reference: " . ($this->reference ?? 'NULL') . "\n" .
            "Secondary Reference: " . ($this->referenceSecondary ?? 'NULL') . "\n" .
            "Status Code: " . (str_replace("R", '', $this->statusCode->value)  ?? 'NULL') . "\n" .
            "Status Text: " . ($this->statusText ?? 'NULL') . "\n" .
            "State: " . ($this->state->value ?? 'NULL') . "\n" .
            "Security Code Result: " . (str_replace("R", '', $this->securityCodeResult->value)  ?? 'NULL') . "\n" .
            "D3S Result: " . ($this->d3sResult->value ?? 'NULL') . "\n" .
            "D3S Reference: " . ($this->d3sReference ?? 'NULL') . "\n" .
            "Address Result: " . (str_replace("R", '', $this->addressResult->value)  ?? 'NULL') . "\n" .
            "Postcode Result: " . (str_replace("R", '', $this->postcodeResult->value)  ?? 'NULL') . "\n" .
            "Card Type: " . ($this->cardType->value ?? 'NULL') . "\n" .
            "Masked Card Number: " . ($this->maskedCardNumber ?? 'NULL') . "\n" .
            "Apple Pay: " . ($this->applePay ? 'Yes' : 'No') . "\n" .
            "Google Pay: " . ($this->googlePay ? 'Yes' : 'No') . "\n" .
            "Security Code Result Classification: " . ($this->securityCodeResultClassification->value ?? 'NULL') . "\n" .
            "D3S Result Classification: " . ($this->d3sResultClassification->value ?? 'NULL') . "\n" .
            "Address Result Classification: " . ($this->addressResultClassification->value ?? 'NULL') . "\n" .
            "Postcode Result Classification: " . ($this->postcodeResultClassification->value ?? 'NULL') . "\n";
    }
}