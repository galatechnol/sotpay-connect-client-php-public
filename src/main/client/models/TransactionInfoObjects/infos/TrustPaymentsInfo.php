<?php
namespace Src\main\client\models\TransactionInfoObjects\infos;
use Src\main\client\models\TransactionInfo;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoPostcodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\ResultClassification;
use Src\main\client\models\TransactionInfoObjects\infos\enums\StripeAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\StripeD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\StripeRiskScoreClassification;
use Src\main\client\models\TransactionInfoObjects\infos\enums\StripeSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\StripeStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\TrustPaymentsAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\TrustPaymentsD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\TrustPaymentsSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\TrustPaymentsStatusCode;

class TrustPaymentsInfo extends TransactionInfo {
    protected string $reference;
    protected TrustPaymentsStatusCode $statusCode;
    protected string $statusText;
    protected TrustPaymentsSecurityCodeResult $securityCodeResult;
    protected TrustPaymentsD3sResult $d3sResult;
    protected TrustPaymentsAddressResult $addressResult;
    protected TrustPaymentsAddressResult $postcodeResult;
    protected CardType $cardType;
    protected string $maskedCardNumber;
    protected ResultClassification $securityCodeResultClassification;
    protected ResultClassification $d3sResultClassification;
    protected ResultClassification $addressResultClassification;
    protected ResultClassification $postcodeResultClassification;

    public function getReference(): string
    {
        return $this->reference;
    }
    public function getStatusCode(): TrustPaymentsStatusCode
    {
        return $this->statusCode;
    }
    public function getStatusText(): string
    {
        return $this->statusText;
    }

    public function getSecurityCodeResult(): TrustPaymentsSecurityCodeResult
    {
        return $this->securityCodeResult;
    }

    public function getD3sResult(): TrustPaymentsD3sResult
    {
        return $this->d3sResult;
    }

    public function getAddressResult(): TrustPaymentsAddressResult
    {
        return $this->addressResult;
    }

    public function getPostcodeResult(): TrustPaymentsAddressResult
    {
        return $this->postcodeResult;
    }
    public function getCardType(): CardType
    {
        return $this->cardType;
    }
    public function getMaskedCardNumber(): string
    {
        return $this->maskedCardNumber;
    }
    public function getSecurityCodeResultClassification(): ResultClassification
    {
        return $this->securityCodeResultClassification;
    }
    public function getD3sResultClassification(): ResultClassification
    {
        return $this->d3sResultClassification;
    }
    public function getAddressResultClassification(): ResultClassification
    {
        return $this->addressResultClassification;
    }
    public function getPostcodeResultClassification(): ResultClassification
    {
        return $this->postcodeResultClassification;
    }
    public function setReference(string $reference): void {
        $this->reference = $reference;
    }
    public function setStatusCode(TrustPaymentsStatusCode $statusCode): void
    {
        $this->statusCode = $statusCode;
    }
    public function setStatusText(string $statusText): void
    {
        $this->statusText = $statusText;
    }
    public function setSecurityCodeResult(TrustPaymentsSecurityCodeResult $securityCodeResult): void {
        $this->securityCodeResult = $securityCodeResult;
    }
    public function setD3sResult(TrustPaymentsD3sResult $d3sResult): void {
        $this->d3sResult = $d3sResult;
    }
    public function setAddressResult(TrustPaymentsAddressResult $addressResult): void {
        $this->addressResult = $addressResult;
    }
    public function setPostcodeResult(TrustPaymentsAddressResult $postcodeResult): void {
        $this->postcodeResult = $postcodeResult;
    }
    public function setCardType(CardType $cardType): void {
        $this->cardType = $cardType;
    }
    public function setMaskedCardNumber(string $maskedCardNumber): void {
        $this->maskedCardNumber = $maskedCardNumber;
    }
    public function setSecurityCodeResultClassification(ResultClassification $securityCodeResultClassification): void {
        $this->securityCodeResultClassification = $securityCodeResultClassification;
    }

    public function setD3sResultClassification(ResultClassification $d3sResultClassification): void {
        $this->d3sResultClassification = $d3sResultClassification;
    }

    public function setAddressResultClassification(ResultClassification $addressResultClassification): void {
        $this->addressResultClassification = $addressResultClassification;
    }

    public function setPostcodeResultClassification(ResultClassification $postcodeResultClassification): void {
        $this->postcodeResultClassification = $postcodeResultClassification;
    }
    public static function withBody($JSONBody): TrustPaymentsInfo
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString() {
        return
            parent::__toString() .
            "Reference: " . ($this->reference ?? "NULL") . "\n" .
            "Status Code: " . ($this->statusCode->value ?? "NULL") . "\n" .
            "Status Text: " . ($this->statusText ?? "NULL") . "\n" .
            "Security Code Result: " . ($this->securityCodeResult->value ?? "NULL") . "\n" .
            "D3S Result: " . ($this->d3sResult->value ?? "NULL") . "\n" .
            "Address Result: " . ($this->addressResult->value ?? "NULL") . "\n" .
            "Postcode Result: " . ($this->postcodeResult->value ?? "NULL") . "\n" .
            "Card Type: " . ($this->cardType->value ?? "NULL") . "\n" .
            "Masked Card Number: " . ($this->maskedCardNumber ?? "NULL") . "\n" .
            "Security Code Result Classification: " . ($this->securityCodeResultClassification->value ?? "NULL") . "\n" .
            "D3S Result Classification: " . ($this->d3sResultClassification->value ?? "NULL") . "\n" .
            "Address Result Classification: " . ($this->addressResultClassification->value ?? "NULL") . "\n" .
            "Postcode Result Classification: " . ($this->postcodeResultClassification->value ?? "NULL") . "\n";
    }
}