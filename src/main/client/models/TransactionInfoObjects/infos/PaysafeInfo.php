<?php
namespace Src\main\client\models\TransactionInfoObjects\infos;
use Src\main\client\models\TransactionInfo;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionInfoObjects\infos\enums\PaySafeAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\PaysafeD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\PaysafeSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\PaysafeStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\ResultClassification;

class PaysafeInfo extends TransactionInfo {
    protected string $reference;
    protected PaysafeStatusCode $statusCode;
    protected string $statusText;
    protected string $errorCode;
    protected string $errorText;
    protected PaysafeSecurityCodeResult $securityCodeResult;
    protected PaysafeD3sResult $d3sResult;
    protected PaySafeAddressResult $addressResult;
    protected CardType $cardType;
    protected string $maskedCardNumber;
    protected ResultClassification $securityCodeResultClassification;
    protected ResultClassification $d3sResultClassification;
    protected ResultClassification $addressResultClassification;


    public function getStatusText(): string
    {
        return $this->statusText;
    }
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }
    public function getErrorText(): string
    {
        return $this->errorText;
    }
    public function getSecurityCodeResult(): PaysafeSecurityCodeResult
    {
        return $this->securityCodeResult;
    }
    public function getD3sResult(): PaysafeD3sResult
    {
        return $this->d3sResult;
    }
    public function getAddressResult(): PaySafeAddressResult
    {
        return $this->addressResult;
    }
    public function getSecurityCodeResultClassification(): ResultClassification
    {
        return $this->securityCodeResultClassification;
    }
    public function getD3sResultClassification(): ResultClassification
    {
        return $this->d3sResultClassification;
    }
    public function getAddressResultClassification(): ResultClassification
    {
        return $this->addressResultClassification;
    }

    public function setReference(string $reference): void {
        $this->reference = $reference;
    }

    public function setStatusCode(PaysafeStatusCode $statusCode): void {
        $this->statusCode = $statusCode;
    }

    public function setStatusText(string $statusText): void {
        $this->statusText = $statusText;
    }

    public function setErrorCode(string $errorCode): void {
        $this->errorCode = $errorCode;
    }

    public function setErrorText(string $errorText): void {
        $this->errorText = $errorText;
    }

    public function setSecurityCodeResult(PaysafeSecurityCodeResult $securityCodeResult): void {
        $this->securityCodeResult = $securityCodeResult;
    }

    public function setD3sResult(PaysafeD3sResult $d3sResult): void {
        $this->d3sResult = $d3sResult;
    }

    public function setAddressResult(PaySafeAddressResult $addressResult): void {
        $this->addressResult = $addressResult;
    }

    public function setCardType(CardType $cardType): void {
        $this->cardType = $cardType;
    }

    public function setMaskedCardNumber(string $maskedCardNumber): void {
        $this->maskedCardNumber = $maskedCardNumber;
    }

    public function setSecurityCodeResultClassification(ResultClassification $securityCodeResultClassification): void {
        $this->securityCodeResultClassification = $securityCodeResultClassification;
    }

    public function setD3sResultClassification(ResultClassification $d3sResultClassification): void {
        $this->d3sResultClassification = $d3sResultClassification;
    }

    public function setAddressResultClassification(ResultClassification $addressResultClassification): void {
        $this->addressResultClassification = $addressResultClassification;
    }
    public static function withBody($JSONBody): PaysafeInfo
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString() {
        return
            parent::__toString() .
            "Reference: " . ($this->reference ?? "NULL") . "\n" .
            "Status Code: " . ($this->statusCode->value ?? "NULL") . "\n" .
            "Status Text: " . ($this->statusText ?? "NULL") . "\n" .
            "Error Code: " . ($this->errorCode ?? "NULL") . "\n" .
            "Error Text: " . ($this->errorText ?? "NULL") . "\n" .
            "Security Code Result: " . ($this->securityCodeResult->value ?? "NULL") . "\n" .
            "D3S Result: " . ($this->d3sResult->value ?? "NULL") . "\n" .
            "Address Result: " . ($this->addressResult->value ?? "NULL") . "\n" .
            "Card Type: " . ($this->cardType->value ?? "NULL") . "\n" .
            "Masked Card Number: " . ($this->maskedCardNumber ?? "NULL") . "\n" .
            "Security Code Result Classification: " . ($this->securityCodeResultClassification->value ?? "NULL") . "\n" .
            "D3S Result Classification: " . ($this->d3sResultClassification->value ?? "NULL") . "\n" .
            "Address Result Classification: " . ($this->addressResultClassification->value ?? "NULL") . "\n";
    }
}