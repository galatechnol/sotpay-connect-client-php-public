<?php
namespace Src\main\client\models\TransactionInfoObjects\infos;
use Src\main\client\models\TransactionInfo;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionInfoObjects\infos\enums\EVOPaymentsStatusCode;

class EVOPaymentsInfo extends TransactionInfo {
    protected string $reference;
    protected EVOPaymentsStatusCode $statusCode;
    protected string $statusText;
    protected CardType $cardType;
    protected string $maskedCardNumber;

    public function getStatusText(): string
    {
        return $this->statusText;
    }

    public function setReference(string $reference): void {
        $this->reference = $reference;
    }

    public function setStatusCode(EVOPaymentsStatusCode $statusCode): void {
        $this->statusCode = $statusCode;
    }

    public function setStatusText(string $statusText): void {
        $this->statusText = $statusText;
    }

    public function setCardType(CardType $cardType): void {
        $this->cardType = $cardType;
    }

    public function setMaskedCardNumber(string $maskedCardNumber): void {
        $this->maskedCardNumber = $maskedCardNumber;
    }
    public static function withBody($JSONBody): EVOPaymentsInfo
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString() {
        return
            parent::__toString() .
            "Reference: " . ($this->reference ?? "NULL") . "\n" .
            "Status Code: " . ($this->statusCode->value ?? "NULL") . "\n" .
            "Status Text: " . ($this->statusText ?? "NULL") . "\n" .
            "Card Type: " . ($this->cardType->value ?? "NULL") . "\n" .
            "Masked Card Number: " . ($this->maskedCardNumber ?? "NULL") . "\n";
    }
}