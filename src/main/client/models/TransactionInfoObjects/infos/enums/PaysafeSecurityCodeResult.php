<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum PaysafeSecurityCodeResult: string {
    case MATCH = 'MATCH';
    case NO_MATCH = 'NO_MATCH';
    case NOT_PROCESSED = 'NOT_PROCESSED';
    case UNKNOWN = 'UNKNOWN';


}