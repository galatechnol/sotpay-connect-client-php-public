<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum FreedomPayD3sResult: string {
    case Y = 'Y';
    case N = 'N';
    case U = 'U';
    case A = 'A';
    case E = 'E';
    case R = 'R';
}