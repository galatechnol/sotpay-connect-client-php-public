<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;

enum BarclaycardStatusCode: string {
    case R0 = '0';
    case R1 = 'R1';
    case R2 = 'R2';
    case R4 = 'R4';
    case R40 = 'R40';
    case R41 = 'R41';
    case R46 = 'R46';
    case R5 = 'R5';
    case R50 = 'R50';
    case R51 = 'R51';
    case R52 = 'R52';
    case R55 = 'R55';
    case R56 = 'R56';
    case R57 = 'R57';
    case R59 = 'R59';
    case R6 = 'R6';
    case R61 = 'R61';
    case R62 = 'R62';
    case R63 = 'R63';
    case R64 = 'R64';
    case R7 = 'R7';
    case R71 = 'R71';
    case R72 = 'R72';
    case R73 = 'R73';
    case R74 = 'R74';
    case R75 = 'R75';
    case R8 = 'R8';
    case R81 = 'R81';
    case R82 = 'R82';
    case R83 = 'R83';
    case R84 = 'R84';
    case R85 = 'R85';
    case R9 = 'R9';
    case R91 = 'R91';
    case R92 = 'R92';
    case R93 = 'R93';
    case R94 = 'R94';
    case R95 = 'R95';
    case R96 = 'R96';
    case R97 = 'R97';
    case R98 = 'R98';
    case R99 = 'R99';

}