<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum Pay360D3sResult: string {
    case AUTHENTICATED = 'AUTHENTICATED';
    case BYPASSED = 'BYPASSED';
    case FAILED = 'FAILED';
    case NOT_ENROLLED = 'NOT_ENROLLED';
    case ATTEMPTED = 'ATTEMPTED';
    case ENROLMENT_CHECK_FAILURE = 'ENROLMENT_CHECK_FAILURE';
    case INCOMPLETE = 'INCOMPLETE';
    case NOT_AVAILABLE = 'NOT_AVAILABLE';
    case NOT_IMPLEMENTED = 'NOT_IMPLEMENTED';

}