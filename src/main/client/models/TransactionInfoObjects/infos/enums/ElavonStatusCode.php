<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;

enum ElavonStatusCode: string {
    case declined = 'declined';
    case authorized  = 'authorized' ;
    case captured  = 'captured' ;
    case voided  = 'voided' ;
    case settled  = 'settled' ;
    case expired  = 'expired' ;
    case settlementDelayed  = 'settlementDelayed' ;
    case rejected = 'rejected' ;
    case heldForReview  = 'heldForReview' ;
    case authorizationPending  = 'authorizationPending' ;
    case unknown = 'unknown ' ;
}