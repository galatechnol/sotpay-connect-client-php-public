<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;

enum OpayoServerStatusCode: string {
    case OK = 'OK';
    case NOTAUTHED = 'NOTAUTHED';
    case PENDING = 'PENDING';
    case ABORT = 'ABORT';
    case REJECTED = 'REJECTED';
    case ERROR = 'ERROR';
}