<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum ElavonD3sResult: string {
    case matched = 'matched';
    case unmatched = 'unmatched';
    case attempted = 'attempted';
    case unprovided = 'unprovided';
    case unsupported = 'unsupported';
    case unavailable = 'unavailable';
    case unknown  = 'unknown';
}