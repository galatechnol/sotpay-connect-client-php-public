<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum GlobalPaymentsRealexAddressResult: string {
    case M = 'M';
    case N = 'N';
    case I = 'I';
    case U = 'U';
    case P = 'P';

}