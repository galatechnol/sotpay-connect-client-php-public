<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum ElavonSecurityCodeResult: string {
    case matched = 'matched';
    case unmatched = 'unmatched';
    case unprovided = 'unprovided';
    case unsupported = 'unsupported';
    case unavailable = 'unavailable';
    case unknown  = 'unknown ';
}