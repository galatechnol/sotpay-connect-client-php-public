<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum Pay360SecurityCodeResult: string {
    case NOT_CHECKED = 'NOT_CHECKED';
    case MATCHED = 'MATCHED';
    case NOT_MATCHED = 'NOT_MATCHED';

}