<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum SYBCSecurityCodeResult: string {
    case match = 'match';
    case nomatch = 'nomatch';
    case unchecked = 'unchecked';

}