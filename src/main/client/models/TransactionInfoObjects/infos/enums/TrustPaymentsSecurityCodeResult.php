<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum
TrustPaymentsSecurityCodeResult: string {
    case R0 = 'R0';
    case R1 = 'R1';
    case R2 = 'R2';
    case R4 = 'R4';
}