<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum GlobalPaymentsRealexPostcodeResult: string {
    case M = 'M';
    case N = 'N';
    case I = 'I';
    case U = 'U';
    case P = 'P';

}