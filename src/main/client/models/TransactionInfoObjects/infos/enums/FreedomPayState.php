<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum FreedomPayState: string {
    case ACCEPT = 'ACCEPT';
    case REJECT = 'REJECT';
    case ERROR = 'ERROR';
    case FAILURE = 'FAILURE';
    case INTERUPT = 'INTERUPT';

}