<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum AcquiredPostcodeResult: string {
    case M = 'M';
    case N = 'N';
    case NC = 'NC';
    case NP = 'NP';
}