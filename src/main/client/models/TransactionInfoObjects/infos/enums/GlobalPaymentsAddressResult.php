<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum GlobalPaymentsAddressResult: string {
    case MATCHED = 'MATCHED';
    case NOT_MATCHED = 'NOT_MATCHED';
    case NOT_CHECKED  = 'NOT_CHECKED ';
}