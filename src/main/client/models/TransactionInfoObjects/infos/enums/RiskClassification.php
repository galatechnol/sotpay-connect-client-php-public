<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum RiskClassification: string {
    case OK = 'OK';
    case CAUTION = 'CAUTION';
    case WARNING = 'WARNING';
    case FAIL = 'FAIL';
    case UNKNOWN = 'UNKNOWN';
}