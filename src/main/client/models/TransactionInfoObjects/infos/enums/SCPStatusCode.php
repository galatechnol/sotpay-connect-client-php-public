<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;

enum SCPStatusCode: string {
    case SUCCESS = 'SUCCESS';
    case CARD_DETAILS_REJECTED = 'CARD_DETAILS_REJECTED';
    case CANCELLED = 'CANCELLED';
    case LOGGED_OUT = 'LOGGED_OUT';
    case NOT_ATTEMPTED = 'NOT_ATTEMPTED';
    case INVALID_REQUEST = 'INVALID_REQUEST';
    case ERROR = 'ERROR';

}