<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum OpayoServerAddressResult: string {
    case MATCHED = 'MATCHED';
    case NOTMATCHED = 'NOTMATCHED';
    case NOTCHECKED = 'NOTCHECKED';
    case NOTPROVIDED = 'NOTPROVIDED';

}