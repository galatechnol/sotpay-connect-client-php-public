<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum SYBCD3sResult: string {
    case success = 'success';
    case attempted = 'attempted';
    case failure = 'failure';
    case unchecked = 'unchecked';

}