<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum GlobalPaymentsRealexD3sResult: string {
    case R0 = 'R0';
    case R1 = 'R1';
    case R2 = 'R2';
    case R5 = 'R5';
    case R6 = 'R6';
    case R7 = 'R7';

}