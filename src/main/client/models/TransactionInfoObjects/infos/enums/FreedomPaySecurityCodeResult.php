<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum FreedomPaySecurityCodeResult: string {
    case RD = 'RD';
    case RI = 'RI';
    case RM = 'RM';
    case RN = 'RN';
    case RP = 'RP';
    case RS = 'RS';
    case RU = 'RU';
    case RX = 'RX';
    case R1 = 'R1';
    case R2 = 'R2';
    case R3 = 'R3';

}