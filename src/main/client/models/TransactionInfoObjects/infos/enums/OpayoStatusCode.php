<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;

enum OpayoStatusCode: string {
    case Ok = 'Ok';
    case Authenticated = 'Authenticated';
    case NotAuthed = 'NotAuthed';
    case Rejected = 'Rejected';
    case Malformed = 'Malformed';
    case Invalid = 'Invalid';
    case Error = 'Error';
}