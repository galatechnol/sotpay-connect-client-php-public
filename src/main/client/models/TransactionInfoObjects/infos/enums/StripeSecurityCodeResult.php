<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum
StripeSecurityCodeResult: string {
    case pass = 'pass';
    case fail = 'fail';
    case unavailable = 'unavailable';
    case unchecked = 'unchecked';
}