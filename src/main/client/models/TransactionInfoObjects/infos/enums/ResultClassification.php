<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum ResultClassification: string {
    case PASS = 'PASS';
    case FAIL = 'FAIL';
    case PARTIAL = 'PARTIAL';
    case UNKNOWN = 'UNKNOWN';
}