<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;

enum StripeStatusCode: string {
    case succeeded = 'succeeded';
    case pending = 'pending';
    case failed = 'failed';
}