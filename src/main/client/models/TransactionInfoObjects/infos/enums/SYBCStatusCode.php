<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;

enum SYBCStatusCode: string {
    case approved = 'approved';
    case declined = 'declined';
    case aborted = 'aborted';
    case challenge = 'challenge';
}