<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum CheckoutComSecurityCodeResult: string {
    case X = 'X';
    case U = 'U';
    case P = 'P';
    case Y = 'Y';
    case D = 'D';
    case N = 'N';

}