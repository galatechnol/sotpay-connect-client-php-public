<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum BarclaycardD3SResult: string {
    case R5 = 'R5';
    case R6 = 'R6';
    case R12 = 'R12';
    case R91 = 'R91';
    case R92 = 'R92';
}