<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum StripeAddressResult: string {
    case pass = 'pass';
    case fail = 'fail';
    case unavailable = 'unavailable';
    case unchecked = 'unchecked';
}