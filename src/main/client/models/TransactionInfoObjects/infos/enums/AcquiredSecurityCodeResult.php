<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum AcquiredSecurityCodeResult: string {
    case M = 'M';
    case N = 'N';
    case NC = 'NC';
    case NP = 'NP';
}