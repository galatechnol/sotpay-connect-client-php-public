<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum OpayoAddressResult: string {
    case Matched = 'Matched';
    case NotMatched = 'NotMatched';
    case NotChecked = 'NotChecked';
    case NotProvided = 'NotProvided';

}