<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum GlobalPaymentsSecurityCodeResult: string {
    case MATCHED = 'MATCHED';
    case NOT_MATCHED = 'NOT_MATCHED';
    case NOT_CHECKED  = 'NOT_CHECKED ';
}