<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum GlobalPaymentsD3sResult: string {
    case SUCCESS_AUTHENTICATED = 'SUCCESS_AUTHENTICATED';
    case FAILED = 'FAILED';
    case NOT_AUTHENTICATED = 'NOT_AUTHENTICATED';
    case SUCCESS_ATTEMPT_MADE = 'SUCCESS_ATTEMPT_MADE';
    case NOT_ENROLLED  = 'NOT_ENROLLED ';
}