<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum CardstreamState: string {
    case received = 'received';
    case approved = 'approved';
    case verified = 'verified';
    case declined = 'declined';
    case referred = 'referred';
    case reversed = 'reversed';
    case captured = 'captured';
    case tendered = 'tendered';
    case deferred = 'deferred';
    case accepted = 'accepted';
    case rejected = 'rejected';
    case cancelled = 'cancelled';
    case finished = 'finished';
}