<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;

enum Pay360StatusCode: string {
    case SUCCESS = 'SUCCESS';
    case FAILED = 'FAILED';
    case PENDING = 'PENDING';
    case EXPIRED = 'EXPIRED';
    case CANCELLED = 'CANCELLED';
    case VOIDED = 'VOIDED';
}