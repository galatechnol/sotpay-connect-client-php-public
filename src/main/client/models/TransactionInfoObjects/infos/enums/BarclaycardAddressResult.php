<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum BarclaycardAddressResult: string {
    case KO = 'KO';
    case OK = 'OK';
    case NO = 'NO';
}