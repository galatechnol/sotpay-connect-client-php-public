<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum Pay360PostcodeResult: string {
    case NOT_CHECKED = 'NOT_CHECKED';
    case FULL_MATCH = 'FULL_MATCH';
    case NOT_MATCHED = 'NOT_MATCHED';
    case NOT_PROVIDED = 'NOT_PROVIDED';

}