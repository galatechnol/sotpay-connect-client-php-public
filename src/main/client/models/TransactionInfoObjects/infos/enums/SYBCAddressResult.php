<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum SYBCAddressResult: string {
    case match = 'match';
    case partialmatch = 'partialmatch';
    case nomatch = 'nomatch';
    case unchecked = 'unchecked';

}