<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum SCPState: string {
    case IN_PROGRESS = 'IN_PROGRESS';
    case COMPLETE = 'COMPLETE';
    case INVALID_REFERENCE = 'INVALID_REFERENCE';

}