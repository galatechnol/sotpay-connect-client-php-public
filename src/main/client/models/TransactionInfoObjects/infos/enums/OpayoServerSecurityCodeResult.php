<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum OpayoServerSecurityCodeResult: string {
    case MATCHED = 'MATCHED';
    case NOTMATCHED = 'NOTMATCHED';
    case NOTCHECKED = 'NOTCHECKED';
    case NOTPROVIDED = 'NOTPROVIDED';

}