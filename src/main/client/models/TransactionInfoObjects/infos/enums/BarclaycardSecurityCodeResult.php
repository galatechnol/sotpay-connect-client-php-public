<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum BarclaycardSecurityCodeResult: string {
    case KO = 'KO';
    case OK = 'OK';
    case NO = 'NO';
}