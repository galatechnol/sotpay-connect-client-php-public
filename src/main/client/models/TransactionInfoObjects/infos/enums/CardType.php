<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum CardType: string {
    case AMERICANEXPRESS = 'AMERICANEXPRESS';
    case CIRRUS = 'CIRRUS';
    case CHINAUNIONPAY = 'CHINAUNIONPAY';
    case DANKORT = 'DANKORT';
    case DINERSCLUB = 'DINERSCLUB';
    case DISCOVER = 'DISCOVER';
    case JCB = 'JCB';
    case MAESTRO = 'MAESTRO';
    case MASTERCARD = 'MASTERCARD';
    case MIR = 'MIR';
    case PAYPAL = 'PAYPAL';
    case RUPAY = 'RUPAY';
    case TROY = 'TROY';
    case VISA = 'VISA';
    case UNKNOWN = 'UNKNOWN';
}