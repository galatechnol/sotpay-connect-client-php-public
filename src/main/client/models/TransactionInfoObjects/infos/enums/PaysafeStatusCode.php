<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;

enum PaysafeStatusCode: string {
    case RECEIVED = 'RECEIVED';
    case COMPLETED = 'COMPLETED';
    case HELD = 'HELD';
    case FAILED = 'FAILED';
    case CANCELLED = 'CANCELLED';
}