<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;

enum EncodedStatusCode: string {
    case accepted = 'accepted';
    case declined = 'declined';
    case error = 'error';
}