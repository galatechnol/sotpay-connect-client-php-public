<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;

enum EVOPaymentsStatusCode: string {
    case SET_FOR_CAPTURE = 'SET_FOR_CAPTURE';
    case NOT_SET_FOR_CAPTURE = 'NOT_SET_FOR_CAPTURE';
    case CAPTURED = 'CAPTURED';
    case VERIFIED = 'VERIFIED';
    case DECLINED = 'DECLINED';
    case ERROR = 'ERROR';
}