<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum OpayoServerD3sResult: string {
    case OK = 'OK';
    case NOTCHECKED = 'NOTCHECKED';
    case NOTAVAILABLE = 'NOTAVAILABLE';
    case NOTAUTHED = 'NOTAUTHED';
    case INCOMPLETE = 'INCOMPLETE';
    case ATTEMPTONLY = 'ATTEMPTONLY';
    case ERROR = 'ERROR';

}