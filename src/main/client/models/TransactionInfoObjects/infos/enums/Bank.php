<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum Bank: string {
    case AIB = 'AIB';
    case BARCLAYS = 'BARCLAYS';
    case COUTTS = 'COUTTS';
    case DANSKE = 'DANSKE';
    case FIRSTDIRECT = 'FIRSTDIRECT';
    case HALIFAX = 'HALIFAX';
    case HSBC = 'HSBC';
    case IRELAND = 'IRELAND';
    case LLOYDS = 'LLOYDS';
    case MONZO = 'MONZO';
    case NATIONWIDE = 'NATIONWIDE';
    case NATWEST = 'NATWEST';
    case NUAPAYASPSP = 'NUAPAYASPSP';
    case RBS = 'RBS';
    case REVOLUT = 'REVOLUT';
    case SANTANDER = 'SANTANDER';
    case SCOTLAND = 'SCOTLAND';
    case STARLING = 'STARLING';
    case TESCO = 'TESCO';
    case TIDE = 'TIDE';
    case TSB = 'TSB';
    case ULSTER = 'ULSTER';
    case VIRGIN = 'VIRGIN';
    case WISE = 'WISE';
    case UNKNOWN = 'UNKNOWN';
}