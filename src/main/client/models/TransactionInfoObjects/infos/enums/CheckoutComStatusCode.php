<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;

enum CheckoutComStatusCode: string {
    case Authorized = 'Authorized';
    case Pending = 'Pending';
    case CardVerified = 'CardVerified';
    case Captured = 'Captured';
    case Declined = 'Declined';
    case Paid = 'Paid';
}