<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum OpayoD3sResult: string {
    case Authenticated = 'Authenticated';
    case NotChecked = 'NotChecked';
    case NotAuthenticated = 'NotAuthenticated';
    case Error = 'Error';
    case CardNotEnrolled = 'CardNotEnrolled';
    case IssuerNotEnrolled = 'IssuerNotEnrolled';
    case MalformedOrInvalid = 'MalformedOrInvalid';
    case AttemptOnly = 'AttemptOnly';
    case Incomplete = 'Incomplete';
}