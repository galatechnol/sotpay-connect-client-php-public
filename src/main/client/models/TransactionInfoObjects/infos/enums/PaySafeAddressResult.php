<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum PaySafeAddressResult: string {
    case MATCH = 'MATCH';
    case MATCH_ADDRESS_ONLY = 'MATCH_ADDRESS_ONLY';
    case MATCH_ZIP_ONLY = 'MATCH_ZIP_ONLY';
    case NO_MATCH = 'NO_MATCH';
    case NOT_PROCESSED = 'NOT_PROCESSED';
    case UNKNOWN = 'UNKNOWN';

}