<?php
namespace Src\main\client\models\TransactionInfoObjects\infos\enums;
enum StripeD3sResult: string {
    case authenticated = 'authenticated';
    case failed = 'failed';
    case not_supported = 'not_supported';
    case attempt_acknowledged = 'attempt_acknowledged';
    case processing_error = 'processing_error';
}