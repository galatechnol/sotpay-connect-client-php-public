<?php
namespace Src\main\client\models\TransactionInfoObjects\infos;
use Src\main\client\models\TransactionInfo;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoPostcodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\ResultClassification;

class OpayoInfo extends TransactionInfo {
    protected string $reference;
    protected string $referenceSecondary;
    protected OpayoStatusCode $statusCode;
    protected string $statusText;
    protected string $errorCode;
    protected string $errorText;
    protected OpayoSecurityCodeResult $securityCodeResult;
    protected OpayoD3sResult $d3sResult;
    protected OpayoAddressResult $addressResult;
    protected OpayoPostcodeResult $postcodeResult;
    protected CardType $cardType;
    protected string $maskedCardNumber;
    protected bool $applePay;
    protected ResultClassification $securityCodeResultClassification;
    protected ResultClassification $d3sResultClassification;
    protected ResultClassification $addressResultClassification;
    protected ResultClassification $postcodeResultClassification;

    public function getReferenceSecondary(): string
    {
        return $this->referenceSecondary;
    }

    public function getStatusText(): string
    {
        return $this->statusText;
    }

    public function getErrorCode(): string
    {
        return $this->errorCode;
    }
    public function getErrorText(): string
    {
        return $this->errorText;
    }

    public function getSecurityCodeResult(): OpayoSecurityCodeResult
    {
        return $this->securityCodeResult;
    }

    public function getD3sResult(): OpayoD3sResult
    {
        return $this->d3sResult;
    }

    public function getAddressResult(): OpayoAddressResult
    {
        return $this->addressResult;
    }

    public function getPostcodeResult(): OpayoPostcodeResult
    {
        return $this->postcodeResult;
    }

    public function isApplePay(): bool
    {
        return $this->applePay;
    }

    public function getSecurityCodeResultClassification(): ResultClassification
    {
        return $this->securityCodeResultClassification;
    }

    public function getD3sResultClassification(): ResultClassification
    {
        return $this->d3sResultClassification;
    }

    public function getAddressResultClassification(): ResultClassification
    {
        return $this->addressResultClassification;
    }

    public function getPostcodeResultClassification(): ResultClassification
    {
        return $this->postcodeResultClassification;
    }

    public function setReference(string $reference): void {
        $this->reference = $reference;
    }

    public function setReferenceSecondary(string $referenceSecondary): void {
        $this->referenceSecondary = $referenceSecondary;
    }

    public function setStatusCode(OpayoStatusCode $statusCode): void {
        $this->statusCode = $statusCode;
    }

    public function setStatusText(string $statusText): void {
        $this->statusText = $statusText;
    }

    public function setErrorCode(string $errorCode): void {
        $this->errorCode = $errorCode;
    }

    public function setErrorText(string $errorText): void {
        $this->errorText = $errorText;
    }

    public function setSecurityCodeResult(OpayoSecurityCodeResult $securityCodeResult): void {
        $this->securityCodeResult = $securityCodeResult;
    }

    public function setD3sResult(OpayoD3sResult $d3sResult): void {
        $this->d3sResult = $d3sResult;
    }

    public function setAddressResult(OpayoAddressResult $addressResult): void {
        $this->addressResult = $addressResult;
    }

    public function setPostcodeResult(OpayoPostcodeResult $postcodeResult): void {
        $this->postcodeResult = $postcodeResult;
    }

    public function setCardType(CardType $cardType): void {
        $this->cardType = $cardType;
    }

    public function setMaskedCardNumber(string $maskedCardNumber): void {
        $this->maskedCardNumber = $maskedCardNumber;
    }

    public function setApplePay(bool $applePay): void {
        $this->applePay = $applePay;
    }

    public function setSecurityCodeResultClassification(ResultClassification $securityCodeResultClassification): void {
        $this->securityCodeResultClassification = $securityCodeResultClassification;
    }

    public function setD3sResultClassification(ResultClassification $d3sResultClassification): void {
        $this->d3sResultClassification = $d3sResultClassification;
    }

    public function setAddressResultClassification(ResultClassification $addressResultClassification): void {
        $this->addressResultClassification = $addressResultClassification;
    }

    public function setPostcodeResultClassification(ResultClassification $postcodeResultClassification): void {
        $this->postcodeResultClassification = $postcodeResultClassification;
    }
    public static function withBody($JSONBody): OpayoInfo
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString() {
        return
            parent::__toString() .
            "Reference: " . ($this->reference ?? "NULL") . "\n" .
            "Secondary Reference: " . ($this->referenceSecondary ?? "NULL") . "\n" .
            "Status Code: " . ($this->statusCode->value ?? "NULL") . "\n" .
            "Status Text: " . ($this->statusText ?? "NULL") . "\n" .
            "Error Code: " . ($this->errorCode ?? "NULL") . "\n" .
            "Error Text: " . ($this->errorText ?? "NULL") . "\n" .
            "Security Code Result: " . ($this->securityCodeResult->value ?? "NULL") . "\n" .
            "D3S Result: " . ($this->d3sResult->value ?? "NULL") . "\n" .
            "Address Result: " . ($this->addressResult->value ?? "NULL") . "\n" .
            "Postcode Result: " . ($this->postcodeResult->value ?? "NULL") . "\n" .
            "Card Type: " . ($this->cardType->value ?? "NULL") . "\n" .
            "Masked Card Number: " . ($this->maskedCardNumber ?? "NULL") . "\n" .
            "Apple Pay: " . ($this->applePay ? 'Yes' : 'No') . "\n" .
            "Security Code Result Classification: " . ($this->securityCodeResultClassification->value ?? "NULL") . "\n" .
            "D3S Result Classification: " . ($this->d3sResultClassification->value ?? "NULL") . "\n" .
            "Address Result Classification: " . ($this->addressResultClassification->value ?? "NULL") . "\n" .
            "Postcode Result Classification: " . ($this->postcodeResultClassification->value ?? "NULL") . "\n";
    }
}