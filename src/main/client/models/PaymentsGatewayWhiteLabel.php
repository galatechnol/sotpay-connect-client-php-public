<?php

namespace Src\main\client\models;

use Src\main\client\models\bases\Base;
use Src\main\client\models\enums\WhiteLabels;

class PaymentsGatewayWhiteLabel extends Base
{
    private string $object;
    private WhiteLabels $result;
    private bool $cardsOnFileEnabled;


    public function getObject(): string
    {
        return $this->object;
    }
    public function getResult(): WhiteLabels
    {
        return $this->result;
    }
    public function isCardsOnFileEnabled(): bool
    {
        return $this->cardsOnFileEnabled;
    }
    public function setCardsOnFileEnabled(bool $cardsOnFileEnabled): void
    {
        $this->cardsOnFileEnabled = $cardsOnFileEnabled;
    }
    public function setObject(string $object): void
    {
        $this->object = $object;
    }
    public function setResult(WhiteLabels $result): void
    {
        $this->result = $result;
    }

    public function __construct()
    {
    }

    public static function withBody(string $JSONBody): PaymentsGatewayWhiteLabel {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString(): string {
        return "[Object: " . ($this->object ?? 'NULL') . "]\n"
            . "Result: " . ($this->result->value ?? 'NULL') . "\n"
            . "Cards On Files Enabled: " . ($this->cardsOnFileEnabled ? 'true' : 'false');
    }
}