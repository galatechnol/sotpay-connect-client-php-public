<?php

namespace Src\main\client\models\CardsOnFileObjects;

use Src\main\client\models\CardOnFile;
use Src\main\client\models\TransactionObjects\transactionAddress;
use Src\main\client\models\TransactionObjects\TransactionContact;

class CheckoutComCardOnFile extends CardOnFile
{
    protected TransactionContact $contact;
    protected transactionAddress $billingAddress;
    protected string $payerReference;
    protected string $cardReference;

    public static function withBody(string $JSONBody): CheckoutComCardOnFile
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }
    public function __toString(): string {
        return parent::__toString()
            . "\nContact: " . "\n" .($this->contact) . "\n"
            . "Billing Address: " . "\n" . ($this->billingAddress) . "\n"
            . "Payer Reference: " . "\n" . ($this->payerReference) . "\n"
            . "Card Reference: " . "\n" . ($this->billingAddress) . "\n";
    }
}