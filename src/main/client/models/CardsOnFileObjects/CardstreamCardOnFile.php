<?php

namespace Src\main\client\models\CardsOnFileObjects;

use Src\main\client\models\CardOnFile;
use Src\main\client\models\TransactionObjects\transactionAddress;
use Src\main\client\models\TransactionObjects\TransactionContact;

class CardstreamCardOnFile extends CardOnFile
{
    protected TransactionContact $contact;
    protected transactionAddress $billingAddress;
    public static function withBody(string $JSONBody): CardstreamCardOnFile
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }
    public function __toString(): string {
        return parent::__toString()
            . "\nContact: " . "\n" .($this->contact) . "\n"
            . "Billing Address: " . "\n" . ($this->billingAddress) . "\n";
    }
}