<?php

namespace Src\main\client\models\CardsOnFileObjects;

use Src\main\client\models\CardOnFile;
use Src\main\client\models\TransactionObjects\transactionAddress;
use Src\main\client\models\TransactionObjects\TransactionContact;

class OpayoServerCardOnFile extends CardOnFile
{
    protected TransactionContact $contact;
    protected transactionAddress $billingAddress;
    private string $relatedSecurityKey;
    private string $txAuthNo;

    public function setContact(TransactionContact $contact): void {
        $this->contact = $contact;
    }

    public function setBillingAddress(transactionAddress $billingAddress): void {
        $this->billingAddress = $billingAddress;
    }

    public function setRelatedSecurityKey(string $relatedSecurityKey): void {
        $this->relatedSecurityKey = $relatedSecurityKey;
    }

    public function setTxAuthNo(string $txAuthNo): void {
        $this->txAuthNo = $txAuthNo;
    }
    public function getContact(): TransactionContact
    {
        return $this->contact;
    }
    public function getBillingAddress(): transactionAddress
    {
        return $this->billingAddress;
    }
    public function getRelatedSecurityKey(): string
    {
        return $this->relatedSecurityKey;
    }

    public function getTxAuthNo(): string
    {
        return $this->txAuthNo;
    }

    public static function withBody(string $JSONBody): OpayoServerCardOnFile
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString(): string {
        return parent::__toString()
            . "\nContact: " . "\n" .($this->contact) . "\n"
            . "Billing Address: " . "\n" . ($this->billingAddress) . "\n"
            . "Related Security Key: " . ($this->relatedSecurityKey) . "\n"
            . "Tx Auth No: " . ($this->txAuthNo);
    }
}