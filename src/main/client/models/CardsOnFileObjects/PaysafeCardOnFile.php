<?php

namespace Src\main\client\models\CardsOnFileObjects;

use Src\main\client\models\CardOnFile;
use Src\main\client\models\TransactionObjects\transactionAddress;
use Src\main\client\models\TransactionObjects\TransactionContact;

class PaysafeCardOnFile extends CardOnFile
{
    protected TransactionContact $contact;
    protected transactionAddress $billingAddress;
    protected string $cardReference;
    protected string $payerReference;

    public function setContact(TransactionContact $contact): void {
        $this->contact = $contact;
    }

    public function setBillingAddress(transactionAddress $billingAddress): void {
        $this->billingAddress = $billingAddress;
    }

    public function setCardReference(string $cardReference): void {
        $this->cardReference = $cardReference;
    }

    public function setPayerReference($payerReference): void {
        $this->payerReference = $payerReference;
    }

    public static function withBody(string $JSONBody) : PaysafeCardOnFile
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString(): string {
        return parent::__toString()
            . "\nContact: " . "\n" .($this->contact) . "\n"
            . "Billing Address: " . "\n" . ($this->billingAddress) . "\n"
            . "Payer Reference: " . ($this->payerReference) . "\n"
            . "Card Reference: " . ($this->cardReference);
    }
}