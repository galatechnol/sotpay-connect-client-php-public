<?php

namespace Src\main\client\models\CardsOnFileObjects;

use Src\main\client\models\CardOnFile;
use Src\main\client\models\TransactionObjects\transactionAddress;
use Src\main\client\models\TransactionObjects\TransactionContact;

class GlobalPaymentsRealexCardOnFile extends CardOnFile
{
    protected TransactionContact $contact;
    protected transactionAddress $billingAddress;
    protected string $cardReference;
    protected string $payerReference;

    /**
     * @return string
     */
    public function getCardReference(): string
    {
        return $this->cardReference;
    }

    public function getPayerReference(): string
    {
        return $this->payerReference;
    }

    public function setPayerReference(string $payerReference): void
    {
        $this->payerReference = $payerReference;
    }
    public function setCardReference(string $cardReference): void
    {
        $this->cardReference = $cardReference;
    }
    public static function withBody(string $JSONBody) : GlobalPaymentsRealexCardOnFile
    {
        $instance = new self();
        $instance->set($JSONBody);
        return $instance;
    }

    public function __toString(): string {
        return parent::__toString()
            . "\nContact: " . "\n" .($this->contact) . "\n"
            . "Billing Address: " . "\n" . ($this->billingAddress) . "\n"
            . "Payer Reference: " . ($this->payerReference) . "\n"
            . "Card Reference: " . ($this->cardReference);
    }
}