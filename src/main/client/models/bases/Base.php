<?php
namespace Src\main\client\models\bases;
use PHPUnit\Logging\Exception;
use JsonMapper;
use stdClass;

class Base {
    protected function set(string $body)
    {
        $data = json_decode($body);
        //Check for valid JSON
        if ($data) {
            $mapper = new JsonMapper();
            $mapper->bIgnoreVisibility = true; //allow private access
            $mapper->bExceptionOnUndefinedProperty = true; //if we get unexpected properties
            $obj = $mapper->map($data, $this);
            return $obj;

        } else { // sending non-JSON data
            throw new Exception('Wrong data type sent, expected JSON body');
        }
    }
}