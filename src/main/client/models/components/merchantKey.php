<?php
namespace Src\main\client\models\components;
use JsonSerializable;
use Src\main\client\models\enums\Domain;

class merchantKey implements JsonSerializable
{
    private string $merchant;
    private Domain $domain;

    public function __construct(
        string $merchant,
        Domain $domain
    ) {
        $this->merchant = $merchant;
        $this->domain = $domain;
    }
    public function exists($property) {
        return isset($this->$property);
    }

    public function getMerchant(): string {
        return $this->merchant;
    }

    public function getDomain(): Domain {
        return $this->domain;
    }

    public function jsonSerialize(): object
    {
        return (object) [
            'merchant' => $this->merchant,
            'domain' => $this->domain
        ];
    }

    public function __toString(): string {
        return
            'merchant:-> ' . $this->merchant . ', ' . 'domain-> ' .   $this->domain->value;
    }
}