<?php
namespace Src\main\client\models\components;
class YearMonth {
    private string $year;
    private string $month;
    private string $monthValue;
    private bool $leapYear;

    public function setYear($val): string {
        preg_match('/[0-9]{4}/',$val, $year);
        return $year[0];
    }

    public function setMonth($val) {
        $month = explode("-", $val);
        $this->monthValue = (int)$month[1];
        return $month[1];
    }

    // Constructor remains the same

    private function isLeapYear($year) {
        return ($year % 4 == 0 && ($year % 100 != 0 || $year % 400 == 0));
    }

    public function __construct($value) {
        $this->year = $this->setYear($value);
        $this->month = $this->setMonth($value);
        $this->leapYear = $this->isLeapYear($this->year);
    }
}