<?php

namespace Src\main\client\models\components;

use JsonSerializable;

class contact implements JsonSerializable {
    private string $email;
    private string $phone;


    public function getEmail(): string
    {
        return $this->email;
    }
    public function getPhone(): string
    {
        return $this->phone;
    }

    public function __construct(string $email, string $phone) {
        $this->email = $email;
        $this->phone = $phone;
    }
    public function exists($property) {
        return isset($this->$property);
    }
    public function jsonSerialize(): array
    {
        return [
            'email' => $this->email,
            'phone' => $this->phone
        ];
    }

    public function __toString() {
        return "Email: " . ($this->email ?? 'NULL') . "\n"
            . "Phone: " . ($this->phone ?? 'NULL') . "\n";
    }

}