<?php

namespace Src\main\client\models\components;

class Page {
private string $pageNumber;
private string $pageSize;
private string $totalElements;
private string $totalPages;

    public function __construct($pageNumber, $pageSize, $totalElements, $totalPages) {
        $this->pageNumber = $pageNumber;
        $this->pageSize = $pageSize;
        $this->totalElements = $totalElements;
        $this->totalPages = $totalPages;
    }
    public function getTotalPages(): string
    {
        return $this->totalPages;
    }
    public function getPageNumber(): string
    {
        return $this->pageNumber;
    }
    public function getPageSize(): string
    {
        return $this->pageSize;
    }
    public function getTotalElements(): string
    {
        return $this->totalElements;
    }
    public function __toString(): string {
        return "\n\nPage Number: $this->pageNumber\n"
            . "Page Size: $this->pageSize\n"
            . "Total Elements: $this->totalElements\n"
            . "Total Pages: " . ($this->totalPages ) . "\n";
    }
}