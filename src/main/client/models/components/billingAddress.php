<?php

namespace Src\main\client\models\components;

use JsonSerializable;
use Src\main\client\models\enums\Country;

class billingAddress implements JsonSerializable
{
    private string $firstName;
    private string $surname;
    private string $displayName;
    private string $line1;
    private string $line2;
    private string $city;
    private string $postcode;
    private string $state;
    private Country $country;

    public function __construct(
        string $firstName,
        string $surname,
        string $displayName,
        string $line1,
        string $line2,
        string $city,
        string $postcode,
        string $state,
        Country $country
    ) {
        $this->firstName = $firstName;
        $this->surname = $surname;
        $this->displayName = $displayName;
        $this->line1 = $line1;
        $this->line2 = $line2;
        $this->city = $city;
        $this->postcode = $postcode;
        $this->state = $state;
        $this->country = $country;
    }
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getCountry(): Country
    {
        return $this->country;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLine1(): string
    {
        return $this->line1;
    }

    public function getLine2(): string
    {
        return $this->line2;
    }

    public function getPostcode(): string
    {
        return $this->postcode;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function exists($property) {
        return isset($this->$property);
    }

    public function jsonSerialize(): array
    {
        return [
            'firstName' => $this->firstName,
            'surname' => $this->surname,
            'displayName' => $this->displayName,
            'line1' => $this->line1,
            'line2' => $this ->line2,
            'city' => $this->city,
            'postcode' => $this ->postcode,
            'state' => $this->state,
            'country' => $this ->country->value,
        ];
    }

    public function __toString() {
        return "First Name: " . ($this->firstName ?? 'NULL') . "\n" .
            "Surname: " . ($this->surname ?? 'NULL') . "\n" .
            "Display Name: " . ($this->displayName ?? 'NULL') . "\n" .
            "Line 1: " . ($this->line1 ?? 'NULL') . "\n" .
            "Line 2: " . ($this->line2 ?? 'NULL') . "\n" .
            "City: " . ($this->city ?? 'NULL') . "\n" .
            "Postcode: " . ($this->postcode ?? 'NULL') . "\n" .
            "State: " . ($this->state ?? 'NULL') . "\n" .
            "Country: " . ($this->country->value ?? 'NULL') . "\n";
    }



}