<?php

namespace Src\main\client\models\components;

use JsonSerializable;

class TransactionWriteMessage implements JsonSerializable
{
    private bool $email;
    private bool $sms;

    public function __construct($email, $sms) {
        $this->email = $email;
        $this->sms = $sms;
    }

    public function jsonSerialize():array {
        return [
            'email' => $this->email,
            'sms' => $this->sms
        ];
    }

    public function __toString() {
        return "Email: " . ($this->email ?? 'NULL') . "\n"
            . "Phone: " . ($this->phone ?? 'NULL') . "\n";
    }
}