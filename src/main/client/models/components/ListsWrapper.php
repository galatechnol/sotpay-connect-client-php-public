<?php

namespace Src\main\client\models\components;

class ListsWrapper {
    private array $list;
    private Page $pages;

    public function __construct(array $list, Page $pages) {
        $this->set($list, $pages);
    }

    private function set(array $list, Page $pages): void
    {
        $this->list = $list;
        $this->pages = $pages;
    }

    // Getter for the 'list' property
    public function getList(): array {
        return $this->list;
    }

    // Getter for the 'pages' property
    public function getPages(): Page {
        return $this->pages;
    }
}