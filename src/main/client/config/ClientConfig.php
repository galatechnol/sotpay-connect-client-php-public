<?php

// Importing our constants and enums types

namespace Src\main\client\config;
use Exception;
use Src\main\client\constants\Constants;
use Src\main\client\models\enums\Domain;
class ClientConfig
{
    private string $baseUrl;
    private string $merchant;
    private Domain $domain;
    private string $merchantToken;
    private string $signatureKeyInbound;
    private string $tokenAdminKey;

    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    public function setBaseUrl(string $url): void
    {
        $this->validateURL($url);
        $this->baseUrl = $url;
    }

    public function validateURL(string $url)
    {
        if (!preg_match(Constants::$URL_MATCH, $url)) {
            throw new Exception('Base URL must begin with https://');
        }
    }

    public function getMerchant(): string
    {
        return $this->merchant;
    }

    public function setMerchant($merchant): void
    {
        $this->merchant = $merchant;
    }

    public function getDomain(): Domain
    {
        return $this->domain;
    }

    public function setDomain(Domain $domain): void
    {
        $this->domain = $domain;
    }

    public function getMerchantToken(): string
    {
        return $this->merchantToken;
    }

    public function setMerchantToken(string $merchantToken): void
    {
        $this->merchantToken = $merchantToken;
    }

    public function getSignatureKeyInbound(): string
    {
        return $this->signatureKeyInbound;
    }

    public function setSignatureKeyInbound(string $signatureKeyInbound): void
    {
        $this->signatureKeyInbound = $signatureKeyInbound;
    }

    public function getTokenAdminKey(): string
    {
        return $this->tokenAdminKey;
    }

    public function setTokenAdminKey(string $tokenAdminKey): void
    {
        $this->tokenAdminKey = $tokenAdminKey;
    }

    public function __construct(string $baseUrl, string $merchant, Domain $domain, string $merchantToken,
                                string $signatureKeyInbound, string $tokenAdminKey)
    {

        //check for valid input of URL
        $this->setBaseUrl($baseUrl);
        $this->setMerchantToken($merchantToken);
        $this->setDomain($domain);
        $this->setMerchant($merchant);
        $this->setSignatureKeyInbound($signatureKeyInbound);
        $this->setTokenAdminKey($tokenAdminKey);
    }

    public function validateGetCardOnFile(): void
    {
        if (empty($this ->getMerchantToken())) {
            throw new Exception('GetCardOnFile(): ClientConfig->MerchantToken is not set');
        }
    }

    public function validateDuplicateTransaction(): void
    {
        if (empty($this ->getMerchantToken())) {
            throw new Exception('duplicateTransaction(): ClientConfig->MerchantToken is not set');
        }
    }

    public function validateIsValidExpireTransaction(): void
    {
        if (empty($this ->getMerchantToken())) {
            throw new Exception('isValidExpireTransaction(): ClientConfig->MerchantToken is not set');
        }
    }
    public function validateIsValidDuplicateTransaction(): void
    {
        if (empty($this ->getMerchantToken())) {
            throw new Exception('isValidDuplicateTransaction(): ClientConfig->MerchantToken is not set');
        }
    }
    public function validateIsValidResendTransaction(): void
    {
        if (empty($this ->getMerchantToken())) {
            throw new Exception('isValidResendTransaction(): ClientConfig->MerchantToken is not set');
        }
    }

    public function validateResendTransaction(): void
    {
        if (empty($this ->getMerchantToken())) {
            throw new Exception('resendTransaction(): ClientConfig->MerchantToken is not set');
        }
    }

    public function validateExpireTransaction(): void
    {
        if (empty($this ->getMerchantToken())) {
            throw new Exception('expireTransaction(): ClientConfig->MerchantToken is not set');
        }
    }
    public function validateGetCardsOnFile() : void
    {
        if (empty($this ->getMerchantToken())) {
            throw new Exception('GetCardsOnFile(): ClientConfig->MerchantToken is not set');
        }
        if (empty($this ->getMerchant())) {
            throw new Exception('GetCardsOnFile(): ClientConfig->Merchant is not set');
        }
        //Mostly Unreachable
        if (empty($this ->getDomain())) {
            throw new Exception('GetCardsOnFile(): ClientConfig->Domain is not set');
        }
    }
    public function validateCreateToken(): void
    {
        if (empty($this ->getTokenAdminKey())) {
            throw new Exception('CreateToken(): ClientConfig->TokenAdminKey is not set');
        }
    }
    public function validateDeleteToken(): void
    {
        if (empty($this ->getMerchant())) {
            throw new Exception('DeleteToken(): ClientConfig->Merchant is not set');
        }
        //Mostly Unreachable
        if (empty($this ->getDomain())) {
            throw new Exception('DeleteToken(): ClientConfig->Domain is not set');
        }
        if (empty($this ->getTokenAdminKey())) {
            throw new Exception('DeleteToken(): ClientConfig->TokenAdminKey is not set');
        }
    }
    public function validateGetTransaction(): void
    {
        if (empty($this ->getMerchantToken())) {
            throw new Exception('GetTransaction(): ClientConfig->MerchantToken is not set');
        }
    }

    public function validateCreateTransaction(): void
    {
        if (empty($this ->getMerchantToken())) {
            throw new Exception('CreateTransaction(): ClientConfig->MerchantToken is not set');
        }
    }
    public function validateGetTransactionInfo(): void
    {
        if (empty($this ->getMerchantToken())) {
            throw new Exception('GetTransactionInfo(): ClientConfig->MerchantToken is not set');
        }
    }
    public function validateGetTransactions(): void
    {
        if (empty($this ->getMerchantToken())) {
            throw new Exception('GetTransactions(): ClientConfig->MerchantToken is not set');
        }
        if (empty($this ->getMerchant())) {
            throw new Exception('GetTransactions(): ClientConfig->Merchant is not set');
        }
        //Mostly Unreachable
        if (empty($this ->getDomain())) {
            throw new Exception('GetTransactions(): ClientConfig->Domain is not set');
        }
    }

    public function validateGetPaymentGatewayWhiteLabel(): void
    {
        if (empty($this ->getMerchant())) {
            throw new Exception('GetPaymentGatewayWhiteLabel(): ClientConfig->Merchant is not set');
        }
        //Mostly Unreachable
        if (empty($this ->getDomain())) {
            throw new Exception('GetPaymentGatewayWhiteLabel(): ClientConfig->Domain is not set');
        }
    }

    public function validateGetMerchant(): void
    {
        if (empty($this->getMerchant())) {
            throw new Exception('GetMerchant(): ClientConfig->Merchant is not set');
        }
    }

}