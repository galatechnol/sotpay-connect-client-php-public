<?php
namespace Src\main\client\constants;
class Constants {
    public static string $BASE_URL = 'connect.sotpay.co.uk';
    public static string $PATH_CARD_ON_FILE = '/cardonfile';
    public static string $PATH_DUPLICATE = "/duplicate";
    public static string $PATH_EXPIRE = "/expire";
    public static string $PATH_HEARTBEAT = "/heartbeat";
    public static string $PATH_INFO = "/info";
    public static string $PATH_MERCHANTTOKEN = "/merchanttoken";
    public static string $PATH_RESENDPAYMESSAGE = "/resendpaymessage";
    public static string $PATH_ROOT = "/api";
    public static string $PATH_TESTCARD = "/testcard";
    public static string $PATH_TRANSACTION = "/transaction";

    public static string $PATH_PAYMENTGATEWAY_WHITELABEL = "/paymentgatewaywhitelabel";
    public static string $PATH_PAYMENTGATEWAY_ICON = "/paymentgatewayicon";

    public static string $PATH_MERCHANT = "/merchant";
    public static string $PATH_MERCHANT_RELATIONSHIP = "/merchantrelationship";

    public static string $PATH_ENABLED_SUBMERCHANTS = "/enabledsubmerchants";
    public static string $PATH_ISVALID = "/isvalid";

    public static string $URL_MATCH = '/^(http|https):\/\/\S+\.\S+$/';
    public static string $URN_MATCH = '/^STN-[D|T|P][A-Z][A-Z0-9]-[A-Z0-9]{4}[S|L]-[A-Z](00|01|02|03|04|05|06|07|08|09|10|11|12)(00|01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31)-[A-Z0-9]{5}$/';

    public static string $ID_MATCH = '/^CDF-[D|T|P][A-Z][A-Z0-9]-[A-Z0-9]{4}[S|L]-[A-Z](00|01|02|03|04|05|06|07|08|09|10|11|12)(00|01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31)-(AQ|BY|CS|DC|EL|EV|FM|GP|GU|NU|PS|P3|SC|SY|S0|Y0)[A-Z0-9]{3}$/';


    public static string $INFO_MATCH = '/{(\s+"|")object.+"/';
}