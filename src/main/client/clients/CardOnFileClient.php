<?php
namespace Src\main\client\clients;
use DateTime;
use Exception;
use GuzzleHttp\Client;
use JsonMapper;
use Src\main\client\config\ClientConfig;
use Src\main\client\constants\Constants;
use Src\main\client\models\components\ListsWrapper;
use Src\main\client\models\components\Page;
use Src\main\client\models\enums\NumberFilterOperator;
use Src\main\client\models\enums\PaymentGateway;
use Src\main\client\models\enums\sortOrder;
use Src\main\client\models\enums\sortPropertyCardOnFile;
use Src\main\client\models\enums\StringFilterOperator;
use Src\main\client\models\ResponseError;

class CardOnFileClient {
    private Client $client;
    public function  __construct(Client $client) {
        $this->setClient($client);
    }
    private function setClient(Client $client): void {

        $this->client = $client;
    }
    function getCardOnFile(ClientConfig $clientConfig, string $id, string $locale) {
        //Validate our Client Config
        $clientConfig->validateGetCardOnFile();
        //Building URI
        if (preg_match(Constants::$ID_MATCH, $id)) {
            $uri = $clientConfig->getBaseUrl();
            $uri .= Constants::$PATH_ROOT;
            $uri .= Constants::$PATH_CARD_ON_FILE;
            $uri .= '/' . $id;
            //Building our headers
            $headers = [
                'accept' => 'application/json;charset=UTF-8',
                'Accept-Language' => $locale,
                'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken()];
        }
        else {
            throw new Exception('Invalid ID format');
        }

        $response = $this->client->request('GET',$uri, [
            'headers' => $headers,
            'http_errors' => false,
        ]);

        if (isset($_GET['action'])) {
            return [
                'body' => json_decode($response->getBody()),
            ];
        }

        if ($response->getStatusCode()) {
            //using switch instead of match due to logic
            switch ($response->getStatusCode()) {
                case 200:
                    //Dynamically chooses the Info Class based on First JSON property
                    if (preg_match(Constants::$INFO_MATCH, $response->getBody(), $matches)) {
                        $gateway = $matches[0] . '}'; //add the end bracket back
                        $gateway = json_decode($gateway, true);
                        $class = 'Src\main\client\models\CardsOnFileObjects\\' . $gateway['object'];
                        return $class::withBody($response->getBody());
                    }
                    else {
                        throw new Exception('Unknown Gateway, could not find "object" property in JSON');
                    }
                case 204:
                    return "Card On File not found";
                case 400:
                case 401:
                case 500:
                    return ResponseError::withBody($response->getBody());
                case 404:
                    throw new Exception('Error 404 Not Found, Invalid Relative URL');
                default:
                    throw new Exception('Response code not recognised, please check Request');
            }

        }
        //Falsy Status Codes
        else {
            throw new Exception('Status Code is invalid, please check request');
        }
    }

    function getCardsOnFile(
        ClientConfig $clientConfig,
        PaymentGateway $paymentGateway,
        ?DateTime $createdFrom = null,
        ?DateTime $createdTo = null,
        ?DateTime $expiryFrom = null,
        ?DateTime $expiryTo = null,
        ?string $idFilter = null,
        ?StringFilterOperator $idFilterOperator = null,
        ?int $pageSize = null,
        ?int $pageNumber = null,
        ?sortPropertyCardOnFile $sortProperty = null,
        ?sortOrder $sortOrder = null,
        ?string $referenceFilter = null,
        ?StringFilterOperator $referenceFilterOperator = null,
        ?string $firstUrnFilter = null,
        ?StringFilterOperator $firstUrnFilterOperator = null,
        ?string $firstReferenceFilter = null,
        ?StringFilterOperator $firstReferenceFilterOperator = null,
        ?string $cardTypeFilter = null,
        ?string $chargeCountFilter = null,
        ?NumberFilterOperator $chargeCountFilterOperator = null,
        ?string $maskedCardNumberFilter = null,
        ?StringFilterOperator $maskedCardNumberFilterOperator = null,
        ?string $cardholderFilter = null,
        ?StringFilterOperator $cardholderFilterOperator = null,
        ?string $contactPhoneFilter = null,
        ?StringFilterOperator $contactPhoneFilterOperator = null,
        ?string $contactEmailFilter = null,
        ?StringFilterOperator $contactEmailFilterOperator = null,
        string $locale = 'en',
    ) {
        $clientConfig->validateGetCardsOnFile();
        $uri = $clientConfig->getBaseUrl();
        $uri .= Constants::$PATH_ROOT;
        $uri .= Constants::$PATH_CARD_ON_FILE;
        $uri .= '/' . $clientConfig->getMerchant() . '/' . $clientConfig->getDomain()->value;
        $uri .= '/' . $paymentGateway->value;
        $uriArr = array_merge(
            isset($createdFrom) ? array('createdFrom' => $createdFrom->format('Y-m-d\TH:i:s\Z')): array(),
            isset($createdTo) ? array('createdTo' => $createdTo->format('Y-m-d\TH:i:s\Z')): array(),
            isset($expiryFrom) ? array('expiryFrom' => $expiryFrom->format('Y-m')): array(),
            isset($expiryTo) ? array('expiryTo' => $expiryTo->format('Y-m')): array(),
            isset($idFilter) ? array('idFilter' => $idFilter): array(),
            isset($idFilterOperator) ? array('idFilterOperator' => $idFilterOperator->value): array(),
            isset($pageSize) ? array('pageSize' => $pageSize): array(),
            isset($pageNumber) ? array('pageNumber' => $pageNumber): array(),
            isset($sortProperty) ? array('sortProperty' => $sortProperty->value): array(),
            isset($sortOrder) ? array('sortOrder' => $sortOrder->value): array(),
            isset($referenceFilter) ? array('referenceFilter' => $referenceFilter): array(),
            isset($referenceFilterOperator) ? array('referenceFilterOperator' => $referenceFilterOperator->value): array(),
            isset($firstUrnFilter) ? array('firstUrnFilter' => $firstUrnFilter): array(),
            isset($firstUrnFilterOperator) ? array('firstUrnFilterOperator' => $firstUrnFilterOperator->value): array(),
            isset($firstReferenceFilter) ? array('firstReferenceFilter' => $firstReferenceFilter): array(),
            isset($firstReferenceFilterOperator) ? array('firstReferenceFilterOperator' => $firstReferenceFilterOperator->value): array(),
            isset($chargeCountFilter) ? array('chargeCountFilter' => $chargeCountFilter): array(),
            isset($chargeCountFilterOperator) ? array ('chargeCountFilterOperator' => $chargeCountFilterOperator->value) : array(),
            isset($maskedCardNumberFilter) ? array('maskedCardNumberFilter' => $maskedCardNumberFilter): array(),
            isset($maskedCardNumberFilterOperator) ? array('maskedCardNumberFilterOperator' => $maskedCardNumberFilterOperator->value): array(),
            isset($cardholderFilter) ? array('cardholderFilter' => $cardholderFilter): array(),
            isset($cardholderFilterOperator) ? array('cardholderFilterOperator' => $cardholderFilterOperator->value): array(),
            isset($contactPhoneFilter) ? array('contactPhoneFilter' => $contactPhoneFilter): array(),
            isset($contactPhoneFilterOperator) ? array('contactPhoneFilterOperator' => $contactPhoneFilterOperator->value): array(),
            isset($contactEmailFilter) ? array('contactEmailFilter' => $contactEmailFilter): array(),
            isset($contactEmailFilterOperator) ? array('contactEmailFilterOperator' => $contactEmailFilterOperator->value): array(),
        );
        if (http_build_query($uriArr)) {
            $uri .= '?' . http_build_query($uriArr);
        }

        //Appending our arrayed search filters
        $uri .= isset($cardTypeFilter) ? implode('', array_map(fn($val) => '&cardTypeFilter=' . $val ,explode(',', $cardTypeFilter))) : '';

        //Building our headers`
        $headers = [
            'accept' => 'application/json;charset=UTF-8',
            'Accept-Language' => $locale,
            'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken()];
        $response = $this->client->request('GET',$uri, [
            'headers' => $headers,
            'http_errors' => false,
        ]);
        //AJAX return
        if (isset($_GET['action'])) {
            return [
                'body' => json_decode($response->getBody()),
                'pagenumber' => $response->getHeader('pagenumber'),
                'pagesize' => $response->getHeader('pagesize'),
                'totalpages' => $response->getHeader('totalpages'),
                'totalelements' => $response->getHeader('totalelements')
            ];
        }
        else {
            switch ($response->getStatusCode()) {
                case 200:
                    $data = json_decode($response->getBody());
                    if (is_array($data)) {
                        //mapper configs
                        $mapper = new JsonMapper();
                        $mapper->bIgnoreVisibility = true; //allow private access
                        $mapper->bExceptionOnUndefinedProperty = true; //if we get unexpected properties
                        //List and Pages
                        $paymentGateway = 'Src\main\client\models\CardsOnFileObjects\\' . $paymentGateway->toCardOnFile();
                        $list = $mapper->mapArray($data, array(), $paymentGateway);

                        $pageNumber = $response->getHeader('pagenumber'); //arr with each link
                        $pageSize = $response->getHeader('pagesize');
                        $totalElements = $response->getHeader('totalelements');
                        $totalPages = $response->getHeader('totalpages');
                        $page = new Page($pageNumber[0], $pageSize[0], $totalElements[0], $totalPages[0]);
                        return new ListsWrapper($list, $page);
                    }
                    else { // sending non-JSON data
                        throw new Exception('Wrong data type sent, expected JSON body');
                    }
                case 204:
                    return null;
                case 400:
                case 401:
                case 500:
                    return ResponseError::withBody($response->getBody());
                case 404:
                    throw new Exception('Error 404 Not Found, Invalid Relative URL');
                default:
                    throw new Exception('Response code not recognised, please check Request');
            }
        }
    }
}