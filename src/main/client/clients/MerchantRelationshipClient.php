<?php

namespace Src\main\client\clients;

use Exception;
use GuzzleHttp\Client;
use Src\main\client\config\ClientConfig;
use Src\main\client\constants\Constants;
use Src\main\client\models\components\merchantKey;
use Src\main\client\models\enums\Domain;
use Src\main\client\models\ResponseError;

class MerchantRelationshipClient
{
    private Client $client;
    public function  __construct(Client $client) {
        $this->setClient($client);
    }
    private function setClient(Client $client): void {

        $this->client = $client;
    }

    function getEnabledSubMerchants(ClientConfig $clientConfig,
                                    merchantKey $merchantKey,
                                    bool $includeMerchant = null,
                                    bool $includeSubMerchants = null,
                                    string $locale = 'en') {

        $clientConfig->validateGetPaymentGatewayWhiteLabel();
        //Build URI
        $uri = $clientConfig->getBaseUrl();
        $uri .= Constants::$PATH_ROOT;
        $uri .= Constants::$PATH_MERCHANT_RELATIONSHIP;
        $uri .= '/' . $merchantKey->getMerchant();
        $uri .= Constants::$PATH_ENABLED_SUBMERCHANTS;

        $uriArr = array_merge(
            $includeMerchant ? array('includeMerchant' => 'true') : array(),
            $includeSubMerchants ? array('includeSubMerchants' => 'true') : array(),
        );

        if (http_build_query($uriArr)) {
            $uri .= '?' . http_build_query($uriArr);
        }

        //Building our headers
        $headers = [
            'accept' => 'application/json;charset=UTF-8',
            'Accept-Language' => $locale,
            'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken(),
            'Connection' => 'keep-alive',
        ];

        $response = $this->client->request('GET', $uri, [
            'headers' => $headers,
            'http_errors' => false
        ]);

        if (isset($_GET['action'])) {
            return [
                'code' => json_decode($response->getStatusCode()),
                'body' => json_decode($response->getBody())
            ];
        }

        if ($response->getStatusCode()) {
            return match ($response->getStatusCode()) {
                200 => json_decode($response->getBody()),
                204 => 'No sub-Merchants exist',
                400, 401, 500 => ResponseError::withBody($response->getBody()),
                404 => throw new Exception('Error 404 Not Found, Invalid Relative URL'),
                default => throw new Exception('Response code not recognised, please check Request')
            };
        }
        else {
            throw new Exception('Status Code is invalid, please check request');
        }
    }
}