<?php

namespace Src\main\client\clients;

use Exception;
use GuzzleHttp\Client;
use Src\main\client\config\ClientConfig;
use Src\main\client\constants\Constants;
use Src\main\client\models\MerchantToken;
use Src\main\client\models\MerchantTokenWrite;
use Src\main\client\models\ResponseError;
use Src\main\client\models\utils\JWSSignature;

class MerchantTokenClient {
    private Client $client;

    public function __construct($client) {
        $this->setClient($client);
    }

    private function setClient(Client $client): void {
        $this->client = $client;
    }

    public function createToken(ClientConfig $clientConfig,MerchantTokenWrite $token, string $locale) {
        //Validation
        $clientConfig->validateCreateToken();

        //encoding data
        $data = json_encode($token);
        //building request URL
        $url = $clientConfig->getBaseUrl();
        $url .= Constants::$PATH_ROOT . Constants::$PATH_MERCHANTTOKEN;
        //signing payload, if its activate
        //will still work regardless
        $signature = JWSSignature::createSignature($clientConfig->getSignatureKeyInbound(), $data);
        $headers = [
            'accept' => 'application/json;charset=UTF-8',
            'x-jws-signature' => $signature,
            'Accept-Language' => $locale,
            'Authorization' => 'Bearer ' . $clientConfig->getTokenAdminKey(),
            'Content-Type' => 'application/json;charset=UTF-8'
        ];
        $options = [
            'body' => $data,
            'headers' => $headers,
            'http_errors' => false,
        ];
        $response = $this->client->request('POST', $url, $options);

        if (isset($_GET['action'])) {
            return [
                'body' => json_decode($response->getBody()),
            ];
        }

        if ($response->getStatusCode()) {
            return match ($response->getStatusCode()) {
                201 => MerchantToken::withBody($response->getBody()),
                400, 401, 403, 422, 500 => ResponseError::withBody($response->getBody()),
                404 => throw new Exception('Error 404 Not Found, Invalid Relative URL'),
                default => throw new Exception('Response code not recognised, please check Request')
            };
        }
        else {
            throw new Exception('Status Code is invalid, please check request');
        }
    }

    public function deleteToken(ClientConfig $clientConfig, int $id,string $locale) {
        //Validation
        $clientConfig->validateDeleteToken();
        $url = $clientConfig->getBaseUrl();
        $url .= Constants::$PATH_ROOT . Constants::$PATH_MERCHANTTOKEN;
        $url .= '/' . $clientConfig->getMerchant();
        $url .= '/' . $clientConfig->getDomain()->value;
        $url .= '/' . $id; //MerchantToken ID
        $headers = [
            'accept' => 'application/json;charset=UTF-8',
            'Accept-Language' => $locale,
            'Authorization' => 'Bearer ' . $clientConfig->getTokenAdminKey()
        ];
        $options = [
            'headers' => $headers,
            'http_errors' => false,
        ];
        $response = $this->client->request('DELETE', $url, $options);

        if (isset($_GET['action'])) {
            return [
                'body' => json_decode($response->getStatusCode()),
            ];
        }

        if ($response->getStatusCode()) {
            return match ($response->getStatusCode()) {
                204 => 'Successfully Deleted',
                400, 401, 403, 404, 500 => ResponseError::withBody($response->getBody()),
                default => throw new Exception('Response code not recognised, please check Request')
            };
        }
        else {
            throw new Exception('Status Code is invalid, please check request');
        }
    }
}