<?php

namespace Src\main\client\clients;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Src\main\client\config\ClientConfig;
use Src\main\client\constants\Constants;
use Src\main\client\models\components\merchantKey;
use Src\main\client\models\enums\Domain;
use Src\main\client\models\enums\PaymentGateway;
use Src\main\client\models\enums\WhiteLabels;
use Src\main\client\models\ResponseError;
use Src\main\client\models\PaymentsGatewayWhiteLabel;

class PaymentGatewayClient {
    private Client $client;
    public function  __construct(Client $client) {
        $this->setClient($client);
    }
    private function setClient(Client $client): void {

        $this->client = $client;
    }

    function getPaymentGatewayWhiteLabel(ClientConfig $clientConfig, merchantKey $merchantKey, PaymentGateway $paymentGateway, string $locale) {

        $clientConfig->validateGetPaymentGatewayWhiteLabel();
        //Build URI
        $uri = $clientConfig->getBaseUrl();
        $uri .= Constants::$PATH_ROOT;
        $uri .= Constants::$PATH_PAYMENTGATEWAY_WHITELABEL;
        $uri .= '/' . $merchantKey->getMerchant();
        $uri .= '/' . Domain::toString($merchantKey->getDomain());
        $uri .= '/' . $paymentGateway->value;
        //Building our headers
        $headers = [
            'accept' => 'application/json;charset=UTF-8',
            'Accept-Language' => $locale,
            'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken(),
            'Connection' => 'keep-alive',
        ];

        $response = $this->client->request('GET', $uri, [
            'headers' => $headers,
            'http_errors' => false
        ]);

        if (isset($_GET['action'])) {
            return [
                'code' => json_decode($response->getStatusCode()),
                'body' => json_decode($response->getBody())
            ];
        }

        if ($response->getStatusCode()) {
            return match ($response->getStatusCode()) {
                200 => PaymentsGatewayWhiteLabel::withBody($response->getBody()),
                400, 401, 500 => ResponseError::withBody($response->getBody()),
                404 => throw new Exception('Error 404 Not Found, Invalid Relative URL'),
                default => throw new Exception('Response code not recognised, please check Request')
            };
        }
        else {
            throw new Exception('Status Code is invalid, please check request');
        }
    }

    //FRONT END USAGE ONLY
    function getAllPaymentGatewayWhiteLabel(ClientConfig $clientConfig, merchantKey $merchantKey, string $locale) {
        $clientConfig->validateGetPaymentGatewayWhiteLabel();
        //Build URI
        $uri = $clientConfig->getBaseUrl();
        $uri .= Constants::$PATH_ROOT;
        $uri .= Constants::$PATH_PAYMENTGATEWAY_WHITELABEL;
        $uri .= '/' . $merchantKey->getMerchant();
        $uri .= '/' . Domain::toString($merchantKey->getDomain());
        //Building our headers
        $headers = [
            'accept' => 'application/json;charset=UTF-8',
            'Accept-Language' => $locale,
            'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken()
        ];
        //Promises
        $promises = [
            $this->client->getAsync($uri . '/acquired' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/barclaycard' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/cardstream' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/checkoutcom' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/elavon' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/encoded' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/evopayments' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/freedompay' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/globalpaymentsrealex' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/globalpayments' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/nuapay' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/opayo' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/opayoserver' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/pay360' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/paysafe' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/scp' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/stripe' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/sybc' , [
                'headers' => $headers
            ]),
            $this->client->getAsync($uri . '/trustpayments' , [
                'headers' => $headers
            ])
        ];
        $responses = Promise\Utils::settle($promises)->wait();

        // Loop through the settled responses
        $returnArr = [];

        foreach ($responses as $response) {
            if ($response['state'] === 'fulfilled') {
                $statusCode = $response['value']->getStatusCode();
                if ($statusCode === 200) {
                    // Successful response - echo the body
                    $returnArr[] = json_decode($response['value']->getBody());
                }
                else {
                    return [
                        "code" => 500,
                        "body" => 'Could not fetch all Payment Gateways Enabled'
                    ];
                }
            } else {
                // Fail - return 500 code + Error msg
                return [
                    "code" => 500,
                    "body" => $response['reason'] . PHP_EOL
                ];
            }
        }
        if (isset($_GET['action'])) {
            return [
                'code' => 200,
                'body' => $returnArr
            ];
        }
        else {
            return $returnArr;
        }
    }

    //FRONT END USAGE ONLY
    function getPaymentGatewayIcon(WhiteLabels $paymentGatewayWhitelabel, string $locale) {
        $uri = Constants::$BASE_URL;
        $uri .= Constants::$PATH_ROOT;
        $uri .= Constants::$PATH_PAYMENTGATEWAY_ICON;
        $uri .= '/' . $paymentGatewayWhitelabel->value;

        //Building our headers
        $headers = [
            'accept' => 'image/png'
        ];
        //REQUEST
        $response = $this->client->request('GET', $uri, [
            'headers' => $headers,
            'http_errors' => false
        ]);
        //Return back for AJAX request
        if (isset($_GET['action'])) {
            return [
                'code' => json_decode($response->getStatusCode()),
                'body' => json_decode($response->getBody())
            ];
        }
    }
}