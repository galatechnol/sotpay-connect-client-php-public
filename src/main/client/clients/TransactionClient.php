<?php
namespace Src\main\client\clients;
use DateTime;
use DateTimeInterface;
use Exception;
use GuzzleHttp\Client;
use JsonMapper;
use Src\main\client\config\ClientConfig;
use Src\main\client\constants\Constants;
use Src\main\client\models\BooleanResult;
use Src\main\client\models\components\ListsWrapper;
use Src\main\client\models\components\Page;
use Src\main\client\models\enums\NumberFilterOperator;
use Src\main\client\models\enums\sortOrder;
use Src\main\client\models\enums\sortPropertyTransaction;
use Src\main\client\models\enums\StringFilterOperator;
use Src\main\client\models\ResponseError;
use Src\main\client\models\Transaction;
use Src\main\client\models\TransactionWrite;
use Src\main\client\models\utils\JWSSignature;

class TransactionClient {
    private Client $client;
    public function  __construct(Client $client) {
        $this->setClient($client);
    }
    private function setClient(Client $client): void {

        $this->client = $client;
    }
    function getTransaction(ClientConfig $clientConfig, string $urn, string $locale) {
        //Validation
        $clientConfig->validateGetTransaction();
        //Building URI
        if (preg_match(Constants::$URN_MATCH, $urn)) {
            $uri = $clientConfig->getBaseUrl();
            $uri .= Constants::$PATH_ROOT;
            $uri .= Constants::$PATH_TRANSACTION;
            $uri .= '/' . $urn;
            //Building our headers
            $headers = [
                'accept' => 'application/json;charset=UTF-8',
                'Accept-Language' => $locale,
                'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken()
            ];
        }
        else {
            throw new Exception('Invalid URN format');
        }

        $response = $this->client->request('GET',$uri, [
            'headers' => $headers,
            'http_errors' => false,
        ]);

        if (isset($_GET['action'])) {
            return [
                'code' => json_decode($response->getStatusCode()),
                'body' => json_decode($response->getBody())
            ];
        }

        if ($response->getStatusCode()) {
            return match ($response->getStatusCode()) {
                200 => Transaction::withBody($response->getBody()),
                204 => "Transaction Not Found",
                400, 401, 500 => ResponseError::withBody($response->getBody()),
                404 => throw new Exception('Error 404 Not Found, Invalid Relative URL'),
                default => throw new Exception('Response code not recognised, please check Request')
            };
        }
        else {
            throw new Exception('Status Code is invalid, please check request');
        }
    }

    function getTransactionInfo(ClientConfig $clientConfig,string $urn, string $locale) {
        //Validation
        $clientConfig->validateGetTransactionInfo();
        //Building URI
        if (preg_match(Constants::$URN_MATCH, $urn)) {
            $uri = $clientConfig->getBaseUrl();
            $uri .= Constants::$PATH_ROOT;
            $uri .= Constants::$PATH_TRANSACTION;
            $uri .= '/' . $urn . Constants::$PATH_INFO;
            //Building our headers
            if ($locale) {
                $headers = [
                    'accept' => 'application/json;charset=UTF-8',
                    'Accept-Language' => $locale,
                    'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken()
                ];
            }
            else {
                $headers = [
                    'accept' => 'application/json;charset=UTF-8',
                    'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken()
                ];
            }
        }
        else {
            throw new Exception('Invalid URN format');
        }

        $response = $this->client->request('GET',$uri, [
            'headers' => $headers,
            'http_errors' => false,
        ]);

        if (isset($_GET['action'])) {
            return [
                'code' => json_decode($response->getStatusCode()),
                'body' => json_decode($response->getBody())
            ];
        }

        if ($response->getStatusCode()) {
            //using switch instead of match due to logic
            switch ($response->getStatusCode()) {
                case 200:
                    //Dynamically chooses the Info Class based on First JSON property
                    if (preg_match(Constants::$INFO_MATCH, $response->getBody(), $matches)) {
                        $gateway = $matches[0] . '}'; //add the end bracket back
                        $gateway = json_decode($gateway, true);
                        $class = 'Src\main\client\models\TransactionInfoObjects\infos\\' . $gateway['object'];
                        return $class::withBody($response->getBody());
                    }
                    else {
                        throw new Exception('Unknown Gateway, could not find "object" property in JSON');
                    }
                case 204:
                    return "Transaction does not have an Info Object";
                case 400:
                case 401:
                case 500:
                    return ResponseError::withBody($response->getBody());
                case 404:
                    throw new Exception('Error 404 Not Found, Invalid Relative URL');
                default:
                    throw new Exception('Response code not recognised, please check Request');
            }

        }
        //Falsy Status Codes
        else {
            throw new Exception('Status Code is invalid, please check request');
        }
    }

    function getTransactions(
        ClientConfig $clientConfig,
        ?DateTime $createdFrom = null,
        ?DateTime $createdTo = null,
        ?DateTime $expiryFrom = null,
        ?DateTime $expiryTo = null,
        ?string $urnFilter = null,
        ?StringFilterOperator $urnFilterOperator = null,
        bool $includeSubMerchants = true,
        ?sortPropertyTransaction $sortProperty = null,
        ?sortOrder $sortOrder = null,
        ?int $pageSize = null,
        ?int $pageNumber = null,
        ?float $amountFilter = null,
        ?NumberFilterOperator $amountFilterOperator = null,
        ?string $currencyFilter = null,
        ?string $paymentGatewayFilter = null,
        ?string $statusFilter = null,
        ?string $detailsReferenceFilter = null,
        ?StringFilterOperator $detailsReferenceFilterOperator = null,
        ?string $detailsReferenceSecondaryFilter = null,
        ?StringFilterOperator $detailsReferenceSecondaryFilterOperator = null,
        ?string $detailsDescriptionFilter = null,
        ?StringFilterOperator $detailsDescriptionFilterOperator = null,
        ?string $detailsAgentFilter = null,
        ?StringFilterOperator $detailsAgentFilterOperator = null,
        ?string $contactEmailFilter = null,
        ?StringFilterOperator $contactEmailFilterOperator = null,
        ?string $contactPhoneFilter = null,
        ?StringFilterOperator $contactPhoneFilterOperator = null,
        ?string $billingAddressFirstNameFilter = null,
        ?StringFilterOperator $billingAddressFirstNameFilterOperator = null,
        ?string $billingAddressSurnameFilter = null,
        ?StringFilterOperator $billingAddressSurnameFilterOperator = null,
        ?string $progressProgressFilter = null,
        string $locale = 'en'
    ) {
        //Validation
        $clientConfig->validateGetTransactions();
        $uri = $clientConfig->getBaseUrl();
        $uri .= Constants::$PATH_ROOT;
        $uri .= Constants::$PATH_TRANSACTION;
        $uri .= '/' . $clientConfig->getMerchant() . '/' . $clientConfig->getDomain()->value;
        $uriArr = array_merge(
            isset($createdFrom) ? array('createdFrom' => $createdFrom->format('Y-m-d\TH:i:s\Z')): array(),
            isset($createdTo) ? array('createdTo' => $createdTo->format('Y-m-d\TH:i:s\Z')): array(),
            isset($expiryFrom) ? array('expiryFrom' => $expiryFrom->format('Y-m-d\TH:i:s\Z')): array(),
            isset($expiryTo) ? array('expiryTo' => $expiryTo->format('Y-m-d\TH:i:s\Z')): array(),
            isset($urnFilter) ? array('urnFilter' => $urnFilter) : array(),
            isset($urnFilterOperator) ? array('urnFilterOperator' => $urnFilterOperator->value) : array(),
            $includeSubMerchants ? array('includeSubMerchants' => 'true') : array(),
            isset($pageSize) ? array('pageSize' => $pageSize): array(),
            isset($pageNumber) ? array('pageNumber' => $pageNumber): array(),
            isset($sortProperty) ? array('sortProperty' => $sortProperty->value): array(),
            isset($sortOrder) ? array('sortOrder' => $sortOrder->value): array(),
            isset($amountFilter) ? array('amountFilter' => $amountFilter) : array(),
            isset($amountFilterOperator) ? array('amountFilterOperator' => $amountFilterOperator->value) : array(),
            isset($currencyFilterOperator) ? array('currencyFilterOperator' => $currencyFilterOperator->value) : array(),
            isset($detailsReferenceFilter) ? array('detailsReferenceFilter' => $detailsReferenceFilter) : array(),
            isset($detailsReferenceFilterOperator) ? array('detailsReferenceFilterOperator' => $detailsReferenceFilterOperator->value) : array(),
            isset($detailsReferenceSecondaryFilter) ? array('detailsReferenceSecondaryFilter' => $detailsReferenceSecondaryFilter) : array(),
            isset($detailsReferenceSecondaryFilterOperator) ? array('detailsReferenceSecondaryFilterOperator'
            => $detailsReferenceSecondaryFilterOperator->value) : array(),
            isset($detailsDescriptionFilter) ? array('detailsDescriptionFilter' => $detailsDescriptionFilter) : array(),
            isset($detailsDescriptionFilterOperator) ? array('detailsDescriptionFilterOperator' => $detailsDescriptionFilterOperator->value) : array(),
            isset($detailsAgentFilter) ? array('detailsAgentFilter' => $detailsAgentFilter) : array(),
            isset($detailsAgentFilterOperator) ? array('detailsAgentFilterOperator' => $detailsAgentFilterOperator->value) : array(),
            isset($contactEmailFilter) ? array('contactEmailFilter' => $contactEmailFilter) : array(),
            isset($contactEmailFilterOperator) ? array('contactEmailFilterOperator' => $contactEmailFilterOperator->value) : array(),
            isset($contactPhoneFilter) ? array('contactPhoneFilter' => $contactPhoneFilter) : array(),
            isset($contactPhoneFilterOperator) ? array('contactPhoneFilterOperator' => $contactPhoneFilterOperator->value) : array(),
            isset($billingAddressFirstNameFilter) ? array('billingAddressFirstNameFilter' => $billingAddressFirstNameFilter) : array(),
            isset($billingAddressFirstNameFilterOperator) ? array('billingAddressFirstNameFilterOperator'
            => $billingAddressFirstNameFilterOperator->value) : array(),
            isset($billingAddressSurnameFilter) ? array('billingAddressSurnameFilter' => $billingAddressSurnameFilter) : array(),
            isset($billingAddressSurnameFilterOperator) ? array('billingAddressSurnameFilterOperator' => $billingAddressSurnameFilterOperator->value) : array()
        );
        if (http_build_query($uriArr)) {
            $uri .= '?' . http_build_query($uriArr);
        }
        //Appending our arrayed search filters
        $uri .= isset($currencyFilter) ? implode('', array_map(fn($val) => '&currencyFilter=' . $val ,explode(',', $currencyFilter))) : '';
        $uri .= isset($paymentGatewayFilter) ? implode('', array_map(fn($val) => '&paymentGatewayFilter=' . $val ,explode(',', $paymentGatewayFilter))) : '';
        $uri .= isset($statusFilter) ? implode('', array_map(fn($val) => '&statusFilter=' . $val ,explode(',', $statusFilter))) : '';
        $uri .= isset($progressProgressFilter) ? implode('', array_map(fn($val) => '&progressProgressFilter=' . $val ,explode(',', $progressProgressFilter))) : '';

        //Building our headers
        $headers = [
            'accept' => 'application/json;charset=UTF-8',
            'Accept-Language' => $locale,
            'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken()
        ];
        $response = $this->client->request('GET',$uri, [
            'headers' => $headers,
            'http_errors' => false,
        ]);

        /////////////////////////
        //FOR AJAX RETURNS ONLY//
        /////////////////////////
        if (isset($_GET['action'])) {

            $body = json_decode($response->getBody());

            ///////////////////////
            ////Return to client///
            return [
                'body' => $body,
                'pagenumber' => $response->getHeader('pagenumber'),
                'pagesize' => $response->getHeader('pagesize'),
                'totalpages' => $response->getHeader('totalpages'),
                'totalelements' => $response->getHeader('totalelements')
            ];
        }
        ///////////////////////
        ////CLIENT RETURNS/////
        ///////////////////////
        else {
            switch ($response->getStatusCode()) {
                case 200:
                    $data = json_decode($response->getBody());
                    if (is_array($data)) {
                        //mapper configs
                        $mapper = new JsonMapper();
                        $mapper->bIgnoreVisibility = true; //allow private access
                        $mapper->bExceptionOnUndefinedProperty = true; //if we get unexpected properties
                        //List
                        $list = $mapper->mapArray($data, array(), 'Src\main\client\models\Transaction');

                        //Page
                        $pageNumber = $response->getHeader('pagenumber'); //arr with each link
                        $pageSize = $response->getHeader('pagesize');
                        $totalElements = $response->getHeader('totalelements');
                        $totalPages = $response->getHeader('totalpages');
                        $page = new Page($pageNumber[0], $pageSize[0], $totalElements[0], $totalPages[0]);
                        return new ListsWrapper($list, $page);
                    }
                    else { // sending non-JSON data
                        throw new Exception('Wrong data type sent, expected JSON array of objects');
                    }
                case 204:
                    return null;
                case 400:
                case 401:
                case 500:
                    return ResponseError::withBody($response->getBody());
                case 404:
                    throw new Exception('Error 404 Not Found, Invalid Relative URL');
                default:
                    throw new Exception('Response code not recognised, please check Request');
            }
        }
    }

    function createTransaction(ClientConfig $clientConfig, TransactionWrite $transaction, string $locale) {
        $clientConfig->validateCreateTransaction();
        $data = json_encode($transaction);
        $url = $clientConfig->getBaseUrl();
        $url .= Constants::$PATH_ROOT . Constants::$PATH_TRANSACTION;
        $signature = JWSSignature::createSignature($clientConfig->getSignatureKeyInbound(), $data);

        $headers = [
            'accept' => 'application/json;charset=UTF-8',
            'x-jws-signature' => $signature,
            'Accept-Language' => $locale,
            'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken(),
            'Content-Type' => 'application/json;charset=UTF-8'
        ];
        $options = [
            'body' => $data,
            'headers' => $headers,
            'http_errors' => false,
        ];
        $response = $this->client->request('POST', $url, $options);

        if (isset($_GET['action'])) {
            return [
                'code' => json_decode($response->getStatusCode()),
                'body' => json_decode($response->getBody())
            ];
        }

        if ($response->getStatusCode()) {
            return match ($response->getStatusCode()) {
                201 => Transaction::withBody($response->getBody()),
                400, 401, 403, 422, 500 => ResponseError::withBody($response->getBody()),
                404 => throw new Exception('Error 404 Not Found, Invalid Relative URL'),
                default => throw new Exception('Response code not recognised, please check Request')
            };
        }
        else {
            throw new Exception('Status Code is invalid, please check request');
        }
    }

    function duplicateTransaction(ClientConfig $clientConfig, string $urn, string $locale = 'en'): ResponseError|Transaction|array
    {
        //Validation
        $clientConfig->validateDuplicateTransaction();
        //Building URI
        if (preg_match(Constants::$URN_MATCH, $urn)) {
            $uri = $clientConfig->getBaseUrl();
            $uri .= Constants::$PATH_ROOT;
            $uri .= Constants::$PATH_TRANSACTION;
            $uri .= '/' . $urn . Constants::$PATH_DUPLICATE;

            //Building our headers
            $headers = [
                'accept' => 'application/json;charset=UTF-8',
                'Accept-Language' => $locale,
                'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken()
            ];
        }
        else {
            throw new Exception('Invalid URN format');
        }

        $response = $this->client->request('POST',$uri, [
            'headers' => $headers,
            'http_errors' => false,
        ]);
        //AJAX return
        if (isset($_GET['action'])) {
            return [
                $response->getStatusCode()
            ];
        }
        //Normal client calling
        else {
            if ($response->getStatusCode()) {
                return match ($response->getStatusCode()) {
                    201 => Transaction::withBody($response->getBody()),
                    400, 401, 500 => ResponseError::withBody($response->getBody()),
                    404 => throw new Exception('Error 404 Not Found, Invalid Relative URL'),
                    default => throw new Exception('Response code not recognised, please check Request')
                };
            }
            else {
                throw new Exception('Status Code is invalid, please check request');
            }
        }
    }

    function expireTransaction(ClientConfig $clientConfig, string $urn, string $locale = 'en'): ResponseError|Exception|string|array {
        //Validation
        $clientConfig->validateExpireTransaction();
        //Building URI
        if (preg_match(Constants::$URN_MATCH, $urn)) {
            $uri = $clientConfig->getBaseUrl();
            $uri .= Constants::$PATH_ROOT;
            $uri .= Constants::$PATH_TRANSACTION;
            $uri .= '/' . $urn . Constants::$PATH_EXPIRE;

            //Building our headers
            $headers = [
                'accept' => 'application/json;charset=UTF-8',
                'Accept-Language' => $locale,
                'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken()
            ];
        }
        else {
            throw new Exception('Invalid URN format');
        }

        $response = $this->client->request('POST',$uri, [
            'headers' => $headers,
            'http_errors' => false,
        ]);
        //AJAX return
        if (isset($_GET['action'])) {
            return [
                $response->getStatusCode()
            ];
        }
        //Normal client calling
        else {
            if ($response->getStatusCode()) {
                return match ($response->getStatusCode()) {
                    200 => 'Transaction ' . $urn . ' has been updated to EXPIRED',
                    304 => 'Transaction ' . $urn . ' is already EXPIRED',
                    400, 401, 500 => ResponseError::withBody($response->getBody()),
                    404 => throw new Exception('Error 404 Not Found, Invalid Relative URL'),
                    default => throw new Exception('Response code not recognised, please check Request')
                };
            }
            else {
                throw new Exception('Status Code is invalid, please check request');
            }
        }
    }

    function resendTransaction(ClientConfig $clientConfig, string $urn, string $locale = 'en'): ResponseError|Exception|string|array {
        //Validation
        $clientConfig->validateResendTransaction();
        //Building URI
        if (preg_match(Constants::$URN_MATCH, $urn)) {
            $uri = $clientConfig->getBaseUrl();
            $uri .= Constants::$PATH_ROOT;
            $uri .= Constants::$PATH_TRANSACTION;
            $uri .= '/' . $urn . Constants::$PATH_RESENDPAYMESSAGE;

            //Building our headers
            $headers = [
                'accept' => 'application/json;charset=UTF-8',
                'Accept-Language' => $locale,
                'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken()
            ];
        }
        else {
            throw new Exception('Invalid URN format');
        }

        $response = $this->client->request('POST',$uri, [
            'headers' => $headers,
            'http_errors' => false,
        ]);

        //AJAX return
        if (isset($_GET['action'])) {
            return [
                $response->getStatusCode()
            ];
        }
        //or... Client Call
        else {
            if ($response->getStatusCode()) {
                return match ($response->getStatusCode()) {
                    200 => 'Transaction ' . $urn . ' has been re-sent',
                    400, 401, 500 => ResponseError::withBody($response->getBody()),
                    404 => throw new Exception('Error 404 Not Found, Invalid Relative URL'),
                    default => throw new Exception('Response code not recognised, please check Request')
                };
            }
            else {
                throw new Exception('Status Code is invalid, please check request');
            }
        }
    }

    function isValidExpireTransaction(ClientConfig $clientConfig, string $urn, string $locale = 'en') {
        //Validation
        $clientConfig->validateIsValidExpireTransaction();
        if (preg_match(Constants::$URN_MATCH, $urn)) {
            $uri = $clientConfig->getBaseUrl();
            $uri .= Constants::$PATH_ROOT;
            $uri .= Constants::$PATH_TRANSACTION;
            $uri .= '/' . $urn . Constants::$PATH_EXPIRE;
            $uri .= Constants::$PATH_ISVALID;

            //build request headers
            $headers = [
                'accept' => 'application/json;charset=UTF-8',
                'Accept-Language' => $locale,
                'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken()
            ];

        }
        else {
            throw new Exception('Invalid URN format');
        }

        $response = $this->client->request('GET',$uri, [
            'headers' => $headers,
            'http_errors' => false,
        ]);

        //AJAX Return
        if (isset($_GET['action'])) {
            return [
                'body' => json_decode($response->getBody()),
            ];
        }
        else {
            if ($response->getStatusCode()) {
                return match ($response->getStatusCode()) {
                    200 => BooleanResult::withBody($response->getBody()),
                    400, 401, 500 => ResponseError::withBody($response->getBody()),
                    404 => throw new Exception('Error 404 Not Found, Invalid Relative URL'),
                    default => throw new Exception('Response code not recognised, please check Request')
                };
            }
            else {
                throw new Exception('Status Code is invalid, please check request');
            }
        }
    }
    function isValidDuplicateTransaction(ClientConfig $clientConfig, string $urn, string $locale = 'en') {
        //Validation
        $clientConfig->validateIsValidDuplicateTransaction();
        if (preg_match(Constants::$URN_MATCH, $urn)) {
            $uri = $clientConfig->getBaseUrl();
            $uri .= Constants::$PATH_ROOT;
            $uri .= Constants::$PATH_TRANSACTION;
            $uri .= '/' . $urn . Constants::$PATH_DUPLICATE;
            $uri .= Constants::$PATH_ISVALID;

            //build request headers
            $headers = [
                'accept' => 'application/json;charset=UTF-8',
                'Accept-Language' => $locale,
                'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken()
            ];

        }
        else {
            throw new Exception('Invalid URN format');
        }

        $response = $this->client->request('GET',$uri, [
            'headers' => $headers,
            'http_errors' => false,
        ]);

        //AJAX Return
        if (isset($_GET['action'])) {
            return [
                'body' => json_decode($response->getBody()),
            ];
        }
        else {
            if ($response->getStatusCode()) {
                return match ($response->getStatusCode()) {
                    200 => BooleanResult::withBody($response->getBody()),
                    400, 401, 500 => ResponseError::withBody($response->getBody()),
                    404 => throw new Exception('Error 404 Not Found, Invalid Relative URL'),
                    default => throw new Exception('Response code not recognised, please check Request')
                };
            }
            else {
                throw new Exception('Status Code is invalid, please check request');
            }
        }
    }
    function isValidResendTransaction(ClientConfig $clientConfig, string $urn, string $locale = 'en') {
        //Validation
        $clientConfig->validateIsValidResendTransaction();
        if (preg_match(Constants::$URN_MATCH, $urn)) {
            $uri = $clientConfig->getBaseUrl();
            $uri .= Constants::$PATH_ROOT;
            $uri .= Constants::$PATH_TRANSACTION;
            $uri .= '/' . $urn . Constants::$PATH_RESENDPAYMESSAGE;
            $uri .= Constants::$PATH_ISVALID;

            //build request headers
            $headers = [
                'accept' => 'application/json;charset=UTF-8',
                'Accept-Language' => $locale,
                'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken()
            ];

        }
        else {
            throw new Exception('Invalid URN format');
        }

        $response = $this->client->request('GET',$uri, [
            'headers' => $headers,
            'http_errors' => false,
        ]);

        //AJAX Return
        if (isset($_GET['action'])) {
            return [
                'body' => json_decode($response->getBody()),
            ];
        }
        else {
            if ($response->getStatusCode()) {
                return match ($response->getStatusCode()) {
                    200 => BooleanResult::withBody($response->getBody()),
                    400, 401, 500 => ResponseError::withBody($response->getBody()),
                    404 => throw new Exception('Error 404 Not Found, Invalid Relative URL'),
                    default => throw new Exception('Response code not recognised, please check Request')
                };
            }
            else {
                throw new Exception('Status Code is invalid, please check request');
            }
        }
    }
}