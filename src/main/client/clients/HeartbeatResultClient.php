<?php
namespace Src\main\client\clients;
use GuzzleHttp\Client;
use Src\main\client\config\ClientConfig;
use Src\main\client\constants\Constants;
use Src\main\client\models\HeartbeatResult;
use Src\main\client\models\ResponseError;
use Exception;

class HeartbeatResultClient {
    private Client $client;

    public function __construct(Client $client) {
        $this->setClient($client);
    }
    private function setClient(Client $client): void {
        $this->client = $client;
    }

    function getHeartbeatResult(ClientConfig $clientConfig, string $locale): HeartbeatResult|ResponseError {

            $uri = $clientConfig->getBaseUrl();
            $uri .= Constants::$PATH_ROOT;
            $uri .= Constants::$PATH_HEARTBEAT;
            //headers to pass to response
            $headers = [
                'accept' => 'application/json;charset=UTF-8',
                'Accept-Language' => $locale,
            ];

            $response = $this->client->request('GET',$uri, [
                'headers' => $headers,
                'http_errors' => false
            ]);
            if ($response->getStatusCode()) {
                return match ($response->getStatusCode()) {
                    200 => HeartbeatResult::withBody($response->getBody()),
                    500 => ResponseError::withBody($response->getBody()),
                    default => throw new Exception('Response code not recognised, please check URL')
                };
            }
            else {
                throw new Exception('Status Code is invalid, please check request');
            }
        }

}