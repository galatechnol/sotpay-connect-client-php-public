<?php
namespace Src\main\client\clients;
use GuzzleHttp\Client;
use Src\main\client\config\ClientConfig;
use Src\main\client\constants\Constants;
use Src\main\client\models\enums\PaymentGateway;
use Src\main\client\models\TestCard;
use Src\main\client\models\ResponseError;
use Exception;

class TestCardClient {
    private Client $client;
    public function  __construct(Client $client) {
        $this->setClient($client);
    }
    private function setClient(Client $client): void {

        $this->client = $client;
    }
    function getTestCard(ClientConfig $clientConfig, PaymentGateway $gateway, string $locale) {

            //Building URI
            $uri = $clientConfig->getBaseUrl();
            $uri .= Constants::$PATH_ROOT;
            $uri .= Constants::$PATH_TESTCARD;
            $uri .= '/' . $gateway->value;

            $headers = [
                'accept' => 'application/json;charset=UTF-8',
                'Accept-Language' => $locale,
            ];

            $response = $this->client->request('GET',$uri, [
                'headers' => $headers,
                'http_errors' => false
            ]);

            if (isset($_GET['action'])) {
                return [
                    'body' => json_decode($response->getBody()),
                ];
            }

        if ($response->getStatusCode()) {
            return match ($response->getStatusCode()) {
                200 => TestCard::withBody($response->getBody()),
                204 => null, //Test Card doesn't exist, return null (nothing)
                400, 500 => ResponseError::withBody($response->getBody()),
                404 => throw new Exception('Error 404 Not Found, Invalid Relative URL'),
                default => throw new Exception('Response code not recognised, please check URL')
            };
        }
        else {
            throw new Exception('Status Code is invalid, please check request');
        }
    }
}