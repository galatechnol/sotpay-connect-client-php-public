<?php

namespace Src\main\client\clients;

use Exception;
use GuzzleHttp\Client;
use Src\main\client\config\ClientConfig;
use Src\main\client\constants\Constants;
use Src\main\client\models\Merchant;
use Src\main\client\models\ResponseError;

class MerchantClient
{
    private Client $client;
    public function  __construct(Client $client) {
        $this->setClient($client);
    }
    private function setClient(Client $client): void {

        $this->client = $client;
    }

    function GetMerchant(ClientConfig $clientConfig, string $locale) {
        //Validation
        $clientConfig->validateGetMerchant();
        //Building URI
        $uri = $clientConfig->getBaseUrl();
        $uri .= Constants::$PATH_ROOT;
        $uri .= Constants::$PATH_MERCHANT;
        $uri .= '/' . $clientConfig->getMerchant();
        //Building our headers
        $headers = [
            'accept' => 'application/json;charset=UTF-8',
            'Accept-Language' => $locale,
            'Authorization' => 'Bearer ' . $clientConfig->getMerchantToken()
        ];

        $response = $this->client->request('GET',$uri, [
            'headers' => $headers,
            'http_errors' => false,
        ]);

        //For AJAX returns
        if (isset($_GET['action'])) {
            return [
                'code' => json_decode($response->getStatusCode()),
                'body' => json_decode($response->getBody())
            ];
        }

        if ($response->getStatusCode()) {
            return match ($response->getStatusCode()) {
                200 => Merchant::withBody($response->getBody()),
                204 => "Merchant Does not Exist",
                400, 401, 500 => ResponseError::withBody($response->getBody()),
                404 => throw new Exception('Error 404 Not Found, Invalid Relative URL'),
                default => throw new Exception('Response code not recognised, please check Request')
            };
        }
        else {
            throw new Exception('Status Code is invalid, please check request');
        }
    }
}