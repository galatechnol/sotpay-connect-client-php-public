<?php
namespace Src\main\demo;
use DateTime;
use DateTimeZone;
use GuzzleHttp\Client;
use Src\main\client\clients\CardOnFileClient;
use Src\main\client\clients\HeartbeatResultClient;
use Src\main\client\clients\MerchantTokenClient;
use Src\main\client\clients\TestCardClient;
use Src\main\client\clients\TransactionClient;
use Src\main\client\config\ClientConfig;
use Src\main\client\models\enums\Domain;
use Src\main\client\models\enums\PaymentGateway;
use Src\main\client\models\enums\sortOrder;
use Src\main\client\models\enums\sortPropertyCardOnFile;
use Src\main\client\models\enums\sortPropertyTransaction;
use Src\main\client\models\MerchantTokenWrite;
use Src\main\client\models\TransactionWrite;

class DemoClass
{
    private string $baseurl;
    private string $merchant;
    private Domain $domain;
    private string $merchantToken;
    private string $signatureKeyInbound;
    private string $tokenAdminKey;
    private string $urn;
    private string $id;
    private PaymentGateway $paymentGateway;
    private string $locale;
    private ClientConfig $clientConfig;

    public function __construct(
        $merchant = 'democo1',
        $domain  = Domain::SANDBOX,
        $baseurl = 'https://connect.sotpay.co.uk',
        $merchantToken = '',
        $signatureKeyInbound = '',
        $tokenAdminKey = '')
    {
        $this->clientConfig = new ClientConfig(
            $this->baseurl = $baseurl,
            $this->merchant = $merchant,
            $this->domain = $domain,
            $this->merchantToken = $merchantToken,
            $this->signatureKeyInbound = $signatureKeyInbound,
            $this->tokenAdminKey = $tokenAdminKey);
    }

    public function getTestCard(
        $paymentGateway = PaymentGateway::sybc, $locale = 'en'): void
    {
        $var = new TestCardClient(new Client());
        echo $var->getTestCard($this->clientConfig, $paymentGateway, $locale);
    }

    public function getTransaction($urn, $locale = 'en'): void
    {
        $var = new TransactionClient(new Client());
    echo $var->getTransaction($this->clientConfig, $urn, $locale);
    }

    public function getHeartbeatResult($locale = 'en'): void
    {
        $var = new HeartbeatResultClient(new Client());
        echo $var->getHeartbeatResult($this->clientConfig, $locale);
    }

    public function getTransactions(
        $startTimestamp = new DateTime('2024-02-25T14:22:55', new DateTimeZone('UTC')),
        $endTimestamp = new DateTime('2024-02-27T14:22:55', new DateTimeZone('UTC')),
        $includeSubMerchants = false,
        $sortProperty = sortPropertyTransaction::CREATED,
        $sortOrder = SortOrder::ASCENDING,
        $pageSize = 20,
        $pageNumber = 1,
        $locale = 'en'
    ): void
    {
        $var = new TransactionClient(new Client());
        $var = $var->getTransactions($startTimestamp, $endTimestamp, $includeSubMerchants,
            $sortProperty, $sortOrder, $pageSize, $pageNumber, $this->clientConfig, $locale);
        foreach($var->getList() as $value) {
            echo $value;
        }
        echo $var->getPages();
    }

    public function getTransactionInfo($urn, $locale = 'en'): void
    {
        $var = new TransactionClient(new Client());
        echo $var->getTransactionInfo($this->clientConfig, $urn, $locale);
    }

    public function createTransaction(
        TransactionWrite $transactionWrite,
        $locale = 'en'
    ): void
    {
        $var = new TransactionClient(new Client());
        echo $var->createTransaction($this->clientConfig, $transactionWrite, $locale);
    }

    public function duplicateTransaction($urn, $locale ='en') {
        $var = new TransactionClient(new Client());
        echo $var->duplicateTransaction($this->clientConfig, $urn, $locale);
    }

    public function expireTransaction($urn, $locale ='en') {
        $var = new TransactionClient(new Client());
        echo $var->expireTransaction($this->clientConfig, $urn, $locale);
    }

    public function resendTransaction($urn, $locale ='en') {
        $var = new TransactionClient(new Client());
        echo $var->resendTransaction($this->clientConfig, $urn, $locale);
    }

    public function getCardOnFile($id, $locale ='en') {
        $var = new CardOnFileClient(new Client());
        echo $var->getCardOnFile($this->clientConfig, $id, $locale);
    }

    public function getCardsOnFile(
        $paymentGateway = PaymentGateway::sybc,
        $sortProperty = sortPropertyCardOnFile::ID,
        $sortOrder = sortOrder::ASCENDING,
        $pageSize = 5,
        $pageNumber = 1,
        $locale = 'en'
    ) {
        $var = new CardOnFileClient(new Client());
        $var = $var->getCardsOnFile(
            $paymentGateway,
            $sortProperty,
            $sortOrder,
            $pageSize,
            $pageNumber,
            $this->clientConfig,
            $locale
        );
        //DemoClass Output
        foreach($var->getList() as $value) {
            echo $value;
        }
        echo $var->getPages();
    }

    public function createMerchantToken(
        MerchantTokenWrite $merchantTokenWrite,
        $locale = 'en'
    ) {
        $var = new MerchantTokenClient(new Client());
        echo $var->createToken($this->clientConfig, $merchantTokenWrite, $locale);
    }

    public function deleteMerchantToken($id, $locale = 'en') {
        $var = new MerchantTokenClient(new Client());
        echo $var->deleteToken($this->clientConfig, $id, $locale);
    }
}