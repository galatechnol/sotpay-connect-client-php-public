<?php
require_once ('../../../vendor/autoload.php');

use GuzzleHttp\Client;
use Src\main\client\clients\CardOnFileClient;
use Src\main\client\clients\HeartbeatResultClient;
use Src\main\client\clients\MerchantClient;
use Src\main\client\clients\MerchantRelationshipClient;
use Src\main\client\clients\MerchantTokenClient;
use Src\main\client\clients\PaymentGatewayClient;
use Src\main\client\clients\TestCardClient;
use Src\main\client\clients\TransactionClient;
use Src\main\client\config\ClientConfig;
use Src\main\client\models\components\merchantKey;
use Src\main\client\models\enums\Country;
use Src\main\client\models\enums\Currency;
use Src\main\client\models\enums\Domain;
use Src\main\client\models\enums\NumberFilterOperator;
use Src\main\client\models\enums\PaymentGateway;
use Src\main\client\models\enums\sortOrder;
use Src\main\client\models\enums\sortPropertyCardOnFile;
use Src\main\client\models\enums\sortPropertyTransaction;
use Src\main\client\models\enums\StringFilterOperator;
use Src\main\client\models\enums\Type;
use Src\main\client\models\MerchantTokenWrite;
use Src\main\client\models\TransactionObjects\transactionAddress;
use Src\main\client\models\TransactionObjects\transactionDetails;
use Src\main\client\models\TransactionObjects\transactionLocales;
use Src\main\client\models\TransactionObjects\TransactionContact;
use Src\main\client\models\TransactionObjects\TransactionOverrideUrls;
use Src\main\client\models\TransactionObjects\TransactionPostbackUrls;
use Src\main\client\models\TransactionObjects\transactionPrimaryRecipient;
use Src\main\client\models\TransactionObjects\TransactionWriteMessage;
use Src\main\client\models\TransactionWrite;


//Config Properties//
$baseurl = 'https://connect.sotpay.co.uk';
$merchant = 'democo1';
$domain = Domain::SANDBOX;
$merchantToken = 'tk_sand_ras35vZfuFCljD9gXpZ6PkZsTVm3Y8PEPMwdDXD2ud';
$signatureKeyInbound = '';
$tokenAdminKey = '';
$clientConfig = new ClientConfig($baseurl, $merchant, $domain,
    $merchantToken, $signatureKeyInbound, $tokenAdminKey);
//Config Properties//


//Parameter Values//
$urn = 'STN-PSY-DEM1S-F0214-KFSC1'; //example URN
$id = 'CDF-PSY-DEM1S-E0131-SY2MV'; //cardOnFile ID
$paymentGateway = PaymentGateway::sybc;
$locale = 'en';
//Parameter Values//


// ----GET TestCard ----//
function demoTestCard(): void {
    global $clientConfig, $paymentGateway, $locale;
    //Creating & config our GUZZLE client
    $res = new TestCardClient(new Client());
    //output result
    echo $res->getTestCard($clientConfig, $paymentGateway, $locale);
    //output result
}
// --------------------//

// ----GET HeartBeatResult ---//
function demoHeartbeatResult(): void {
    global $clientConfig, $locale;
    //Creating & config our GUZZLE client
    $res = new HeartbeatResultClient(new Client());
    //output result
    echo $res->getHeartbeatResult($clientConfig, $locale);
}
// --------------------//

// ----GET Transaction ---//
function demoGetTransaction(): void {
    global $clientConfig, $urn, $locale;
    //Creating & config our GUZZLE client
    $res = new TransactionClient(new Client());
    //output result
    $res =  $res->getTransaction($clientConfig, $urn, $locale);
    echo $res;

}
// --------------------//

// ----GET Transactions ---//
function demoGetTransactions(): void {
    global $clientConfig, $locale;
//    $startTimestamp = new DateTime('2020-12-31 14:20:59 ', new DateTimeZone(date_default_timezone_get()));
//    $endTimestamp = new DateTime('2023-12-31 14:20:59 ');
//    $includeSubMerchants = true;
//    $sortProperty = sortPropertyTransaction::URN;
//    $sortOrder = SortOrder::ASCENDING;
    $pageSize = 25;
    $pageNumber = 2;
//    $progressProgressFilter = 'REGISTERED,STARTED';
    //Creating & config our GUZZLE client
    $res = new TransactionClient(new Client());
    //output result
    $vals = $res->getTransactions(
        $clientConfig,
        createdFrom: new DateTime('2025-02-14 00:00:00'),
        createdTo: new DateTime('2025-02-14 23:59:59'),
        pageSize: $pageSize,
        pageNumber: $pageNumber,
        amountFilter: 100.99,
        amountFilterOperator: NumberFilterOperator::EQUALS,
        contactPhoneFilter: '+44',
        contactPhoneFilterOperator: StringFilterOperator::CONTAINS);
    //Vals returns a wrapper obj

    //For demo, we output our array of objs + pages
    foreach($vals->getList() as $value) {
        echo $value;
    }
    echo $vals->getPages();
}
// --------------------//

// ----GET Transaction Info---//
function demoGetTransactionInfo(): void {
    global $clientConfig, $urn, $locale;
    //Creating & config our GUZZLE client
    $res = new TransactionClient(new Client());
    //output result
    $res = $res->getTransactionInfo($clientConfig, $urn, $locale);
    echo $res;
}
// --------------------//

// ----CREATE Transaction ----//
function demoCreateTransaction(): void {
    global $clientConfig, $locale;

    //Transaction Object
    $transaction = new TransactionWrite(
        new merchantKey('democo1',Domain::SANDBOX),
        100.99,
        Currency::GBP,
        PaymentGateway::globalpayments
    );
    $transaction->setDetails(new transactionDetails('REF1', 'REF2', attended: true));
    $transaction->setContact(
        new TransactionContact('csmith@test.com','+441234567890'));
    $transaction->setNotificationContactSecondary(
        new TransactionContact('ccovrig@democo1.com','+440969696969'));
    $transaction->setBillingAddress(
        new TransactionAddress(
            'Chris',
            'Smith',
            'Chris Smith',
            '123a High Street',
            Country::GB,
            'Wath-upon-Dearne',
            'S63 1AA',
        ));
//    $transaction->setDeliveryAddress(
//        new TransactionAddress(
//            'Chris',
//            'Smith',
//            'Chris Smith',
//            '123a High Street',
//            'Rotherham',
//            Country::GB,
//            'Wath-upon-Dearne',
//            'S63 1AA',
//        ));
    $transaction->setPrimaryRecipient(new transactionPrimaryRecipient(
        '1234567890',
        'Jones',
        'B1 1BA',
        '1970-03-12'
    ));
    $transaction->setLocales(
        new transactionLocales(
            'fr',
            'fr_FR'));
    $transaction->setPostbackUrls(
        new TransactionPostbackUrls(
            'https://www.myurl.com/order/123/status?urn={0}&status={1}',
            progressPostbacks: true
        )
    );
    $transaction->setOverrideUrls(new TransactionOverrideUrls());
    $transaction->setCreateCardOnFile(true);
    $transaction->setPayMessage(
        new TransactionWriteMessage(
            true,
            true));
    $transaction->setReceiptMessage(
        new TransactionWriteMessage(
            true,
            true));
    $transaction->setNotificationMessage(
        new TransactionWriteMessage(
            true,
            true));
    $transaction->setNotificationMessageSecondary(
        new TransactionWriteMessage(
            true,
            true));

    $res = new TransactionClient(new Client());
    //output result
    echo $res->createTransaction($clientConfig, $transaction, $locale);
}
// --------------------//

// ----DUPLICATE Transaction ----//
function demoDuplicateTransaction(): void {
    global $clientConfig, $urn, $locale;
    //Creating & config our GUZZLE client
    $res = new TransactionClient(new Client());
    //output result
    echo $res->duplicateTransaction($clientConfig, $urn, $locale);

}
// ----EXPIRE Transaction ----//
function demoExpireTransaction(): void {
    global $clientConfig, $urn, $locale;
    //Creating & config our GUZZLE client
    $res = new TransactionClient(new Client());
    //output result
    echo $res->expireTransaction($clientConfig, $urn, $locale);

}
// --------------------//

// ----RESEND Transaction ----//
function demoResendTransaction(): void {
    global $clientConfig, $urn, $locale;
    //Creating & config our GUZZLE client
    $res = new TransactionClient(new Client());
    //output result
    echo $res->resendTransaction($clientConfig, $urn, $locale);

}
// --------------------//


// ----GET CardOnFile-----//
function demoGetCardOnFile(): void {
    global $clientConfig, $id, $locale;
    $res = new CardOnFileClient(new Client());
    //output result
    echo $res->getCardOnFile($clientConfig, $id, $locale);
}
// --------------------//

// ----GET CardsOnFile-----//
function demoGetCardsOnFile(): void {
    global $clientConfig, $paymentGateway, $locale;
    $sortProperty = sortPropertyCardOnFile::REFERENCE;
    $sortOrder = sortOrder::ASCENDING;
    $pageSize = 3;
    $pageNumber = 1;

    $expiryFrom = new DateTime();
    $expiryFrom->setDate(2025, 01, 01);

    $expiryTo = new DateTime();
    $expiryTo->setDate(2025, 12, 01);


    $res = new CardOnFileClient(new Client());
    //output result
    $vals = $res->getCardsOnFile(
        $clientConfig, $paymentGateway, expiryFrom: $expiryFrom, expiryTo: $expiryTo
    );

    //DemoClass Output
    foreach($vals->getList() as $value) {
        echo $value;
    }
    echo $vals->getPages();
    foreach ($vals as $val) {
        echo $val;
    }
}
// --------------------//
function DemoCreateMerchantToken(): void {
    global $clientConfig, $locale;

    //Token Object
    $token = new MerchantTokenWrite(
        new merchantKey($clientConfig->getMerchant(), $clientConfig->getDomain()), Type::READ);
    $token->setDescription("test");
    $token->setExpiresAfter(31536000);

    $res = new MerchantTokenClient(new Client());
    //output
    echo $res->createToken($clientConfig, $token, $locale);

}

function DemoDeleteMerchantToken():void {
    global $clientConfig, $locale;
    $id = 63;

    $res = new MerchantTokenClient(new Client());
    //output
    echo $res->deleteToken($clientConfig, $id, $locale);
}

function demoGetPaymentGatewayWhiteLabel():void {
    global $clientConfig, $locale;

    $res = new PaymentGatewayClient(new Client());

    echo $res->getPaymentGatewayWhiteLabel($clientConfig,
        (new merchantKey($clientConfig->getMerchant(), $clientConfig->getDomain())),
        PaymentGateway::globalpaymentsrealex,
        $locale);
}

function demoGetEnabledSubMerchants():void {
    global $clientConfig, $locale;

    $res = new MerchantRelationshipClient(new Client());

    $res = $res->getEnabledSubMerchants($clientConfig,
        new merchantKey($clientConfig->getMerchant(),$clientConfig->getDomain()));
    var_dump($res);
}
function demoGetAllPaymentGatewayWhiteLabel(): void {
    global $clientConfig, $locale;

    $res = new PaymentGatewayClient(new Client());

    var_dump($res->getAllPaymentGatewayWhiteLabel($clientConfig,
        new merchantKey($clientConfig->getMerchant(), $clientConfig->getDomain()),
        'en'
    ));
}

function demoGetMerchant(): void {
    global $clientConfig, $locale;

    $res = new MerchantClient(new Client());

    echo $res->GetMerchant($clientConfig, $locale);
}


//Call methods here//

//    demoHeartbeatResult();
//    demoTestCard();
//    demoGetTransaction();
//    demoGetTransactions();
//    demoGetTransactionInfo();
//    demoGetCardOnFile();
//    demoGetCardsOnFile();
//demoCreateTransaction();
//    demoCreateMerchantToken();
//    demoDeleteMerchantToken();
//    demoDuplicateTransaction();
//    demoExpireTransaction();
//    demoResendTransaction();
//    demoGetPaymentGatewayWhiteLabel();
//    demoGetEnabledSubMerchants();
//    demoGetMerchant();
//demoGetAllPaymentGatewayWhiteLabel();