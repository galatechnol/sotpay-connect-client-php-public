<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Src\main\client\clients\TransactionClient;
use Src\main\client\config\ClientConfig;
use Src\main\client\models\components\merchantKey;
use Src\main\client\models\enums\Domain;
use Src\main\client\models\ResponseError;
use Src\main\client\models\Transaction;
use Src\main\client\models\TransactionInfoObjects\infos\AcquiredInfo;
use Src\main\client\models\TransactionInfoObjects\infos\BarclaycardInfo;
use Src\main\client\models\TransactionInfoObjects\infos\CardstreamInfo;
use Src\main\client\models\TransactionInfoObjects\infos\enums\AcquiredAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\AcquiredD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\AcquiredPostcodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\AcquiredSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\AcquiredStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\Bank;
use Src\main\client\models\TransactionInfoObjects\infos\enums\BarclaycardAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\BarclaycardD3SResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\BarclaycardSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\BarclaycardStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardstreamAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardstreamD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardstreamPostcodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardstreamSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardstreamState;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardstreamStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionInfoObjects\infos\enums\EVOPaymentsStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\FreedomPayAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\FreedomPayD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\FreedomPaySecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\FreedomPayState;
use Src\main\client\models\TransactionInfoObjects\infos\enums\FreedomPayStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\GlobalPaymentsRealexAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\GlobalPaymentsRealexD3SResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\GlobalPaymentsRealexPostcodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\GlobalPaymentsRealexSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\NuapayStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoPostcodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoServerAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoServerD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoServerPostcodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoServerSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoServerStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\OpayoStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\Pay360AddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\Pay360D3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\Pay360PostcodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\Pay360SecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\Pay360StatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\PaySafeAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\PaysafeD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\PaysafeSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\PaysafeStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\ResultClassification;
use Src\main\client\models\TransactionInfoObjects\infos\enums\RiskClassification;
use Src\main\client\models\TransactionInfoObjects\infos\enums\SCPState;
use Src\main\client\models\TransactionInfoObjects\infos\enums\SCPStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\enums\SYBCAddressResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\SYBCD3sResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\SYBCSecurityCodeResult;
use Src\main\client\models\TransactionInfoObjects\infos\enums\SYBCStatusCode;
use Src\main\client\models\TransactionInfoObjects\infos\EVOPaymentsInfo;
use Src\main\client\models\TransactionInfoObjects\infos\FreedomPayInfo;
use Src\main\client\models\TransactionInfoObjects\infos\GlobalPaymentsRealexInfo;
use Src\main\client\models\TransactionInfoObjects\infos\NuapayInfo;
use Src\main\client\models\TransactionInfoObjects\infos\OpayoInfo;
use Src\main\client\models\TransactionInfoObjects\infos\OpayoServerInfo;
use Src\main\client\models\TransactionInfoObjects\infos\Pay360Info;
use Src\main\client\models\TransactionInfoObjects\infos\PaysafeInfo;
use Src\main\client\models\TransactionInfoObjects\infos\SCPInfo;
use Src\main\client\models\TransactionInfoObjects\infos\SYBCInfo;

class GetTransactionInfoClientTest extends TestCase
{
    private Client $clientMock;
    private ClientConfig $clientConfig;
    private Transaction $getTransaction;
    private TransactionClient $getTransactionClient;

    protected function setUp(): void
    {
        //create and config guzzle mockClient
        $this->clientMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock(); //direct request

        $baseurl = 'https://connect.sotpay.co.uk';
        $merchant = 'democo1';
        $domain = Domain::SANDBOX;
        $merchantToken = 'tk_sand_ras35vZfuFCzjD9gXpZ7PkZsTVm4Y8PEPTwdDXD3xz';
        $signatureKeyInbound = '';
        $tokenAdminKey = '';
        $this->clientConfig = new ClientConfig($baseurl, $merchant, $domain, $merchantToken,
            $signatureKeyInbound, $tokenAdminKey);

    }

    function testGetTransactionInfoClient200SYBC()
    {
        $expectedTransaction = new SYBCInfo();
        $expectedTransaction->setObject('SYBCInfo');
        $expectedTransaction->setUrn('STN-PSY-DEM1S-E0119-12345');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setAddressResult(SYBCAddressResult::match);
        $expectedTransaction->setAddressResultClassification(ResultClassification::PASS);
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setCreated(new DateTime('2024-01-25T16:38:52Z'));
        $expectedTransaction->setD3sResult(SYBCD3sResult::success);
        $expectedTransaction->setD3sResultClassification(ResultClassification::PASS);
        $expectedTransaction->setMaskedCardNumber('4129********9012');
        $expectedTransaction->setReference('825209466');
        $expectedTransaction->setRiskScore(4);
        $expectedTransaction->setRiskScoreClassification(RiskClassification::OK);
        $expectedTransaction->setSecurityCodeResult(SYBCSecurityCodeResult::match);
        $expectedTransaction->setSecurityCodeResultClassification(ResultClassification::PASS);
        $expectedTransaction->setStatusCode(SYBCStatusCode::approved);
        $expectedTransaction->setUpdated(new DateTime('2024-01-25T16:41:10Z'));


        $JSONData = '{
              "object": "SYBCInfo",
              "urn": "STN-PSY-DEM1S-E0119-12345",
              "merchantKey": {
                "merchant": "democo1",
                "domain": "SANDBOX"
              },
              "addressResult": "match",
              "addressResultClassification": "PASS",
              "cardType": "VISA",
              "created": "2024-01-25T16:38:52Z",
              "d3sResult": "success",
              "d3sResultClassification": "PASS",
              "maskedCardNumber": "4129********9012",
              "reference": "825209466",
              "riskScore": 4,
              "riskScoreClassification": "OK",
              "securityCodeResult": "match",
              "securityCodeResultClassification": "PASS",
              "statusCode": "approved",
              "updated": "2024-01-25T16:41:10Z"
        }';
        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetTransactionInfoClient200Acquired()
    {
        $expectedTransaction = new AcquiredInfo();
        $expectedTransaction->setObject('AcquiredInfo');
        $expectedTransaction->setUrn('STN-PSY-DEM1S-E0119-12345');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setAddressResult(AcquiredAddressResult::M);
        $expectedTransaction->setAddressResultClassification(ResultClassification::PASS);
        $expectedTransaction->setApplePay(false);
        $expectedTransaction->setCardType(CardType::MASTERCARD);
        $expectedTransaction->setCreated(new DateTime('2024-01-31T10:35:03Z'));
        $expectedTransaction->setD3sResult(AcquiredD3sResult::Y);
        $expectedTransaction->setD3sResultClassification(ResultClassification::PASS);
        $expectedTransaction->setGooglePay(false);
        $expectedTransaction->setMaskedCardNumber('XXXX-XXXX-XXXX-7478');
        $expectedTransaction->setPostcodeResult(AcquiredPostcodeResult::M);
        $expectedTransaction->setPostcodeResultClassification(ResultClassification::PASS);
        $expectedTransaction->setReference('6450394');
        $expectedTransaction->setSecurityCodeResult(AcquiredSecurityCodeResult::M);
        $expectedTransaction->setSecurityCodeResultClassification(ResultClassification::PASS);
        $expectedTransaction->setStatusCode(AcquiredStatusCode::R1);
        $expectedTransaction->setStatusText('AUTHCODE: 123456');
        $expectedTransaction->setUpdated(new DateTime('2024-01-31T10:36:45Z'));


        $JSONData = '{
              "object": "AcquiredInfo",
              "urn": "STN-PSY-DEM1S-E0119-12345",
              "merchantKey": {
                "merchant": "democo1",
                "domain": "SANDBOX"
              },
              "addressResult": "M",
              "addressResultClassification": "PASS",
              "applePay": false,
              "cardType": "MASTERCARD",
              "created": "2024-01-31T10:35:03Z",
              "d3sResult": "Y",
              "d3sResultClassification": "PASS",
              "googlePay": false,
              "maskedCardNumber": "XXXX-XXXX-XXXX-7478",
              "postcodeResult": "M",
              "postcodeResultClassification": "PASS",
              "reference": "6450394",
              "securityCodeResult": "M",
              "securityCodeResultClassification": "PASS",
              "statusCode": "R1",
              "statusText": "AUTHCODE: 123456",
              "updated": "2024-01-31T10:36:45Z"
        }';
        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetTransactionInfoClient200Barclayscard()
    {
        $expectedTransaction = new BarclaycardInfo();
        $expectedTransaction->setObject('BarclaycardInfo');
        $expectedTransaction->setUrn('STN-PSY-DEM1S-E0119-12345');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setAddressResult(BarclaycardAddressResult::NO);
        $expectedTransaction->setAddressResultClassification(ResultClassification::UNKNOWN);
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setCreated(new DateTime('2024-01-31T10:39:13Z'));
        $expectedTransaction->setD3sResult(BarclaycardD3SResult::R5);
        $expectedTransaction->setD3sResultClassification(ResultClassification::PASS);
        $expectedTransaction->setMaskedCardNumber('XXXXXXXXXXXX2022');
        $expectedTransaction->setReference('3847575980');
        $expectedTransaction->setSecurityCodeResult(BarclaycardSecurityCodeResult::NO);
        $expectedTransaction->setSecurityCodeResultClassification(ResultClassification::UNKNOWN);
        $expectedTransaction->setStatusCode(BarclaycardStatusCode::R9);
        $expectedTransaction->setStatusText('AUTHCODE: test123');
        $expectedTransaction->setUpdated(new DateTime('2024-01-31T10:39:46Z'));


        $JSONData = '{
              "object": "BarclaycardInfo",
              "urn": "STN-PSY-DEM1S-E0119-12345",
              "merchantKey": {
                "merchant": "democo1",
                "domain": "SANDBOX"
              },
              "addressResult": "NO",
              "addressResultClassification": "UNKNOWN",
              "cardType": "VISA",
              "created": "2024-01-31T10:39:13Z",
              "d3sResult": "5",
              "d3sResultClassification": "PASS",
              "maskedCardNumber": "XXXXXXXXXXXX2022",
              "reference": "3847575980",
              "securityCodeResult": "NO",
              "securityCodeResultClassification": "UNKNOWN",
              "statusCode": "9",
              "statusText": "AUTHCODE: test123",
              "updated": "2024-01-31T10:39:46Z"
        }';
        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetTransactionInfoClient200Cardstream()
    {
        $expectedTransaction = new CardstreamInfo();
        $expectedTransaction->setObject('CardstreamInfo');
        $expectedTransaction->setUrn('STN-PSY-DEM1S-E0119-12345');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setAddressResult(CardstreamAddressResult::R2);
        $expectedTransaction->setAddressResultClassification(ResultClassification::PASS);
        $expectedTransaction->setApplePay(false);
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setCreated(new DateTime('2024-01-31T10:41:44Z'));
        $expectedTransaction->setD3sResult(CardstreamD3sResult::Y);
        $expectedTransaction->setD3sResultClassification(ResultClassification::PASS);
        $expectedTransaction->setGooglePay(false);
        $expectedTransaction->setMaskedCardNumber('401200******1112');
        $expectedTransaction->setPostcodeResult(CardstreamPostcodeResult::R2);
        $expectedTransaction->setPostcodeResultClassification(ResultClassification::PASS);
        $expectedTransaction->setReference('300754028');
        $expectedTransaction->setReferenceSecondary('24013110SC42WG22DS86BCX');
        $expectedTransaction->setSecurityCodeResult(CardstreamSecurityCodeResult::R2);
        $expectedTransaction->setSecurityCodeResultClassification(ResultClassification::PASS);
        $expectedTransaction->setState(CardstreamState::captured);
        $expectedTransaction->setStatusCode(CardstreamStatusCode::R0);
        $expectedTransaction->setStatusText('AUTHCODE:993639');
        $expectedTransaction->setUpdated(new DateTime('2024-01-31T10:42:45Z'));

        $JSONData = '{
              "object": "CardstreamInfo",
              "urn": "STN-PSY-DEM1S-E0119-12345",
              "merchantKey": {
                "merchant": "democo1",
                "domain": "SANDBOX"
              },
              "addressResult": "2",
              "addressResultClassification": "PASS",
              "applePay": false,
              "cardType": "VISA",
              "created": "2024-01-31T10:41:44Z",
              "d3sResult": "Y",
              "d3sResultClassification": "PASS",
              "googlePay": false,
              "maskedCardNumber": "401200******1112",
              "postcodeResult": "2",
              "postcodeResultClassification": "PASS",
              "reference": "300754028",
              "referenceSecondary": "24013110SC42WG22DS86BCX",
              "securityCodeResult": "2",
              "securityCodeResultClassification": "PASS",
              "state": "captured",
              "statusCode": "0",
              "statusText": "AUTHCODE:993639",
              "updated": "2024-01-31T10:42:45Z"
        }';
        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetTransactionInfoClient200EVOPayments()
    {
        $expectedTransaction = new EVOPaymentsInfo();
        $expectedTransaction->setObject('EVOPaymentsInfo');
        $expectedTransaction->setUrn('STN-PSY-DEM1S-E0119-12345');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));;
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setCreated(new DateTime('2024-01-31T10:47:14Z'));
        $expectedTransaction->setMaskedCardNumber('411111******1111');
        $expectedTransaction->setReference('12481094');
        $expectedTransaction->setStatusCode(EVOPaymentsStatusCode::CAPTURED);
        $expectedTransaction->setStatusText('AUTHCODE: N9Z4JZ');
        $expectedTransaction->setUpdated(new DateTime('2024-01-31T10:49:02Z'));

        $JSONData = '{
              "object": "EVOPaymentsInfo",
              "urn": "STN-PSY-DEM1S-E0119-12345",
              "merchantKey": {
                "merchant": "democo1",
                "domain": "SANDBOX"
              },
              "cardType": "VISA",
              "created": "2024-01-31T10:47:14Z",
              "maskedCardNumber": "411111******1111",
              "reference": "12481094",
              "statusCode": "CAPTURED",
              "statusText": "AUTHCODE: N9Z4JZ",
              "updated": "2024-01-31T10:49:02Z"
        }';
        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetTransactionInfoClient200FreedomPay()
    {
        $expectedTransaction = new FreedomPayInfo();
        $expectedTransaction->setObject('FreedomPayInfo');
        $expectedTransaction->setUrn('STN-PSY-DEM1S-E0119-12345');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setAddressResult(FreedomPayAddressResult::M);
        $expectedTransaction->setAddressResultClassification(ResultClassification::PASS);
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setCreated(new DateTime('2024-01-31T10:50:00Z'));
        $expectedTransaction->setD3sResult(FreedomPayD3sResult::Y);
        $expectedTransaction->setD3sResultClassification(ResultClassification::PASS);
        $expectedTransaction->setMaskedCardNumber('400000XXXXXX2503');
        $expectedTransaction->setReference('01Z6LMKNC497U6E39QSVS8QCGG9M6QOX');
        $expectedTransaction->setSecurityCodeResult(FreedomPaySecurityCodeResult::RM);
        $expectedTransaction->setSecurityCodeResultClassification(ResultClassification::PASS);
        $expectedTransaction->setState(FreedomPayState::ACCEPT);
        $expectedTransaction->setStatusCode(FreedomPayStatusCode::R100);
        $expectedTransaction->setStatusText('AUTHCODE: 355868');
        $expectedTransaction->setUpdated(new DateTime('2024-01-31T10:50:45Z'));

        $JSONData = '{
          "object": "FreedomPayInfo",
          "urn": "STN-PSY-DEM1S-E0119-12345",
          "merchantKey": {
            "merchant": "democo1",
            "domain": "SANDBOX"
          },
          "addressResult": "M",
          "addressResultClassification": "PASS",
          "cardType": "VISA",
          "created": "2024-01-31T10:50:00Z",
          "d3sResult": "Y",
          "d3sResultClassification": "PASS",
          "maskedCardNumber": "400000XXXXXX2503",
          "reference": "01Z6LMKNC497U6E39QSVS8QCGG9M6QOX",
          "securityCodeResult": "M",
          "securityCodeResultClassification": "PASS",
          "state": "ACCEPT",
          "statusCode": "100",
          "statusText": "AUTHCODE: 355868",
          "updated": "2024-01-31T10:50:45Z"
        }';
        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetTransactionInfoClient200GlobalPaymentsRealex()
    {
        $expectedTransaction = new GlobalPaymentsRealexInfo();
        $expectedTransaction->setObject('GlobalPaymentsRealexInfo');
        $expectedTransaction->setUrn('STN-PSY-DEM1S-E0119-12345');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setAddressResult(GlobalPaymentsRealexAddressResult::M);
        $expectedTransaction->setAddressResultClassification(ResultClassification::PASS);
        $expectedTransaction->setApplePay(false);
        $expectedTransaction->setCreated(new DateTime('2024-01-31T10:51:24Z'));
        $expectedTransaction->setD3sResult(GlobalPaymentsRealexD3SResult::R5);
        $expectedTransaction->setD3sResultClassification(ResultClassification::PASS);
        $expectedTransaction->setGooglePay(false);
        $expectedTransaction->setPostcodeResult(GlobalPaymentsRealexPostcodeResult::M);
        $expectedTransaction->setPostcodeResultClassification(ResultClassification::PASS);
        $expectedTransaction->setReference('17066983258142372');
        $expectedTransaction->setSecurityCodeResult(GlobalPaymentsRealexSecurityCodeResult::M);
        $expectedTransaction->setSecurityCodeResultClassification(ResultClassification::PASS);
        $expectedTransaction->setStatusCode(0);
        $expectedTransaction->setStatusText('[ test system ] AUTHORISED');
        $expectedTransaction->setUpdated(new DateTime('2024-01-31T10:52:06Z'));

        $JSONData = '{
          "object": "GlobalPaymentsRealexInfo",
          "urn": "STN-PSY-DEM1S-E0119-12345",
          "merchantKey": {
            "merchant": "democo1",
            "domain": "SANDBOX"
          },
          "addressResult": "M",
          "addressResultClassification": "PASS",
          "applePay": false,
          "created": "2024-01-31T10:51:24Z",
          "d3sResult": "5",
          "d3sResultClassification": "PASS",
          "googlePay": false,
          "postcodeResult": "M",
          "postcodeResultClassification": "PASS",
          "reference": "17066983258142372",
          "securityCodeResult": "M",
          "securityCodeResultClassification": "PASS",
          "statusCode": 0,
          "statusText": "[ test system ] AUTHORISED",
          "updated": "2024-01-31T10:52:06Z"
        }';
        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetTransactionInfoClient200Nuapay()
    {
        $expectedTransaction = new NuapayInfo();
        $expectedTransaction->setObject('NuapayInfo');
        $expectedTransaction->setUrn('STN-PSY-DEM1S-E0119-12345');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setBank(Bank::NUAPAYASPSP);
        $expectedTransaction->setBankAccount('11**74 12****69');
        $expectedTransaction->setCreated(new DateTime('2024-01-31T10:56:04Z'));;
        $expectedTransaction->setReference('qj29owqa2x');
        $expectedTransaction->setReferenceSecondary('PSYDEM1SE0131U8TKE');
        $expectedTransaction->setStatusCode(NuapayStatusCode::PAYMENT_RECEIVED);
        $expectedTransaction->setUpdated(new DateTime('2024-01-31T10:58:15Z'));


        $JSONData = '{
              "object": "NuapayInfo",
              "urn": "STN-PSY-DEM1S-E0119-12345",
              "merchantKey": {
                "merchant": "democo1",
                "domain": "SANDBOX"
              },
              "bank": "NUAPAYASPSP",
              "bankAccount": "11**74 12****69",
              "created": "2024-01-31T10:56:04Z",
              "reference": "qj29owqa2x",
              "referenceSecondary": "PSYDEM1SE0131U8TKE",
              "statusCode": "PAYMENT_RECEIVED",
              "updated": "2024-01-31T10:58:15Z"
        }';
        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetTransactionInfoClient200Opayo()
    {
        $expectedTransaction = new OpayoInfo();
        $expectedTransaction->setObject('OpayoInfo');
        $expectedTransaction->setUrn('STN-PSY-DEM1S-E0119-12345');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setAddressResult(OpayoAddressResult::Matched);
        $expectedTransaction->setAddressResultClassification(ResultClassification::PASS);
        $expectedTransaction->setApplePay(false);
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setCreated(new DateTime('2024-01-31T10:58:41Z'));
        $expectedTransaction->setD3sResult(OpayoD3sResult::Authenticated);
        $expectedTransaction->setD3sResultClassification(ResultClassification::PASS);
        $expectedTransaction->setMaskedCardNumber('******0006');
        $expectedTransaction->setPostcodeResult(OpayoPostcodeResult::Matched);
        $expectedTransaction->setPostcodeResultClassification(ResultClassification::PASS);
        $expectedTransaction->setReference('702E5FB2-C9C9-5480-0A11-66CBE098C752');
        $expectedTransaction->setReferenceSecondary('20850092');
        $expectedTransaction->setSecurityCodeResult(OpayoSecurityCodeResult::Matched);
        $expectedTransaction->setSecurityCodeResultClassification(ResultClassification::PASS);
        $expectedTransaction->setStatusCode(OpayoStatusCode::Ok);
        $expectedTransaction->setStatusText('The Authorisation was Successful.');
        $expectedTransaction->setUpdated(new DateTime('2024-01-31T10:59:07Z'));

        $JSONData = '{
          "object": "OpayoInfo",
          "urn": "STN-PSY-DEM1S-E0119-12345",
          "merchantKey": {
            "merchant": "democo1",
            "domain": "SANDBOX"
          },
          "addressResult": "Matched",
          "addressResultClassification": "PASS",
          "applePay": false,
          "cardType": "VISA",
          "created": "2024-01-31T10:58:41Z",
          "d3sResult": "Authenticated",
          "d3sResultClassification": "PASS",
          "maskedCardNumber": "******0006",
          "postcodeResult": "Matched",
          "postcodeResultClassification": "PASS",
          "reference": "702E5FB2-C9C9-5480-0A11-66CBE098C752",
          "referenceSecondary": "20850092",
          "securityCodeResult": "Matched",
          "securityCodeResultClassification": "PASS",
          "statusCode": "Ok",
          "statusText": "The Authorisation was Successful.",
          "updated": "2024-01-31T10:59:07Z"
        }';
        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetTransactionInfoClient200OpayoServer()
    {
        $expectedTransaction = new OpayoServerInfo();
        $expectedTransaction->setObject('OpayoServerInfo');
        $expectedTransaction->setUrn('STN-PSY-DEM1S-E0119-12345');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setAddressResult(OpayoServerAddressResult::MATCHED);
        $expectedTransaction->setAddressResultClassification(ResultClassification::PASS);
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setCreated(new DateTime('2024-01-31T10:59:54Z'));
        $expectedTransaction->setD3sResult(OpayoServerD3sResult::OK);
        $expectedTransaction->setD3sResultClassification(ResultClassification::PASS);
        $expectedTransaction->setMaskedCardNumber('******0006');
        $expectedTransaction->setPostcodeResult(OpayoServerPostcodeResult::MATCHED);
        $expectedTransaction->setPostcodeResultClassification(ResultClassification::PASS);
        $expectedTransaction->setReference('{C7387D28-4495-4C53-4E9E-73B60E42530D}');
        $expectedTransaction->setSecurityCodeResult(OpayoServerSecurityCodeResult::MATCHED);
        $expectedTransaction->setSecurityCodeResultClassification(ResultClassification::PASS);
        $expectedTransaction->setStatusCode(OpayoServerStatusCode::OK);
        $expectedTransaction->setStatusText('0000 : The Authorisation was Successful.');
        $expectedTransaction->setUpdated(new DateTime('2024-01-31T11:00:52Z'));

        $JSONData = '{
              "object": "OpayoServerInfo",
              "urn": "STN-PSY-DEM1S-E0119-12345",
              "merchantKey": {
                "merchant": "democo1",
                "domain": "SANDBOX"
              },
              "addressResult": "MATCHED",
              "addressResultClassification": "PASS",
              "cardType": "VISA",
              "created": "2024-01-31T10:59:54Z",
              "d3sResult": "OK",
              "d3sResultClassification": "PASS",
              "maskedCardNumber": "******0006",
              "postcodeResult": "MATCHED",
              "postcodeResultClassification": "PASS",
              "reference": "{C7387D28-4495-4C53-4E9E-73B60E42530D}",
              "securityCodeResult": "MATCHED",
              "securityCodeResultClassification": "PASS",
              "statusCode": "OK",
              "statusText": "0000 : The Authorisation was Successful.",
              "updated": "2024-01-31T11:00:52Z"
        }';
        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetTransactionInfoClient200PaySafe()
    {
        $expectedTransaction = new PaysafeInfo();
        $expectedTransaction->setObject('PaysafeInfo');
        $expectedTransaction->setUrn('STN-PSY-DEM1S-E0119-12345');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setAddressResult(PaySafeAddressResult::MATCH_ZIP_ONLY);
        $expectedTransaction->setAddressResultClassification(ResultClassification::PARTIAL);
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setCreated(new DateTime('2024-01-31T11:03:58Z'));
        $expectedTransaction->setD3sResult(PaysafeD3sResult::Y);
        $expectedTransaction->setD3sResultClassification(ResultClassification::PASS);
        $expectedTransaction->setMaskedCardNumber('******1091');
        $expectedTransaction->setReference('dcf3652a-580c-4b19-93a0-b3f4a177d76a');
        $expectedTransaction->setSecurityCodeResult(PaysafeSecurityCodeResult::MATCH);
        $expectedTransaction->setSecurityCodeResultClassification(ResultClassification::PASS);
        $expectedTransaction->setStatusCode(PaysafeStatusCode::COMPLETED);
        $expectedTransaction->setStatusText('AUTHCODE: 717179');
        $expectedTransaction->setUpdated(new DateTime('2024-01-31T11:04:22Z'));

        $JSONData = '{
          "object": "PaysafeInfo",
          "urn": "STN-PSY-DEM1S-E0119-12345",
          "merchantKey": {
            "merchant": "democo1",
            "domain": "SANDBOX"
          },
          "addressResult": "MATCH_ZIP_ONLY",
          "addressResultClassification": "PARTIAL",
          "cardType": "VISA",
          "created": "2024-01-31T11:03:58Z",
          "d3sResult": "Y",
          "d3sResultClassification": "PASS",
          "maskedCardNumber": "******1091",
          "reference": "dcf3652a-580c-4b19-93a0-b3f4a177d76a",
          "securityCodeResult": "MATCH",
          "securityCodeResultClassification": "PASS",
          "statusCode": "COMPLETED",
          "statusText": "AUTHCODE: 717179",
          "updated": "2024-01-31T11:04:22Z"
        }';
        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetTransactionInfoClient200Pay360()
    {
        $expectedTransaction = new Pay360Info();
        $expectedTransaction->setObject('Pay360Info');
        $expectedTransaction->setUrn('STN-PSY-DEM1S-E0119-12345');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setAddressResult(Pay360AddressResult::FULL_MATCH);
        $expectedTransaction->setAddressResultClassification(ResultClassification::PASS);
        $expectedTransaction->setCardType(CardType::MASTERCARD);
        $expectedTransaction->setCreated(new DateTime('2024-01-31T11:01:38'));
        $expectedTransaction->setD3sResult(Pay360D3sResult::AUTHENTICATED);
        $expectedTransaction->setD3sResultClassification(ResultClassification::PASS);
        $expectedTransaction->setMaskedCardNumber('990000******5159');
        $expectedTransaction->setPostcodeResult(Pay360PostcodeResult::FULL_MATCH);
        $expectedTransaction->setPostcodeResultClassification(ResultClassification::PASS);
        $expectedTransaction->setReference('10201194880');
        $expectedTransaction->setSecurityCodeResult(Pay360SecurityCodeResult::MATCHED);
        $expectedTransaction->setSecurityCodeResultClassification(ResultClassification::PASS);
        $expectedTransaction->setStatusCode(Pay360StatusCode::SUCCESS);
        $expectedTransaction->setStatusText('Approved - no action');
        $expectedTransaction->setUpdated(new DateTime('2024-01-31T11:02:50Z'));

        $JSONData = '{
          "object": "Pay360Info",
          "urn": "STN-PSY-DEM1S-E0119-12345",
          "merchantKey": {
            "merchant": "democo1",
            "domain": "SANDBOX"
          },
          "addressResult": "FULL_MATCH",
          "addressResultClassification": "PASS",
          "cardType": "MASTERCARD",
          "created": "2024-01-31T11:01:38Z",
          "d3sResult": "AUTHENTICATED",
          "d3sResultClassification": "PASS",
          "maskedCardNumber": "990000******5159",
          "postcodeResult": "FULL_MATCH",
          "postcodeResultClassification": "PASS",
          "reference": "10201194880",
          "securityCodeResult": "MATCHED",
          "securityCodeResultClassification": "PASS",
          "statusCode": "SUCCESS",
          "statusText": "Approved - no action",
          "updated": "2024-01-31T11:02:50Z"
        }';
        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetTransactionInfoClient200SCPInfo()
    {
        $expectedTransaction = new SCPInfo();
        $expectedTransaction->setObject('SCPInfo');
        $expectedTransaction->setUrn('STN-PSY-DEM1S-E0119-12345');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setCreated(new DateTime('2024-01-31T11:05:06Z'));;
        $expectedTransaction->setMaskedCardNumber('454305******6721');
        $expectedTransaction->setReference('2760teg6ew54xs6e8c0caocq5sj2adr');
        $expectedTransaction->setReferenceSecondary('LWPQVWKKG7M6');
        $expectedTransaction->setState(SCPState::COMPLETE);
        $expectedTransaction->setStatusCode(SCPStatusCode::SUCCESS);
        $expectedTransaction->setUpdated(new DateTime('2024-01-31T11:06:40Z'));


        $JSONData = '{
              "object": "SCPInfo",
              "urn": "STN-PSY-DEM1S-E0119-12345",
              "merchantKey": {
                "merchant": "democo1",
                "domain": "SANDBOX"
              },
              "cardType": "VISA",
              "created": "2024-01-31T11:05:06Z",
              "maskedCardNumber": "454305******6721",
              "reference": "2760teg6ew54xs6e8c0caocq5sj2adr",
              "referenceSecondary": "LWPQVWKKG7M6",
              "state": "COMPLETE",
              "statusCode": "SUCCESS",
              "updated": "2024-01-31T11:06:40Z"
        }';
        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    ///Other Codes
    function testGetTransactionInfoClient204SYBC() {
        $this->clientMock->method('request')
            ->willReturn(new Response(204));

        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals('Transaction does not have an Info Object',$receivedTransaction);
    }

    function testGetTransactionInfoClient401SYBC() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_UNAUTHORIZED');
        $expectedResponse->setMessage('Unauthorized');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_UNAUTHORIZED",
          "message": "Unauthorized"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(401, [], $JSONData));

        $service = new TransactionClient($this->clientMock);
        $receivedTransaction =
            $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedResponse, $receivedTransaction);
    }

    function testGetTransactionInfoClient404SYBC() {
        $this->clientMock->method('request')
            ->willReturn(new Response(404));

        $service = new TransactionClient($this->clientMock);
        $this->expectExceptionMessage('Error 404 Not Found, Invalid Relative URL');
        $receivedTransaction =
            $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');
    }

    function testGetTransactionInfoClient500SYBC() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_INTERNAL');
        $expectedResponse->setMessage('An Exception Occurred');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_INTERNAL",
          "message": "An Exception Occurred"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(500, [], $JSONData));

        $service = new TransactionClient($this->clientMock);
        $receivedTransaction =
            $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedResponse, $receivedTransaction);
    }

    function testGetTransactionClient100() {
        $this->clientMock->method('request')
            ->willReturn(new Response(100));

        $this->expectExceptionMessage('Response code not recognised, please check Request');
        $service = new TransactionClient($this->clientMock);
        $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');
    }

    function testGetTransactionInfoClientNullConfig() {
        $service = new TransactionClient($this->clientMock);
        $this->expectException(TypeError::class);
        $service->getTransactionInfo(null,'STN-PSY-DEM1S-E0119-12345', 'en');
    }

    function testGetTransactionInfoClientNullUrn() {
        $service = new TransactionClient($this->clientMock);
        $this->expectException(TypeError::class);
        $service->getTransactionInfo($this->clientConfig,null, 'en');
    }

    function testGetTransactionInfoInvalidJSON() {
        $this->expectExceptionMessage('Wrong data type sent, expected JSON body');
        $service = SYBCInfo::withBody('invalid JSON as string');
    }

    function testGetTransactionInfoInvalidJSONProperty() {
        $JSONBody =  '{
              "object": "SYBCInfo",
              "urn": "STN-PSY-DEM1S-E0119-12345",
              "merchantKey": {
                "merchant": "democo1",
                "domain": "SANDBOX"
              },
              "addressResult": "match",
              "badProperty": "errorExp"
        }';
        $this->expectExceptionMessage('JSON property "badProperty" does not exist in object of type Src\main\client\models\TransactionInfoObjects\infos\SYBCInfo');
        $service = SYBCInfo::withBody($JSONBody);
    }

    function testGetTransactionInfoClientInvalidUrnFormat() {
        $service = new TransactionClient($this->clientMock);
        $this->expectExceptionMessage('Invalid URN format');
        $service->getTransactionInfo($this->clientConfig, 'PTN-PSY-DEM1S-E0119-12345', 'en');

    }
    function testGetTransactionInfoInvalidMerchantToken() {
        $service = new TransactionClient($this->clientMock);
        $this->clientConfig->setMerchantToken('');

        $this->expectExceptionMessage('GetTransactionInfo(): ClientConfig->MerchantToken is not set');
        $received =
            $service->getTransactionInfo($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');
    }

}