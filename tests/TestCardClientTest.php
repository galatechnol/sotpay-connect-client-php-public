<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Src\main\client\models\enums\Domain;
use Src\main\client\models\enums\PaymentGateway;
use Src\main\client\config\ClientConfig;
use Src\main\client\models\TestCard;
use Src\main\client\clients\TestCardClient;
use Src\main\client\models\ResponseError;

class TestCardClientTest extends TestCase
{
    private Client $clientMock;
    private TestCard $testCard;
    private TestCardClient $testCardClient;
    private ClientConfig $clientConfig;
    private PaymentGateway $paymentGateway;

    protected function setUp(): void {

        //Creating mock GUZZLE client
        $this->clientMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock(); //direct request

        //Creating our config class
        $baseurl = 'https://connect.sotpay.co.uk';
        $merchant = '';
        $domain = Domain::SANDBOX;
        $merchantToken = '';
        $signatureKeyInbound = '';
        $tokenAdminKey = '';
        $this->paymentGateway = PaymentGateway::sybc;
        $this->clientConfig = new ClientConfig($baseurl, $merchant, $domain, $merchantToken,
            $signatureKeyInbound, $tokenAdminKey);

    }

    ///TestCardClient testing
    function testTestCardClient200() {

        //Create our expected outcome
        $expectedTestCard = new TestCard();
        $expectedTestCard->setObject('TestCard');
        $expectedTestCard->setPaymentGateway(PaymentGateway::sybc);
        $expectedTestCard->setCardType('VISA');
        $expectedTestCard->setCardNumber('4129123456789012');
        $expectedTestCard->setCardholderName('C. SMITH');
        $expectedTestCard->setExpiry('2024-12');
        $expectedTestCard->setSecurityCode('123');
        $expectedTestCard->setD3s('PASS');
        $expectedTestCard->setDetailsLink('https://connect.sotpay.co.uk/documents/sybc-test-scenarios.pdf');

        //Response Body
        $JSONdata = '{
          "object" : "TestCard",
          "paymentGateway" : "sybc",
          "cardType" : "VISA",
          "cardNumber" : "4129123456789012",
          "cardholderName" : "C. SMITH",
          "expiry" : "2024-12",
          "securityCode" : "123",
          "d3s" : "PASS",
          "detailsLink" : "https://connect.sotpay.co.uk/documents/sybc-test-scenarios.pdf"
        }';

        //class stub

        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONdata));
        $service = new TestCardClient($this->clientMock);

        //return value
        $receivedTestCard = $service->getTestCard($this->clientConfig, $this->paymentGateway, 'en');

        // Compare values
        $this->assertEquals($expectedTestCard, $receivedTestCard);
    }

    function testTestCardClient200Nua() {

        //Create our expected outcome

        $expectedTestCard = new TestCard();
        $expectedTestCard->setObject('TestCard');
        $expectedTestCard->setPaymentGateway(PaymentGateway::nuapay);
        $expectedTestCard->setAccountUsername('psu');
        $expectedTestCard->setAccountPassword('psu');
        $expectedTestCard->setDetailsLink('https://developer.nuapay.com/ob_sandbox.html');

        //Response Body
        $JSONdata = '{
          "object": "TestCard",
          "paymentGateway": "nuapay",
          "accountUsername": "psu",
          "accountPassword": "psu",
          "detailsLink": "https://developer.nuapay.com/ob_sandbox.html"
        }';

        //class stub

        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONdata));
        $service = new TestCardClient($this->clientMock);

        //return value
        $receivedTestCard = $service->getTestCard($this->clientConfig, $this->paymentGateway, 'en');

        // Compare values
        $this->assertEquals($expectedTestCard, $receivedTestCard);
    }

    function testTestCardClient204() {
        //Response Body


        //class stub

        $this->clientMock->method('request')
            ->willReturn(new Response(204));
        $service = new TestCardClient($this->clientMock);

        //return value
        $receivedTestCard = $service->getTestCard($this->clientConfig, $this->paymentGateway, 'en');

        // Compare values
        $this->assertNull($receivedTestCard);
    }

    //NULL Payment Gateway + other error 400 codes
    function testTestCardClient400() {

        //Create our expected Response Error
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_NULL_PAYMENTGATEWAY');
        $expectedResponse->setMessage('The PaymentGateway was null');

        $JSONdata = '{
          "object": "Error",
          "id": "ERROR_NULL_PAYMENTGATEWAY",
          "message": "The PaymentGateway was null"
        }';

        //class stub
        $this->clientMock->method('request')
            ->willReturn(new Response(400, [], $JSONdata));
        $service = new TestCardClient($this->clientMock);

        //return value
        $receivedResponse = $service->getTestCard($this->clientConfig, $this->paymentGateway, 'en');

        //Compare values
        $this->assertEquals($expectedResponse, $receivedResponse);
    }

    //For internal server errors + invalid Gateways
    function testTestCardClient500() {

        //Create our expected Response Error
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_INTERNAL');
        $expectedResponse->setMessage('An Exception Occurred');

        $JSONdata = '{
          "object": "Error",
          "id": "ERROR_INTERNAL",
          "message": "An Exception Occurred"
        }';

        //class stub
        $this->clientMock->method('request')
            ->willReturn(new Response(500, [], $JSONdata));
        $service = new TestCardClient($this->clientMock);

        //return value
        $receivedResponse = $service->getTestCard($this->clientConfig, $this->paymentGateway, 'en');

        //Compare values
        $this->assertEquals($expectedResponse, $receivedResponse);
    }

    //Other response codes apart from the ones in match statement
    function testTestCardClientOtherResponseCodes() {
        $this->clientMock->method('request')
            ->willReturn(new Response(100, [],));
        $service = new TestCardClient($this->clientMock);
        $this->expectException(Exception::class);
        $service->getTestCard($this->clientConfig, $this->paymentGateway, 'en');

    }

    //Invalid Base URL
    function testTestCardClientInvalidBaseURL() {

        //class stub
        $this->clientMock->method('request')
            ->willThrowException(new ConnectException(
                'cURL error 6: Could not resolve host: https://connnnnect.sotpay.co.uk/api/testcard/sybc
                        (see https://curl.haxx.se/libcurl/c/libcurl-errors.html)',
                new Request('GET', 'https://connnnnect.sotpay.co.uk/api/testcard/sybc')
            ));

        //assertion
        $service = new TestCardClient($this->clientMock);
        $this->expectException(ConnectException::class);
        $service->getTestCard($this->clientConfig, $this->paymentGateway,'en');
    }

    function testTestCardClientGatewayNull() {
        $testCardClient = new TestCardClient($this->clientMock);
        $this->expectException(TypeError::class);
        $res = $testCardClient->getTestCard($this->clientConfig, null, 'en');

    }

    function testTestCardClientConfigNull() {
        $testCardClient = new TestCardClient($this->clientMock);
        $this->expectException(TypeError::class);
        $res = $testCardClient->getTestCard(null, $this->paymentGateway, 'en');
    }

    function testTestCardInvalidJSON() {
        $this->expectExceptionMessage('Wrong data type sent to instance of TestCard, expected JSON body');
        $service = TestCard::withBody('invalid JSON as string');
    }


}