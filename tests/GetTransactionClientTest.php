<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Src\main\client\clients\TransactionClient;
use Src\main\client\config\ClientConfig;
use Src\main\client\models\components\contact;
use Src\main\client\models\components\merchantKey;
use Src\main\client\models\enums\Country;
use Src\main\client\models\enums\Currency;
use Src\main\client\models\enums\Domain;
use Src\main\client\models\enums\DurationClassification;
use Src\main\client\models\enums\EmailStatus;
use Src\main\client\models\enums\Medium;
use Src\main\client\models\enums\Progress;
use Src\main\client\models\enums\Status;
use Src\main\client\models\ResponseError;
use Src\main\client\models\Transaction;
use Src\main\client\models\TransactionObjects\transactionAddress;
use Src\main\client\models\TransactionObjects\deliveryAddress;
use Src\main\client\models\TransactionObjects\transactionDetails;
use Src\main\client\models\TransactionObjects\transactionLocales;
use Src\main\client\models\TransactionObjects\notificationMessage;
use Src\main\client\models\TransactionObjects\payMessage;
use Src\main\client\models\TransactionObjects\TransactionContact;
use Src\main\client\models\TransactionObjects\transactionMessage;
use Src\main\client\models\TransactionObjects\transactionProgress;

class GetTransactionClientTest extends TestCase
{
    private Client $clientMock;
    private ClientConfig $clientConfig;
    private Transaction $getTransaction;
    private TransactionClient $getTransactionClient;

    protected function setUp(): void
    {
        //create and config guzzle mockClient
        $this->clientMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock(); //direct request

        $baseurl = 'https://connect.sotpay.co.uk';
        $merchant = 'democo1';
        $domain = Domain::SANDBOX;
        $merchantToken = 'tk_sand_ras35vZfuFCzjD9gXpZ7PkZsTVm4Y8PEPTwdDXD3xz';
        $signatureKeyInbound = '';
        $tokenAdminKey = '';
        $this->clientConfig = new ClientConfig($baseurl, $merchant, $domain, $merchantToken,
            $signatureKeyInbound, $tokenAdminKey);

    }

    function testGetTransactionClient200()
    {
        $expectedTransaction = new Transaction();
        $expectedTransaction->setObject('Transaction');
        $expectedTransaction->setUrn('STN-PSY-DEM1S-E0119-12345');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setAmount(100.99);
        $expectedTransaction->setCurrency(Currency::GBP);
        $expectedTransaction->setTermsUrl('https://www.myurl.com/terms');
        $expectedTransaction->setTermsAccepted(false);
        $expectedTransaction->setExpiresAfter(259200);
        $expectedTransaction->setStatus(STATUS::EXPIRED);
        $expectedTransaction->setDetails(
            new transactionDetails('ORDER123456',
                        'INV7890',
                        'Test Transaction',
                        'RJONES',
                        true));
        $expectedTransaction->setContact(new TransactionContact('rjones@democo1.com','+441234567890'));
        $expectedTransaction->setNotificationContact(
            new TransactionContact('rjones@democo1.com','+440987654321'));
        $expectedTransaction->setBillingAddress(new transactionAddress(
            'Chris',
            'Smith',
            'Chris Smith',
            '123a High Street',
            'Wath-upon-Dearne',
            'Rotherham',
            'S63 1AA',
            'South Yorkshire',
            Country::GB
        ));
        $expectedTransaction->setDeliveryAddress(new transactionAddress(
            'Brendan',
            'White',
            'Brendan White',
            '77 Saint Mary\'s Walk',
            '77 Saint Mary\'s Walk',
            'Cambridge',
            'CB4 3BP',
            'Cambridgeshire',
            Country::GB
        ));
        $expectedTransaction->setLocales(new transactionLocales('en','en_GB'));
        $expectedTransaction->setPayMessage(
            new TransactionMessage(true, false,));
        $expectedTransaction->setReceiptMessage(new transactionMessage(true,false));
        $expectedTransaction->setNotificationMessage(new TransactionMessage(true,false));;
        $expectedTransaction->setProgress(new transactionProgress(
            Progress::COMPLETED,
            Medium::EMAIL,
            new DateTime('2024-01-25T16:38:40Z'),
            new DateTime('2024-01-25T16:39:45Z'),
            new DateTime('2024-01-25T16:41:06Z'),
            new DateTime('2024-01-25T16:41:10Z'),
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:121.0) Gecko/20100101 Firefox/121.0',
            true,
            150,
            '2m 30s',
            DurationClassification::FASTEST
        ));
        $expectedTransaction->setCreateCardOnFile(false);
        $expectedTransaction->setDebug(false);
        $expectedTransaction->setCreated('2024-01-19T15:25:45Z');
        $expectedTransaction->setUpdated('2024-01-22T15:40:05Z');
        $expectedTransaction->setExpiry('2024-01-22T15:25:44Z');

        $JSONData = '{
           "object":"Transaction",
           "urn":"STN-PSY-DEM1S-E0119-12345",
           "merchantKey":{
              "merchant":"democo1",
              "domain":"SANDBOX"
           },
           "amount":100.99,
           "currency":"GBP",
           "termsUrl":"https://www.myurl.com/terms",
           "termsAccepted":false,
           "expiresAfter":259200,
           "status":"EXPIRED",
           "details":{
              "reference":"ORDER123456",
              "referenceSecondary":"INV7890",
              "description":"Test Transaction",
              "agent":"RJONES",
              "attended":true
           },
           "contact":{
              "email":"rjones@democo1.com",
              "phone":"+441234567890"
           },
           "notificationContact":{
              "email":"rjones@democo1.com",
              "phone":"+440987654321"
           },
           "billingAddress":{
              "firstName":"Chris",
              "surname":"Smith",
              "displayName":"Chris Smith",
              "line1":"123a High Street",
              "line2":"Wath-upon-Dearne",
              "city":"Rotherham",
              "postcode":"S63 1AA",
              "state":"South Yorkshire",
              "country":"GB"
           },
           "deliveryAddress":{
              "firstName":"Brendan",
              "surname":"White",
              "displayName":"Brendan White",
              "line1":"77 Saint Mary\'s Walk",
              "line2":"77 Saint Mary\'s Walk",
              "city":"Cambridge",
              "postcode":"CB4 3BP",
              "state":"Cambridgeshire",
              "country":"GB"
           },
           "locales":{
              "agent":"en",
              "customer":"en_GB"
           },
           "payMessage":{
              "email":true,
              "sms":false
           },
           "receiptMessage":{
              "email":true,
              "sms":false
           },
           "notificationMessage":{
              "email":true,
              "sms":false
           },
           "progress":{
              "progress":"COMPLETED",
              "completionMedium":"EMAIL",
              "startedTimestamp":"2024-01-25T16:38:40Z",
              "payingTimestamp":"2024-01-25T16:39:45Z",
              "authenticatingTimestamp":"2024-01-25T16:41:06Z",
              "completedTimestamp":"2024-01-25T16:41:10Z",
              "payingAgent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:121.0) Gecko/20100101 Firefox/121.0",
              "locked":true,
              "completionDuration":150,
              "completionDurationDescription":"2m 30s",
              "completionDurationClassification":"FASTEST"
           },
           "createCardOnFile":false,
           "debug":false,
           "created":"2024-01-19T15:25:45Z",
           "updated":"2024-01-22T15:40:05Z",
           "expiry":"2024-01-22T15:25:44Z"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetTransactionClient204() {
        $this->clientMock->method('request')
            ->willReturn(new Response(204));

        $service = new TransactionClient($this->clientMock);
        $receivedTransaction
            = $service->getTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');
        $this->assertEquals("Transaction Not Found", $receivedTransaction);
    }

    function testGetTransactionClient401() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_UNAUTHORIZED');
        $expectedResponse->setMessage('Unauthorized');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_UNAUTHORIZED",
          "message": "Unauthorized"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(401, [], $JSONData));

        $service = new TransactionClient($this->clientMock);
        $receivedTransaction =
            $service->getTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedResponse, $receivedTransaction);
    }

    function testGetTransactionClient404() {
        $this->clientMock->method('request')
            ->willReturn(new Response(404));

        $service = new TransactionClient($this->clientMock);
        $this->expectExceptionMessage('Error 404 Not Found, Invalid Relative URL');
        $receivedTransaction =
            $service->getTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');
    }

    function testGetTransactionClient500() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_INTERNAL');
        $expectedResponse->setMessage('An Exception Occurred');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_INTERNAL",
          "message": "An Exception Occurred"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(500, [], $JSONData));

        $service = new TransactionClient($this->clientMock);
        $receivedTransaction =
            $service->getTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedResponse, $receivedTransaction);
    }

    function testGetTransactionClient100() {
        $this->clientMock->method('request')
            ->willReturn(new Response(100));

        $this->expectExceptionMessage('Response code not recognised, please check Request');
        $service = new TransactionClient($this->clientMock);
        $service->getTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');
    }

    function testGetTransactionClientNullConfig() {
        $service = new TransactionClient($this->clientMock);
       $this->expectException(TypeError::class);
        $service->getTransaction(null,'STN-PSY-DEM1S-E0119-12345', 'en');
    }

    function testGetTransactionClientNullUrn() {
        $service = new TransactionClient($this->clientMock);
        $this->expectException(TypeError::class);
        $service->getTransaction($this->clientConfig,null, 'en');
    }

    function testGetTransactionInvalidJSON() {
        $this->expectExceptionMessage('Wrong data type sent, expected JSON body');
        $service = Transaction::withBody('invalid JSON as string');
    }

    function testGetTransactionInvalidJSONProperty() {
        $JSONBody = '{
          "object": "string",
          "urn": "STN-PSY-DEM1S-A0000-AB123",
          "merchantKey": {
          "merchant": "democo1",
          "domain": "SANDBOX"
          },
          "breakcode": 100.99
        }';
        $this->expectExceptionMessage('JSON property "breakcode" does not exist in object of type Src\main\client\models\Transaction');
        $service = Transaction::withBody($JSONBody);
    }

    function testGetTransactionClientInvalidUrnFormat() {
        $service = new TransactionClient($this->clientMock);
        $this->expectExceptionMessage('Invalid URN format');
        $service->getTransaction($this->clientConfig, 'PTN-PSY-DEM1S-E0119-12345', 'en');

    }

    function testGetTransactionInvalidMerchantToken() {
        $service = new TransactionClient($this->clientMock);
        $this->clientConfig->setMerchantToken('');

        $this->expectExceptionMessage('GetTransaction(): ClientConfig->MerchantToken is not set');
        $received =
            $service->getTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');
    }
}

