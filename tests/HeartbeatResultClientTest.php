<?php


use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Src\main\client\models\enums\Domain;
use Src\main\client\clients\HeartbeatResultClient;
use Src\main\client\models\HeartbeatResult;
use Src\main\client\config\ClientConfig;
use Src\main\client\models\ResponseError;

class HeartbeatResultClientTest extends TestCase
{
    private Client $clientMock;
    private ClientConfig $clientConfig;
    private HeartbeatResult $heartbeat;
    private HeartbeatResultClient $heartbeatClient;

    protected function setUp(): void
    {
        //Creating our GUZZLE client
        $this->clientMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock(); //direct request

        //Creating our local client
        $baseurl = 'https://connect.sotpay.co.uk';
        $merchant = '';
        $domain = Domain::SANDBOX;
        $merchantToken = '';
        $signatureKeyInbound = '';
        $tokenAdminKey = '';


        $this->clientConfig = new ClientConfig($baseurl, $merchant, $domain, $merchantToken,
            $signatureKeyInbound, $tokenAdminKey);
    }

    ///HeartbeatResultClient testing

    function testHeartbeatResultClient200()
    {

        $expectedHeartbeat = new HeartbeatResult();
        $expectedHeartbeat->setObject('HeartbeatResult');
        $expectedHeartbeat->setTimeZone('UTC');
        $expectedHeartbeat->setTimeStamp(new DateTime('2024-01-01 12:00:00.000000'));
        $timezone = new DateTimeZone('Europe/London');
        $expectedHeartbeat->setTimeStampLondon((new DateTime('2024-01-01 12:00:00.000000', $timezone)));

        $JSONData = '{
          "object": "HeartbeatResult",
          "dbTimezone": "UTC",
          "dbTimestampNow": "2024-01-01 12:00:00.000000",
          "dbTimestampNowLondon": "2024-01-01 12:00:00.000000"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $receivedHeartbeat = new HeartbeatResultClient($this->clientMock);
        $receivedHeartbeat = $receivedHeartbeat->getHeartbeatResult($this->clientConfig, 'en');

        $this->assertEquals($expectedHeartbeat, $receivedHeartbeat);
    }

    function testHeartbeatResultClient500()
    {
        $expectedHeartbeat = new ResponseError();
        $expectedHeartbeat->setObject('Error');
        $expectedHeartbeat->setId('ERROR_INTERNAL');
        $expectedHeartbeat->setMessage('An Exception Occurred');

        $JSONdata = '{
          "object": "Error",
          "id": "ERROR_INTERNAL",
          "message": "An Exception Occurred"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(500, [], $JSONdata));
        $receivedHeartbeat = new HeartbeatResultClient($this->clientMock);
        $receivedHeartbeat = $receivedHeartbeat->getHeartbeatResult($this->clientConfig, 'en');

        $this->assertEquals($expectedHeartbeat, $receivedHeartbeat);
    }

    function testHeartbeatResultOtherResponseCodes()
    {
        $this->clientMock->method('request')
            ->willReturn(new Response (100, [],));
        $service = new HeartbeatResultClient($this->clientMock);;
        $this->expectExceptionMessage('Response code not recognised, please check URL');
        $service->getHeartbeatResult($this->clientConfig, 'en');
    }
}