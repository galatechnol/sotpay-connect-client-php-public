<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Src\main\client\clients\TransactionClient;
use Src\main\client\config\ClientConfig;
use Src\main\client\models\components\merchantKey;
use Src\main\client\models\enums\Country;
use Src\main\client\models\enums\Currency;
use Src\main\client\models\enums\Domain;
use Src\main\client\models\enums\Progress;
use Src\main\client\models\enums\Status;
use Src\main\client\models\ResponseError;
use Src\main\client\models\Transaction;
use Src\main\client\models\TransactionObjects\transactionAddress;
use Src\main\client\models\TransactionObjects\transactionDetails;
use Src\main\client\models\TransactionObjects\transactionLocales;
use Src\main\client\models\TransactionObjects\TransactionContact;
use Src\main\client\models\TransactionObjects\transactionMessage;
use Src\main\client\models\TransactionObjects\transactionProgress;
use Src\main\client\models\TransactionObjects\TransactionWriteMessage;
use Src\main\client\models\TransactionWrite;

class CreateTransactionClientTest extends TestCase
{
    private Client $clientMock;
    private ClientConfig $clientConfig;
    private Transaction $expectedTransaction;
    private TransactionWrite $createTransaction;
    private string $JSONData;

    protected function setUp(): void
    {
        //create and config guzzle mock client
        $this->clientMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock(); //direct request

        //ClientConfig
        $baseurl = 'https://connect.sotpay.co.uk';
        $merchant = '';
        $domain = Domain::SANDBOX;
        $merchantToken = 'tk_sand_ras35vZfuFCzjD9gXpZ7PkZsTVm4Y8PEPTwdDXD3xz';
        $signatureKeyInbound = '';
        $tokenAdminKey = '';
        $this->clientConfig = new ClientConfig($baseurl, $merchant, $domain, $merchantToken,
            $signatureKeyInbound, $tokenAdminKey);

        //Model Transaction
        $expectedTransaction = new Transaction();
        $expectedTransaction->setObject('Transaction');
        $expectedTransaction->setUrn('STN-PSY-DEM1S-E0119-12345');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setAmount(100.99);
        $expectedTransaction->setCurrency(Currency::GBP);
        $expectedTransaction->setTermsUrl('https://www.myurl.com/terms');
        $expectedTransaction->setTermsAccepted(false);
        $expectedTransaction->setExpiresAfter(259200);
        $expectedTransaction->setStatus(STATUS::NEW);
        $expectedTransaction->setDetails(
            new transactionDetails('ORDER123456',
                'INV7890',
                'Test Transaction',
                'RJONES',
                true));
        $expectedTransaction->setContact(new TransactionContact('csmith@test.com','+441234567890'));
        $expectedTransaction->setNotificationContact(
            new TransactionContact('rjones@democo1.com','+440987654321'));
        $expectedTransaction->setBillingAddress(new transactionAddress(
            'Chris',
            'Smith',
            'Chris Smith',
            '123a High Street',
            'Wath-upon-Dearne',
            'Rotherham',
            'S63 1AA',
            'South Yorkshire',
            Country::GB
        ));
        $expectedTransaction->setDeliveryAddress(new transactionAddress(
            'Brendan',
            'White',
            'Brendan White',
            '77 Saint Mary\'s Walk',
            '77 Saint Mary\'s Walk',
            'Cambridge',
            'CB4 3BP',
            'Cambridgeshire',
            Country::GB
        ));
        $expectedTransaction->setLocales(new transactionLocales('en','en_GB'));
        $expectedTransaction->setPayMessage(
            new TransactionMessage(true, false));
        $expectedTransaction->setReceiptMessage(new transactionMessage(true,false));
        $expectedTransaction->setNotificationMessage(new TransactionMessage(true,false));;
        $expectedTransaction->setProgress(new transactionProgress(
            Progress::REGISTERED,
            null,
            null,
            null,
            null,
            null,
            null,
            false,
            null,
            null,
            null
        ));
        $expectedTransaction->setCreateCardOnFile(false);
        $expectedTransaction->setDebug(false);
        $expectedTransaction->setCreated('2024-02-07T10:22:10Z');
        $expectedTransaction->setUpdated('2024-02-07T10:22:10Z');
        $expectedTransaction->setExpiry('2024-02-10T10:22:09Z');
        $this->expectedTransaction = $expectedTransaction;

        //Model JSON response
        $JsonBody = '{
            "object": "Transaction",
            "urn": "STN-PSY-DEM1S-E0119-12345",
            "merchantKey": {
                "merchant": "democo1",
                "domain": "SANDBOX"
            },
            "amount": 100.99,
            "currency": "GBP",
            "termsUrl": "https://www.myurl.com/terms",
            "termsAccepted": false,
            "expiresAfter": 259200,
            "status": "NEW",
            "details": {
                "reference": "ORDER123456",
                "referenceSecondary": "INV7890",
                "description": "Test Transaction",
                "agent": "RJONES",
                "attended": true
            },
            "contact": {
                "email": "csmith@test.com",
                "phone": "+441234567890"
            },
            "notificationContact": {
                "email": "rjones@democo1.com",
                "phone": "+440987654321"
            },
            "billingAddress": {
                "firstName": "Chris",
                "surname": "Smith",
                "displayName": "Chris Smith",
                "line1": "123a High Street",
                "line2": "Wath-upon-Dearne",
                "city": "Rotherham",
                "postcode": "S63 1AA",
                "state": "South Yorkshire",
                "country": "GB"
            },
            "deliveryAddress": {
                "firstName": "Brendan",
                "surname": "White",
                "displayName": "Brendan White",
                "line1": "77 Saint Mary\'s Walk",
                "line2": "77 Saint Mary\'s Walk",
                "city": "Cambridge",
                "postcode": "CB4 3BP",
                "state": "Cambridgeshire",
                "country": "GB"
            },
            "locales": {
                "agent": "en",
                "customer": "en_GB"
            },
            "payMessage": {
                "email": true,
                "sms": false
            },
            "receiptMessage": {
                "email": true,
                "sms": false
            },
            "notificationMessage": {
                "email": true,
                "sms": false
            },
            "progress": {
                "progress": "REGISTERED",
                "completionMedium": null,
                "startedTimestamp": null,
                "payingTimestamp": null,
                "authenticatingTimestamp": null,
                "completedTimestamp": null,
                "payingAgent": null,
                "locked": false,
                "completionDuration": null,
                "completionDurationDescription": null,
                "completionDurationClassification": null
            },
            "createCardOnFile": false,
            "debug": false,
            "created": "2024-02-07T10:22:10Z",
            "updated": "2024-02-07T10:22:10Z",
            "expiry": "2024-02-10T10:22:09Z"
        }';

        $this->JSONData = $JsonBody;


        //The transaction we use to create
        $this->createTransaction = new TransactionWrite(
            new merchantKey('democo1',Domain::SANDBOX),
            100.99,
            Currency::GBP
        );
        $this->createTransaction->setTermsUrl('https://www.myurl.com/terms');
        $this->createTransaction->setExpiresAfter(259200);
        $this->createTransaction->setDetails(
            new transactionDetails(
                'ORDER123456,',
                'INV7890',
                'Test Transaction',
                'RJONONES',
                true
            ));
        $this->createTransaction->setContact(new TransactionContact('csmith@test.com','+441234567890'));
        $this->createTransaction->setNotificationContact(
            new TransactionContact('rjones@democo1.com','+440987654321'));
        $this->createTransaction->setBillingAddress(new transactionAddress(
            'Chris',
            'Smith',
            'Chris Smith',
            '123a High Street',
            'Wath-upon-Dearne',
            'Rotherham',
            'S63 1AA',
            'South Yorkshire',
            Country::GB
        ));
        $this->createTransaction->setDeliveryAddress(new transactionAddress(
            'Brendan',
            'White',
            'Brendan White',
            '77 Saint Mary\'s Walk',
            '77 Saint Mary\'s Walk',
            'Cambridge',
            'CB4 3BP',
            'Cambridgeshire',
            Country::GB
        ));
        $this->createTransaction->setLocales(new transactionLocales('en','en_GB'));
        $this->createTransaction->setPayMessage(
            new TransactionWriteMessage(true, false, ));
        $this->createTransaction->setReceiptMessage(
            new TransactionWriteMessage(true,false));
        $this->createTransaction->setNotificationMessage(
            new TransactionWriteMessage(true,false));;
    }

    function testCreateTransaction201() {

        $this->clientMock->method('request')
            ->willReturn(new Response(201, [], $this->JSONData));

        $service = new TransactionClient($this->clientMock);
        $receivedTransaction =
            $service->createTransaction($this->clientConfig, $this->createTransaction, 'en');

        $this->assertEquals($this->expectedTransaction, $receivedTransaction);
    }

    //When bearer token is invalid or not existing
    function testCreateTransaction401() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setId('ERROR_UNAUTHORIZED');
        $expectedResponse->setObject('Error');
        $expectedResponse->setMessage('Unauthorized');
        $JSONData = '{
          "object": "Error",
          "id": "ERROR_UNAUTHORIZED",
          "message": "Unauthorized"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(401, [], $JSONData));
        $service = new TransactionClient($this->clientMock);

        $receiveTransaction =
            $service->createTransaction($this->clientConfig, $this->createTransaction, 'en');
        $this->assertEquals($expectedResponse, $receiveTransaction);
    }

    //when authentication fails (incorrect x-jws-signature etc.)
    function testCreateTransaction403() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setId('ERROR_FORBIDDEN');
        $expectedResponse->setObject('Error');
        $expectedResponse->setMessage('Forbidden');
        $JSONData = '{
          "object": "Error",
          "id": "ERROR_FORBIDDEN",
          "message": "Forbidden"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(403, [], $JSONData));
        $service = new TransactionClient($this->clientMock);

        $receiveTransaction =
            $service->createTransaction($this->clientConfig, $this->createTransaction, 'en');
        $this->assertEquals($expectedResponse, $receiveTransaction);
    }

    //giving it some invalid TransactionWrite object
    function testCreateTransaction422() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setId('ERROR_INVALID_TRANSACTIONWRITE');
        $expectedResponse->setObject('Error');
        $expectedResponse->setMessage('The TransactionWrite was invalid');
        $JSONData = '{
          "object": "Error",
          "id": "ERROR_INVALID_TRANSACTIONWRITE",
          "message": "The TransactionWrite was invalid"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(422, [], $JSONData));
        $service = new TransactionClient($this->clientMock);

        $receiveTransaction =
            $service->createTransaction($this->clientConfig, $this->createTransaction, 'en');
        $this->assertEquals($expectedResponse, $receiveTransaction);
    }

    //Internal server error
    function testCreateTransaction500() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setId('ERROR_INTERNAL');
        $expectedResponse->setObject('Error');
        $expectedResponse->setMessage('An Exception Occurred');
        $JSONData = '{
          "object": "Error",
          "id": "ERROR_INTERNAL",
          "message": "An Exception Occurred"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(500, [], $JSONData));
        $service = new TransactionClient($this->clientMock);

        $receiveTransaction =
            $service->createTransaction($this->clientConfig, $this->createTransaction, 'en');
        $this->assertEquals($expectedResponse, $receiveTransaction);
    }

    function testCreateTransactionNullTransaction() {
        $service = new TransactionClient($this->clientMock);

        $this->expectException(TypeError::class);
        $received =
            $service->createTransaction($this->clientConfig, null, 'en');
    }

    function testCreateTransactionNullConfig() {
        $service = new TransactionClient($this->clientMock);

        $this->expectException(TypeError::class);
        $received =
            $service->createTransaction(null, $this->createTransaction, 'en');
    }

    function testCreateTransactionInvalidMerchantToken() {
        $service = new TransactionClient($this->clientMock);
        $this->clientConfig->setMerchantToken('');

        $this->expectExceptionMessage('CreateTransaction(): ClientConfig->MerchantToken is not set');
        $received =
            $service->createTransaction($this->clientConfig, $this->createTransaction, 'en');
    }
}