<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Src\main\client\clients\TransactionClient;
use Src\main\client\config\ClientConfig;
use Src\main\client\models\enums\Domain;
use Src\main\client\models\ResponseError;
use Src\main\client\models\TransactionObjects\notificationMessage;
use Src\main\client\models\TransactionObjects\payMessage;

class ResendTransactionClientTest1 extends TestCase
{
    private Client $clientMock;
    private ClientConfig $clientConfig;

    protected function setUp(): void
    {
        //create and config guzzle mockClient
        $this->clientMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock(); //direct request

        $baseurl = 'https://connect.sotpay.co.uk';
        $merchant = 'democo1';
        $domain = Domain::SANDBOX;
        $merchantToken = 'tk_sand_ras35vZfuFCzjD9gXpZ7PkZsTVm4Y8PEPTwdDXD3xz';
        $signatureKeyInbound = '';
        $tokenAdminKey = '';
        $this->clientConfig = new ClientConfig($baseurl, $merchant, $domain, $merchantToken,
            $signatureKeyInbound, $tokenAdminKey);

    }

    function testResendTransactionClient200()
    {
        $this->clientMock->method('request')
            ->willReturn(new Response(200));

        $service = new TransactionClient($this->clientMock);
        $received = $service->resendTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345','en');

        $this->assertEquals('Transaction STN-PSY-DEM1S-E0119-12345 has been re-sent', $received);
    }

    //Same for invalid format, null/invalid transaction
    function testResendTransactionClient400() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_NULL_URN');
        $expectedResponse->setMessage('The urn was null');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_NULL_URN",
          "message": "The urn was null"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(400, [], $JSONData));

        $service = new TransactionClient($this->clientMock);
        $receivedTransaction =
            $service->resendTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedResponse, $receivedTransaction);
    }

    function testResendTransactionClient401() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_UNAUTHORIZED');
        $expectedResponse->setMessage('Unauthorized');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_UNAUTHORIZED",
          "message": "Unauthorized"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(401, [], $JSONData));

        $service = new TransactionClient($this->clientMock);
        $receivedTransaction =
            $service->resendTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedResponse, $receivedTransaction);
    }

    function testResendTransactionClient500() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_INTERNAL');
        $expectedResponse->setMessage('An Exception Occurred');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_INTERNAL",
          "message": "An Exception Occurred"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(500, [], $JSONData));

        $service = new TransactionClient($this->clientMock);
        $receivedTransaction =
            $service->resendTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedResponse, $receivedTransaction);
    }


    function testResendTransactionClientNullConfig() {
        $service = new TransactionClient($this->clientMock);
       $this->expectException(TypeError::class);
        $service->resendTransaction(null,'STN-PSY-DEM1S-E0119-12345', 'en');
    }

    function testResendTransactionClientNullUrn() {
        $service = new TransactionClient($this->clientMock);
        $this->expectException(TypeError::class);
        $service->resendTransaction($this->clientConfig,null, 'en');
    }


    function testResendTransactionClientInvalidUrnFormat() {
        $service = new TransactionClient($this->clientMock);
        $this->expectExceptionMessage('Invalid URN format');
        $service->resendTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

    }

    function testResendTransactionInvalidMerchantToken() {
        $service = new TransactionClient($this->clientMock);
        $this->clientConfig->setMerchantToken('');

        $this->expectExceptionMessage('resendTransaction(): ClientConfig->MerchantToken is not set');
        $received =
            $service->resendTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');
    }
}