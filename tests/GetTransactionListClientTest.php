<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Src\main\client\clients\TransactionClient;
use Src\main\client\config\ClientConfig;
use Src\main\client\models\components\contact;
use Src\main\client\models\components\ListsWrapper;
use Src\main\client\models\components\merchantKey;
use Src\main\client\models\components\Page;
use Src\main\client\models\enums\Currency;
use Src\main\client\models\enums\Domain;
use Src\main\client\models\enums\EmailStatus;
use Src\main\client\models\enums\PaymentGateway;
use Src\main\client\models\enums\sortOrder;
use Src\main\client\models\enums\sortPropertyTransaction;
use Src\main\client\models\enums\Status;
use Src\main\client\models\ResponseError;
use Src\main\client\models\Transaction;
use Src\main\client\models\TransactionObjects\transactionAddress;
use Src\main\client\models\TransactionObjects\deliveryAddress;
use Src\main\client\models\TransactionObjects\transactionDetails;
use Src\main\client\models\TransactionObjects\transactionLocales;
use Src\main\client\models\TransactionObjects\notificationMessage;
use Src\main\client\models\TransactionObjects\payMessage;
use Src\main\client\models\TransactionObjects\TransactionContact;
use Src\main\client\models\TransactionObjects\transactionMessage;
use Src\main\client\models\TransactionObjects\transactionProgress;

class GetTransactionListClientTest extends TestCase
{
    private Client $clientMock;
    private ClientConfig $clientConfig;
    private Transaction $getTransaction;
    private TransactionClient $getTransactionClient;


    protected function setUp(): void
    {
        //create and config guzzle mockClient
        $this->clientMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock(); //direct request

        $baseurl = 'https://connect.sotpay.co.uk';
        $merchant = 'democo1';
        $domain = Domain::SANDBOX;
        $merchantToken = 'tk_sand_ras35vZfuFCzjD9gXpZ7PkZsTVm4Y8PEPTwdDXD3xz';
        $signatureKeyInbound = '';
        $tokenAdminKey = '';
        $this->clientConfig = new ClientConfig($baseurl, $merchant, $domain, $merchantToken,
            $signatureKeyInbound, $tokenAdminKey);

    }

    function testGetTransactionListClient200()
    {

        //1st element
        $transactionOne = new Transaction();
        $transactionOne->setObject("Transaction");
        $transactionOne->setUrn("STN-PSY-DEM1S-E0119-12345");
        $transactionOne->setMerchantKey(new merchantKey("democo1",Domain::SANDBOX));
        $transactionOne->setAmount(13.34);
        $transactionOne->setCurrency(Currency::GBP);
        $transactionOne->setPaymentGateway(PaymentGateway::globalpaymentsrealex);
        $transactionOne->setTermsAccepted(false);
        $transactionOne->setStatus(Status::FAILED);
        $transactionOne->setDetails(
            new transactionDetails("ORDER123456",
            "Testing",
            "Test Transaction",
            "carl",
            false,
        ));
        $transactionOne->setNotificationContact(
            new TransactionContact("carl@democo1.com", "07123456789")
        );
        $transactionOne->setLocales(new transactionLocales('en','en'));
        $transactionOne->setPayMessage(
            new payMessage(true, false, EmailStatus::DELIVERY_SUCCESS));
        $transactionOne->setProgress(new transactionProgress(
            'COMPLETED',
            'EMAIL',
            new DateTime('2020-01-05T15:54:00Z'),
            new DateTime('2020-01-05T15:57:00Z'),
            new DateTime('2020-01-09T18:43:00Z'),
            new DateTime('2020-01-05T16:01:00Z'),
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/73.0.3683.86 Safari/537.36',
            false,
            420,
            '7m',
            'FASTEST'
        ));
        $transactionOne->setCreateCardOnFile(false);
        $transactionOne->setDebug(false);
        $transactionOne->setCreated("2023-08-25T10:35:33Z");
        $transactionOne->setUpdated("2023-09-07T02:00:01Z");
        $transactionOne->setExpiry("2024-08-25T10:35:33Z");

        //2nd element
        $transactionTwo = new Transaction();
        $transactionTwo->setObject('Transaction');
        $transactionTwo->setUrn('STN-PSY-DEM1S-E0119-12345');
        $transactionTwo->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $transactionTwo->setAmount(100.99);
        $transactionTwo->setCurrency(Currency::GBP);
        $transactionTwo->setTermsUrl('https://www.myurl.com/terms');
        $transactionTwo->setTermsAccepted(false);
        $transactionTwo->setExpiresAfter(259200);
        $transactionTwo->setStatus(STATUS::EXPIRED);
        $transactionTwo->setDetails(
            new transactionDetails('ORDER123456',
                'INV7890',
                'Test Transaction',
                'RJONES',
                true));
        $transactionTwo->setContact(new contact('rjones@democo1.com','+441234567890'));
        $transactionTwo->setNotificationContact(
            new TransactionContact('rjones@democo1.com','+440987654321'));
        $transactionTwo->setBillingAddress(new transactionAddress(
            'Chris',
            'Smith',
            'Chris Smith',
            '123a High Street',
            'Wath-upon-Dearne',
            'Rotherham',
            'S63 1AA',
            'South Yorkshire',
            'GB'
        ));
        $transactionTwo->setDeliveryAddress(new deliveryAddress(
            'Brendan',
            'White',
            'Brendan White',
            '77 Saint Mary\'s Walk',
            'Cambridge',
            'CB4 3BP',
            'GB',
        ));
        $transactionTwo->setLocales(new transactionLocales('en','en_GB'));
        $transactionTwo->setPayMessage(
            new payMessage(true, false, EmailStatus::DELIVERY_FAILURE));
        $transactionTwo->setReceiptMessage(new transactionMessage(true,false));
        $transactionTwo->setNotificationMessage(new notificationMessage(true,false));;
        $transactionTwo->setProgress(new transactionProgress(
            'COMPLETED',
            'EMAIL',
            new DateTime('2024-01-25T16:38:40Z'),
            new DateTime('2024-01-25T16:39:45Z'),
            new DateTime('2024-01-25T16:41:06Z'),
            new DateTime('2024-01-25T16:41:10Z'),
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:121.0) Gecko/20100101 Firefox/121.0',
            true,
            150,
            '2m 30s',
            'FASTEST'
        ));
        $transactionTwo->setCreateCardOnFile(false);
        $transactionTwo->setDebug(false);
        $transactionTwo->setCreated('2024-01-19T15:25:45Z');
        $transactionTwo->setUpdated('2024-01-22T15:40:05Z');
        $transactionTwo->setExpiry('2024-01-22T15:25:44Z');
        $transactionArr = array($transactionOne, $transactionTwo);
        $page = new Page(array(
            '<https://connect.sotpay.co.uk/api/cardonfile/democo1/SANDBOX/acquired?pageSize=1&pageNumber=1>; title="_self"',
            '<https://connect.sotpay.co.uk/api/cardonfile/democo1/SANDBOX/acquired?pageSize=1&pageNumber=1>; title="_first"',
            '<https://connect.sotpay.co.uk/api/cardonfile/democo1/SANDBOX/acquired?pageSize=1&pageNumber=2>; title="_last"',
            '<https://connect.sotpay.co.uk/api/cardonfile/democo1/SANDBOX/acquired?pageSize=1&pageNumber=2>; title="_next"'
        ));

        $expectedArr = new ListsWrapper($transactionArr, $page);




        $JSONData = '[
          {
            "object": "Transaction",
            "urn": "STN-PSY-DEM1S-E0119-12345",
            "merchantKey": {
              "merchant": "democo1",
              "domain": "SANDBOX"
            },
            "amount": 13.34,
            "currency": "GBP",
            "paymentGateway": "globalpaymentsrealex",
            "termsAccepted": false,
            "status": "FAILED",
            "transactionDetails": {
              "reference": "ORDER123456",
              "referenceSecondary": "Testing",
              "description": "Test Transaction",
              "agent": "carl",
              "attended": false
            },
            "transactionContact": {
              "email": "carl@democo1.com",
              "phone": "07123456789"
            },
            "transactionLocales": {
              "agent": "en",
              "customer": "en"
            },
            "payMessage": {
              "email": true,
              "sms": false,
              "emailStatus": "DELIVERY_SUCCESS"
            },
            "transactionProgress": {
              "transactionProgress": "COMPLETED",
              "completionMedium": "EMAIL",
              "startedTimestamp": "2020-01-05T15:54:00Z",
              "payingTimestamp": "2020-01-05T15:57:00Z",
              "completedTimestamp": "2020-01-05T16:01:00Z",
              "authenticatingTimestamp": "2020-01-09T18:43:00Z",
              "payingAgent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/73.0.3683.86 Safari/537.36",
              "locked": false,
              "completionDuration": 420,
              "completionDurationDescription": "7m",
              "completionDurationClassification": "FASTEST"
            },
            "createCardOnFile": false,
            "debug": false,
            "created": "2023-08-25T10:35:33Z",
            "updated": "2023-09-07T02:00:01Z",
            "expiry": "2024-08-25T10:35:33Z"
          },
          {
            "object": "Transaction",
            "urn": "STN-PSY-DEM1S-E0119-12345",
            "merchantKey": {
                "merchant": "democo1",
                "domain": "SANDBOX"
            },
            "amount": 100.99,
            "currency": "GBP",
            "termsUrl": "https://www.myurl.com/terms",
            "termsAccepted": false,
            "expiresAfter": 259200,
            "status": "EXPIRED",
            "transactionDetails": {
                "reference": "ORDER123456",
                "referenceSecondary": "INV7890",
                "description": "Test Transaction",
                "agent": "RJONES",
                "attended": true
            },
            "contact": {
                "email": "rjones@democo1.com",
                "phone": "+441234567890"
            },
            "transactionContact": {
                "email": "rjones@democo1.com",
                "phone": "+440987654321"
            },
            "transactionAddress": {
                "firstName": "Chris",
                "surname": "Smith",
                "displayName": "Chris Smith",
                "line1": "123a High Street",
                "line2": "Wath-upon-Dearne",
                "city": "Rotherham",
                "postcode": "S63 1AA",
                "state": "South Yorkshire",
                "country": "GB"
            },
            "deliveryAddress": {
                "firstName": "Brendan",
                "surname": "White",
                "displayName": "Brendan White",
                "line1": "77 Saint Mary\'s Walk",
                "city": "Cambridge",
                "postcode": "CB4 3BP",
                "country": "GB"
            },
            "transactionLocales": {
                "agent": "en",
                "customer": "en_GB"
            },
            "payMessage": {
                "email": true,
                "sms": false,
                "emailStatus": "DELIVERY_FAILURE"
            },
            "transactionMessage": {
                "email": true,
            "sms": false
            },
            "notificationMessage": {
                "email": true,
                "sms": false
            },
             "transactionProgress" : {
                "transactionProgress" : "COMPLETED",
                "completionMedium" : "EMAIL",
                "startedTimestamp" : "2024-01-25T16:38:40Z",
                "payingTimestamp" : "2024-01-25T16:39:45Z",
                "authenticatingTimestamp" : "2024-01-25T16:41:06Z",
                "completedTimestamp" : "2024-01-25T16:41:10Z",
                "payingAgent" : "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:121.0) Gecko/20100101 Firefox/121.0",
                "locked" : true,
                "completionDuration" : 150,
                "completionDurationDescription" : "2m 30s",
                "completionDurationClassification" : "FASTEST"
            },
            "createCardOnFile": false,
            "debug": false,
            "created": "2024-01-19T15:25:45Z",
            "updated": "2024-01-22T15:40:05Z",
            "expiry": "2024-01-22T15:25:44Z"
           }
        ]';

        //simulated data
        $expectedPage = [
            'Link' => [
                '<https://connect.sotpay.co.uk/api/cardonfile/democo1/SANDBOX/acquired?pageSize=1&pageNumber=1>; rel="_self"',
                '<https://connect.sotpay.co.uk/api/cardonfile/democo1/SANDBOX/acquired?pageSize=1&pageNumber=1>; rel="_first"',
                '<https://connect.sotpay.co.uk/api/cardonfile/democo1/SANDBOX/acquired?pageSize=1&pageNumber=2>; rel="_last"',
                '<https://connect.sotpay.co.uk/api/cardonfile/democo1/SANDBOX/acquired?pageSize=1&pageNumber=2>; rel="_next"'
            ]
        ];

        //simulated data

        $this->clientMock->method('request')
            ->willReturn(new Response(200, $expectedPage, $JSONData));
        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactions(
            new DateTime('2020-12-31 14:20:59 ', new DateTimeZone(date_default_timezone_get())),
            new DateTime('2023-12-31 14:20:59 '),
            false,
            sortPropertyTransaction::URN,
            SortOrder::ASCENDING,
            10,
            1,
            $this->clientConfig, 'en'
        );

        $this->assertEquals($expectedArr, $receivedTransaction);
    }

    function testGetTransactionClient204() {
        $this->clientMock->method('request')
            ->willReturn(new Response(204));

        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactions(
            new DateTime('2020-12-31 14:20:59 ', new DateTimeZone(date_default_timezone_get())),
            new DateTime('2023-12-31 14:20:59 '),
            false,
            sortPropertyTransaction::URN,
            SortOrder::ASCENDING,
            10,
            1,
            $this->clientConfig, 'en'
        );
        $this->assertNull($receivedTransaction);
    }

    function testGetTransactionClient401() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_UNAUTHORIZED');
        $expectedResponse->setMessage('Unauthorized');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_UNAUTHORIZED",
          "message": "Unauthorized"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(401, [], $JSONData));

        $service = new TransactionClient($this->clientMock);
        $receivedTransaction = $service->getTransactions(
            new DateTime('2020-12-31 14:20:59 ', new DateTimeZone(date_default_timezone_get())),
            new DateTime('2023-12-31 14:20:59 '),
            false,
            sortPropertyTransaction::URN,
            SortOrder::ASCENDING,
            10,
            1,
            $this->clientConfig, 'en'
        );
        $this->assertEquals($expectedResponse, $receivedTransaction);
    }

    function testGetTransactionClient404() {
        $this->clientMock->method('request')
            ->willReturn(new Response(404));

        $service = new TransactionClient($this->clientMock);
        $this->expectExceptionMessage('Error 404 Not Found, Invalid Relative URL');
        $service->getTransactions(
            new DateTime('2020-12-31 14:20:59 ', new DateTimeZone(date_default_timezone_get())),
            new DateTime('2023-12-31 14:20:59 '),
            false,
            sortPropertyTransaction::URN,
            SortOrder::ASCENDING,
            10,
            1,
            $this->clientConfig, 'en'
        );
    }

    function testGetTransactionClient500() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_INTERNAL');
        $expectedResponse->setMessage('An Exception Occurred');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_INTERNAL",
          "message": "An Exception Occurred"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(500, [], $JSONData));

        $service = new TransactionClient($this->clientMock);
        $receivedTransaction =
            $service->getTransactions(
                new DateTime('2020-12-31 14:20:59 ', new DateTimeZone(date_default_timezone_get())),
                new DateTime('2023-12-31 14:20:59 '),
                false,
                sortPropertyTransaction::URN,
                SortOrder::ASCENDING,
                10,
                1,
                $this->clientConfig, 'en'
            );

        $this->assertEquals($expectedResponse, $receivedTransaction);
    }

    function testGetTransactionClient100() {
        $this->clientMock->method('request')
            ->willReturn(new Response(100));

        $this->expectExceptionMessage('Response code not recognised, please check Request');
        $service = new TransactionClient($this->clientMock);
        $service->getTransactions(
            new DateTime('2020-12-31 14:20:59 ', new DateTimeZone(date_default_timezone_get())),
            new DateTime('2023-12-31 14:20:59 '),
            false,
            sortPropertyTransaction::URN,
            SortOrder::ASCENDING,
            10,
            1,
            $this->clientConfig, 'en'
        );
    }

    function testGetTransactionClientNullConfig() {
        $service = new TransactionClient($this->clientMock);
        $this->expectException(TypeError::class);
        $service->getTransactions(
            new DateTime('2020-12-31 14:20:59 ', new DateTimeZone(date_default_timezone_get())),
            new DateTime('2023-12-31 14:20:59 '),
            false,
            sortPropertyTransaction::URN,
            SortOrder::ASCENDING,
            10,
            1,
            null, 'en'
        );
    }

    function testGetTransactionInvalidMerchantToken() {
        $service = new TransactionClient($this->clientMock);
        $this->clientConfig->setMerchantToken('');

        $this->expectExceptionMessage('GetTransactions(): ClientConfig->MerchantToken is not set');
        $service->getTransactions(
            new DateTime('2020-12-31 14:20:59 ', new DateTimeZone(date_default_timezone_get())),
            new DateTime('2023-12-31 14:20:59 '),
            false,
            sortPropertyTransaction::URN,
            SortOrder::ASCENDING,
            10,
            1,
            $this->clientConfig, 'en'
        );
    }
    function testGetTransactionInvalidMerchant() {
        $service = new TransactionClient($this->clientMock);
        $this->clientConfig->setMerchant('');

        $this->expectExceptionMessage('GetTransactions(): ClientConfig->Merchant is not set');
        $service->getTransactions(
            new DateTime('2020-12-31 14:20:59 ', new DateTimeZone(date_default_timezone_get())),
            new DateTime('2023-12-31 14:20:59 '),
            false,
            sortPropertyTransaction::URN,
            SortOrder::ASCENDING,
            10,
            1,
            $this->clientConfig, 'en'
        );
    }


}