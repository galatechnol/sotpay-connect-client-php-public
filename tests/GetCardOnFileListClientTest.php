<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Src\main\client\clients\CardOnFileClient;
use Src\main\client\clients\TransactionClient;
use Src\main\client\config\ClientConfig;
use Src\main\client\models\CardsOnFileObjects\GlobalPaymentsRealexCardOnFile;
use Src\main\client\models\components\contact;
use Src\main\client\models\components\ListsWrapper;
use Src\main\client\models\components\merchantKey;
use Src\main\client\models\components\Page;
use Src\main\client\models\enums\Country;
use Src\main\client\models\enums\Domain;
use Src\main\client\models\enums\PaymentGateway;
use Src\main\client\models\enums\sortOrder;
use Src\main\client\models\enums\sortPropertyCardOnFile;
use Src\main\client\models\ResponseError;
use Src\main\client\models\Transaction;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionObjects\transactionAddress;
use Src\main\client\models\TransactionObjects\notificationMessage;
use Src\main\client\models\TransactionObjects\payMessage;
use Src\main\client\models\TransactionObjects\TransactionContact;

class GetCardOnFileListClientTest extends TestCase
{
    private Client $clientMock;
    private ClientConfig $clientConfig;
    private Transaction $getTransaction;
    private TransactionClient $getTransactionClient;


    protected function setUp(): void
    {
        //create and config guzzle mockClient
        $this->clientMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock(); //direct request

        $baseurl = 'https://connect.sotpay.co.uk';
        $merchant = 'democo1';
        $domain = Domain::SANDBOX;
        $merchantToken = 'tk_sand_ras35vZfuFCzjD9gXpZ7PkZsTVm4Y8PEPTwdDXD3xz';
        $signatureKeyInbound = '';
        $tokenAdminKey = '';
        $this->clientConfig = new ClientConfig($baseurl, $merchant, $domain, $merchantToken,
            $signatureKeyInbound, $tokenAdminKey);

    }

    function testGetCardOnFileListClient200()
    {

            //1st element
            $card1 = new GlobalPaymentsRealexCardOnFile();
            $card1->setObject('GlobalPaymentsRealexCardOnFile');
            $card1->setId('CDF-PSY-DEM1S-A0000-GPMCU');
            $card1->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
            $card1->setReference('car');
            $card1->setFirstUrn('STN-PSY-DEM1S-A0000-L2NQE');
            $card1->setFirstReference('150893525766023');
            $card1->setSequenceCount(1);
            $card1->setCardType(CardType::VISA);
            $card1->setMaskedCardNumber('40120010****1113');
            $card1->setExpiry('2023-12');
            $card1->setContact(new TransactionContact('mike@test.com', '07123456789'));
            $card1->setBillingAddress(new transactionAddress(
                'Mike',
                'Rasp',
                'Mike Rasp',
                '310 The Avenue',
                '310 The Avenue',
                'Doncaster',
                'DN80 5LM',
                'South Yorkshire',
                Country::GB
            ));
            $card1->setPayerReference('abc124');
            $card1->setCardReference('def567');
            $card1->setCreated(new DateTime('2023-08-25T10:35:34Z'));
            $card1->setUpdated(new DateTime('2023-08-25T10:35:34Z'));


            $card2 = new GlobalPaymentsRealexCardOnFile();
            $card2->setObject("GlobalPaymentsRealexCardOnFile");
            $card2->setId("CDF-PSY-DEM1S-A0000-GPZBJ");
            $card2->setMerchantKey(new merchantKey('democo1', Domain::SANDBOX));
            $card2->setReference("yacht");
            $card2->setFirstUrn("STN-PSY-DEM1S-A0000-ZYCTE");
            $card2->setFirstReference("150893525766024");
            $card2->setSequenceCount(2);
            $card2->setCardType(CardType::VISA);
            $card2->setMaskedCardNumber("40120010****1114");
            $card2->setExpiry("2024-12");
            $card2->setContact(new TransactionContact('jeremy@test.com', '07123456789'));
            $card2->setBillingAddress(new transactionAddress(
                'Jeremy',
                'Webb',
                'Jeremy Webb',
                '845 Queens Road',
                '845 Queens Road',
                'Dundee',
                'DD93 6HY',
                'South Yorkshire',
                Country::GB
            ));
            $card2->setPayerReference("abc125");
            $card2->setCardReference("def569");
            $card2->setCreated(new DateTime('2023-08-25T10:35:34Z'));
            $card2->setUpdated(new DateTime('2023-08-25T10:35:34Z'));
            $cardarr = array($card1, $card2);
            $page = new Page(array(
                '<https://connect.sotpay.co.uk/api/cardonfile/democo1/SANDBOX/acquired?pageSize=1&pageNumber=1>; title="_self"',
                '<https://connect.sotpay.co.uk/api/cardonfile/democo1/SANDBOX/acquired?pageSize=1&pageNumber=1>; title="_first"',
                '<https://connect.sotpay.co.uk/api/cardonfile/democo1/SANDBOX/acquired?pageSize=1&pageNumber=2>; title="_last"',
                '<https://connect.sotpay.co.uk/api/cardonfile/democo1/SANDBOX/acquired?pageSize=1&pageNumber=2>; title="_next"'
            ));

            $expectedArr = new ListsWrapper($cardarr, $page);

        $JSONData = '[
           {
              "object":"GlobalPaymentsRealexCardOnFile",
              "id":"CDF-PSY-DEM1S-A0000-GPMCU",
              "merchantKey":{
                 "merchant":"democo1",
                 "domain":"SANDBOX"
              },
              "reference":"car",
              "firstUrn":"STN-PSY-DEM1S-A0000-L2NQE",
              "firstReference":"150893525766023",
              "sequenceCount":1,
              "cardType":"VISA",
              "maskedCardNumber":"40120010****1113",
              "expiry":"2023-12",
              "contact":{
                 "email":"mike@test.com",
                 "phone":"07123456789"
              },
              "transactionAddress":{
                 "firstName":"Mike",
                 "surname":"Rasp",
                 "displayName":"Mike Rasp",
                 "line1":"310 The Avenue",
                 "line2":"310 The Avenue",
                 "city":"Doncaster",
                 "postcode":"DN80 5LM",
                 "state":"South Yorkshire",
                 "country":"GB"
              },
              "payerReference":"abc124",
              "cardReference":"def567",
              "created":"2023-08-25T10:35:34Z",
              "updated":"2023-08-25T10:35:34Z"
           },
           {
              "object":"GlobalPaymentsRealexCardOnFile",
              "id":"CDF-PSY-DEM1S-A0000-GPZBJ",
              "merchantKey":{
                 "merchant":"democo1",
                 "domain":"SANDBOX"
              },
              "reference":"yacht",
              "firstUrn":"STN-PSY-DEM1S-A0000-ZYCTE",
              "firstReference":"150893525766024",
              "sequenceCount":2,
              "cardType":"VISA",
              "maskedCardNumber":"40120010****1114",
              "expiry":"2024-12",
              "contact":{
                 "email":"jeremy@test.com",
                 "phone":"07123456789"
              },
              "transactionAddress":{
                 "firstName":"Jeremy",
                 "surname":"Webb",
                 "displayName":"Jeremy Webb",
                 "line1":"845 Queens Road",
                 "line2":"845 Queens Road",
                 "city":"Dundee",
                 "postcode":"DD93 6HY",
                 "state":"South Yorkshire",
                 "country":"GB"
              },
              "payerReference":"abc125",
              "cardReference":"def569",
              "created":"2023-08-25T10:35:34Z",
              "updated":"2023-08-25T10:35:34Z"
           }
        ]';

        //simulated data
        $expectedPage = [
            'Link' => [
                '<https://connect.sotpay.co.uk/api/cardonfile/democo1/SANDBOX/acquired?pageSize=1&pageNumber=1>; rel="_self"',
                '<https://connect.sotpay.co.uk/api/cardonfile/democo1/SANDBOX/acquired?pageSize=1&pageNumber=1>; rel="_first"',
                '<https://connect.sotpay.co.uk/api/cardonfile/democo1/SANDBOX/acquired?pageSize=1&pageNumber=2>; rel="_last"',
                '<https://connect.sotpay.co.uk/api/cardonfile/democo1/SANDBOX/acquired?pageSize=1&pageNumber=2>; rel="_next"'
            ]
        ];



        $this->clientMock->method('request')
            ->willReturn(new Response(200, $expectedPage,
                $JSONData));
        $service = new CardOnFileClient($this->clientMock);
        $receivedTransaction = $service->getCardsOnFile(
            PaymentGateway::globalpaymentsrealex,
            sortPropertyCardOnFile::ID,
            sortOrder::ASCENDING,
            20,
            1,
            $this->clientConfig,
            'en',
        );

        $this->assertEquals($expectedArr, $receivedTransaction);
    }

    function testGetCardOnFileListClient204() {
        $this->clientMock->method('request')
            ->willReturn(new Response(204));

        $service = new CardOnFileClient($this->clientMock);
        $receivedTransaction = $service->getCardsOnFile(
            PaymentGateway::globalpaymentsrealex,
            sortPropertyCardOnFile::ID,
            sortOrder::ASCENDING,
            20,
            1,
            $this->clientConfig,
            'en',
        );

        $this->assertNull($receivedTransaction);
    }

    function testGetCardOnFileListClient401() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_UNAUTHORIZED');
        $expectedResponse->setMessage('Unauthorized');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_UNAUTHORIZED",
          "message": "Unauthorized"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(401, [], $JSONData));

        $service = new CardOnFileClient($this->clientMock);
        $receivedTransaction = $service->getCardsOnFile(
            PaymentGateway::globalpaymentsrealex,
            sortPropertyCardOnFile::ID,
            sortOrder::ASCENDING,
            20,
            1,
            $this->clientConfig,
            'en',
        );
        $this->assertEquals($expectedResponse, $receivedTransaction);
    }

    function testGetCardOnFileListClient404() {
        $this->clientMock->method('request')
            ->willReturn(new Response(404));

        $service = new CardOnFileClient($this->clientMock);
        $this->expectExceptionMessage('Error 404 Not Found, Invalid Relative URL');
        $service->getCardsOnFile(
            PaymentGateway::globalpaymentsrealex,
            sortPropertyCardOnFile::ID,
            sortOrder::ASCENDING,
            20,
            1,
            $this->clientConfig,
            'en',
        );
    }

    function testGetCardOnFileListClient500() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_INTERNAL');
        $expectedResponse->setMessage('An Exception Occurred');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_INTERNAL",
          "message": "An Exception Occurred"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(500, [], $JSONData));

        $service = new CardOnFileClient($this->clientMock);
        $receivedTransaction =
            $service->getCardsOnFile(
                PaymentGateway::globalpaymentsrealex,
                sortPropertyCardOnFile::ID,
                sortOrder::ASCENDING,
                20,
                1,
                $this->clientConfig,
                'en',
            );

        $this->assertEquals($expectedResponse, $receivedTransaction);
    }

    function testGetCardOnFileListClient100() {
        $this->clientMock->method('request')
            ->willReturn(new Response(100));

        $this->expectExceptionMessage('Response code not recognised, please check Request');
        $service = new CardOnFileClient($this->clientMock);
        $service->getCardsOnFile(
            PaymentGateway::globalpaymentsrealex,
            sortPropertyCardOnFile::ID,
            sortOrder::ASCENDING,
            20,
            1,
            $this->clientConfig,
            'en',
        );
    }

    function testGetCardOnFileListNullConfig() {
        $service = new CardOnFileClient($this->clientMock);
        $this->expectException(TypeError::class);
        $service->getCardsOnFile(
            PaymentGateway::globalpaymentsrealex,
            sortPropertyCardOnFile::ID,
            sortOrder::ASCENDING,
            20,
            1,
           null,
            'en',
        );
    }

    function testGetCardOnFileListNullGateway() {
        $service = new CardOnFileClient($this->clientMock);
        $this->expectException(TypeError::class);
        $service->getCardsOnFile(
            null,
            sortPropertyCardOnFile::ID,
            sortOrder::ASCENDING,
            20,
            1,
            $this->clientConfig,
            'en',
        );
    }
    function testGetCardOnfileInvalidMerchant() {
        $service = new CardOnFileClient($this->clientMock);
        $this->clientConfig->setMerchant('');
        $this->expectExceptionMessage('GetCardsOnFile(): ClientConfig->Merchant is not set');
        $service->getCardsOnFile(
            PaymentGateway::sybc,
            sortPropertyCardOnFile::ID,
            sortOrder::ASCENDING,
            20,
            1,
            $this->clientConfig,
            'en',
        );
    }

    function testGetCardOnfileInvalidMerchantKey() {
        $service = new CardOnFileClient($this->clientMock);
        $this->clientConfig->setMerchantToken('');
        $this->expectExceptionMessage('GetCardsOnFile(): ClientConfig->MerchantToken is not set');
        $service->getCardsOnFile(
            PaymentGateway::sybc,
            sortPropertyCardOnFile::ID,
            sortOrder::ASCENDING,
            20,
            1,
            $this->clientConfig,
            'en',
        );
    }
    function testGetCardOnfileInvalidDomain() {
        $service = new CardOnFileClient($this->clientMock);
        $this->expectException(TypeError::class);
        $this->clientConfig->setDomain('');
    }
}