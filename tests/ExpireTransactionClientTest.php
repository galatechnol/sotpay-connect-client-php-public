<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Src\main\client\clients\TransactionClient;
use Src\main\client\config\ClientConfig;
use Src\main\client\models\enums\Domain;
use Src\main\client\models\ResponseError;
use Src\main\client\models\TransactionObjects\notificationMessage;
use Src\main\client\models\TransactionObjects\payMessage;

class ExpireTransactionClientTest extends TestCase
{
    private Client $clientMock;
    private ClientConfig $clientConfig;

    protected function setUp(): void
    {
        //create and config guzzle mockClient
        $this->clientMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock(); //direct request

        $baseurl = 'https://connect.sotpay.co.uk';
        $merchant = 'democo1';
        $domain = Domain::SANDBOX;
        $merchantToken = 'tk_sand_ras35vZfuFCzjD9gXpZ7PkZsTVm4Y8PEPTwdDXD3xz';
        $signatureKeyInbound = '';
        $tokenAdminKey = '';
        $this->clientConfig = new ClientConfig($baseurl, $merchant, $domain, $merchantToken,
            $signatureKeyInbound, $tokenAdminKey);

    }

    function testExpireTransactionClient200()
    {
        $this->clientMock->method('request')
            ->willReturn(new Response(200));

        $service = new TransactionClient($this->clientMock);
        $received = $service->expireTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345','en');

        $this->assertEquals('Transaction STN-PSY-DEM1S-E0119-12345 has been updated to EXPIRED', $received);
    }

    function testExpireTransactionClient304()
    {
        $this->clientMock->method('request')
            ->willReturn(new Response(304));

        $service = new TransactionClient($this->clientMock);
        $received = $service->expireTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345','en');

        $this->assertEquals('Transaction STN-PSY-DEM1S-E0119-12345 is already EXPIRED', $received);
    }

    //Same for invalid format, null/invalid transaction
    function testExpireTransactionClient400() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_NULL_URN');
        $expectedResponse->setMessage('The urn was null');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_NULL_URN",
          "message": "The urn was null"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(400, [], $JSONData));

        $service = new TransactionClient($this->clientMock);
        $receivedTransaction =
            $service->expireTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedResponse, $receivedTransaction);
    }

    function testExpireTransactionClient401() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_UNAUTHORIZED');
        $expectedResponse->setMessage('Unauthorized');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_UNAUTHORIZED",
          "message": "Unauthorized"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(401, [], $JSONData));

        $service = new TransactionClient($this->clientMock);
        $receivedTransaction =
            $service->expireTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedResponse, $receivedTransaction);
    }

    function testExpireTransactionClient500() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_INTERNAL');
        $expectedResponse->setMessage('An Exception Occurred');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_INTERNAL",
          "message": "An Exception Occurred"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(500, [], $JSONData));

        $service = new TransactionClient($this->clientMock);
        $receivedTransaction =
            $service->expireTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');

        $this->assertEquals($expectedResponse, $receivedTransaction);
    }


    function testExpireTransactionClientNullConfig() {
        $service = new TransactionClient($this->clientMock);
       $this->expectException(TypeError::class);
        $service->expireTransaction(null,'STN-PSY-DEM1S-E0119-12345', 'en');
    }

    function testExpireTransactionClientNullUrn() {
        $service = new TransactionClient($this->clientMock);
        $this->expectException(TypeError::class);
        $service->expireTransaction($this->clientConfig,null, 'en');
    }


    function testExpireTransactionClientInvalidUrnFormat() {
        $service = new TransactionClient($this->clientMock);
        $this->expectExceptionMessage('Invalid URN format');
        $service->expireTransaction($this->clientConfig, 'PTN-PSY-DEM1S-E0119-12345', 'en');

    }

    function testExpireTransactionInvalidMerchantToken() {
        $service = new TransactionClient($this->clientMock);
        $this->clientConfig->setMerchantToken('');

        $this->expectExceptionMessage('GetTransaction(): ClientConfig->MerchantToken is not set');
        $received =
            $service->getTransaction($this->clientConfig, 'STN-PSY-DEM1S-E0119-12345', 'en');
    }
}