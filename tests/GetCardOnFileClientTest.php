<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Src\main\client\clients\CardOnFileClient;
use Src\main\client\config\ClientConfig;
use Src\main\client\models\CardsOnFileObjects\AcquiredCardOnFile;
use Src\main\client\models\CardsOnFileObjects\CardstreamCardOnFile;
use Src\main\client\models\CardsOnFileObjects\EVOPaymentsCardOnFile;
use Src\main\client\models\CardsOnFileObjects\FreedomPayCardOnFile;
use Src\main\client\models\CardsOnFileObjects\GlobalPaymentsRealexCardOnFile;
use Src\main\client\models\CardsOnFileObjects\OpayoCardOnFile;
use Src\main\client\models\CardsOnFileObjects\OpayoServerCardOnFile;
use Src\main\client\models\CardsOnFileObjects\Pay360CardOnFile;
use Src\main\client\models\CardsOnFileObjects\PaysafeCardOnFile;
use Src\main\client\models\CardsOnFileObjects\SYBCCardOnFile;
use Src\main\client\models\components\merchantKey;
use Src\main\client\models\enums\Country;
use Src\main\client\models\enums\Domain;
use Src\main\client\models\ResponseError;
use Src\main\client\models\TransactionInfoObjects\infos\enums\CardType;
use Src\main\client\models\TransactionObjects\transactionAddress;
use Src\main\client\models\TransactionObjects\TransactionContact;

class GetCardOnFileClientTest extends TestCase
{
    private Client $clientMock;
    private ClientConfig $clientConfig;

    protected function setUp(): void
    {
        //create and config guzzle mockClient
        $this->clientMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock(); //direct request

        $baseurl = 'https://connect.sotpay.co.uk';
        $merchant = 'democo1';
        $domain = Domain::SANDBOX;
        $merchantToken = 'tk_sand_ras35vZfuFCzjD9gXpZ7PkZsTVm4Y8PEPTwdDXD3xz';
        $signatureKeyInbound = '';
        $tokenAdminKey = '';
        $this->clientConfig = new ClientConfig($baseurl, $merchant, $domain, $merchantToken,
            $signatureKeyInbound, $tokenAdminKey);

    }

    function testGetCardOnFile200Acquired()
    {
        $expectedTransaction = new AcquiredCardOnFile();
        $expectedTransaction->setObject('AcquiredCardOnFile');
        $expectedTransaction->setId('CDF-PS0-FIXIL-K0719-BYEXI');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setReference('DemoClass Company 1 Main Card');
        $expectedTransaction->setFirstUrn('STN-PSY-DEM1S-E0119-12345');
        $expectedTransaction->setFirstReference('01Z6J0MFGD01U6992AT4GDTL43F5RPKY');
        $expectedTransaction->setSequenceCount(0);
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setMaskedCardNumber('411111xxxxxxxxxx');
        $expectedTransaction->setContact(new TransactionContact('carl@democo1.com', '07123456789'));
        $expectedTransaction->setBillingAddress(new transactionAddress(
            'Chris',
            'Smith',
            'Chris Smith',
            '123a High Street',
            'Wath-upon-Dearne',
            'Rotherham',
            'S63 1AA',
            'South Yorkshire',
            Country::GB
        ));
        $expectedTransaction->setCreated(new DateTime('2023-08-25T10:35:34Z'));
        $expectedTransaction->setUpdated(new DateTime('2023-08-25T10:35:34Z'));

        $JSONData = '{
              "object": "AcquiredCardOnFile",
              "id": "CDF-PS0-FIXIL-K0719-BYEXI",
              "merchantKey": {
                "merchant": "democo1",
                "domain": "SANDBOX"
              },
              "reference": "DemoClass Company 1 Main Card",
              "firstUrn": "STN-PSY-DEM1S-E0119-12345",
              "firstReference": "01Z6J0MFGD01U6992AT4GDTL43F5RPKY",
              "sequenceCount": 0,
              "cardType": "VISA",
              "maskedCardNumber": "411111xxxxxxxxxx",
              "contact": {
                "email": "carl@democo1.com",
                "phone": "07123456789"
              },
              "billingAddress": {
                "firstName": "Chris",
                "surname": "Smith",
                "displayName": "Chris Smith",
                "line1": "123a High Street",
                "line2": "Wath-upon-Dearne",
                "city": "Rotherham",
                "postcode": "S63 1AA",
                "state": "South Yorkshire",
                "country": "GB"
              },
              "created": "2023-08-25T10:35:34Z",
              "updated": "2023-08-25T10:35:34Z"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new CardOnFileClient($this->clientMock);
        $receivedTransaction = $service->getCardOnFile($this->clientConfig, 'CDF-PS0-FIXIL-K0719-BYEXI', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetCardOnFile200CardStream()
    {
        $expectedTransaction = new CardstreamCardOnFile();
        $expectedTransaction->setObject('CardstreamCardOnFile');
        $expectedTransaction->setId('CDF-PSY-DEM1S-A0000-CSSUB');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setReference('DemoClass Company 1 Main Card');
        $expectedTransaction->setFirstUrn('STN-PSY-DEM1S-A0000-65HC0');
        $expectedTransaction->setFirstReference('19012110LF22BY18LB60TKT');
        $expectedTransaction->setSequenceCount(0);
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setMaskedCardNumber('40120010****1112');
        $expectedTransaction->setContact(new TransactionContact('carl@democo1.com', '07123456789'));
        $expectedTransaction->setBillingAddress(new transactionAddress(
            'Chris',
            'Smith',
            'Chris Smith',
            '123a High Street',
            'Wath-upon-Dearne',
            'Rotherham',
            'S63 1AA',
            'South Yorkshire',
            Country::GB
        ));
        $expectedTransaction->setCreated(new DateTime('2023-08-25T10:35:34Z'));
        $expectedTransaction->setUpdated(new DateTime('2023-08-25T10:35:34Z'));

        $JSONData = '{
          "object": "CardstreamCardOnFile",
          "id": "CDF-PSY-DEM1S-A0000-CSSUB",
          "merchantKey": {
            "merchant": "democo1",
            "domain": "SANDBOX"
          },
          "reference": "DemoClass Company 1 Main Card",
          "firstUrn": "STN-PSY-DEM1S-A0000-65HC0",
          "firstReference": "19012110LF22BY18LB60TKT",
          "sequenceCount": 0,
          "cardType": "VISA",
          "maskedCardNumber": "40120010****1112",
          "contact": {
            "email": "carl@democo1.com",
            "phone": "07123456789"
          },
          "billingAddress": {
            "firstName": "Chris",
            "surname": "Smith",
            "displayName": "Chris Smith",
            "line1": "123a High Street",
            "line2": "Wath-upon-Dearne",
            "city": "Rotherham",
            "postcode": "S63 1AA",
            "state": "South Yorkshire",
            "country": "GB"
          },
          "created": "2023-08-25T10:35:34Z",
          "updated": "2023-08-25T10:35:34Z"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new CardOnFileClient($this->clientMock);
        $receivedTransaction = $service->getCardOnFile($this->clientConfig, 'CDF-PSY-DEM1S-A0000-CSSUB', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetCardOnFile200EVOPayments()
    {
        $expectedTransaction = new EVOPaymentsCardOnFile();
        $expectedTransaction->setObject('EVOPaymentsCardOnFile');
        $expectedTransaction->setId('CDF-PSY-DEM1S-A0000-EVG44');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setReference('apple');
        $expectedTransaction->setFirstUrn('STN-PSY-DEM1S-A0000-WX84X');
        $expectedTransaction->setFirstReference('23456');
        $expectedTransaction->setSequenceCount(0);
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setMaskedCardNumber('42424242****4242');
        $expectedTransaction->setExpiry('2022-12');
        $expectedTransaction->setContact(new TransactionContact('jeremy@test.com', '07123456789'));
        $expectedTransaction->setBillingAddress(new transactionAddress(
            'Chris',
            'Smith',
            'Chris Smith',
            '123a High Street',
            'Wath-upon-Dearne',
            'Rotherham',
            'S63 1AA',
            'South Yorkshire',
            Country::GB
        ));
        $expectedTransaction->setPayerReference('abc123');
        $expectedTransaction->setCardReference('def567');
        $expectedTransaction->setCreated(new DateTime('2023-08-25T10:35:34Z'));
        $expectedTransaction->setUpdated(new DateTime('2023-08-25T10:35:34Z'));

        $JSONData = '{
          "object": "EVOPaymentsCardOnFile",
          "id": "CDF-PSY-DEM1S-A0000-EVG44",
          "merchantKey": {
            "merchant": "democo1",
            "domain": "SANDBOX"
          },
          "reference": "apple",
          "firstUrn": "STN-PSY-DEM1S-A0000-WX84X",
          "firstReference": "23456",
          "sequenceCount": 0,
          "cardType": "VISA",
          "maskedCardNumber": "42424242****4242",
          "expiry": "2022-12",
          "contact": {
            "email": "jeremy@test.com",
            "phone": "07123456789"
          },
          "billingAddress": {
            "firstName": "Chris",
            "surname": "Smith",
            "displayName": "Chris Smith",
            "line1": "123a High Street",
            "line2": "Wath-upon-Dearne",
            "city": "Rotherham",
            "postcode": "S63 1AA",
            "state": "South Yorkshire",
            "country": "GB"
          },
          "payerReference": "abc123",
          "cardReference": "def567",
          "created": "2023-08-25T10:35:34Z",
          "updated": "2023-08-25T10:35:34Z"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new CardOnFileClient($this->clientMock);
        $receivedTransaction = $service->getCardOnFile($this->clientConfig, 'CDF-PSY-DEM1S-A0000-EVG44', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetCardOnFile200FreedomPay()
    {
        $expectedTransaction = new FreedomPayCardOnFile();
        $expectedTransaction->setObject('FreedomPayCardOnFile');
        $expectedTransaction->setId('CDF-PSY-DEM1S-A0000-FMAVV');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setReference('DemoClass Company 1 Main Card');
        $expectedTransaction->setFirstUrn('STN-PSY-DEM1S-A0000-7D8AF');
        $expectedTransaction->setFirstReference('01Z6J0MFGD01U6992AT4GDTL43F5RPKY');
        $expectedTransaction->setSequenceCount(0);
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setMaskedCardNumber('411111xxxxxxxxxx');
        $expectedTransaction->setContact(new TransactionContact('carl@democo1.com', '07123456789'));
        $expectedTransaction->setBillingAddress(new transactionAddress(
            'Chris',
            'Smith',
            'Chris Smith',
            '123a High Street',
            'Wath-upon-Dearne',
            'Rotherham',
            'S63 1AA',
            'South Yorkshire',
            Country::GB
        ));
        $expectedTransaction->setCardReference('def567');
        $expectedTransaction->setCreated(new DateTime('2023-08-25T10:35:34Z'));
        $expectedTransaction->setUpdated(new DateTime('2023-08-25T10:35:34Z'));

        $JSONData = '{
          "object": "FreedomPayCardOnFile",
          "id": "CDF-PSY-DEM1S-A0000-FMAVV",
          "merchantKey": {
            "merchant": "democo1",
            "domain": "SANDBOX"
          },
          "reference": "DemoClass Company 1 Main Card",
          "firstUrn": "STN-PSY-DEM1S-A0000-7D8AF",
          "firstReference": "01Z6J0MFGD01U6992AT4GDTL43F5RPKY",
          "sequenceCount": 0,
          "cardType": "VISA",
          "maskedCardNumber": "411111xxxxxxxxxx",
          "contact": {
            "email": "carl@democo1.com",
            "phone": "07123456789"
          },
          "billingAddress": {
            "firstName": "Chris",
            "surname": "Smith",
            "displayName": "Chris Smith",
            "line1": "123a High Street",
            "line2": "Wath-upon-Dearne",
            "city": "Rotherham",
            "postcode": "S63 1AA",
            "state": "South Yorkshire",
            "country": "GB"
          },
          "cardReference": "def567",
          "created": "2023-08-25T10:35:34Z",
          "updated": "2023-08-25T10:35:34Z"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new CardOnFileClient($this->clientMock);
        $receivedTransaction = $service->getCardOnFile($this->clientConfig, 'CDF-PSY-DEM1S-A0000-EVG44', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetCardOnFile200GlobalPaymentsRealex()
    {
        $expectedTransaction = new GlobalPaymentsRealexCardOnFile();
        $expectedTransaction->setObject('GlobalPaymentsRealexCardOnFile');
        $expectedTransaction->setId('CDF-PSY-DEM1S-A0000-GPMCU');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setReference('car');
        $expectedTransaction->setFirstUrn('STN-PSY-DEM1S-A0000-L2NQE');
        $expectedTransaction->setFirstReference('150893525766023');
        $expectedTransaction->setSequenceCount(1);
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setMaskedCardNumber('40120010****1113');
        $expectedTransaction->setExpiry('2023-12');
        $expectedTransaction->setContact(new TransactionContact('carl@democo1.com', '07123456789'));
        $expectedTransaction->setBillingAddress(new transactionAddress(
            'Chris',
            'Smith',
            'Chris Smith',
            '123a High Street',
            'Wath-upon-Dearne',
            'Rotherham',
            'S63 1AA',
            'South Yorkshire',
            Country::GB
        ));
        $expectedTransaction->setPayerReference('abc124');
        $expectedTransaction->setCardReference('def567');
        $expectedTransaction->setCreated(new DateTime('2023-08-25T10:35:34Z'));
        $expectedTransaction->setUpdated(new DateTime('2023-08-25T10:35:34Z'));

        $JSONData = '{
          "object": "GlobalPaymentsRealexCardOnFile",
          "id": "CDF-PSY-DEM1S-A0000-GPMCU",
          "merchantKey": {
            "merchant": "democo1",
            "domain": "SANDBOX"
          },
          "reference": "car",
          "firstUrn": "STN-PSY-DEM1S-A0000-L2NQE",
          "firstReference": "150893525766023",
          "sequenceCount": 1,
          "cardType": "VISA",
          "maskedCardNumber": "40120010****1113",
          "expiry": "2023-12",
          "contact": {
            "email": "carl@democo1.com",
            "phone": "07123456789"
          },
          "billingAddress": {
            "firstName": "Chris",
            "surname": "Smith",
            "displayName": "Chris Smith",
            "line1": "123a High Street",
            "line2": "Wath-upon-Dearne",
            "city": "Rotherham",
            "postcode": "S63 1AA",
            "state": "South Yorkshire",
            "country": "GB"
          },
          "payerReference": "abc124",
          "cardReference": "def567",
          "created": "2023-08-25T10:35:34Z",
          "updated": "2023-08-25T10:35:34Z"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new CardOnFileClient($this->clientMock);
        $receivedTransaction = $service->getCardOnFile($this->clientConfig, 'CDF-PSY-DEM1S-A0000-GPMCU', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetCardOnFile200Opayo()
{
    $expectedTransaction = new OpayoCardOnFile();
    $expectedTransaction->setObject('OpayoCardOnFile');
    $expectedTransaction->setId('CDF-PSY-DEM1S-A0000-Y09F7');
    $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
    $expectedTransaction->setReference('DemoClass Company 1 Main Card');
    $expectedTransaction->setFirstUrn('STN-PSY-DEM1S-A0000-FAASX');
    $expectedTransaction->setFirstReference('{8CD77B9F-A51E-359A-4809-D7D74CD90101}');
    $expectedTransaction->setSequenceCount(0);
    $expectedTransaction->setCardType(CardType::VISA);
    $expectedTransaction->setMaskedCardNumber('40120010****1112');
    $expectedTransaction->setExpiry('2023-11');
    $expectedTransaction->setContact(new TransactionContact('carl@democo1.com', '07123456789'));
    $expectedTransaction->setBillingAddress(new transactionAddress(
        'Chris',
        'Smith',
        'Chris Smith',
        '123a High Street',
        'Wath-upon-Dearne',
        'Rotherham',
        'S63 1AA',
        'South Yorkshire',
        Country::GB
    ));
    $expectedTransaction->setCardReference('73B7FA7B-D51E-49F6-9638-C58D5A1C146B');
    $expectedTransaction->setCreated(new DateTime('2023-08-25T10:35:34Z'));
    $expectedTransaction->setUpdated(new DateTime('2023-08-25T10:35:34Z'));

    $JSONData = '{
          "object": "OpayoCardOnFile",
          "id": "CDF-PSY-DEM1S-A0000-Y09F7",
          "merchantKey": {
            "merchant": "democo1",
            "domain": "SANDBOX"
          },
          "reference": "DemoClass Company 1 Main Card",
          "firstUrn": "STN-PSY-DEM1S-A0000-FAASX",
          "firstReference": "{8CD77B9F-A51E-359A-4809-D7D74CD90101}",
          "sequenceCount": 0,
          "cardType": "VISA",
          "maskedCardNumber": "40120010****1112",
          "expiry": "2023-11",
          "contact": {
            "email": "carl@democo1.com",
            "phone": "07123456789"
          },
          "billingAddress": {
            "firstName": "Chris",
            "surname": "Smith",
            "displayName": "Chris Smith",
            "line1": "123a High Street",
            "line2": "Wath-upon-Dearne",
            "city": "Rotherham",
            "postcode": "S63 1AA",
            "state": "South Yorkshire",
            "country": "GB"
          },
          "cardReference": "73B7FA7B-D51E-49F6-9638-C58D5A1C146B",
          "created": "2023-08-25T10:35:34Z",
          "updated": "2023-08-25T10:35:34Z"
        }';

    $this->clientMock->method('request')
        ->willReturn(new Response(200, [], $JSONData));
    $service = new CardOnFileClient($this->clientMock);
    $receivedTransaction = $service->getCardOnFile($this->clientConfig, 'CDF-PSY-DEM1S-A0000-GPMCU', 'en');

    $this->assertEquals($expectedTransaction, $receivedTransaction);
}

    function testGetCardOnFile200OpayoServer()
    {
        $expectedTransaction = new OpayoServerCardOnFile();
        $expectedTransaction->setObject('OpayoServerCardOnFile');
        $expectedTransaction->setId('CDF-PSY-DEM1S-A0000-S07D3');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setReference('DemoClass Company 1 Main Card');
        $expectedTransaction->setFirstUrn('STN-PSY-DEM1S-A0000-ERZ8B');
        $expectedTransaction->setFirstReference('{8CD77B9F-A51E-359A-4809-D7D74CD90101}');
        $expectedTransaction->setSequenceCount(0);
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setMaskedCardNumber('40120010****1112');
        $expectedTransaction->setExpiry('2023-11');
        $expectedTransaction->setContact(new TransactionContact('carl@democo1.com', '07123456789'));
        $expectedTransaction->setBillingAddress(new transactionAddress(
            'Chris',
            'Smith',
            'Chris Smith',
            '123a High Street',
            'Wath-upon-Dearne',
            'Rotherham',
            'S63 1AA',
            'South Yorkshire',
            Country::GB
        ));
        $expectedTransaction->setCreated(new DateTime('2023-08-25T10:35:34Z'));
        $expectedTransaction->setUpdated(new DateTime('2023-08-25T10:35:34Z'));

        $JSONData = '{
          "object": "OpayoServerCardOnFile",
          "id": "CDF-PSY-DEM1S-A0000-S07D3",
          "merchantKey": {
            "merchant": "democo1",
            "domain": "SANDBOX"
          },
          "reference": "DemoClass Company 1 Main Card",
          "firstUrn": "STN-PSY-DEM1S-A0000-ERZ8B",
          "firstReference": "{8CD77B9F-A51E-359A-4809-D7D74CD90101}",
          "sequenceCount": 0,
          "cardType": "VISA",
          "maskedCardNumber": "40120010****1112",
          "expiry": "2023-11",
          "contact": {
            "email": "carl@democo1.com",
            "phone": "07123456789"
          },
          "billingAddress": {
            "firstName": "Chris",
            "surname": "Smith",
            "displayName": "Chris Smith",
            "line1": "123a High Street",
            "line2": "Wath-upon-Dearne",
            "city": "Rotherham",
            "postcode": "S63 1AA",
            "state": "South Yorkshire",
            "country": "GB"
          },
          "created": "2023-08-25T10:35:34Z",
          "updated": "2023-08-25T10:35:34Z"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new CardOnFileClient($this->clientMock);
        $receivedTransaction = $service->getCardOnFile($this->clientConfig, 'CDF-PSY-DEM1S-A0000-S07D3', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetCardOnFile200OPaySafe()
    {
        $expectedTransaction = new PaysafeCardOnFile();
        $expectedTransaction->setObject('PaysafeCardOnFile');
        $expectedTransaction->setId('CDF-PSY-DEM1S-A0000-PS806');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setReference('apple');
        $expectedTransaction->setFirstUrn('STN-PSY-DEM1S-A0000-V0GZY');
        $expectedTransaction->setFirstReference('65919b4b-17a9-4d87-95c4-798854c116a5');
        $expectedTransaction->setSequenceCount(0);
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setMaskedCardNumber('*******0002');
        $expectedTransaction->setExpiry('2022-12');
        $expectedTransaction->setContact(new TransactionContact('jeremy@test.com', '07123456789'));
        $expectedTransaction->setBillingAddress(new transactionAddress(
            'Chris',
            'Smith',
            'Chris Smith',
            '123a High Street',
            'Wath-upon-Dearne',
            'Rotherham',
            'S63 1AA',
            'South Yorkshire',
            Country::GB
        ));
        $expectedTransaction->setCardReference('def567');
        $expectedTransaction->setPayerReference('abc123');
        $expectedTransaction->setCreated(new DateTime('2023-08-25T10:35:34Z'));
        $expectedTransaction->setUpdated(new DateTime('2023-08-25T10:35:34Z'));

        $JSONData = '{
          "object": "PaysafeCardOnFile",
          "id": "CDF-PSY-DEM1S-A0000-PS806",
          "merchantKey": {
            "merchant": "democo1",
            "domain": "SANDBOX"
          },
          "reference": "apple",
          "firstUrn": "STN-PSY-DEM1S-A0000-V0GZY",
          "firstReference": "65919b4b-17a9-4d87-95c4-798854c116a5",
          "sequenceCount": 0,
          "cardType": "VISA",
          "maskedCardNumber": "*******0002",
          "expiry": "2022-12",
          "contact": {
            "email": "jeremy@test.com",
            "phone": "07123456789"
          },
          "billingAddress": {
            "firstName": "Chris",
            "surname": "Smith",
            "displayName": "Chris Smith",
            "line1": "123a High Street",
            "line2": "Wath-upon-Dearne",
            "city": "Rotherham",
            "postcode": "S63 1AA",
            "state": "South Yorkshire",
            "country": "GB"
          },
          "payerReference": "abc123",
          "cardReference": "def567",
          "created": "2023-08-25T10:35:34Z",
          "updated": "2023-08-25T10:35:34Z"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new CardOnFileClient($this->clientMock);
        $receivedTransaction = $service->getCardOnFile($this->clientConfig, 'CDF-PSY-DEM1S-A0000-PS806', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetCardOnFile200OPay360()
    {
        $expectedTransaction = new Pay360CardOnFile();
        $expectedTransaction->setObject('Pay360CardOnFile');
        $expectedTransaction->setId('CDF-PSY-DEM1S-A0000-P31XJ');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setReference('DemoClass Company 1 Main Card');
        $expectedTransaction->setFirstUrn('STN-PSY-DEM1S-A0000-HSM1K');
        $expectedTransaction->setFirstReference('12345678901');
        $expectedTransaction->setSequenceCount(0);
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setMaskedCardNumber('40120010****1112');
        $expectedTransaction->setExpiry('2023-11');
        $expectedTransaction->setContact(new TransactionContact('carl@democo1.com', '07123456789'));
        $expectedTransaction->setBillingAddress(new transactionAddress(
            'Chris',
            'Smith',
            'Chris Smith',
            '123a High Street',
            'Wath-upon-Dearne',
            'Rotherham',
            'S63 1AA',
            'South Yorkshire',
            Country::GB
        ));
        $expectedTransaction->setCreated(new DateTime('2023-08-25T10:35:34Z'));
        $expectedTransaction->setUpdated(new DateTime('2023-08-25T10:35:34Z'));

        $JSONData = '{
          "object": "Pay360CardOnFile",
          "id": "CDF-PSY-DEM1S-A0000-P31XJ",
          "merchantKey": {
            "merchant": "democo1",
            "domain": "SANDBOX"
          },
          "reference": "DemoClass Company 1 Main Card",
          "firstUrn": "STN-PSY-DEM1S-A0000-HSM1K",
          "firstReference": "12345678901",
          "sequenceCount": 0,
          "cardType": "VISA",
          "maskedCardNumber": "40120010****1112",
          "expiry": "2023-11",
          "contact": {
            "email": "carl@democo1.com",
            "phone": "07123456789"
          },
          "billingAddress": {
            "firstName": "Chris",
            "surname": "Smith",
            "displayName": "Chris Smith",
            "line1": "123a High Street",
            "line2": "Wath-upon-Dearne",
            "city": "Rotherham",
            "postcode": "S63 1AA",
            "state": "South Yorkshire",
            "country": "GB"
          },
          "created": "2023-08-25T10:35:34Z",
          "updated": "2023-08-25T10:35:34Z"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new CardOnFileClient($this->clientMock);
        $receivedTransaction = $service->getCardOnFile($this->clientConfig, 'CDF-PSY-DEM1S-A0000-P31XJ', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetCardOnFile200SYBC()
    {
        $expectedTransaction = new SYBCCardOnFile();
        $expectedTransaction->setObject('SYBCCardOnFile');
        $expectedTransaction->setId('CDF-PSY-DEM1S-A0000-SYDJE');
        $expectedTransaction->setMerchantKey(new merchantKey('democo1',Domain::SANDBOX));
        $expectedTransaction->setReference('DemoClass Company 1 Main Card');
        $expectedTransaction->setFirstUrn('STN-PSY-DEM1S-A0000-7PCKU');
        $expectedTransaction->setFirstReference('3852601347');
        $expectedTransaction->setSequenceCount(0);
        $expectedTransaction->setCardType(CardType::VISA);
        $expectedTransaction->setMaskedCardNumber('40120010****1112');
        $expectedTransaction->setExpiry('2023-11');
        $expectedTransaction->setContact(new TransactionContact('carl@democo1.com', '07123456789'));
        $expectedTransaction->setBillingAddress(new transactionAddress(
            'Chris',
            'Smith',
            'Chris Smith',
            '123a High Street',
            'Wath-upon-Dearne',
            'Rotherham',
            'S63 1AA',
            'South Yorkshire',
            Country::GB
        ));
        $expectedTransaction->setCreated(new DateTime('2023-08-25T10:35:34Z'));
        $expectedTransaction->setUpdated(new DateTime('2023-08-25T10:35:34Z'));

        $JSONData = '{
          "object": "SYBCCardOnFile",
          "id": "CDF-PSY-DEM1S-A0000-SYDJE",
          "merchantKey": {
            "merchant": "democo1",
            "domain": "SANDBOX"
          },
          "reference": "DemoClass Company 1 Main Card",
          "firstUrn": "STN-PSY-DEM1S-A0000-7PCKU",
          "firstReference": "3852601347",
          "sequenceCount": 0,
          "cardType": "VISA",
          "maskedCardNumber": "40120010****1112",
          "expiry": "2023-11",
          "contact": {
            "email": "carl@democo1.com",
            "phone": "07123456789"
          },
          "billingAddress": {
            "firstName": "Chris",
            "surname": "Smith",
            "displayName": "Chris Smith",
            "line1": "123a High Street",
            "line2": "Wath-upon-Dearne",
            "city": "Rotherham",
            "postcode": "S63 1AA",
            "state": "South Yorkshire",
            "country": "GB"
          },
          "created": "2023-08-25T10:35:34Z",
          "updated": "2023-08-25T10:35:34Z"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(200, [], $JSONData));
        $service = new CardOnFileClient($this->clientMock);
        $receivedTransaction = $service->getCardOnFile($this->clientConfig, 'CDF-PSY-DEM1S-A0000-SYDJE', 'en');

        $this->assertEquals($expectedTransaction, $receivedTransaction);
    }

    function testGetTransactionClient204() {
        $this->clientMock->method('request')
            ->willReturn(new Response(204));

        $service = new CardOnFileClient($this->clientMock);
        $receivedTransaction = $service->getCardOnFile($this->clientConfig, 'CDF-PSY-DEM1S-A0000-SYDJE', 'en');

        $this->assertEquals('Card On File not found', $receivedTransaction);
    }

    function testGetTransactionClient401() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_UNAUTHORIZED');
        $expectedResponse->setMessage('Unauthorized');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_UNAUTHORIZED",
          "message": "Unauthorized"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(401, [], $JSONData));

        $service = new CardOnFileClient($this->clientMock);
        $receivedTransaction = $service->getCardOnFile($this->clientConfig, 'CDF-PSY-DEM1S-A0000-SYDJE', 'en');
        $this->assertEquals($expectedResponse, $receivedTransaction);
    }

    function testGetTransactionClient404() {
        $this->clientMock->method('request')
            ->willReturn(new Response(404));

        $service = new CardOnFileClient($this->clientMock);
        $this->expectExceptionMessage('Error 404 Not Found, Invalid Relative URL');
        $service->getCardOnFile($this->clientConfig, 'CDF-PSY-DEM1S-A0000-SYDJE', 'en');
    }

    function testGetTransactionClient500() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setObject('Error');
        $expectedResponse->setId('ERROR_INTERNAL');
        $expectedResponse->setMessage('An Exception Occurred');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_INTERNAL",
          "message": "An Exception Occurred"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(500, [], $JSONData));

        $service = new CardOnFileClient($this->clientMock);
        $receivedTransaction =
            $service->GetCardOnFile($this->clientConfig, 'CDF-PSY-DEM1S-A0000-SYDJE', 'en');

        $this->assertEquals($expectedResponse, $receivedTransaction);
    }

    function testGetTransactionClientNullConfig() {
        $service = new CardOnFileClient($this->clientMock);
        $this->expectException(TypeError::class);
        $service->getCardOnFile(null,'CDF-PSY-DEM1S-A0000-SYDJE', 'en');
    }

    function testGetTransactionClientNullUrn() {
        $service = new CardOnFileClient($this->clientMock);
        $this->expectException(TypeError::class);
        $service->getCardOnFile($this->clientConfig,null, 'en');
    }

    function testGetTransactionInvalidJSON() {
        $this->expectExceptionMessage('Wrong data type sent, expected JSON body');
        $service = SYBCCardOnFile::withBody('invalid JSON as string');
    }

    function testGetTransactionInvalidJSONProperty() {
        $JSONBody = '{
          "object": "string",
          "id": "CDF-PSY-DEM1S-A0000-P31XJ",
          "merchantKey": {
          "merchant": "democo1",
          "domain": "SANDBOX"
          },
          "breakcode": 100.99
        }';
        $this->expectExceptionMessage('JSON property "breakcode" does not exist in object of type Src\main\client\models\CardsOnFileObjects\SYBCCardOnFile');
        $service = SYBCCardOnFile::withBody($JSONBody);
    }

    function testGetTransactionClientInvalidUrnFormat() {
        $service = new CardOnFileClient($this->clientMock);
        $this->expectExceptionMessage('Invalid ID format');
        $service->getCardOnFile($this->clientConfig, 'INVALID ID', 'en');

    }
}

