<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Src\main\client\clients\MerchantTokenClient;
use Src\main\client\config\ClientConfig;
use Src\main\client\models\components\merchantKey;
use Src\main\client\models\enums\Domain;
use Src\main\client\models\enums\Type;
use Src\main\client\models\MerchantToken;
use Src\main\client\models\MerchantTokenWrite;
use Src\main\client\models\ResponseError;

class MerchantTokenClientTest extends TestCase
{
    private Client $clientMock;
    private ClientConfig $clientConfig;
    private MerchantToken $merchantToken;
    private MerchantTokenWrite $createToken;
    private string $JSONData;

    protected function setUp(): void
    {
        //create and config guzzle mock client
        $this->clientMock = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock(); //direct request

        //ClientConfig
        $baseurl = 'https://connect.sotpay.co.uk';
        $merchant = 'democo1';
        $domain = Domain::SANDBOX;
        $merchantToken = 'tk_sand_ras35vZfuFCzjD9gXpZ7PkZsTVm4Y8PEPTwdDXD3xz';
        $signatureKeyInbound = 'sk_in_Kc5UTppaZxTyclR0dpcU4qF63pcBrifk7SWWDabmCC9z';
        $tokenAdminKey = 'SezgFr0CWGFyXRqGa438Mgv9jPFFm8s9cpZS4hDm8s';
        $this->clientConfig = new ClientConfig($baseurl, $merchant, $domain, $merchantToken,
            $signatureKeyInbound, $tokenAdminKey);


        //Model Token Expected
        $expectedToken = new MerchantToken();
        $expectedToken->setObject('MerchantToken');
        $expectedToken->setId('60');
        $expectedToken->setValue('tk_sand_ras35vZfuFCzjD9gXpZ7PkZsTVm4Y8PEPTwdDXD3xy');
        $expectedToken->setMerchantKey(new merchantKey(
            'democo1', Domain::SANDBOX));
        $expectedToken->setType(Type::READ);
        $expectedToken->setDescription('for integration 2');
        $expectedToken->setExpiry(new DateTime('2025-02-18T16:08:00Z') );
        $expectedToken->setCreated(new DateTime('2024-02-19T16:08:00Z'));
        $expectedToken->setUpdated(new DateTime('2024-02-19T16:08:00Z'));
        $this->merchantToken = $expectedToken;

        //Model JSON response expected
        $JsonBody = '{
          "object": "MerchantToken",
          "id": 60,
          "value": "tk_sand_ras35vZfuFCzjD9gXpZ7PkZsTVm4Y8PEPTwdDXD3xy",
          "merchantKey": {
            "merchant": "democo1",
            "domain": "SANDBOX"
          },
          "type": "READ",
          "description": "for integration 2",
          "expiry": "2025-02-18T16:08:00Z",
          "created": "2024-02-19T16:08:00Z",
          "updated": "2024-02-19T16:08:00Z"
        }';
        $this->JSONData = $JsonBody;


        //The token we use to create
        $this->createToken = new MerchantTokenWrite(
            new merchantKey('democo1', Domain::SANDBOX), Type::READ);
        $this->createToken->setDescription('for integration 2');
        $this->createToken->setExpiresAfter(31536000);


    }

    function testCreateToken201() {

        $this->clientMock->method('request')
            ->willReturn(new Response(201, [], $this->JSONData));

        $service = new MerchantTokenClient($this->clientMock);
        $receivedTransaction =
            $service->createToken($this->clientConfig, $this->createToken, 'en');

        $this->assertEquals($this->merchantToken, $receivedTransaction);
    }

    //When bearer token is invalid or not existing
    function testCreateToken401() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setId('ERROR_UNAUTHORIZED');
        $expectedResponse->setObject('Error');
        $expectedResponse->setMessage('Unauthorized');
        $JSONData = '{
          "object": "Error",
          "id": "ERROR_UNAUTHORIZED",
          "message": "Unauthorized"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(401, [], $JSONData));
        $service = new MerchantTokenClient($this->clientMock);

        $receiveTransaction =
            $service->createToken($this->clientConfig, $this->createToken, 'en');
        $this->assertEquals($expectedResponse, $receiveTransaction);
    }

    //when authentication fails (incorrect x-jws-signature etc.)
    function testCreateToken403() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setId('ERROR_FORBIDDEN');
        $expectedResponse->setObject('Error');
        $expectedResponse->setMessage('Forbidden');
        $JSONData = '{
          "object": "Error",
          "id": "ERROR_FORBIDDEN",
          "message": "Forbidden"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(403, [], $JSONData));
        $service = new MerchantTokenClient($this->clientMock);

        $receiveTransaction =
            $service->createToken($this->clientConfig, $this->createToken, 'en');
        $this->assertEquals($expectedResponse, $receiveTransaction);
    }

    //giving it some invalid TransactionWrite object
    function testCreateToken422() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setId('ERROR_INVALID_TRANSACTIONWRITE');
        $expectedResponse->setObject('Error');
        $expectedResponse->setMessage('The TransactionWrite was invalid');
        $JSONData = '{
          "object": "Error",
          "id": "ERROR_INVALID_TRANSACTIONWRITE",
          "message": "The TransactionWrite was invalid"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(422, [], $JSONData));
        $service = new MerchantTokenClient($this->clientMock);

        $receiveTransaction =
            $service->createToken($this->clientConfig, $this->createToken, 'en');
        $this->assertEquals($expectedResponse, $receiveTransaction);
    }

    //Internal server error
    function testCreateToken500() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setId('ERROR_INTERNAL');
        $expectedResponse->setObject('Error');
        $expectedResponse->setMessage('An Exception Occurred');
        $JSONData = '{
          "object": "Error",
          "id": "ERROR_INTERNAL",
          "message": "An Exception Occurred"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(500, [], $JSONData));
        $service = new MerchantTokenClient($this->clientMock);

        $receiveTransaction =
            $service->createToken($this->clientConfig, $this->createToken, 'en');
        $this->assertEquals($expectedResponse, $receiveTransaction);
    }

    function testCreateTokenNullMerchantTokenWrite() {
        $service = new MerchantTokenClient($this->clientMock);

        $this->expectException(TypeError::class);
        $received =
            $service->createToken($this->clientConfig, null, 'en');
    }

    function testCreateTokenNullConfig() {
        $service = new MerchantTokenClient($this->clientMock);

        $this->expectException(TypeError::class);
        $received =
            $service->createToken(null, $this->createToken, 'en');
    }

    function testCreateTokenInvalidAdminKey() {
        $service = new MerchantTokenClient($this->clientMock);
        $this->clientConfig->setTokenAdminKey('');

        $this->expectExceptionMessage('CreateToken(): ClientConfig->TokenAdminKey is not set');
        $received =
            $service->createToken($this->clientConfig, $this->createToken, 'en');
    }

    function testDeleteToken204() {
        $this->clientMock->method('request')
            ->willReturn(new Response(204));
        $service = new MerchantTokenClient($this->clientMock);

        $received =
            $service->deleteToken($this->clientConfig, 1, 'en');

        $this->assertEquals('Successfully Deleted', $received);
    }

    //Won't ever get this response due to checks of null merchant, domain & ID
    function testDeleteToken400() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setId('ERROR_NULL_MERCHANT');
        $expectedResponse->setObject('Error');
        $expectedResponse->setMessage('The merchant was null');
        $JSONData = '{
          "object": "Error",
          "id": "ERROR_NULL_MERCHANT",
          "message": "The merchant was null"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(400, [], $JSONData));
        $service = new MerchantTokenClient($this->clientMock);

        $receiveTransaction =
            $service->createToken($this->clientConfig, $this->createToken, 'en');
        $this->assertEquals($expectedResponse, $receiveTransaction);
    }

    function testDeleteToken401() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setId('ERROR_UNAUTHORIZED');
        $expectedResponse->setObject('Error');
        $expectedResponse->setMessage('Unauthorized');
        $JSONData = '{
          "object": "Error",
          "id": "ERROR_UNAUTHORIZED",
          "message": "Unauthorized"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(401, [], $JSONData));
        $service = new MerchantTokenClient($this->clientMock);

        $receiveTransaction =
            $service->createToken($this->clientConfig, $this->createToken, 'en');
        $this->assertEquals($expectedResponse, $receiveTransaction);
    }

    function testDeleteToken403() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setId('ERROR_FORBIDDEN');
        $expectedResponse->setObject('Error');
        $expectedResponse->setMessage('Forbidden');

        $JSONData = '{
          "object": "Error",
          "id": "ERROR_FORBIDDEN",
          "message": "Forbidden"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(403, [], $JSONData));
        $service = new MerchantTokenClient($this->clientMock);

        $receiveTransaction =
            $service->deleteToken($this->clientConfig, 1, 'en');
        $this->assertEquals($expectedResponse, $receiveTransaction);
    }

    function testDeleteToken404() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setId('ERROR_NONEXISTING_MERCHANTTOKEN');
        $expectedResponse->setObject('Error');
        $expectedResponse->setMessage('The MerchantToken does not exist');
        $JSONData = '{
          "object": "Error",
          "id": "ERROR_NONEXISTING_MERCHANTTOKEN",
          "message": "The MerchantToken does not exist"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(404, [], $JSONData));
        $service = new MerchantTokenClient($this->clientMock);

        $receiveTransaction =
            $service->deleteToken($this->clientConfig, 1, 'en');
        $this->assertEquals($expectedResponse, $receiveTransaction);
    }

    function testDeleteToken500() {
        $expectedResponse = new ResponseError();
        $expectedResponse->setId('ERROR_INTERNAL');
        $expectedResponse->setObject('Error');
        $expectedResponse->setMessage('An Exception Occurred');
        $JSONData = '{
          "object": "Error",
          "id": "ERROR_INTERNAL",
          "message": "An Exception Occurred"
        }';

        $this->clientMock->method('request')
            ->willReturn(new Response(500, [], $JSONData));
        $service = new MerchantTokenClient($this->clientMock);

        $receiveTransaction =
            $service->deleteToken($this->clientConfig, 1, 'en');
        $this->assertEquals($expectedResponse, $receiveTransaction);
    }

    function testDeleteTokenInvalidMerchant() {
        $service = new MerchantTokenClient($this->clientMock);
        $this->clientConfig->setMerchant('');

        $this->expectExceptionMessage('DeleteToken(): ClientConfig->Merchant is not set');
        $received =
            $service->deleteToken($this->clientConfig, 1, 'en');
    }

    function testDeleteTokenInvalidAdminKey() {
        $service = new MerchantTokenClient($this->clientMock);
        $this->clientConfig->setTokenAdminKey('');

        $this->expectExceptionMessage('DeleteToken(): ClientConfig->TokenAdminKey is not set');
        $received =
            $service->deleteToken($this->clientConfig, 1, 'en');
    }

    function testDeleteTokenInvalidDomain() {
        $service = new MerchantTokenClient($this->clientMock);
        $this->expectException(TypeError::class);
        $this->clientConfig->setDomain(null);
    }

    function testDeleteTokenInvalidID() {
        $service = new MerchantTokenClient($this->clientMock);
        $this->expectException(TypeError::class);
        $received =
            $service->deleteToken($this->clientConfig, null, 'en');
    }
}